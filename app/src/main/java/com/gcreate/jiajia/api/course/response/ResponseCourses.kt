package com.gcreate.jiajia.api.course.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseCourses(
    val total: Int,
    val cat: Cat?,
    val courses: MutableList<Course>
)
data class Cat(
    val id: Int,
    val title: String,
    val isFollowed: Boolean
)
