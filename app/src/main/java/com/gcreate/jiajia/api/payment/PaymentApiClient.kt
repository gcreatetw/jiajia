package com.gcreate.jiajia.api.payment

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.request.*
import com.gcreate.jiajia.api.payment.response.*

object PaymentApiClient {

    /** 取得訂閱方案 */
    fun subscriptions(token: String, controller: ApiController<ResponseSubscriptions>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.subscriptions(headers, ApiConstants.SERVER_PATH))
    }

    /** 購買訂閱方案 */
    fun addSubscription(token: String?, request: RequestAddSubscription, controller: ApiController<ResponseAddSubscription>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.addSubscription(headers, ApiConstants.SERVER_PATH, request))
    }

    /** 取得加值頻道 */
    fun plusChannels(token: String, controller: ApiController<ResponsePlusChannels>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.plusChannels(headers, ApiConstants.SERVER_PATH))
    }

    /** 購買加值頻道 */
    fun processToCheckout(token: String?, request: RequestProcessToCheckout, controller: ApiController<ResponseProcessToCheckout>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.processToCheckout(headers, ApiConstants.SERVER_PATH, request))
    }

    /** 選擇金流(OpenWallet) */
    fun payStoreByOpenWallet(token: String?, controller: ApiController<ResponsePayStoreByOpenWallet>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        val request = RequestPayStore("openwallet", "being")
        controller.enqueue(ApiClient.payment.payStoreByOpenWallet(headers, ApiConstants.SERVER_PATH, request))
    }

    /** 選擇金流(ICASH) */
    fun payStoreByICash(token: String?, controller: ApiController<ResponsePayStoreByICash>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        val request = RequestPayStore("icashpay", "being")
        controller.enqueue(ApiClient.payment.payStoreByICash(headers, ApiConstants.SERVER_PATH, request))
    }

    /** 選擇金流(Cathaybk) */
    fun payStoreByCathaybk(token: String?, controller: ApiController<ResponsePayStoreByCathaybk>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        val request = RequestPayStore("cathaybk", "being")
        controller.enqueue(ApiClient.payment.payStoreByCathaybk(headers, ApiConstants.SERVER_PATH, request))
    }

    /** 用 DeepLink 回傳的訂單去取得交易結果 */
    fun getOrderPurchaseResult(token: String, requestBody: RequestPurchaseResult, controller: ApiController<ResponseOrderPurchaseResult>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.getOrderPurchaseResult(headers, ApiConstants.SERVER_PATH, requestBody))
    }

    // 使用優惠券
    fun applyCoupon(token: String, requestBody: RequestApplyCoupon, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.applyCoupon(headers, ApiConstants.SERVER_PATH, requestBody))
    }

    // 移除使用優惠券
    fun removeCoupon(token: String, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.payment.removeCoupon(headers, ApiConstants.SERVER_PATH))
    }
}