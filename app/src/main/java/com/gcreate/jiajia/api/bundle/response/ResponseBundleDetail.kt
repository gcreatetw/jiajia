package com.gcreate.jiajia.api.bundle.response

import com.gcreate.jiajia.api.dataobj.Bundle
import com.gcreate.jiajia.api.dataobj.BundleDetail

data class ResponseBundleDetail(
    val bundle: BundleDetail,
    val bundle_courses :List<Bundle>
)
