package com.gcreate.jiajia.api.payment

import com.gcreate.jiajia.api.payment.request.*
import com.gcreate.jiajia.api.payment.response.*
import retrofit2.Call
import retrofit2.http.*

interface PaymentApi {

    @GET("{serverPath}/subscriptions")
    fun subscriptions(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSubscriptions>

    @POST("{serverPath}/addSubscription")
    fun addSubscription(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestAddSubscription,
    ): Call<ResponseAddSubscription>

    @GET("{serverPath}/channels")
    fun plusChannels(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponsePlusChannels>

    @POST("{serverPath}/processToCheckout")
    fun processToCheckout(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestProcessToCheckout,
    ): Call<ResponseProcessToCheckout>

    @POST("{serverPath}/pay/store")
    fun payStoreByOpenWallet(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestPayStore,
    ): Call<ResponsePayStoreByOpenWallet>

    @POST("{serverPath}/pay/store")
    fun payStoreByICash(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestPayStore,
    ): Call<ResponsePayStoreByICash>

    @POST("{serverPath}/pay/store")
    fun payStoreByCathaybk(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestPayStore,
    ): Call<ResponsePayStoreByCathaybk>

    @POST("{serverPath}/purchaseResult")
    fun getOrderPurchaseResult(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestPurchaseResult,
    ): Call<ResponseOrderPurchaseResult>

    // 使用優惠券
    @POST("{serverPath}/apply_coupon")
    fun applyCoupon(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestApplyCoupon,
    ): Call<ResponseSimpleFormat2>

    // 移除使用優惠券
    @POST("{serverPath}/remove_coupon")
    fun removeCoupon(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat2>
}