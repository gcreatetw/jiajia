package com.gcreate.jiajia.api.homerecommend.request

data class RequestChannelCategory(
    val channel_id : Int
)
