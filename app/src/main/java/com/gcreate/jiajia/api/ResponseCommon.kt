package com.gcreate.jiajia.api

data class ResponseCommon(
    val code: Int,
    val message: String
)