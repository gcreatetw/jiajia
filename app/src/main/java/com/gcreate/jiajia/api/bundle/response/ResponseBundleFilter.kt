package com.gcreate.jiajia.api.bundle.response

import com.gcreate.jiajia.data.ItemName
import com.gcreate.jiajia.data.ItemTitle

data class ResponseBundleFilter(
    val category: List<ItemName>,
    val accessory: List<ItemName>,
    val bmi: List<ItemName>
)
