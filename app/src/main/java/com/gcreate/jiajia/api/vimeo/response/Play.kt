package com.gcreate.jiajia.api.vimeo.response

import java.math.BigInteger

data class Play(
    val progressive: List<Progressive>,
    val source: Source,
    val hls: Hls,
    val dash: Dash,
    val status: String
)

data class Progressive(
    val type: String,
    val codec: String,
    val width: Int,
    val height: Int,
    val link_expiration_time: String,
    val link: String,
    val created_time: String,
    val fps: Int,
    val size: BigInteger,
    val md5: String?,
)

data class Source(
    val quality: String,
    val rendition: String,
    val type: String,
    val width: Int,
    val height: Int,
    val expires: String,
    val link: String,
    val created_time: String,
    val fps: Int,
    val size: BigInteger,
    val md5: String,
    val public_name: String,
    val size_short: String,
)

data class Hls(
    val link_expiration_time: String,
    val link: String,
)

data class Dash(
    val link_expiration_time: String,
    val link: String,
)