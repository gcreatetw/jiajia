package com.gcreate.jiajia.api.user.response

data class ResponseUserCouponRecord(
    val data: MutableList<UserCouponRecordData>,
    val error: Boolean,
    val error_message: String
)

data class UserCouponRecordData(
    val code: String,
    val created_at: String,
    val order_id: String,
    val type: String
)