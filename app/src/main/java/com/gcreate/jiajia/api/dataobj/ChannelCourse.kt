package com.gcreate.jiajia.api.dataobj

data class ChannelCourse(
    val id: String,
    val title: String,
    val preview_image: String,
    val views: String,
    val created_at: String
)
