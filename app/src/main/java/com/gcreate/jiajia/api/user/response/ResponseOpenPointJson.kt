package com.gcreate.jiajia.api.user.response

data class ResponseOpenPointJson(
    val access_token: String,
    val error: Int,
    val expires: String,
    val gid_bar_code: String,
    val op_vcode: String
)