package com.gcreate.jiajia.api.user.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseBrowserCourseRecord(
    val record: MutableList<Record>
)

data class Record(
    val course: MutableList<Course>,
    val id: Int,
    val title: String
)
