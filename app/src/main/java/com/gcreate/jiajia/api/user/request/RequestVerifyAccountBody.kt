package com.gcreate.jiajia.api.user.request

data class RequestVerifyAccountBody(
    val mobile: String
)