package com.gcreate.jiajia.api.schedule.response

data class ResponseScheduleList(
    val calendars: Map<String, List<DataModel>>,
)

data class DataModel(
    val id: Int,
    val user_id: Int,
    val bundle_id: Int,
    val course_id: Int,
    val start: String,
    val bundle_group: String,
)
