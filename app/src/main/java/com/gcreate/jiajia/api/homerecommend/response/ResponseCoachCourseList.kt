package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseCoachCourseList(
    val course: List<Course>,
    val total: Int,                 //  資料總筆數
    val lastPage: Int               //  資料總頁數
)
