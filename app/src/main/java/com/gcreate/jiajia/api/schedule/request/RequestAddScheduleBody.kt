package com.gcreate.jiajia.api.schedule.request

data class RequestAddScheduleBody(
    val bundle_id: String?,
    val course_id: String?,
    val start_datetime: String
)