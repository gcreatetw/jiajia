package com.gcreate.jiajia.api.homerecommend

import com.gcreate.jiajia.api.homerecommend.request.*
import com.gcreate.jiajia.api.homerecommend.response.*
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*

interface HomeRecommendApi {

    // banner 1
    @GET("{serverPath}/slider")
    fun slider(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSlider>

    // 最新消息(4)
    @GET("{serverPath}/latestblog")
    fun news(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseNews>

    // 最新消息(All)
    @POST("{serverPath}/blog")
    fun newsAll(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestNews,
    ): Call<ResponseNews>

    // 最新消息文章內頁
    @POST("{serverPath}/blog/detail")
    fun newsDetail(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseNewsDetail>

    // banner 2
    @GET("{serverPath}/secondslider")
    fun slider2(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSlider>

    // 推薦師資
    @GET("{serverPath}/instructor")
    fun coach(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseCoach>

    // 教練列表 (12)
    @POST("{serverPath}/instructors")
    fun coachList(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCoach,
    ): Call<ResponseCoachAll>

    // 教練內頁
    @POST("{serverPath}/instructor/profile")
    fun coachProfile(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCoachProfile,
    ): Call<ResponseCoachProfile>

    // 教練內頁之分類課程
    @POST("{serverPath}/instructor/courses")
    fun coachCourseList(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCoachCourseList,
    ): Call<ResponseCoachCourseList>

    // 加值頻道列表
    @GET("{serverPath}/channel")
    fun channel(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseChannel>

    // 頻道分類
    @POST("{serverPath}/channel/category")
    fun channelCategory(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestChannelCategory,
    ): Call<ResponseChannelCategory>

    // 頻道分類課程
    @POST("{serverPath}/channel/course")
    fun channelCourse(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestChannelCourse,
    ): Call<ResponseChannelCourse>

}