package com.gcreate.jiajia.api.user.request

data class RequestFollowChannel(
    val channel_id: String
)
