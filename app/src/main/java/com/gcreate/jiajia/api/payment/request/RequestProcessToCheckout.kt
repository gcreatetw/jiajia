package com.gcreate.jiajia.api.payment.request

data class RequestProcessToCheckout(
    val plus_channel_ids: List<Int>,
)