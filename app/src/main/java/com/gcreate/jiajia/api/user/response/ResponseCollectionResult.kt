package com.gcreate.jiajia.api.user.response

data class ResponseCollectionResult(
    val message: String?,
)