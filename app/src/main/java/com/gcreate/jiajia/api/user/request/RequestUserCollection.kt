package com.gcreate.jiajia.api.user.request

data class RequestUserCollection(
    val course_cat_id: Int
)