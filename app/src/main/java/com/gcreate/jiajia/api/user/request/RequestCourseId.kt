package com.gcreate.jiajia.api.user.request

data class RequestCourseId(
    val course_id: Int
)