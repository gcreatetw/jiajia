package com.gcreate.jiajia.api.user.request

data class RequestFollowCategory(
    val instructor_id : String?,
    val course_category_id : String?,
    val channel_id : String?,
    val bundle_category_id :String,
)
