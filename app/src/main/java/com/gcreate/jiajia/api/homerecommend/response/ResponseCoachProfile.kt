package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.api.dataobj.LastestCourseCoach
import com.gcreate.jiajia.api.dataobj.User
import com.gcreate.jiajia.data.ClassCategory

data class ResponseCoachProfile (
    val user: User,
    val latest: List<LastestCourseCoach>,
    val category: List<ClassCategory>,
    val course_count: Int
){
    fun completImageUrl() {
        user.user_img = ApiUtil.completImageUrl(user.user_img)

        for(course in latest) {
            course.preview_image = ApiUtil.completImageUrl(course.preview_image)
        }
    }
}