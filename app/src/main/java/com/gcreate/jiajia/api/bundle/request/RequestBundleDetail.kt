package com.gcreate.jiajia.api.bundle.request

data class RequestBundleDetail(
    val bundle_id: Int
)
