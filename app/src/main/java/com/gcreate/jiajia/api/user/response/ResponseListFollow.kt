package com.gcreate.jiajia.api.user.response

import com.gcreate.jiajia.api.dataobj.Channel
import com.gcreate.jiajia.data.ClassCategory

data class ResponseListFollow(
    val user_id: Int,
    val category: List<ClassCategory>,
    val instructor: List<CoachListFollow>,
    val channel: List<Channel>,
    val bundle_category : List<BundleCategory>,
)

data class CoachListFollow(
    val id: Int,
    val name: String,
    val cate: List<ClassCategory>,
    var user_img: String,
    var followed: Boolean,
    val detail: String,
)

data class BundleCategory(
    val id: Int,
    val name: String,
    var followed: Boolean,
)
