package com.gcreate.jiajia.api.schedule.response

data class ResponseAddScheduleBody(
    val calendars: List<Calendar>,
)

data class Calendar(
    val bundle_group: String,
    val bundle_id: Int?,
    val course_id: Int?,
    val id: Int,
    val start: String,
    val user_id: Int,
)