package com.gcreate.jiajia.api.vimeo.response

data class RootPictures(
    val uri: String,
    val active: Boolean,
    val type: String,
    val base_link: String,
    val sizes: List<RootPictureSize>,
    val resource_key: String,
    val default_picture: Boolean,
)

data class RootPictureSize(
    val width: Int,
    val height: Int,
    val link: String,
    val link_with_play_button: String,
)