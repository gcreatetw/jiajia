package com.gcreate.jiajia.api.search

import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.search.request.RequestSearchCourseBody
import com.gcreate.jiajia.api.search.response.ResponseSearchWidget
import retrofit2.Call
import retrofit2.http.*

interface SearchApi {

    /** 搜尋工具 */
    @GET("{serverPath}/search_widget")
    fun searchWidget(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSearchWidget>

    // 搜尋課程(含直播)
    @POST("{serverPath}/search_course")
    fun searchCourse(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestSearchCourseBody,
    ): Call<ResponseCourses>

    // 搜尋訓練課程
    @POST("{serverPath}/search_bundle")
    fun searchBundle(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestSearchCourseBody,
    ): Call<ResponseBundleCourses>

}