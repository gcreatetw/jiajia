package com.gcreate.jiajia.api.user.response

data class ResponseAffSetting(
    val contant: String,
    val user_referral: Boolean,
    val user_referral_renew: Boolean,
    val user_referral_subscript: Boolean,
)
