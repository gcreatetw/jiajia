package com.gcreate.jiajia.api.course.request

data class RequestCourses (
    val course_cat_id: String,
    val featured: Int?,
    val date: String,
    val level: List<Int>,
    val instructor: List<Int>,
    val accessory: List<Int>,
    val limit: Int,
    val page: Int,
    val fitness_level: Int?,
    val fitness_target: Int?,
)
