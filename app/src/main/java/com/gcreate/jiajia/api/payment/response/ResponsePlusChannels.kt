package com.gcreate.jiajia.api.payment.response

data class ResponsePlusChannels(
    val cart: MutableList<Any?>,
    val plus_channels: MutableList<PlusChannel>
)


data class PlusChannel(
    val id: Int,
    val name: String,
    val detail: String,
    val price: Int,
    val offer_price: Int,
    val preview_image:String,
    val purchased: Boolean,
    val channel: String,
    val start: String,
    val expire: String,
)