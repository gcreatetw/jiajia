package com.gcreate.jiajia.api.user.request


/**
 * @param email : 二聯式發票(必填)
 * @param invoice_tax_company : 公司行號抬頭 | 三聯式發票(必填)
 * @param invoice_tax_number :     統一編號 | 三聯式發票(必填)
 * @param invoice_tax_email :      公司信箱 | 三聯式發票(必填)
 * @param invoice_donate_number :  受贈機構團體ID | 捐贈發票(必填)
 * @param invoice_carrier_type :  0:會員載具、1:手機載具、2:自然人載具 | 電子發票
 * @param invoice_carrier_number :  載具號碼 | 電子發票(必填)
 * @param invoice_paper :   0:不開紙本、1:開紙本發票 | 電子發票(必填)
 * @param type :  2:二聯、3:三聯、4:捐贈 (必填) | 更新其他資訊(非必填)
 * */
data class RequestInvoice(
    val email: String?,
    val invoice_tax_company: String?,
    val invoice_tax_number: String?,
    val invoice_tax_email: String?,
    val invoice_donate_number: String?,
    val invoice_carrier_type: String?,
    val invoice_carrier_number: String?,
    val invoice_paper: String?,
    val type: Int,
)