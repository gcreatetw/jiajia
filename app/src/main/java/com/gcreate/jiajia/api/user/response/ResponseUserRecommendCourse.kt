package com.gcreate.jiajia.api.user.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseUserRecommendCourse(
    val courses: MutableList<Course>
)