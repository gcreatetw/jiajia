package com.gcreate.jiajia.api.user.request

data class RequestUserCoupon(
    val coupon: String,
)