package com.gcreate.jiajia.api.schedule.request

data class RequestDayScheduleBody(
    val date: String
)