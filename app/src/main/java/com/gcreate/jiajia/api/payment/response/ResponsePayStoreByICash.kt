package com.gcreate.jiajia.api.payment.response

data class ResponsePayStoreByICash(
    val error: Int,
    val data: Data?,
    val paymentUrl: String
) {
    data class Data(
        val url: String,
        val qrcode_url: String,
    )
}


