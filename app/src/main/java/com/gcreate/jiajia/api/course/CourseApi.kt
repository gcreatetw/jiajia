package com.gcreate.jiajia.api.course

import com.gcreate.jiajia.api.course.request.RequestCourseDetail
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.course.response.ResponseCourseDetail
import com.gcreate.jiajia.api.course.response.ResponseCourseFilter
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseVideoEnable
import retrofit2.Call
import retrofit2.http.*

interface CourseApi {

    // 課程( 最新上架、熱門課程、課程分類(Tab-2) )
    @POST("{serverPath}/courses")
    fun courses(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourses,
    ): Call<ResponseCourses>

    // 課程篩選器
    @GET("{serverPath}/filtercourse")
    fun courseFilter(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseCourseFilter>

    // 課程分類
    @GET("{serverPath}/category")
    fun category(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseCategory>

    // 課程詳細資訊
    @POST("{serverPath}/course/detail")
    fun courseDetail(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourseDetail,
    ): Call<ResponseCourseDetail>

    // 紀錄影片撥放次數
    @POST("{serverPath}/course/view/create")
    fun recordCourseViewCount(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourseDetail,
    ): Call<ResponseSimpleFormat>

    // 確認影片是否能觀看
    @POST("{serverPath}/course/video/enable")
    fun checkViewEnable(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourseDetail,
    ): Call<ResponseVideoEnable>

}