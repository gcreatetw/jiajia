package com.gcreate.jiajia.api.payment.request

data class RequestApplyCoupon(
    val coupon_code: String
)