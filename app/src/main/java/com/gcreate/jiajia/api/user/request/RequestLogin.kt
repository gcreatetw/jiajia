package com.gcreate.jiajia.api.user.request

data class RequestLogin(
    val mobile: String,
    val password: String,
    val firebaseToken:String,
)