package com.gcreate.jiajia.api.liveStream.request

data class RequestLiveCourses(
    val date: String,
    val limit: Int,
    val page: Int,
)