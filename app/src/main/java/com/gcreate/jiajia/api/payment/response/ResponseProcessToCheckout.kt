package com.gcreate.jiajia.api.payment.response


data class ResponseProcessToCheckout(
    val mode: Boolean,
    val cart: Cart,
    val total_amount: Int,
    val firstpay:FirstPay,
    ) {
    data class Cart(
        val SubscriptionPlan: SubscriptionPlan?,
        val PlusChannel: MutableList<PlusChannel>,
        val Coupon: String?,
    )

    data class SubscriptionPlan(
        val name: String,
        val price: Int,
        val start: String,
        val expire: String,
    )

    data class PlusChannel(
        val channel: String,
        val price: Int,
        val start: String,
        val expire: String,
    )

    data class FirstPay(
        val amount: Int,
        val days: String,
    )
}