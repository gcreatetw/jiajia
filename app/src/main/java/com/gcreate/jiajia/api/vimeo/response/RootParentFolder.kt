package com.gcreate.jiajia.api.vimeo.response

data class RootParentFolder(
    val created_time: String,
    val modified_time: String,
    val last_user_action_event_date: String,
    val name: String,
    val privacy: RootParentFolderPrivacy,
    val resource_key: String,
    val uri: String,
    val link: Any,
    val pinned_on: Any,
    val is_pinned: Boolean,
    val is_private_to_user: Boolean,
    val user: RootUser,
    val access_grant: Any,
    val metadata: Metadata,
)

data class RootParentFolderPrivacy(
    val view: String
)
