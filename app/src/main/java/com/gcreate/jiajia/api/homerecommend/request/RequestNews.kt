package com.gcreate.jiajia.api.homerecommend.request

data class RequestNews(
    val cat_id: Int,    // 1 = 活動檔期,2 = 運動知識
    val limit: Int,     // 預設自行定義數量
    val page: Int       // 預設:1，可依回傳頁數判斷下一頁
)
