package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.dataobj.ChannelCategory

data class ResponseChannelCategory(
    val channel_category: List<ChannelCategory>
)
