package com.gcreate.jiajia.api

import com.gcreate.jiajia.data.ItemName


object ApiUtil {
    fun completImageUrl(url: String?): String {
        if (url == null) {
            return ""
        }

        return url

    }

    fun itemNameDisplayString(list: List<ItemName>): String {
        if (list.isEmpty()) {
            return ""
        }
        var str = StringBuilder(list[0].name)
        for (index in 1..list.lastIndex) {
            str.append(" ${list[index].name}")
        }

        return str.toString()
    }
}