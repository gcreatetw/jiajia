package com.gcreate.jiajia.api.user.response

data class ResponseFirebaseNotification(
    val notification: MutableList<FirebaseNotification>
)

data class FirebaseNotification(
    val title: String,
    val message: String,
    val url: String?,
)