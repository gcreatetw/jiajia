package com.gcreate.jiajia.api.user.request

data class RequestReadNotificationBody(
    val notification_id: Int,
)
