package com.gcreate.jiajia.api.dataobj

import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.data.ItemName

data class CourseDetail(
    val detail: String,
    val accessories: List<ItemName>,
    val suggest : String?,           // 課程建議
    val fit_situation : String?,     // 適用
    val unfit_situation: String?,    // 不適用情形
    val before_fitness: String,     // 事前準備
    val body_response: String,      // 身體反應
    val note: String                // 其他
){
    fun accessoriesDisplayString(): String{
        return ApiUtil.itemNameDisplayString(this.accessories)
    }
}
