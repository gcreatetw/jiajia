package com.gcreate.jiajia.api.vimeo.response

data class Embed(
    val html: String,
    val badges: Badges,
    val buttons: Buttons,
    val logos: Logos,
    val title: Title,
    val end_screen: List<String>,
    val playbar: Boolean,
    val volume: Boolean,
    val color: String,
    val event_schedule: Boolean,
    val interactive: Boolean,
    val uri: String?,
    val speed: Boolean,
)

data class Badges(
    val hdr: Boolean,
    val live: Live,
    val staff_pick: StaffPick,
    val vod: Boolean,
    val weekend_challenge: Boolean
)

data class Live(
    val archived: Boolean,
    val streaming: Boolean
)

data class StaffPick(
    val best_of_the_month: Boolean,
    val best_of_the_year: Boolean,
    val normal: Boolean,
    val premiere: Boolean
)

data class Buttons(
    val embed: Boolean,
    val fullscreen: Boolean,
    val hd: Boolean,
    val like: Boolean,
    val scaling: Boolean,
    val share: Boolean,
    val watchlater: Boolean
)

data class Logos(
    val custom: Custom,
    val vimeo: Boolean
)

data class Custom(
    val active: Boolean,
    val link: Boolean,
    val sticky: Boolean,
    val url: String?
)

data class Title(
    val name: String,
    val owner: String,
    val portrait: String
)

