package com.gcreate.jiajia.api.schedule.request

data class RequestListScheduleBody(
    val month: String
)