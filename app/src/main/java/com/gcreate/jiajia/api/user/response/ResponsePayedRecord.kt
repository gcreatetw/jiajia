package com.gcreate.jiajia.api.user.response

data class ResponsePayedRecord(
    val order: List<Order>,
    val total_amount: Double,
)

data class ResponseAvailableRecord(
    val status: String,
    val order: List<Order>,
    val icashpay_token: Boolean,
    val cathay_card_id: Boolean,
)

data class Order(
    val orderid: Int,
    val price: Any,
    val enroll_start: String,
    val enroll_expire: String,
    val products: Products?,
    val type: String,
    val status: String,
    val amount: Int,
    val id: Int,
    val openpoint: Double,
    val point_counted: Boolean,
)

data class Products(
    val id: Int,
    val name: String,
)