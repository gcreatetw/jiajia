package com.gcreate.jiajia.api.user.request

data class RequestUserHobby(
    val gender: Int,
    val birthday: String,
    val height: Int,
    val weight: Int,
    val fitness_level: Int,
    val fitness_target: Int,
    val fitness_howlong: Int
)