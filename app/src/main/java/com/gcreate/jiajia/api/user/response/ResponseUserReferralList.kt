package com.gcreate.jiajia.api.user.response

data class ResponseUserReferralList(
    val code: String?,
    val list: MutableList<ReferralListData>,
)

data class ReferralListData(
    val name: String,
    val referral_id: Int,
    val register_datetime: String,
    val subscription_datetime: String,
    val user_img: String,
    var get_point: Boolean,
)

