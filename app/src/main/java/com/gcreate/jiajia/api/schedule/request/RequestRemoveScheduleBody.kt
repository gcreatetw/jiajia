package com.gcreate.jiajia.api.schedule.request

data class RequestRemoveScheduleBody(
    val course_id: Int,
    val schedule_id: Int
)