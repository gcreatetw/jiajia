package com.gcreate.jiajia.api.dataobj

import com.gcreate.jiajia.data.BeingRecyclerViewItemData
import com.gcreate.jiajia.data.ClassPageItem
import com.gcreate.jiajia.data.CoachItem
import com.gcreate.jiajia.data.VideoPageItem
import com.google.gson.annotations.SerializedName
import io.requery.Column

data class Course(
    val id: Int,
    val category_id: String,
    val title: String,
    val short_detail: String,
    val created_at: String,
    val level: String,
    val calorie: String,
    val preview_image: String?,
    val video_url: String?, // Vimeo
    val video_id: String?,  // 喬山
    val is_free: Boolean = false,
    val is_subscription: Boolean,
    val live_time: String?,
    val isCollected: Boolean,
    var isCalendared: Boolean,
    val isChannel: Boolean,
    val isBuyChannel: Boolean,
    val preview_images: String?,
    val name: String,
    val user_img: String,
    val type: String,
    val views: String,
    val isFollowed: Boolean,
    val start: String,
    val PlusChannelParentId: Int?,
    val chat_iframe: String?,
    val video_iframe: String?,
    val video_type: String,
    val video_enable: Boolean?,
) : BeingRecyclerViewItemData, CoachItem, VideoPageItem, ClassPageItem {

    val courseImage: String?
        get() {
            preview_image?.apply {
                return this
            }
            return preview_image
        }

    val formatedCreateDate: String
        get() {
            created_at?.apply {
                return substring(0, 10).replace('-', '/').replaceFirst('/', ' ')
            }
            return created_at
        }

    val viewCunts: String?
        get() {
            if (views.isNullOrEmpty()) {
                return "0"
            }
            return views
        }

    val videoEnable: Boolean
        get() {
            if (video_enable == null) {
                return true
            }
            return video_enable
        }
}
