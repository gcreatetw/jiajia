package com.gcreate.jiajia.api.dataobj

data class Channel (
    val id: Int,
    val name: String,
    val image: String,
    val detail: String,
    val isFollowed: Boolean,
    var followed: Boolean,
    val language: String
)
