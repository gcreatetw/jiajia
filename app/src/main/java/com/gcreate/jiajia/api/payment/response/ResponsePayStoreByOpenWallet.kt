package com.gcreate.jiajia.api.payment.response

data class ResponsePayStoreByOpenWallet(
    val error: Int,
    val data: Data?,
    val objectID :String?,
    val paymentUrl: String
) {
    data class Data(
        val paymentUrl: String,
        val paymentUrlApp: String,
        val merchantTradeNo: String,
        val statusCode: String,
        val statusMessage: String,
    )
}


