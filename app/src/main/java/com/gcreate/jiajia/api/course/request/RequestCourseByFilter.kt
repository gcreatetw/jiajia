package com.gcreate.jiajia.api.course.request

data class RequestCourseByFilter(
    val date: String,
    val level: List<Int>,
    val instructor: List<Int>,
    val accessory: List<Int>
)
