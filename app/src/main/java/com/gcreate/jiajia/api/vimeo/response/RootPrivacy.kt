package com.gcreate.jiajia.api.vimeo.response

data class RootPrivacy(
    val add: Boolean,
    val comments: String,
    val download: Boolean,
    val embed: String,
    val view: String
)