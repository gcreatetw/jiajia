package com.gcreate.jiajia.api.dataobj

data class User(
    val id: Int,
    val name: String,
    val lname: String,
    val fname: String,
    var user_img: String,
    val detail: String,
    val isFollowed:Boolean
)
