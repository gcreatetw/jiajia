package com.gcreate.jiajia.api.user.request

/**
 * @param providor :
 * facebook
 * google
 * line
 * openpoint
 * apple
 */
data class RequestVerifyThirdPartyBody(
    val providor: String,
    val code: String
)