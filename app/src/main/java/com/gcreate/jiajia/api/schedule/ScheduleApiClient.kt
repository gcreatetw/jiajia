package com.gcreate.jiajia.api.schedule

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.request.RequestAddScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestDayScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestListScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestRemoveScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseAddScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseDaySchedule
import com.gcreate.jiajia.api.schedule.response.ResponseScheduleList
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat

object ScheduleApiClient {


    /** 加入行事曆 */
    fun addSchedule(token: String, body: RequestAddScheduleBody, controller: ApiController<ResponseAddScheduleBody>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.schedule.addSchedule(headers, ApiConstants.SERVER_PATH, body))
    }

    /** 行事曆列表 */
    fun listSchedule(token: String, body: RequestListScheduleBody, controller: ApiController<ResponseScheduleList>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.schedule.listSchedule(headers, ApiConstants.SERVER_PATH, body))
    }

    /** 行事曆單日列表 */
    fun daySchedule(token: String, body: RequestDayScheduleBody, controller: ApiController<ResponseDaySchedule>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.schedule.daySchedule(headers, ApiConstants.SERVER_PATH, body))
    }

    /** 移除行事曆(單一課程) */
    fun removeSchedule(token: String, body: RequestRemoveScheduleBody, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.schedule.removeSchedule(headers, ApiConstants.SERVER_PATH, body))
    }
}