package com.gcreate.jiajia.api.vimeo.response

data class SingleVideoResponse(
    val uri: String,
    val name: String,
    val description: String?,
    val type: String,
    val link: String,
    val player_embed_url: String,
    val duration: Int,
    val width: Int,
    val language: String?,
    val height: Int,
    val embed: Embed,
    val created_time: String,
    val modified_time: String,
    val release_time: String,
    val content_rating: List<String>,
    val content_rating_class: String,
    val rating_mod_locked: Boolean,
    val license: Any,
    val privacy: RootPrivacy,
    val pictures: RootPictures,
    val tags: List<Any>,
    val stats: Stats,
    val categories: List<Any>,
    val uploader: Uploader,
    val metadata: Metadata,
    val manage_link: String,
    val user: RootUser,
    val parent_folder: RootParentFolder?,
    val last_user_action_event_date: String,
    val review_page: ReviewPage,
    val files: List<File>,
    val download: List<Download>,
    val play: Play,
    val app: App,
    val status: String,
    val resource_key: String,
    val upload: Upload,
    val transcode: Transcode,
    val is_playable: Boolean,
    val has_audio: Boolean,
)

data class Stats(
    val plays: Int,
)

data class ReviewPage(
    val active: Boolean,
    val link: String,
)

data class Transcode(
    val status: String,
)

data class Upload(
    val status: String,
    val link: Any,
    val upload_link: Any,
    val complete_uri: Any,
    val form: Any,
    val approach: Any,
    val size: Any,
    val redirect_url: Any,
)

data class App(
    val name: String,
    val uri: String,
)