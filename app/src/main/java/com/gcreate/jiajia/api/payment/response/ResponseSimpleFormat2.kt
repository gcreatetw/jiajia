package com.gcreate.jiajia.api.payment.response

data class ResponseSimpleFormat2(
    val error: Int,
    val error_message: String,
    val message: String
)