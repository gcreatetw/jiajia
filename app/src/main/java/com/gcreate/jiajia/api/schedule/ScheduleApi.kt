package com.gcreate.jiajia.api.schedule

import com.gcreate.jiajia.api.schedule.request.RequestAddScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestDayScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestListScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestRemoveScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseAddScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseDaySchedule
import com.gcreate.jiajia.api.schedule.response.ResponseScheduleList
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Path

interface ScheduleApi {

    // 加入行事曆
    @POST("{serverPath}/add_schedule")
    fun addSchedule(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestAddScheduleBody,
    ): Call<ResponseAddScheduleBody>

    // 行事曆列表
    @POST("{serverPath}/list_schedule")
    fun listSchedule(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestListScheduleBody,
    ): Call<ResponseScheduleList>

    // 行事曆單日列表
    @POST("{serverPath}/day_schedule")
    fun daySchedule(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestDayScheduleBody,
    ): Call<ResponseDaySchedule>

    // 移除行事曆(單一課程)
    @POST("{serverPath}/remove_schedule")
    fun removeSchedule(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestRemoveScheduleBody,
    ): Call<ResponseSimpleFormat>

}