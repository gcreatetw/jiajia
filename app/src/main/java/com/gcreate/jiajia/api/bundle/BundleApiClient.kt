package com.gcreate.jiajia.api.bundle

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.bundle.request.RequestBundleCourses
import com.gcreate.jiajia.api.bundle.request.RequestBundleDetail
import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.bundle.response.ResponseBundleDetail
import com.gcreate.jiajia.api.bundle.response.ResponseBundleFilter
import com.gcreate.jiajia.api.handle.ApiController


object BundleApiClient {

    // 訓練計畫篩選工具
    fun bundleFilter(controller: ApiController<ResponseBundleFilter>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.bundle.bundleFilter(headers, ApiConstants.SERVER_PATH))
    }

    // 訓練計畫列表
    fun bundleCourses(request: RequestBundleCourses, controller: ApiController<ResponseBundleCourses>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.bundle.bundleCourses(headers, ApiConstants.SERVER_PATH, request))
    }

    // 訓練計畫內頁
    fun bundleDetail(token: String?, request: RequestBundleDetail, controller: ApiController<ResponseBundleDetail>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.bundle.bundleDetail(headers, ApiConstants.SERVER_PATH, request))
    }
}