package com.gcreate.jiajia.api.liveStream.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseLiveCourses(
    val total: Int,
    val courses: List<Course>,
)