package com.gcreate.jiajia.api.user.response

data class ResponseVideoEnable(
    val enable: Boolean
)