package com.gcreate.jiajia.api.payment.response

data class ResponseAddSubscription(
    val price: String,
    val offer_price: String,
    val plus_channel_id: List<String>
)