package com.gcreate.jiajia.api.homerecommend.response

data class ResponseNewsDetail(
    val blog: Blog
)

data class Blog(
    val created_at: String,
    val date: String,
    val detail: String,
    val heading: String,
    val id: Int,
    val image: String,
    val user_id: String
)