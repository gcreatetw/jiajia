package com.gcreate.jiajia.api.payment.request

data class RequestPurchaseResult(
    val objectID: String,
)