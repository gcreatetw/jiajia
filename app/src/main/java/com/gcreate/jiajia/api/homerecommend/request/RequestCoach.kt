package com.gcreate.jiajia.api.homerecommend.request

data class RequestCoach(
    val cat_id: Int,    // 類別 id
    val limit: Int,     // 預設自行定義數量
    val page: Int       // 預設:1，可依回傳頁數判斷下一頁
)
