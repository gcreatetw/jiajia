package com.gcreate.jiajia.api.viewModel


import android.annotation.SuppressLint
import android.view.LayoutInflater
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.data.ClassCategory
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.VideoType
import com.gcreate.jiajia.data.VideoTypeItem
import com.gcreate.jiajia.databinding.FragmentVideoBinding
import com.gcreate.jiajia.adapters.course.VideoCoursByTypeAdapter
import com.gcreate.jiajia.adapters.course.VideoTypeAdapter
import kotlinx.coroutines.runBlocking

@SuppressLint("NotifyDataSetChanged")
class VideoTypeViewModel(
    private val binding: FragmentVideoBinding,
    private val inflater: LayoutInflater,
    private val fragmentActivity: FragmentActivity,
    private val navController: NavController,
    private val mainModel: MainViewModel,
) : ViewModel() {
    private var dataList = mutableListOf<VideoTypeItem>()

    // 課程分類
    private var videoTypeDataList: MutableLiveData<ResponseCategory> = MutableLiveData()
    private var videoTypeCategoryAdapter: VideoTypeAdapter = VideoTypeAdapter()

    fun getVideoTypeCategoryAdapter(): VideoTypeAdapter {
        return videoTypeCategoryAdapter
    }

    fun setVideoTypeCategoryAdapterData(data: MutableList<ClassCategory>) {
        for (i in 0 until data.size) {
            dataList.add(VideoType(data[i].id, data[i].title, mutableListOf()))

            runBlocking {
                getVideoCourseByTypeApiData(i, data[i].id.toString())
            }

//            getVideoCourseByTypeDataObserver().observe(fragmentActivity, {
//                setVideoCourseByTypeAdapterData(i, it.courses)
//            })
        }

        videoTypeCategoryAdapter.data = dataList
        videoTypeCategoryAdapter.notifyDataSetChanged()

    }

    fun getVideoTypeCategoryDataObserver(): MutableLiveData<ResponseCategory> {
        return videoTypeDataList
    }

    fun getVideoTypeCategoryApiData() {
        CourseApiClient.category(object : ApiController<ResponseCategory>(binding.root.context, false) {
            override fun onSuccess(response: ResponseCategory) {
                videoTypeDataList.postValue(response)
            }
        })
    }

    // 依分類取得課程
    private var videoCourseByTypeDataList: MutableLiveData<ResponseCourses> = MutableLiveData()
    private var videoCourseByTypeAdapter: VideoCoursByTypeAdapter = VideoCoursByTypeAdapter(mutableListOf())

    fun getVideoCourseByTypeAdapter(): VideoCoursByTypeAdapter {
        return VideoCoursByTypeAdapter(mutableListOf())
    }

    fun setVideoCourseByTypeAdapterData(position: Int, data: MutableList<Course>) {
        val itemView = binding.recyclerView.findViewHolderForAdapterPosition(position)!!.itemView
//        VideoCoursByTypeAdapter().data = data
//        VideoCoursByTypeAdapter().notifyDataSetChanged()

    }

    fun getVideoCourseByTypeDataObserver(): MutableLiveData<ResponseCourses> {
        return videoCourseByTypeDataList
    }


    private fun getVideoCourseByTypeApiData(index: Int, categoryId: String) {
        val request = RequestCourses(
            categoryId, null, "全部",
            listOf(), listOf(), listOf(), 12, 1,null,null)

        val token = mainModel.appState.accessToken.get()!!
        CourseApiClient.courses(token, request, object : ApiController<ResponseCourses>(binding.root.context, false) {
            override fun onSuccess(response: ResponseCourses) {
                //videoCourseByTypeDataList.postValue(response)
                (dataList[index] as VideoType).classList = response.courses
//                binding.recyclerView.adapter!!.notifyItemChanged(index)

            }
        })
    }

}

class VideoTypeModelFactory(
    private val binding: FragmentVideoBinding,
    private val inflater: LayoutInflater,
    private val fragmentActivity: FragmentActivity,
    private val navController: NavController,
    private val mainModel: MainViewModel,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return VideoTypeViewModel(binding, inflater, fragmentActivity, navController, mainModel) as T
    }

}