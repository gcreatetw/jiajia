package com.gcreate.jiajia.api.payment.request

data class RequestAddSubscription(
    val subscription_id: Int,
)