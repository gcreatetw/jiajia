package com.gcreate.jiajia.api.user.response

import com.gcreate.jiajia.api.dataobj.Course

data class ResponseUserInfo(
    val collect: List<Collect>,
//    val follow: Follow,
    val more: More,
    var user: User,
    val family: Family?,
) {
    data class Collect(
        val course: List<Course>,
        val id: Int,
        val title: String,
    )
}

data class Family(
    val owner: Int,
    val user_image: String,
    val name: String,
)

data class Follow(
    val category: List<Category>,
    val channel: List<Channel>,
    val instructor: List<Instructor>,
)

data class More(
    val about: String,
    val faq: String,
    val privacy_policy: String,
    val terms_condition: String,
)

data class User(
    val showBonusChannel: Boolean,
    val birthday: String,
    var email_verified: Boolean,
    var fitness_howlong: String,
    var fitness_level: String,
    var fitness_target: String,
    var gender: String,
    var height: String,
    var weight: String,
    val id: Int,
    val member_number: String,
    var name: String,
    val subscription: Boolean,
    val subscription_type: String?,
    val subscription_expire: String,
    val subscription_level: String,
    val user_img: String,
    val news: Int,
    val instructor: Int,
    val category: Int,
    val calendar: Int,
    var op_bundled: Boolean,
    var payment_method: String?,
    var google: Boolean,
    var facebook: Boolean,
    var line: Boolean,
    val apple: Boolean,
    var county: String?,
    var district: String?,
    var address: String?,
    var pin_code: String?,
    var first_purchase: String,
    var first_purchase_days: String?,
    var have_member: Boolean,
    var phone: String?,
    var referral_code: String?,
)

//data class Course(
//    val created_at: String,
//    val id: Int,
//    val name: String,
//    val preview_image: String,
//    val title: String,
//    val type: String,
//    val user_img: String
//)

data class Category(
    val id: Int,
    val isFollowed: Boolean,
    val title: String,
)

data class Channel(
    val id: Int,
    val name: String,
    val image: String,
    val detail: Any,
    val created_at: String,
    val price: String,
)

data class Instructor(
    val id: Int,
    val isFollowed: Boolean,
    val name: String,
    val user_img: String,
)