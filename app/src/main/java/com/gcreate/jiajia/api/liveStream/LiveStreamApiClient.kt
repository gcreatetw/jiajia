package com.gcreate.jiajia.api.liveStream

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.liveStream.request.RequestLiveCourses
import com.gcreate.jiajia.api.liveStream.response.ResponseLiveCourses

object LiveStreamApiClient {

    /** 取得某天直接課程列表 */
    fun liveCourses(token: String, request: RequestLiveCourses, controller: ApiController<ResponseLiveCourses>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.liveStream.getLiveCourses(headers, ApiConstants.SERVER_PATH, request))
    }

}