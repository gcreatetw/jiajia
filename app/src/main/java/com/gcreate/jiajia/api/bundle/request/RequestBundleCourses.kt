package com.gcreate.jiajia.api.bundle.request

data class RequestBundleCourses(
    val bundle_cat_id: String,
    val accessory: List<Int>,
    val bmi: List<Int>
)
