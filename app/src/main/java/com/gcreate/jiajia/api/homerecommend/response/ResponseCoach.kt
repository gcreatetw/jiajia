package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.data.HomeCoach

data class ResponseCoach(
    val instructor: MutableList<HomeCoach>,
)

