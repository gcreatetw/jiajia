package com.gcreate.jiajia.api.liveStream

import com.gcreate.jiajia.api.liveStream.request.RequestLiveCourses
import com.gcreate.jiajia.api.liveStream.response.ResponseLiveCourses
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Path

interface LiveStreamApi {

    @POST("{serverPath}/live_courses")
    fun getLiveCourses(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestLiveCourses,
    ): Call<ResponseLiveCourses>

}