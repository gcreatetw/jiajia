package com.gcreate.jiajia.api.vimeo.response

data class Uploader(
    val pictures: UploaderPictures
)

data class UploaderPictures(
    val uri: Any,
    val active: Boolean,
    val type: String,
    val base_link: String,
    val sizes: List<UploaderPicturesSize>,
    val resource_key: String,
    val default_picture: Boolean,
)

data class UploaderPicturesSize(
    val height: Int,
    val link: String,
    val width: Int
)