package com.gcreate.jiajia.api.vimeo.response

import java.math.BigInteger

data class Download(
    val quality: String,
    val rendition: String,
    val type: String,
    val width: Int,
    val height: Int,
    val expires: String,
    val link: String,
    val created_time: String,
    val fps: Int,
    val size: BigInteger,
    val md5: String?,
    val public_name: String,
    val size_short: String,
)