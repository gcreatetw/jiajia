package com.gcreate.jiajia.api.vimeo


import com.gcreate.jiajia.api.vimeo.response.SingleVideoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Path

interface VimeoApi {

    @GET("videos/{videoId}")
    fun getVideoData(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "videoId", encoded = true) videoId: String,
    ): Call<SingleVideoResponse>

}