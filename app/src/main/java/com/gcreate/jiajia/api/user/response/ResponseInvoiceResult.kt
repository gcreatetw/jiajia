package com.gcreate.jiajia.api.user.response

data class ResponseInvoiceResult(
    val email_verified: Boolean,
    val message: String,
    val user_data: UserData
)

data class UserData(
    var email: String?,
    val invoice_carrier_default: String?,
    val invoice_donate_number: String?,
    val invoice_mobile_carrier: String? = "",
    val invoice_nature_carrier: String? = "",
    val invoice_tax_company: String?,
    val invoice_tax_email: String?,
    val invoice_tax_number: String?,
    val invoice_type_default: String?,
    var invoice_paper: String?
)