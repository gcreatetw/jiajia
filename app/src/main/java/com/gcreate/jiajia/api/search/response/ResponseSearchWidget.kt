package com.gcreate.jiajia.api.search.response

data class ResponseSearchWidget(
    val hot: MutableList<Hot>,
    val records: MutableList<Record>
)

data class Hot(
    val id: String,
    val keyword: String
)

data class Record(
    val id: String,
    val keyword: String?
)