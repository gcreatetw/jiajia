package com.gcreate.jiajia.api.vimeo.response

data class File(
    val quality: String,
    val rendition: String,
//    val type: String,
//    val width: Int,
//    val height: Int,
    val link: String,
//    val created_time: String,
//    val fps: Int,
//    val size: Long,
    val md5: String?,
    val public_name: String,
    val size_short: String,
)