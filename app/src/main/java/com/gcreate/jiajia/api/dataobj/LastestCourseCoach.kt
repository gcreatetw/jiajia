package com.gcreate.jiajia.api.dataobj

data class LastestCourseCoach(
    val id: Int,
    var preview_image: String,
    val short_detail: String,
    val type: String,
    val price: String,
    val discount_price: String
)