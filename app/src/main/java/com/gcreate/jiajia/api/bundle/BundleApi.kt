package com.gcreate.jiajia.api.bundle

import com.gcreate.jiajia.api.bundle.request.RequestBundleCourses
import com.gcreate.jiajia.api.bundle.request.RequestBundleDetail
import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.bundle.response.ResponseBundleDetail
import com.gcreate.jiajia.api.bundle.response.ResponseBundleFilter
import retrofit2.Call
import retrofit2.http.*

interface BundleApi {

    // 訓練計畫篩選工具
    @GET("{serverPath}/bundle_filter")
    fun bundleFilter(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseBundleFilter>

    // 訓練計畫列表
    @POST("{serverPath}/bundle_courses")
    fun bundleCourses(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestBundleCourses,
    ): Call<ResponseBundleCourses>

    // 訓練計畫內頁
    @POST("{serverPath}/bundle/detail")
    fun bundleDetail(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestBundleDetail,
    ): Call<ResponseBundleDetail>
}