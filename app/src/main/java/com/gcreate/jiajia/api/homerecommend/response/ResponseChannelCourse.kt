package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.dataobj.ChannelCourse

data class ResponseChannelCourse(
    val total: Int,
    val courses: List<ChannelCourse>
)
