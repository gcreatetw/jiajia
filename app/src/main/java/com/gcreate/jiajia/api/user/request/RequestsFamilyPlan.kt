package com.gcreate.jiajia.api.user.request


// 家庭方案-成員搜尋
data class RequestSearchFamilyMember(
    val mobile: String
)

// 家庭方案-成員新增
data class RequestAddFamilyMember(
    val user_id: Int
)

// 家庭方案-成員刪除
data class RequestDeleteFamilyMember(
    val user_id: MutableList<Int>
)