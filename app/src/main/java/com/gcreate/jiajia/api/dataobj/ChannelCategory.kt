package com.gcreate.jiajia.api.dataobj

data class ChannelCategory(
    val id: Int,
    val name: String
)
