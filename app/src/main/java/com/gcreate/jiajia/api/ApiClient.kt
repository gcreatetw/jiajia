package com.gcreate.jiajia.api

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import com.gcreate.jiajia.api.bundle.BundleApi
import com.gcreate.jiajia.api.course.CourseApi
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApi
import com.gcreate.jiajia.api.liveStream.LiveStreamApi
import com.gcreate.jiajia.api.payment.PaymentApi
import com.gcreate.jiajia.api.schedule.ScheduleApi
import com.gcreate.jiajia.api.search.SearchApi
import com.gcreate.jiajia.api.user.UserApi
import com.gcreate.jiajia.api.vimeo.VimeoApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
object ApiClient {

    lateinit var homeRecommend: HomeRecommendApi
    lateinit var course: CourseApi
    lateinit var user: UserApi
    lateinit var bundle: BundleApi
    lateinit var payment: PaymentApi
    lateinit var liveStream: LiveStreamApi
    lateinit var search: SearchApi
    lateinit var schedule: ScheduleApi
    lateinit var vimeo : VimeoApi

//    private var ivAes: String = ""
    lateinit var mContext: Context

    fun header(includeToken: Boolean): HashMap<String, String> {
        val map = HashMap<String, String>()
//            map["X-Tripbay-app-key"] = apiKey
//        if (!com.gcreate.jiajia.MainApp.loginToken.isNullOrEmpty())
//            map["X-Tripbay-key"] = com.gcreate.jiajia.MainApp.loginToken!!
//        val languageInfo = Utility.languageSet()
//        if (includeToken)
//            map["Authorization"] = "Bearer " + com.gcreate.jiajia.MainApp.loginToken
//        map["Content-Language"] = languageInfo[0].plus("_").plus(languageInfo[1])

//            if (BuildConfig.DEBUG) {
//                var textString = "Header\n"
//                textString += "x-hap-api-key:$apiKey\n"
//                textString += "x-hap-dev-uid:" + SavedData.apiDevUid + "\n"
//                textString += "Authorization:" + "Bearer " + SavedData.authToken + "\n\n"
//                Log.d("Header", textString)
//            }
        return map
    }

    /*
    因為 Retrofit 的限制，如果要將 API 分不同介面的話，必須分別 create，無法使用類似以下的方式來重構
    public interface ApiEndpoints extends InvoiceApi, ConfigApi, CreditCardApi, EventApi, LoginApi, MessageApi, MyApi, PayApi, ReimburseApi, ProductApi {
    }
    */
    fun initApi(context: Context) {
        mContext = context

        val apiServerUrl = ApiConstants.getServerUrl(context, ApiConstants.ServerType.API_FORMAL_UAT)
//        val apiServerUrl = ApiConstants.getServerUrl(context, ApiConstants.ServerType.API_FORMAL)
//        val apiServerUrl = ApiConstants.getServerUrl(context, ApiConstants.ServerType.API_DEBUG)
        val vimeoApiServerUrl = ApiConstants.getServerUrl(context, ApiConstants.ServerType.Viemo_API_FORMAL)

        homeRecommend = getRetrofit(apiServerUrl).create(HomeRecommendApi::class.java)
        course = getRetrofit(apiServerUrl).create(CourseApi::class.java)
        user = getRetrofit(apiServerUrl).create(UserApi::class.java)
        bundle = getRetrofit(apiServerUrl).create(BundleApi::class.java)
        payment = getRetrofit(apiServerUrl).create(PaymentApi::class.java)
        liveStream = getRetrofit(apiServerUrl).create(LiveStreamApi::class.java)
        search = getRetrofit(apiServerUrl).create(SearchApi::class.java)
        schedule = getRetrofit(apiServerUrl).create(ScheduleApi::class.java)
        vimeo = getRetrofit(vimeoApiServerUrl).create(VimeoApi::class.java)
    }

    private fun getRetrofit(serverPath: String): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
            val builder = chain.request().newBuilder()
            val build = builder.build()
            chain.proceed(build)
        }.connectTimeout(60, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

        val apiInstance = Retrofit.Builder().baseUrl(serverPath).addConverterFactory(GsonConverterFactory.create()).client(client)

        return apiInstance.build()
    }
}
