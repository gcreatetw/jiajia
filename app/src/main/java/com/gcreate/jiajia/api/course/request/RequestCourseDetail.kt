package com.gcreate.jiajia.api.course.request

data class RequestCourseDetail(
    val course_id: Int
)
