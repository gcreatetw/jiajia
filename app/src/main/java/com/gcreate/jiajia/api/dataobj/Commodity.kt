package com.gcreate.jiajia.api.dataobj

data class Commodity(
    val id: Int,
    val name: String,
    val description: String,
    val url: String,
    val images: String
)
