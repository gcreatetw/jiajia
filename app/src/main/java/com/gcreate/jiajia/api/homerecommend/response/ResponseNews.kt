package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.data.HomeNews

data class ResponseNews(
    val blog : MutableList<HomeNews>
)