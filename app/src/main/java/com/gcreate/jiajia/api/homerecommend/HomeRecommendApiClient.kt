package com.gcreate.jiajia.api.homerecommend

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.request.*
import com.gcreate.jiajia.api.homerecommend.response.*
import com.google.gson.JsonObject

object HomeRecommendApiClient {

    fun slider(controller: ApiController<ResponseSlider>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.homeRecommend.slider(headers, ApiConstants.SERVER_PATH))
    }

    fun news(controller: ApiController<ResponseNews>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.homeRecommend.news(headers, ApiConstants.SERVER_PATH))
    }

    fun newsAll(request: RequestNews, controller: ApiController<ResponseNews>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.homeRecommend.newsAll(headers, ApiConstants.SERVER_PATH, request))
    }

    fun newsDetail(jsonObj: JsonObject, controller: ApiController<ResponseNewsDetail>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.homeRecommend.newsDetail(headers, ApiConstants.SERVER_PATH, jsonObj))
    }

    fun slider2(controller: ApiController<ResponseSlider>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.homeRecommend.slider2(headers, ApiConstants.SERVER_PATH))
    }

    fun coach(token: String, controller: ApiController<ResponseCoach>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.homeRecommend.coach(headers, ApiConstants.SERVER_PATH))
    }

    fun coachList(request: RequestCoach, controller: ApiController<ResponseCoachAll>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.homeRecommend.coachList(headers, ApiConstants.SERVER_PATH, request))
    }

    fun coachProfile(request: RequestCoachProfile, controller: ApiController<ResponseCoachProfile>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.homeRecommend.coachProfile(headers, ApiConstants.SERVER_PATH, request))
    }

    fun coachCourseList(request: RequestCoachCourseList, controller: ApiController<ResponseCoachCourseList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.homeRecommend.coachCourseList(headers, ApiConstants.SERVER_PATH, request))
    }

    fun channel(token: String?, controller: ApiController<ResponseChannel>) {
        val headers = ApiClient.header(false)
        if (token != null) {
            headers["Authorization"] = "Bearer $token"
        }
        controller.enqueue(ApiClient.homeRecommend.channel(headers, ApiConstants.SERVER_PATH))
    }

    fun channelCategory(token: String, request: RequestChannelCategory, controller: ApiController<ResponseChannelCategory>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.homeRecommend.channelCategory(headers, ApiConstants.SERVER_PATH, request))
    }

    fun channelCourse(token: String, request: RequestChannelCourse, controller: ApiController<ResponseChannelCourse>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.homeRecommend.channelCourse(headers, ApiConstants.SERVER_PATH, request))
    }
}