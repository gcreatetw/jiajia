package com.gcreate.jiajia.api.user.response

data class ResponseSocialLoginSetting(
    val data: SocialSetting,
)

data class SocialSetting(
    val facebook: Boolean? = true,
    val google: Boolean? = true,
    val line: Boolean? = true,
    val op: Boolean? = true,
)