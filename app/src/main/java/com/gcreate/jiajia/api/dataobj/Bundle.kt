package com.gcreate.jiajia.api.dataobj

import com.gcreate.jiajia.data.TrainingClassItem

data class Bundle(
    val id: Int,
    val title: String,
    val preview_image: String,
    val views : String,
    val created_at : String,
    val short_detail: String,
    val detail: String,
): TrainingClassItem
