package com.gcreate.jiajia.api.user.response

// 家庭方案-成員列表
data class ResponseFamilyList(
    val data: MutableList<FamilyListData>?,
    val error: Boolean,
    val error_message: String,
)

data class FamilyListData(
    val id: Int,
    val name: String,
    val user_image: String,
)

// 家庭方案-成員搜尋
data class ResponseSearchFamilyMemberResult(
    val data: SearchFamilyMemberResultData?,
    val error: Boolean,
    val error_message: String
)
data class SearchFamilyMemberResultData(
    val id: Int,
    val mobile: String,
    val name: String,
    val user_image: String
)

// 家庭方案-成員新增
data class ResponseAddFamilyMember(
    val data: MutableList<AddFamilyMemberData>,
    val error: Boolean,
    val error_message: String
)

data class AddFamilyMemberData(
    val created_at: String,
    val deleted_at: Any,
    val id: Int,
    val main_account: String,
    val member_account: String,
    val updated_at: String
)

// 家庭方案-成員刪除
data class ResponseDeleteFamilyMember(
    val data: MutableList<FamilyListData>?,
    val error: Boolean,
    val error_message: String,
)

