package com.gcreate.jiajia.api.user.response

import com.gcreate.jiajia.api.dataobj.Bundle

data class ResponseBrowserBundleReCord(
    val record: List<Bundle>
)
