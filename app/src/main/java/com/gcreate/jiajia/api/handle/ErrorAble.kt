package com.gcreate.jiajia.api.handle

interface ErrorAble {
    val errorCode: String
}
