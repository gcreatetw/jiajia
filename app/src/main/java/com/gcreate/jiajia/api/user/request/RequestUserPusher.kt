package com.gcreate.jiajia.api.user.request

data class RequestUserPusher(
    val news: Int,
    val instructor: Int,
    val category: Int,
    val calendar: Int,
)
