package com.gcreate.jiajia.api.user.response

data class ResponseUserAES(
    val error: Int,
    val message: String,
    val token: String,
)