package com.gcreate.jiajia.api.user.response

data class ResponseLogin (
    val token_type: String,
    val expires_in: Int,
    val access_token: String,
    val refresh_token: String,
    val errors: Errors,
    val message: String
)
data class Errors(
    val name: List<String>,
    val mobile: List<String>,
    val password: List<String>
)