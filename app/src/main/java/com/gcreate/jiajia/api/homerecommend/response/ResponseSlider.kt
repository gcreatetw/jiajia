package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.data.HomeBanner

data class ResponseSlider(
    val sliders: MutableList<HomeBanner>
)


