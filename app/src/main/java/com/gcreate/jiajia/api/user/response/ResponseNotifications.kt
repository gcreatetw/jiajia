package com.gcreate.jiajia.api.user.response

data class ResponseNotifications(
    val notifications: MutableList<Notification>,
    val unread_count: Int,
)

data class Notification(
//    val channel_id: ChannelId,
//    val notify_message: String,
//    val notify_message_2: String,
//    val object_id: Int,
//    var unread: Boolean = false,

    val id: Int,
    val type: String,
    val type_id: String,
    val title: String,
    val message: String,
    val user_image: String,
    val time: String,
    val is_followed: Boolean,
    var is_read: Boolean,
)

data class ChannelId(
    val detail: String,
    val id: Int,
    val image: String,
    val isFollowed: Boolean,
    val name: String,
)