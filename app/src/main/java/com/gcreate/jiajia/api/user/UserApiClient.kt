package com.gcreate.jiajia.api.user

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.request.*
import com.gcreate.jiajia.api.user.response.*
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody


object UserApiClient {

    // 登入
    fun login(request: JsonObject, controller: ApiController<ResponseLogin>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.login(headers, ApiConstants.SERVER_PATH, request))
    }

    // 登出
    fun logout(token: String, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.logout(headers, ApiConstants.SERVER_PATH))
    }

    // 驗證裝置
    fun verifyToken(token: String, request: JsonObject, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.verifyToken(headers, ApiConstants.SERVER_PATH, request))
    }

    // 追蹤列表
    fun listFollow(token: String, controller: ApiController<ResponseListFollow>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.listFollow(headers, ApiConstants.SERVER_PATH))
    }

    // 追蹤( 新增/刪除 ), 頻道
    fun followChannel(token: String, channelId: Int, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        val request = RequestFollowChannel("$channelId")
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.followChannel(headers, ApiConstants.SERVER_PATH, request))
    }

    // 追蹤( 新增/刪除 ), 分類
    fun followCategory(token: String, request: RequestFollowCategory, controller: ApiController<Unit>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.followCategory(headers, ApiConstants.SERVER_PATH, request))
    }

    // 取得會員資訊
    fun uerInfo(token: String, controller: ApiController<ResponseUserInfo>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getUserInfo(headers, ApiConstants.SERVER_PATH))
    }

    // 更新會員喜好設定
    fun updateUserHobby(token: String, request: RequestUserHobby, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.updateUserInfo(headers, ApiConstants.SERVER_PATH, request))
    }

    // 收藏
    fun getUserCollection(token: String, request: RequestUserCollection, controller: ApiController<ResponseUserCollection>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getUserCollects(headers, ApiConstants.SERVER_PATH, request))
    }

    // 加入收藏
    fun addUserCollection(token: String, request: RequestCourseId, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.addUserCollects(headers, ApiConstants.SERVER_PATH, request))
    }

    // 移除收藏
    fun deleteUserCollection(token: String, request: RequestCourseId, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.deleteUserCollects(headers, ApiConstants.SERVER_PATH, request))
    }

    // 瀏覽紀錄-課程
    fun getBrowserCourseRecord(token: String, controller: ApiController<ResponseBrowserCourseRecord>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getBrowserCourseRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 瀏覽紀錄-訓練計畫
    fun getBrowserBundleRecord(token: String, controller: ApiController<ResponseBrowserBundleReCord>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getBrowserBundleRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 瀏覽紀錄-訓練計畫
    fun setInvoice(token: String, request: RequestInvoice, controller: ApiController<ResponseInvoice>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.setInvoice(headers, ApiConstants.SERVER_PATH, request))
    }

    // 瀏覽紀錄-訓練計畫
    fun getInvoice(token: String, controller: ApiController<ResponseInvoiceResult>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getInvoice(headers, ApiConstants.SERVER_PATH))
    }

    // 個人化設定
    fun updateUserProfile(
        token: String,
        useName: RequestBody?,
        userAvatar: MultipartBody.Part?,
        county: RequestBody?,
        district: RequestBody?,
        address: RequestBody?,
        pinCode: RequestBody?,
        controller: ApiController<ResponseSimpleFormat>,
    ) {
        val headers = ApiClient.header(true)
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.updateUserProfile(headers,
            ApiConstants.SERVER_PATH,
            useName,
            userAvatar,
            county,
            district,
            address,
            pinCode))
    }

    // 設定密碼
    fun setUserPassword(token: String, request: RequestUserPassword, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["beingAuth"] = token
        controller.enqueue(ApiClient.user.setUserPassword(headers, ApiConstants.SERVER_PATH, request))
    }

    // 設定優惠券
    fun setUserCoupon(token: String, request: RequestUserCoupon, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.setUserCoupon(headers, ApiConstants.SERVER_PATH, request))
    }

    // 優惠券查詢
    fun getUserCouponRecord(token: String, controller: ApiController<ResponseUserCouponRecord>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getUserCouponRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 訂閱紀錄
    fun getPayedRecord(token: String, controller: ApiController<ResponsePayedRecord>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getPayedRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 管理訂閱
    fun getAvailableRecord(token: String, controller: ApiController<ResponseAvailableRecord>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getAvailableRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 取消訂閱
    fun setUserCancelRecord(token: String, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.setUserCancelRecord(headers, ApiConstants.SERVER_PATH))
    }

    //重新訂閱
    fun setUserReRecord(token: String, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.setUserReRecord(headers, ApiConstants.SERVER_PATH))
    }

    // 第三方登入(OP) request url
    fun getOpRequestUrl(controller: ApiController<ResponseBody>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.getOpRequestUrl(headers, ApiConstants.SERVER_PATH))
    }

    // 第三方登入(OP) ResponseOpenPointJson
    fun getOpenPointResponseJSON(path: String, query: String, controller: ApiController<ResponseOpenPointJson>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.getOpenPointResponseJSON(headers, path, query))
    }


    // 註冊
    fun register(request: RequestLogin, controller: ApiController<ResponseLogin>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.register(headers, ApiConstants.SERVER_PATH, request))
    }

    // 驗證手機號碼
    fun verifyMobile(request: RequestVerifyAccountBody, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.verifyMobile(headers, ApiConstants.SERVER_PATH, request))
    }

    // 驗證使用者第三方ID是否存在
    fun verifyThirdPartyId(request: RequestVerifyThirdPartyBody, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.verifyThirdPartyId(headers, ApiConstants.SERVER_PATH, request))
    }

    // 系統通知
    fun getFirebaseNotifications(controller: ApiController<ResponseFirebaseNotification>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getFirebaseNotifications(headers, ApiConstants.SERVER_PATH))
    }

    // 通知列表
    fun getNotifications(token: String, controller: ApiController<ResponseNotifications>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getNotifications(headers, ApiConstants.SERVER_PATH))
    }

    // 通知列表
    fun readNotification(token: String, request: RequestReadNotificationBody, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.readNotification(headers, ApiConstants.SERVER_PATH, request))
    }

    // 推播設定
    fun setUserPusher(token: String, request: RequestUserPusher, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.setUserPusher(headers, ApiConstants.SERVER_PATH, request))
    }

    // 綁訂open point
    fun bindOpenPoint(token: String, request: JsonObject, controller: ApiController<ResponseBingOP>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.bindOpenPoint(headers, ApiConstants.SERVER_PATH, request))
    }

    // 綁訂其他第三方
    fun bindOtherThirdLogin(token: String, request: JsonObject, controller: ApiController<ResponseThirdBindResult>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.bindOtherThirdLogin(headers, ApiConstants.SERVER_PATH, request))
    }

    // 家庭方案-成員列表
    fun getFamilyMember(token: String, controller: ApiController<ResponseFamilyList>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.getFamilyMember(headers, ApiConstants.SERVER_PATH))
    }

    // 家庭方案-成員搜尋
    fun searchFamilyMember(token: String, request: RequestSearchFamilyMember, controller: ApiController<ResponseSearchFamilyMemberResult>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.searchFamilyMember(headers, ApiConstants.SERVER_PATH, request))
    }

    // 家庭方案-成員新增
    fun addFamilyMember(token: String, request: RequestAddFamilyMember, controller: ApiController<ResponseAddFamilyMember>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.addFamilyMember(headers, ApiConstants.SERVER_PATH, request))
    }

    // 家庭方案-成員刪除
    fun deleteFamilyMember(token: String, request: RequestDeleteFamilyMember, controller: ApiController<ResponseDeleteFamilyMember>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.deleteFamilyMember(headers, ApiConstants.SERVER_PATH, request))
    }

    // 家庭方案-成員刪除
    fun leaveFamilyGroup(token: String, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.leaveFamilyGroup(headers, ApiConstants.SERVER_PATH))
    }

    // 解除 ICP 綁定
    fun cancelICPBinding(token: String, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.cancelICPBinding(headers, ApiConstants.SERVER_PATH))
    }

    // 解除 信用卡 綁定
    fun cancelCathaybkBinding(token: String, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.cancelCathaybkBinding(headers, ApiConstants.SERVER_PATH))
    }

    // 解除第三方平台
    fun cancelThirdPartyBinding(token: String, requestBody: JsonObject, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.cancelThirdPartyBinding(headers, ApiConstants.SERVER_PATH, requestBody))
    }

    // 版本比對
    fun appVersion(controller: ApiController<ResponseVersion>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.appVersion(headers, ApiConstants.SERVER_PATH))
    }

    // 第三方登入啟用開關
    fun socialLoginInfo(controller: ApiController<ResponseSocialLoginSetting>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.socialLoginInfo(headers, ApiConstants.SERVER_PATH))
    }

    // 取得捐贈發票
    fun getDonateMechanism(token: String, controller: ApiController<ResponseDonateMechanism>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.getDonateMechanism(headers, ApiConstants.SERVER_PATH))
    }

    // 取得推薦課程
    fun getUserRecommendCourse(token: String, controller: ApiController<ResponseUserRecommendCourse>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.getUserRecommendCourse(headers, ApiConstants.SERVER_PATH))
    }

    // 取得短暫有效token
    fun getShortTimeToken(request: RequestUserAES, controller: ApiController<ResponseUserAES>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.user.getShortTimeToken(headers, ApiConstants.SERVER_PATH, request))
    }

    // 取得聯盟行銷開關設定與文案
    fun getAffSetting(controller: ApiController<ResponseAffSetting>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        controller.enqueue(ApiClient.user.getAffSetting(headers, ApiConstants.SERVER_PATH))
    }

    // 取得推薦代碼分享紀錄
    fun getAffiliateMarketingList(token: String,controller: ApiController<ResponseUserReferralList>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.getAffiliateMarketingList(headers, ApiConstants.SERVER_PATH))
    }

    // 領取推薦OP點數
    fun getReferralReceive(token: String,requestBody: JsonObject, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.getReferralReceive(headers, ApiConstants.SERVER_PATH, requestBody))
    }

    // 填入推薦人
    fun addAffiliateMarketingReferral(token: String,requestBody: JsonObject, controller: ApiController<ResponseSimpleFormat2>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.user.addAffiliateMarketingReferral(headers, ApiConstants.SERVER_PATH, requestBody))
    }

}