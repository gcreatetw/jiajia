package com.gcreate.jiajia.api.vimeo.response


data class Metadata(
    val connections: Connections,
    val interactions: Interactions,
    val is_vimeo_create: Boolean,
    val is_screen_record: Boolean,
)

/* ----------------------- Connections ----------------------- */
data class Connections(
    val comments: Comments,
    val credits: Credits?,
    val likes: Likes,
    val pictures: ConnectionsPictures,
    val texttracks: Texttracks,
    val related: Any,
    val recommendations: Recommendations?,
    val albums: Albums,
    val available_albums: AvailableAlbums,
    val versions: Versions,
    val appearances: Appearances,
    val categories: Categories,
    val channels: Channels,
    val feed: Feed,
    val followers: Followers,
    val following: Following,
    val groups: Groups,
    val membership: Membership,
    val moderated_channels: ModeratedChannels,
    val portfolios: Portfolios,
    val videos: Videos,
    val watchlater: Watchlater,
    val shared: Shared,
    val watched_videos: WatchedVideos,
    val folders_root: FoldersRoot,
    val folders: Folders,
    val teams: Teams,
    val permission_policies: PermissionPolicies,
    val block: Block,
    val items: Items,
    val ancestor_path: List<Any>,
)

data class Comments(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Credits(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Likes(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class ConnectionsPictures(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Texttracks(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Recommendations(
    val options: List<String>,
    val uri: String
)

data class Albums(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class AvailableAlbums(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Versions(
    val uri: String,
    val options: List<String>,
    val total: Int,
    val current_uri: String,
    val resource_key: String,
    val latest_incomplete_version : Any
)

data class Appearances(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Categories(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Channels(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Feed(
    val uri: String,
    val options: List<String>,
)

data class Followers(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Following(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Groups(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Membership(
    val uri: String,
    val options: List<String>,
)

data class ModeratedChannels(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Portfolios(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Videos(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Shared(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class WatchedVideos(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class FoldersRoot(
    val uri: String,
    val options: List<String>,
)

data class Folders(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Teams(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class PermissionPolicies(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Block(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

data class Items(
    val uri: String,
    val options: List<String>,
    val total: Int,
)

/* ----------------------- Interactions ----------------------- */
data class Interactions(
    val watchlater: Watchlater,
    val report: Report,
    val view_team_members: ViewTeamMembers,
    val edit: Edit,
    val edit_content_rating: EditContentRating,
    val edit_privacy: EditPrivacy,
    val delete: Delete,
    val can_update_privacy_to_public: CanUpdatePrivacyToPublic,
    val trim: Trim,
    val validate: Validate,
    ////////////////////
    val view: View,
    val invite: Invite,
    val edit_settings: EditSettings,
    val delete_video: DeleteVideo,
    val add_subfolder: AddSubfolder,
)

data class Watchlater(
    val uri: String,
    val options: List<String>,
    val added: Boolean,
    val added_time: Any,
    val total: Int,
)

data class Report(
    val uri: String,
    val options: List<String>,
    val reason: List<String>,
)

data class ViewTeamMembers(
    val uri: String,
    val options: List<String>,
)

data class Edit(
    val uri: String,
    val options: List<String>,
    val blocked_fields: List<Any>,
)

data class EditContentRating(
    val uri: String,
    val options: List<String>,
    val content_rating: List<String>,
)

data class EditPrivacy(
    val uri: String,
    val options: List<String>,
    val content_type: String,
    val properties: List<Property>,
)

data class Property(
    val name: String,
    val required: Boolean,
    val options: List<String>,
    val value: String,
)

data class Delete(
    val uri: String,
    val options: List<String>,
)

data class CanUpdatePrivacyToPublic(
    val uri: String,
    val options: List<String>,
)

data class Trim(
    val uri: String,
    val options: List<String>,
)

data class Validate(
    val uri: String,
    val options: List<String>,
)

data class View(
    val uri: String,
    val options: List<String>,
)

data class Invite(
    val uri: String,
    val options: List<String>,
)

data class EditSettings(
    val uri: String,
    val options: List<String>,
)

data class DeleteVideo(
    val uri: String,
    val options: List<String>,
)

data class AddSubfolder(
    val uri: String,
    val options: List<String>,
    val can_add_subfolders: Boolean,
    val subfolder_depth_limit_reached: Boolean,
    val content_type: String,
    val properties: List<Property>,
)

