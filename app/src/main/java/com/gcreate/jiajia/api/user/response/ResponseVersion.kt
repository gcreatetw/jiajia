package com.gcreate.jiajia.api.user.response

data class ResponseVersion(
    val android: List<String>,
    val ios: List<String>
)