package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.dataobj.Channel

data class ResponseChannel(
    val channel: List<Channel>
)
