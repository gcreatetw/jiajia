package com.gcreate.jiajia.api

import android.content.Context
//import com.gcreate.jiajia.BuildConfig
import com.gcreate.jiajia.R

object ApiConstants {

    enum class ServerType {
        API_DEBUG,
        API_FORMAL,
        API_FORMAL_UAT,
        Viemo_API_FORMAL,
    }

    var SERVER_PATH = ""
    private var SERVER_URL = ""

    fun getServerUrl(context: Context, serverType: ServerType): String {
        when (serverType) {

            ServerType.API_DEBUG -> {
                SERVER_URL = context.getString(R.string.server_url_debug)
                SERVER_PATH = context.getString(R.string.server_path_debug)
            }

            ServerType.API_FORMAL -> {
                SERVER_URL = context.getString(R.string.server_url_formal)
                SERVER_PATH = context.getString(R.string.server_path_formal)
            }

            ServerType.API_FORMAL_UAT -> {
                SERVER_URL = context.getString(R.string.server_url_formal_uat)
                SERVER_PATH = context.getString(R.string.server_path_formal)
            }

            ServerType.Viemo_API_FORMAL -> {
                SERVER_URL = context.getString(R.string.server_vimeo_url_formal)
            }
        }
        return SERVER_URL
    }

}