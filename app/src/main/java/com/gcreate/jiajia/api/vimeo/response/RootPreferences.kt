package com.gcreate.jiajia.api.vimeo.response

data class RootPreferences(
    val videos: PreferencesVideos
)

data class PreferencesVideos(
    val rating: List<String>,
    val privacy: PreferencesVideoPrivacy,
)

data class PreferencesVideoPrivacy(
    val add: Boolean,
    val comments: String,
    val download: Boolean,
    val embed: String,
    val view: String
)