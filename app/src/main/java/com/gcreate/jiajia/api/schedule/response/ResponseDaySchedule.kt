package com.gcreate.jiajia.api.schedule.response

data class ResponseDaySchedule(
    val Course: List<Course>
)

data class Course(
    val id: Int,
    val schedule_id: Int,
    val name: String,
    val preview_image: String,
    val start: String,
    val title: String,
    val user_img: String,
    val video_enable:Boolean,
)