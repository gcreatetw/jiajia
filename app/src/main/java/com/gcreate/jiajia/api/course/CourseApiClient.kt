package com.gcreate.jiajia.api.course

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.course.request.RequestCourseDetail
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.course.response.ResponseCourseDetail
import com.gcreate.jiajia.api.course.response.ResponseCourseFilter
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseVideoEnable

object CourseApiClient {

    // 課程( 最新上架、熱門課程、課程分類(Tab-2) )
    fun courses(token: String, request: RequestCourses, controller: ApiController<ResponseCourses>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.course.courses(headers, ApiConstants.SERVER_PATH, request))
    }

    // 課程篩選器
    fun courseFilter(controller: ApiController<ResponseCourseFilter>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.course.courseFilter(headers, ApiConstants.SERVER_PATH))
    }

    // 課程分類
    fun category(controller: ApiController<ResponseCategory>) {
        val headers = ApiClient.header(false)
        controller.enqueue(ApiClient.course.category(headers, ApiConstants.SERVER_PATH))
    }

    // 課程詳細資訊
    fun courseDetail(token: String?, request: RequestCourseDetail, controller: ApiController<ResponseCourseDetail>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.course.courseDetail(headers, ApiConstants.SERVER_PATH, request))
    }

    // 紀錄影片撥放次數
    fun recordCourseViewCount(token: String?, request: RequestCourseDetail, controller: ApiController<ResponseSimpleFormat>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.course.recordCourseViewCount(headers, ApiConstants.SERVER_PATH, request))
    }

    // 確認影片是否能觀看
    fun checkViewEnable(request: RequestCourseDetail, controller: ApiController<ResponseVideoEnable>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        controller.enqueue(ApiClient.course.checkViewEnable(headers, ApiConstants.SERVER_PATH, request))
    }

}