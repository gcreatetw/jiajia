package com.gcreate.jiajia.api.payment.response

data class ResponsePayStoreByCathaybk(
    val error: Int,
    val data: Data?,
    val paymentUrl: String
) {
    data class Data(
        val sessionId: String,
        val acsUrl: String,
        val statusCode: String,
        val statusMessage: String
    )
}


