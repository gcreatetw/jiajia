package com.gcreate.jiajia.api.user


import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.request.*
import com.gcreate.jiajia.api.user.response.*
import com.google.gson.JsonObject

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody


import retrofit2.Call

import retrofit2.http.*

interface UserApi {

    // 登入
    @POST("{serverPath}/login")
    fun login(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseLogin>

    // 登出
    @POST("{serverPath}/logout")
    fun logout(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat>

    // 驗證裝置
    @POST("{serverPath}/verify_token")
    fun verifyToken(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseSimpleFormat>

    // 追蹤列表
    @GET("{serverPath}/list_follow")
    fun listFollow(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseListFollow>

    // 追蹤( 新增/刪除 ), 頻道
    @POST("{serverPath}/follow")
    fun followChannel(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestFollowChannel,
    ): Call<ResponseSimpleFormat>

    // 追蹤( 新增/刪除 ), 分類
    @POST("{serverPath}/follow")
    fun followCategory(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestFollowCategory,
    ): Call<Unit>

    // 會員資訊
    @POST("{serverPath}/user/profile")
    fun getUserInfo(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseUserInfo>

    // 喜好設定
    @POST("{serverPath}/user/hobbies")
    fun updateUserInfo(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserHobby,
    ): Call<ResponseSimpleFormat>

    // 收藏列表
    @POST("{serverPath}/list_collect")
    fun getUserCollects(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserCollection,
    ): Call<ResponseUserCollection>

    // 加入收藏
    @POST("{serverPath}/add_collect")
    fun addUserCollects(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourseId,
    ): Call<ResponseSimpleFormat>

    // 移除收藏
    @POST("{serverPath}/del_collect")
    fun deleteUserCollects(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestCourseId,
    ): Call<ResponseSimpleFormat>

    // 瀏覽紀錄-課程
    @GET("{serverPath}/user/browser_course_record")
    fun getBrowserCourseRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseBrowserCourseRecord>

    // 瀏覽紀錄-訓練計畫
    @GET("{serverPath}/user/browser_bundle_record")
    fun getBrowserBundleRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseBrowserBundleReCord>

    // 發票設定
    @POST("{serverPath}/user/invoice")
    fun setInvoice(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestInvoice,
    ): Call<ResponseInvoice>

    // 取得發票
    @GET("{serverPath}/user/get_invoice")
    fun getInvoice(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseInvoiceResult>

    // 個人化設定
    @Multipart
    @POST("{serverPath}/user/profile/edit")
    fun updateUserProfile(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Part("name") name: RequestBody?,
        @Part file: MultipartBody.Part?,
        @Part("county") county: RequestBody?,
        @Part("district") district: RequestBody?,
        @Part("address") address: RequestBody?,
        @Part("pin_code") pin_code: RequestBody?,
    ): Call<ResponseSimpleFormat>

    // 設定密碼
    @POST("{serverPath}/resetpassword")
    fun setUserPassword(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserPassword,
    ): Call<ResponseSimpleFormat>

    // 設定優惠券
    @POST("{serverPath}/user/coupon")
    fun setUserCoupon(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserCoupon,
    ): Call<ResponseSimpleFormat>

    // 優惠券查詢
    @POST("{serverPath}/user/coupon_record")
    fun getUserCouponRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseUserCouponRecord>

    // 訂閱紀錄
    @GET("{serverPath}/user/payed_record")
    fun getPayedRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponsePayedRecord>

    // 管理訂閱
    @GET("{serverPath}/user/available_record")
    fun getAvailableRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseAvailableRecord>

    // 取消訂閱
    @POST("{serverPath}/user/cancel_record")
    fun setUserCancelRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat>

    // 重新訂閱
    @POST("{serverPath}/user/re_record")
    fun setUserReRecord(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat>

    // 第三方登入(OP) request url
    @GET("{serverPath}/op_request_url")
    fun getOpRequestUrl(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseBody>

    // 第三方登入(OP) get login JSON
    @GET("{serverPath}")
    fun getOpenPointResponseJSON(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Query(value = "v", encoded = true) value: String,
        ): Call<ResponseOpenPointJson>

    // 註冊
    @POST("{serverPath}/register")
    fun register(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestLogin
    ): Call<ResponseLogin>

    // 驗證手機號碼
    @POST("{serverPath}/verifymobile")
    fun verifyMobile(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestVerifyAccountBody
    ): Call<ResponseSimpleFormat>

    // 驗證使用者第三方ID是否存在
    @POST("{serverPath}/verify_thirdparty")
    fun verifyThirdPartyId(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestVerifyThirdPartyBody
    ): Call<ResponseSimpleFormat>

    // 取得第三方登入後台開關設定
    @GET("{serverPath}/social_login_info")
    fun socialLoginInfo(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSocialLoginSetting>

    // 系統通知
    @GET("{serverPath}/firebase/notification")
    fun getFirebaseNotifications(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseFirebaseNotification>


    // 通知列表
    @GET("{serverPath}/user/notifications")
    fun getNotifications(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseNotifications>

    // 已讀單一通知
    @POST("{serverPath}/user/readnotification")
    fun readNotification(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestReadNotificationBody
    ): Call<ResponseSimpleFormat>

    // 推播設定
    @POST("{serverPath}/user/pusher")
    fun setUserPusher(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserPusher
    ): Call<ResponseSimpleFormat>

    // 綁訂open point
    @POST("{serverPath}/user/bundle_openpoint")
    fun bindOpenPoint(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseBingOP>

    // 綁訂其他第三方
    @POST("{serverPath}/user/bundle_thirdpart")
    fun bindOtherThirdLogin(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseThirdBindResult>

    // 家庭方案-成員列表
    @POST("{serverPath}/user/family/list")
    fun getFamilyMember(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseFamilyList>

    // 家庭方案-成員搜尋
    @POST("{serverPath}/user/family/search")
    fun searchFamilyMember(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestSearchFamilyMember
    ): Call<ResponseSearchFamilyMemberResult>

    // 家庭方案-成員新增
    @POST("{serverPath}/user/family/set")
    fun addFamilyMember(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestAddFamilyMember
    ): Call<ResponseAddFamilyMember>

    // 家庭方案-成員刪除
    @POST("{serverPath}/user/family/del")
    fun deleteFamilyMember(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestDeleteFamilyMember
    ): Call<ResponseDeleteFamilyMember>

    // 家庭方案-子帳自行離開
    @POST("{serverPath}/user/family/leave")
    fun leaveFamilyGroup(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat>

    // 解除 ICP 綁定
    @POST("{serverPath}/cancelICPBinding")
    fun cancelICPBinding(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat2>

    // 解除 信用卡 綁定
    @POST("{serverPath}/cancelCathaybkBinding")
    fun cancelCathaybkBinding(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseSimpleFormat2>

    // 解除第三方平台
    @POST("{serverPath}/user/lift_thirdpart")
    fun cancelThirdPartyBinding(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseSimpleFormat>

    // 版本比對
    @GET("{serverPath}/app_ver")
    fun appVersion(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseVersion>

    // 取得捐贈發票
    @GET("{serverPath}/user/donate_mechanism")
    fun getDonateMechanism(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseDonateMechanism>

    // 取得推薦課程
    @GET("{serverPath}/user/recommend_course")
    fun getUserRecommendCourse(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseUserRecommendCourse>


    // 取得短暫有效token
    @POST("{serverPath}/get_token")
    fun getShortTimeToken(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: RequestUserAES
    ): Call<ResponseUserAES>


    // 取得聯盟行銷開關設定與文案
    @GET("{serverPath}/aff_settings")
    fun getAffSetting(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseAffSetting>

    // 取得推薦代碼分享紀錄
    @GET("{serverPath}/user/referral/list")
    fun getAffiliateMarketingList(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
    ): Call<ResponseUserReferralList>

    // 領取推薦OP點數
    @POST("{serverPath}/user/referral/receive")
    fun getReferralReceive(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseSimpleFormat2>

    // 填入推薦人
    @POST("{serverPath}/user/referral/add")
    fun addAffiliateMarketingReferral(
        @HeaderMap headers: HashMap<String, String>,
        @Path(value = "serverPath", encoded = true) serverPath: String,
        @Body body: JsonObject,
    ): Call<ResponseSimpleFormat2>

}