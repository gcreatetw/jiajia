package com.gcreate.jiajia.api.user.request

data class RequestUserPassword(
    val mobile: String,
    val password: String,
)