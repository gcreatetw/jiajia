package com.gcreate.jiajia.api.user.response

data class ResponseBingOP(
    val error: Boolean,
    val error_message: String,
    val data: UserOP,
)

data class UserOP(
    val address: String,
    val amazon_id: Any,
    val apple_id: Any,
    val bank_acc_name: Any,
    val bank_acc_no: Any,
    val bank_name: Any,
    val birthday: String,
    val braintree_id: String,
    val calendar: String,
    val category: String,
    val cathay_card_id: String,
    val city_id: Any,
    val code: Any,
    val country_id: Any,
    val county: String,
    val created_at: String,
    val detail: Any,
    val district: Any,
    val doa: Any,
    val dob: Any,
    val email: String,
    val email_verified_at: String,
    val facebook_id: Any,
    val fb_url: Any,
    val fitness_howlong: String,
    val fitness_level: String,
    val fitness_target: String,
    val fname: String,
    val gender: String,
    val gitlab_id: Any,
    val google2fa_enable: String,
    val google_id: Any,
    val height: String,
    val icash_op_id: Any,
    val icashpay_maskedPan: Any,
    val icashpay_token: Any,
    val id: Int,
    val ifsc_code: Any,
    val instructor: String,
    val invoice_carrier_default: Any,
    val invoice_donate_number: Any,
    val invoice_mobile_carrier: Any,
    val invoice_nature_carrier: Any,
    val invoice_tax_company: Any,
    val invoice_tax_email: Any,
    val invoice_tax_number: Any,
    val invoice_type_default: Any,
    val jht_introuctor_id: Any,
    val jht_introuctor_img: Any,
    val jwt_token: Any,
    val line_id: Any,
    val linkedin_id: Any,
    val linkedin_url: Any,
    val lname: String,
    val married_status: Any,
    val mobile: String,
    val news: String,
    val op_id: String,
    val payment_method: String,
    val paypal_email: Any,
    val paytm_mobile: Any,
    val pin_code: Any,
    val prefer_pay_method: Any,
    val role: String,
    val state_id: Any,
    val status: String,
    val twitter_id: Any,
    val twitter_url: Any,
    val updated_at: String,
    val user_img: Any,
    val vacation_end: Any,
    val vacation_start: Any,
    val verified: String,
    val weight: String,
    val youtube_url: Any,
    val zoom_email: Any
)