package com.gcreate.jiajia.api.payment.request

data class RequestPayStore(
    val payment_method: String,
    val app_name: String,
)