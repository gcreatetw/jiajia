package com.gcreate.jiajia.api.payment.response

data class ResponseOrderPurchaseResult(
    val error: Int,
    val message: String,
)

