package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.data.HomeCoach

class ResponseCoachAll(
    val instructor: List<HomeCoach>,
)