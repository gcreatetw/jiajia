package com.gcreate.jiajia.api.payment.response

data class ResponseSubscriptions(
    val plan: MutableList<Plan>,
    val subscription: Any,
)

data class Plan(
    val description: String,
    val id: Int,
    val name: String,
    val period: Int,
    val price: String,
)