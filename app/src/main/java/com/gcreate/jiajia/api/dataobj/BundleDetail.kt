package com.gcreate.jiajia.api.dataobj

import com.gcreate.jiajia.data.ItemName

data class BundleDetail(
    val id: Int,
    val title: String,
    val detail: String,
    val unfit_situation: String,
    val fit_situation: String?,
    val before_fitness: String,
    val body_response: String,
    val suggest: String,
    val note: String,
    val preview_image: String,
    val Calorie: String,
    val Level: List<String>,
    val accessory: List<ItemName>,
    val isCalendar: Boolean
)
