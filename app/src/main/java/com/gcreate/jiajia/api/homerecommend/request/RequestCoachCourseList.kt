package com.gcreate.jiajia.api.homerecommend.request

data class RequestCoachCourseList(
    val instructor_id: Int,
    val cat_id: Int,
    val limit: Int,
    val page: Int
)
