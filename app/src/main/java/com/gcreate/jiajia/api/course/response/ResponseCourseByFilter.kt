package com.gcreate.jiajia.api.course.response

import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.data.BeingClass

data class ResponseCourseByFilter(
    val result: List<BeingClass>
){
    fun completImageUrl(){
        for(item in result){
            item.preview_image = ApiUtil.completImageUrl(item.preview_image)
            item.user_img = ApiUtil.completImageUrl(item.user_img)
        }
    }
}