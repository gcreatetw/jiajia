package com.gcreate.jiajia.api.homerecommend.request

data class RequestCoachProfile(
    val instructor_id: Int
)