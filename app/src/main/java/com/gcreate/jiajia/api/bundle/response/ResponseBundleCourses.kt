package com.gcreate.jiajia.api.bundle.response

import com.gcreate.jiajia.api.dataobj.Bundle

data class ResponseBundleCourses(
    val bundle: List<Bundle>
)
