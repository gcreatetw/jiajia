package com.gcreate.jiajia.api.homerecommend.request

data class RequestChannelCourse(
    val cat_id: List<Int>,
    val date: String,
    val level: List<Int>,
    val instructor: List<Int>,
    val accessory: List<Int>,
    val limit: Int,
    val offset: Int
)
