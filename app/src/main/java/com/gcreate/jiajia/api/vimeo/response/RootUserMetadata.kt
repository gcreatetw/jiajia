package com.gcreate.jiajia.api.vimeo.response

data class RootUserMetadata(
    val connections: Connections
)