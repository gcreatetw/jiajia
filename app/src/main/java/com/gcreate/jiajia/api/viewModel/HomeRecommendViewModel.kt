package com.gcreate.jiajia.api.viewModel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.response.ResponseCoach
import com.gcreate.jiajia.api.homerecommend.response.ResponseNews
import com.gcreate.jiajia.api.homerecommend.response.ResponseSlider
import com.gcreate.jiajia.adapters.home.banner.HomeBannerAdapter
import com.gcreate.jiajia.adapters.home.banner.HomeBannerAdapter2
import com.gcreate.jiajia.data.HomeCoach
import com.gcreate.jiajia.data.HomeNews
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.databinding.FragmentHomeRecommendBinding
import com.gcreate.jiajia.adapters.home.HomeHotCourseAdapter
import com.gcreate.jiajia.adapters.home.HomeLatestCourseByCategoryAdapter
import com.gcreate.jiajia.adapters.home.HomeLatestNewsAdapter
import com.gcreate.jiajia.adapters.home.HomeTeacherAdapter
import retrofit2.Response

@SuppressLint("NotifyDataSetChanged")
class HomeRecommendViewModel(
    private val binding: FragmentHomeRecommendBinding,
    private val navController: NavController,
    private val mainModel: MainViewModel,
) : ViewModel() {

    // Banner 1
    private var bannerList: MutableLiveData<ResponseSlider> = MutableLiveData()
    private var homeBannerAdapter: HomeBannerAdapter = HomeBannerAdapter {
        val sliderItem = (bannerList.value as ResponseSlider).sliders[it]
        if (!sliderItem.link.isNullOrEmpty()) {
            val action = MainFragmentDirections.actionMainFragmentToEventFragment(sliderItem.link)
            navController.navigate(action)
        }
    }

    fun getBannerAdapter(): HomeBannerAdapter {
        return homeBannerAdapter
    }

    fun getBannerDataObserver(): MutableLiveData<ResponseSlider> {
        return bannerList
    }

    fun getBannerApiData() {
        HomeRecommendApiClient.slider(object : ApiController<ResponseSlider>(binding.root.context, false) {
            override fun onSuccess(response: ResponseSlider) {
                bannerList.postValue(response)
            }
        })
    }

    // 最新上架-分類
    private var categoryDataList: MutableLiveData<ResponseCategory> = MutableLiveData()

    fun getLatestCourseCategoryDataObserver(): MutableLiveData<ResponseCategory> {
        return categoryDataList
    }

    fun getLatestCourseCategoryApiData() {
        CourseApiClient.category(object : ApiController<ResponseCategory>(binding.root.context, true) {
            override fun onSuccess(response: ResponseCategory) {
                categoryDataList.postValue(response)
            }

            override fun onFail(httpResponse: Response<ResponseCategory>): Boolean {
                categoryDataList.postValue(null)
                return super.onFail(httpResponse)
            }
        })
    }

    // 最新上架 依分類取得文章
    private var latestCourseByCategoryDataList: MutableLiveData<ResponseCourses> = MutableLiveData()
    private var latestCourseByCategoryAdapter: HomeLatestCourseByCategoryAdapter = HomeLatestCourseByCategoryAdapter(mutableListOf())

    fun getLatestCourseByCategoryAdapter(): HomeLatestCourseByCategoryAdapter {
        return latestCourseByCategoryAdapter
    }

    fun setLatestCourseByCategoryAdapterData(data: MutableList<Course>) {
        latestCourseByCategoryAdapter.data = data
        latestCourseByCategoryAdapter.notifyDataSetChanged()

        if (data.isNotEmpty()){
            latestCourseByCategoryAdapter.setOnItemClickListener { _, _, position ->
                if (data[position].videoEnable){
                    val action = MainFragmentDirections.actionMainFragmentToVideoPageFragment()
                    action.courseId = data[position].id
                    navController.navigate(action)
                }
            }
        }else{
            latestCourseByCategoryAdapter.setEmptyView(R.layout.layout_no_data)
        }

    }

    fun getLatestCourseByCategoryDataObserver(): MutableLiveData<ResponseCourses> {
        return latestCourseByCategoryDataList
    }

    fun getLatestCourseByCategoryApiData(id: Int) {
        val request = RequestCourses(
            "$id", null, "全部",
            listOf(), listOf(), listOf(), 12, 1,null,null)

        val token = mainModel.appState.accessToken.get()!!
        CourseApiClient.courses(token, request, object : ApiController<ResponseCourses>(binding.root.context, false) {
            override fun onSuccess(response: ResponseCourses) {
                latestCourseByCategoryDataList.postValue(response)
            }

            override fun onFail(httpResponse: Response<ResponseCourses>): Boolean {
                latestCourseByCategoryDataList.postValue(null)
                return super.onFail(httpResponse)
            }
        })
    }

    // 熱門課程
    private var hotCourseDataList: MutableLiveData<ResponseCourses> = MutableLiveData()
    private var hotCourseAdapter: HomeHotCourseAdapter = HomeHotCourseAdapter(mutableListOf())

    fun getHotCourseAdapter(): HomeHotCourseAdapter {
        return hotCourseAdapter
    }

    fun setHotCourseAdapterData(data: MutableList<Course>) {
        hotCourseAdapter.data = data
        hotCourseAdapter.notifyDataSetChanged()

        if (data.isNotEmpty()){
            hotCourseAdapter.setOnItemClickListener { _, _, position ->
                if(data[position].videoEnable){
                    val action = MainFragmentDirections.actionMainFragmentToVideoPageFragment()
                    action.courseId = data[position].id
                    navController.navigate(action)
                }
            }
        }else{
            hotCourseAdapter.setEmptyView(R.layout.layout_no_data)
        }
    }

    fun getHotCourseDataObserver(): MutableLiveData<ResponseCourses> {
        return hotCourseDataList
    }

    fun getHotCourseApiData() {
        val request = RequestCourses(
            "", 1,
            "全部", listOf(),
            listOf(), listOf(),
            12, 1,null,null)

        val token = mainModel.appState.accessToken.get()!!

        CourseApiClient.courses(token, request, object : ApiController<ResponseCourses>(binding.root.context, false) {
            override fun onSuccess(response: ResponseCourses) {
                hotCourseDataList.postValue(response)
            }

            override fun onFail(httpResponse: Response<ResponseCourses>): Boolean {
                hotCourseDataList.postValue(null)
                return super.onFail(httpResponse)
            }
        })
    }

    // 最新消息
    private var latestNewsDataList: MutableLiveData<ResponseNews> = MutableLiveData()
    private var homeLatestNewsAdapter: HomeLatestNewsAdapter = HomeLatestNewsAdapter(mutableListOf())

    fun getLatestNewsAdapter(): HomeLatestNewsAdapter {
        return homeLatestNewsAdapter
    }

    fun setLatestNewsAdapterData(data: MutableList<HomeNews>) {
        homeLatestNewsAdapter.data = data
        homeLatestNewsAdapter.notifyDataSetChanged()
        homeLatestNewsAdapter.setOnItemClickListener { _, _, position ->
            when (data[position].category) {
                "活動檔期" -> {
                    // event 外聯網址
                    // article
                    val action = MainFragmentDirections.actionMainFragmentToSportKnowledgeFragment(data[position].url)
                    navController.navigate(action)
//                    val action = MainFragmentDirections.actionMainFragmentToActivityScheduleFragment()
//                    action.imageUrl = data[position].image
//                    action.imageLink = data[position].url
//                    navController.navigate(action)
                }
                "運動知識" -> {
                    // article
                    val action = MainFragmentDirections.actionMainFragmentToSportKnowledgeFragment(data[position].url)
//                    action.blogId = data[position].id
                    navController.navigate(action)
                }
            }
        }
    }

    fun getLatestNewsDataObserver(): MutableLiveData<ResponseNews> {
        return latestNewsDataList
    }

    fun getLatestNewsApiData() {
        HomeRecommendApiClient.news(object : ApiController<ResponseNews>(binding.root.context, false) {
            override fun onSuccess(response: ResponseNews) {
                latestNewsDataList.postValue(response)
            }

            override fun onFail(httpResponse: Response<ResponseNews>): Boolean {
                hotCourseDataList.postValue(null)
                return super.onFail(httpResponse)
            }
        })
    }

    // Banner 2
    private var banner2List: MutableLiveData<ResponseSlider> = MutableLiveData()
    private var homeBanner2Adapter: HomeBannerAdapter2 = HomeBannerAdapter2 {
        val sliderItem = (bannerList.value as ResponseSlider).sliders[it]
        if (!sliderItem.link.isNullOrEmpty()) {
            val action = MainFragmentDirections.actionMainFragmentToEventFragment(sliderItem.link)
            navController.navigate(action)
        }
    }

    fun getBanner2Adapter(): HomeBannerAdapter2 {
        return homeBanner2Adapter
    }

    fun getBanner2DataObserver(): MutableLiveData<ResponseSlider> {
        return banner2List
    }

    fun getBanner2ApiData() {
        HomeRecommendApiClient.slider2(object : ApiController<ResponseSlider>(binding.root.context, false) {
            override fun onSuccess(response: ResponseSlider) {
                banner2List.postValue(response)
            }
        })

    }

    // 推薦教練
    private var teacherDataList: MutableLiveData<ResponseCoach> = MutableLiveData()
    private var teacherAdapter: HomeTeacherAdapter = HomeTeacherAdapter(navController, mainModel, mutableListOf())

    fun getTeacherAdapter(): HomeTeacherAdapter {
        return teacherAdapter
    }

    fun setTeacherAdapterData(data: MutableList<HomeCoach>) {
        teacherAdapter.data = data
        teacherAdapter.notifyDataSetChanged()
        teacherAdapter.setOnItemClickListener { _, _, position ->
            val action = MainFragmentDirections.actionMainFragmentToCoachFragment()
            action.title = data[position].name
            action.id = data[position].id

            if (mainModel.appState.accessToken.get().isNullOrEmpty()) {
                action.isFollowed = false
                navController.navigate(action)
            } else {
                action.isFollowed = data[position].isFollowed
                navController.navigate(action)
            }
        }
    }

    fun getTeacherDataObserver(): MutableLiveData<ResponseCoach> {
        return teacherDataList
    }

    fun getTeacherApiData() {
        val token = mainModel.appState.accessToken.get()!!
        HomeRecommendApiClient.coach(token, object : ApiController<ResponseCoach>(binding.root.context, false) {
            override fun onSuccess(response: ResponseCoach) {
                teacherDataList.postValue(response)
            }

            override fun onFail(httpResponse: Response<ResponseCoach>): Boolean {
                hotCourseDataList.postValue(null)
                return super.onFail(httpResponse)
            }
        })
    }

}

class HomeRecommendModelFactory(
    private val binding: FragmentHomeRecommendBinding,
    private val navController: NavController,
    private val mainModel: MainViewModel,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeRecommendViewModel(binding, navController, mainModel) as T
    }

}