package com.gcreate.jiajia.api.search.request

data class RequestSearchCourseBody(
    val keyword: String,
    val live_course: Boolean?,
    val date: String,
    val level: List<Int>,
    val instructor: List<Int>,
    val accessory: List<Int>,
    val limit: Int,
    val page: Int
)