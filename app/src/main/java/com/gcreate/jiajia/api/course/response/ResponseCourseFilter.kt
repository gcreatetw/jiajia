package com.gcreate.jiajia.api.course.response

import com.gcreate.jiajia.data.ItemName

data class ResponseCourseFilter (
    val level: List<ItemName>,
    val instructor: List<ItemName>,
    val accessories: List<ItemName>
)