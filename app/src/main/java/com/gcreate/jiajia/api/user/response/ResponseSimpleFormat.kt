package com.gcreate.jiajia.api.user.response

data class ResponseSimpleFormat(
    val error: Boolean,
    val error_message: String,
    val message: String?,
    val have_user: Boolean,
)