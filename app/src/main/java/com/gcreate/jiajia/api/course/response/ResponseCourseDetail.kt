package com.gcreate.jiajia.api.course.response

import com.gcreate.jiajia.api.dataobj.*
import com.gcreate.jiajia.data.HomeCoach
import com.gcreate.jiajia.data.ItemName

data class ResponseCourseDetail(
    val course: Course,
    val course_detail: CourseDetail,
    val instructor: HomeCoach,
    val commodity: List<Commodity>,
    val accessories: List<ItemName>,
    val bundle: List<Bundle>,       // 相關訓練
    val featured: List<Course>,    // 推薦課程
    val vimeo_token:String,
)
