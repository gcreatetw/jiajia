package com.gcreate.jiajia.api.user.response

data class ResponseDonateMechanism(
    val data: List<DonateMechanismItem>
)
data class DonateMechanismItem(
    val code: Int,
    val name: String
)