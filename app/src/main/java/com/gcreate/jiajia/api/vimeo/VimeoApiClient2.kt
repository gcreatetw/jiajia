package com.gcreate.jiajia.api.vimeo

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.vimeo.response.SingleVideoResponse

object VimeoApiClient2 {

    fun getVideoData(token: String, vimeoId: String, controller: ApiController<SingleVideoResponse>) {

        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.vimeo.getVideoData(headers, vimeoId))
    }

}