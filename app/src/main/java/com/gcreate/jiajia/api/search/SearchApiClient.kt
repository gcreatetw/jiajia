package com.gcreate.jiajia.api.search

import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.ApiConstants
import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.search.request.RequestSearchCourseBody
import com.gcreate.jiajia.api.search.response.ResponseSearchWidget

object SearchApiClient {

    /** 搜尋工具 */
    fun searchWidget(token: String, controller: ApiController<ResponseSearchWidget>) {
        val headers = ApiClient.header(false)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.search.searchWidget(headers, ApiConstants.SERVER_PATH))
    }

    /** 搜尋工具 */
    fun searchCourse(token: String, body: RequestSearchCourseBody, controller: ApiController<ResponseCourses>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.search.searchCourse(headers, ApiConstants.SERVER_PATH, body))
    }

    /** 搜尋訓練課程 */
    fun searchBundle(token: String, body: RequestSearchCourseBody, controller: ApiController<ResponseBundleCourses>) {
        val headers = ApiClient.header(true)
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = "Bearer $token"
        controller.enqueue(ApiClient.search.searchBundle(headers, ApiConstants.SERVER_PATH, body))
    }
}