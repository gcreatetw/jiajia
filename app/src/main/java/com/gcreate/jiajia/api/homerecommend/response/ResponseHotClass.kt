package com.gcreate.jiajia.api.homerecommend.response

import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.data.BeingClass

data class ResponseHotClass(
    val res : List<BeingClass>
){
    fun completImageUrl(){
        for(item in res){
            item.preview_image = ApiUtil.completImageUrl(item.preview_image)
            item.user_img = ApiUtil.completImageUrl(item.user_img)
        }
    }
}