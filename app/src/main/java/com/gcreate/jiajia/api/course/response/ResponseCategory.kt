package com.gcreate.jiajia.api.course.response

import com.gcreate.jiajia.data.ClassCategory

data class ResponseCategory(
    val category: MutableList<ClassCategory>
)
