package com.gcreate.jiajia.api.vimeo.response

data class RootUser(
    val uri: String,
    val name: String,
    val link: String,
    val capabilities: RootUserCapabilities,
    val location: String,
    val gender: String,
    val bio: Any,
    val short_bio: Any,
    val created_time: String,
    val pictures: RootUserPictures,
    val websites: List<Any>,
    val metadata: RootUserMetadata,
    val location_details: LocationDetails,
    val skills: List<Any>,
    val available_for_hire: Boolean,
    val can_work_remotely: Boolean,
    val preferences: RootPreferences,
    val content_filter: List<String>,
    val resource_key: String,
    val account: String,
)

data class RootUserCapabilities(
    val hasLiveSubscription: Boolean,
    val hasEnterpriseLihp: Boolean,
    val hasSvvTimecodedComments: Boolean,
)

data class RootUserPictures(
    val uri: Any,
    val active: Boolean,
    val type: String,
    val base_link: String,
    val sizes: List<RootUserSize>,
    val resource_key: String,
    val default_picture: Boolean,
)

data class RootUserSize(
    val width: Int,
    val height: Int,
    val link: String,
)


