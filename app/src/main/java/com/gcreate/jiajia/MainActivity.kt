package com.gcreate.jiajia

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseAffSetting
import com.gcreate.jiajia.api.user.response.ResponseVersion
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory


val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "appState")

class MainActivity : AppCompatActivity() {

    companion object {
        var width: Int = 0
        var height = 0

    }

    private val model: MainViewModel by viewModels {
        MainViewModelFactory(dataStore)
    }

    lateinit var rootNavController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intentValue = intent.getStringExtra("messageTitle")
        if (!intentValue.isNullOrEmpty()) {
            model.receiveNotificationTitle = intent.getStringExtra("messageTitle").toString()
        }

        //取得螢幕寬度
        val metric = DisplayMetrics()
        windowManager?.defaultDisplay?.getMetrics(metric)
        width = metric.widthPixels
        height = metric.heightPixels

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        rootNavController = navHostFragment.navController

        UserApiClient.getAffSetting(object : ApiController<ResponseAffSetting>(this@MainActivity, false) {
            override fun onSuccess(response: ResponseAffSetting) {
                model.affSetting = response
            }
        })
    }

    override fun onBackPressed() {
        when (rootNavController.currentDestination!!.id) {
            // 以下畫面按下物理返回鍵，無動作
            R.id.signupOtpFragment -> {}
//            R.id.signupOtpCodeInputFragment -> {
//                val action = SignupOtpCodeInputFragmentDirections.actionSignupOtpCodeInputFragmentToSignupOtpPhoneNumberFragment()
//                rootNavController.navigate(action)
//            }
            R.id.setPersonalizeFragment -> {}
            R.id.paymentFrgment -> {}
            R.id.memberSignupSetPasswordFragment -> {}
            R.id.loginCheckAccountFragment -> {}

            else -> {
                super.onBackPressed()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        UserApiClient.appVersion(object : ApiController<ResponseVersion>(this@MainActivity, false) {
            override fun onSuccess(response: ResponseVersion) {
                if (!response.android.contains(packageManager.getPackageInfo(packageName, 0).versionName)) {
                    val builder = AlertDialog.Builder(this@MainActivity)
                    builder.apply {
                        setMessage("已釋出新版本，請至商店更新。")
                        setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                            val appPackageName = this@MainActivity.packageName
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                        }
                        setCancelable(false)
                    }

                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.white))
                }
            }
        })
    }
}