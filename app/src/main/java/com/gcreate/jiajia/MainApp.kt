package com.gcreate.jiajia

import android.app.Application
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.api.ApiClient
import com.gcreate.jiajia.api.handle.ApiErrorManager
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        apiErrorManager = ApiErrorManager(applicationContext)
        ApiClient.initApi(applicationContext)
    }

    companion object {
        lateinit var apiErrorManager: ApiErrorManager

        fun removeLoginInfo() {
        }

//        var loginToken: String? = ""
//            get() = myPref!!.getString("login_token", null)
//            set(loginToken) {
//                val editor = myPref!!.edit()
//                editor.putString("login_token", loginToken)
//                editor.apply()
//
//            }
    }
}