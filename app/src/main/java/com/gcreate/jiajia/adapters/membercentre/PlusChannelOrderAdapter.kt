package com.gcreate.jiajia.adapters.membercentre

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.payment.response.ResponseProcessToCheckout.PlusChannel
import com.gcreate.jiajia.databinding.RvItemPlusOrderBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder


class PlusChannelOrderAdapter(data: MutableList<PlusChannel>?) :
    BaseQuickAdapter<PlusChannel, DataBindBaseViewHolder>(R.layout.rv_item_plus_order, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: PlusChannel) {
        val binding: RvItemPlusOrderBinding = holder.getViewBinding() as RvItemPlusOrderBinding
        binding.recyclerData = item
        binding.executePendingBindings()
    }

}