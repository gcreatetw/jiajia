package com.gcreate.jiajia.adapters.home

import android.util.Log
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.HomeCoach
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.databinding.CardHomeRecommendTeacherBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.google.android.material.button.MaterialButton

class HomeTeacherAdapter(private val navController: NavController, private val model: MainViewModel, data: MutableList<HomeCoach>) :
    BaseQuickAdapter<HomeCoach, DataBindBaseViewHolder>(R.layout.card_home_recommend_teacher, data) {

    private val subscriptionList = mutableListOf<Int>()

    override fun convert(holder: DataBindBaseViewHolder, item: HomeCoach) {
        val binding: CardHomeRecommendTeacherBinding = holder.getViewBinding() as CardHomeRecommendTeacherBinding

        if (item.isFollowed) {
            subscriptionList.add(holder.layoutPosition)
            //有訂閱教練
            holder.itemView.findViewById<MaterialButton>(R.id.btn_subscription).apply {
                backgroundTintList = ContextCompat.getColorStateList(binding.root.context, R.color.green)
                setTextColor(ContextCompat.getColor(binding.root.context, R.color.white))
                iconTint = ContextCompat.getColorStateList(binding.root.context, R.color.white)
            }
        } else {
            //沒訂閱教練
            holder.itemView.findViewById<MaterialButton>(R.id.btn_subscription).apply {
                backgroundTintList = ContextCompat.getColorStateList(binding.root.context, R.color.green_alpha_28)
                setTextColor(ContextCompat.getColor(binding.root.context, R.color.green))
                iconTint = ContextCompat.getColorStateList(binding.root.context, R.color.green)
            }
        }

        binding.data = item
        binding.executePendingBindings()

        holder.itemView.findViewById<MaterialButton>(R.id.btn_subscription).setOnClickListener {
            if (model.appState.accessToken.get().isNullOrEmpty()) {
                val action = MainFragmentDirections.actionMainFragmentToLoginAllinOneFragment()
                navController.navigate(action)
            } else {
                if (item.isFollowed) {
                    //有訂閱教練，點了取消訂閱
                    holder.itemView.findViewById<MaterialButton>(R.id.btn_subscription).apply {
                        backgroundTintList = ContextCompat.getColorStateList(binding.root.context, R.color.green_alpha_28)
                        setTextColor(ContextCompat.getColor(binding.root.context, R.color.green))
                        iconTint = ContextCompat.getColorStateList(binding.root.context, R.color.green)
                        subscriptionList.remove(holder.layoutPosition)
                        text = "訂閱教練"
                    }
                } else {
                    holder.itemView.findViewById<MaterialButton>(R.id.btn_subscription).apply {
                        backgroundTintList = ContextCompat.getColorStateList(binding.root.context, R.color.green)
                        setTextColor(ContextCompat.getColor(binding.root.context, R.color.white))
                        iconTint = ContextCompat.getColorStateList(binding.root.context, R.color.white)
                        text = "已訂閱   "
                    }
                }
                item.isFollowed = !item.isFollowed
                switchFollowState(item.id)
            }
        }

    }

    private fun switchFollowState(coachId: Int) {
        val request = RequestFollowCategory(coachId.toString(), "", "","")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(context, false) {
            override fun onSuccess(response: Unit) {
                model.loadFollowState(context)
            }
        })
    }
}

