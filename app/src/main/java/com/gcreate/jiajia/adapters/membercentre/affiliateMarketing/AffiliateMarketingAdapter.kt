package com.gcreate.jiajia.adapters.membercentre.affiliateMarketing

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.api.user.response.ReferralListData
import com.gcreate.jiajia.databinding.RvItemAffilateMarkingBinding
import com.gcreate.jiajia.util.AES256Util

class AffiliateMarketingAdapter(dataList: MutableList<ReferralListData>) :
    BaseQuickAdapter<ReferralListData, DataBindBaseViewHolder>(R.layout.rv_item_affilate_marking, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: ReferralListData) {
        val binding: RvItemAffilateMarkingBinding = holder.getViewBinding() as RvItemAffilateMarkingBinding
        binding.itemData = item
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.user_img)
            .error(R.drawable.baseline_account_circle_24)
            .into(binding.imgMemberAvatar)

        binding.apply{
            tvName.text = AES256Util.AES256Decrypt(context.getString(R.string.AESkey), context.getString(R.string.AESIVkey), item.name)
            tvCreateDate.text = "加入時間： ${item.register_datetime.split("T")[0]}"
            tvSubscriptionDate.text = "訂閱時間： ${item.subscription_datetime.split("T")[0]}"
        }


    }

}
