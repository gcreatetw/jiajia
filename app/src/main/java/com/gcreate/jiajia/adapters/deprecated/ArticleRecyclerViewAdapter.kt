package com.gcreate.jiajia.adapters.deprecated

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.ArticleImg
import com.gcreate.jiajia.data.ArticleItem
import com.gcreate.jiajia.data.ArticleText
import com.gcreate.jiajia.data.ArticleTitle

class ArticleRecyclerViewAdapter(
    private val dataList : MutableList<ArticleItem>, val context: Context, val width : Int?, val activity: Activity)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    open class ViewHolder(ItemView : View) : RecyclerView.ViewHolder(ItemView){
        val logo = ItemView.findViewById<TextView>(R.id.card_logo)
        val copyright = ItemView.findViewById<TextView>(R.id.card_copyright)
    }

    inner class ViewHolderTitle(ItemView : View) : ViewHolder(ItemView){
        val imgTitle = ItemView.findViewById<ImageView>(R.id.img_title)
        val tvTitle: TextView = ItemView.findViewById<TextView>(R.id.tv_title)
    }

    inner class ViewHolderImg(ItemView : View) : ViewHolder(ItemView){
        val img = ItemView.findViewById<ImageView>(R.id.img)
    }

    inner class ViewHolderText(ItemView : View) : ViewHolder(ItemView){
        val tvSubTitle: TextView = ItemView.findViewById<TextView>(R.id.tv_sub_title)
        val tvText: TextView = ItemView.findViewById<TextView>(R.id.tv_text)
    }

    override fun getItemViewType(position: Int): Int {
        when(dataList[position]){
            is ArticleTitle -> return 0
            is ArticleImg -> return 1
            is ArticleText -> return 2
        }
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType){
            0 ->{
                val view = LayoutInflater.from(parent.context).inflate(R.layout.card_article_title,parent,false)
                return ViewHolderTitle(view)
            }
            1 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.card_article_img,parent,false)
                return ViewHolderImg(view)
            }
            2 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.card_article_text,parent,false)
                return ViewHolderText(view)
            }
        }
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_article_title,parent,false)
        return ViewHolderTitle(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(dataList[position]){
            is ArticleTitle -> {
                val data = dataList[position] as ArticleTitle
                (holder as ViewHolderTitle).tvTitle.text = data.title
            }
            is ArticleImg -> {
                val data = dataList[position] as ArticleImg

            }
            is ArticleText -> {
                val data = dataList[position] as ArticleText
                (holder as ViewHolderText).tvSubTitle.text = data.subTitle
                (holder as ViewHolderText).tvText.text = data.text
            }
        }
        if(position < dataList.size - 1){
            (holder as ViewHolder).logo.visibility = View.GONE
            holder.copyright.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}