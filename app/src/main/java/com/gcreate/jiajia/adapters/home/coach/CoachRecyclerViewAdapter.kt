package com.gcreate.jiajia.adapters.home.coach

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.databinding.*
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.initClassCategoryChipGroup
import kotlin.math.roundToInt

class CoachRecyclerViewAdapter(
    val context: Context,
    val inflater: LayoutInflater,
    private val onClickNewLesson: (Int) ->Unit,
    private val onClickLesson: (Int) -> Unit,
    private val onClickChip: (checkedId: Int) -> Unit)
    : ListAdapter<CoachItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<CoachItem>() {

        override fun areItemsTheSame(oldItem: CoachItem, newItem: CoachItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: CoachItem, newItem: CoachItem): Boolean {
            return false
        }
    }

    inner class ViewHolderTitle(private val binding: CardCoach1TitleBinding) : RecyclerView.ViewHolder(binding.root){
        fun setResult(dataItem: CoachTitle) {
            binding.title = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderNewLessonList(private val binding: CardCoach2NewLessonListBinding) : RecyclerView.ViewHolder(binding.root){
        fun setResult(dataItem: CoachNewLessonList) {
            binding.newLessonList = dataItem

            val layoutManager = LinearLayoutManager(context)
            val rv = binding.recyclerView
            layoutManager.orientation = LinearLayoutManager.HORIZONTAL
            rv?.layoutManager = layoutManager
            rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, context).roundToInt()))
            rv?.adapter = CoachNewLessonRecyclerViewAdapter(){
                onClickNewLesson(it)
            }

            binding.executePendingBindings()
        }
    }

    inner class ViewHolderLessonTitle(private val binding: CardCoach3LessonTitleBinding) : RecyclerView.ViewHolder(binding.root){
        fun setResult(dataItem: CoachLessonTitle) {
            binding.lessonTitle = dataItem
            binding.executePendingBindings()

            var chipGroup = binding?.chipGroup
            initClassCategoryChipGroup(inflater, chipGroup!!, dataItem.categoryList)
            chipGroup.check(dataItem.categoryId)
            chipGroup?.setOnCheckedChangeListener { group, checkedId ->
                chipGroup?.setOnCheckedChangeListener(null)
                onClickChip(checkedId)
            }
        }
    }

    inner class ViewHolderLesson(private val binding: CardLessonBinding) : RecyclerView.ViewHolder(binding.root){

        init {
            binding.root.setOnClickListener{
                onClickLesson(adapterPosition)
            }

        }
        fun setResult(dataItem: Course) {
            binding.lesson = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardCoach5TailBinding) : RecyclerView.ViewHolder(binding.root) {}

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is CoachTitle -> return 0
            is CoachNewLessonList -> return 1
            is CoachLessonTitle -> return 2
            is Course -> return 3
            is CoachTail -> return 4
        }
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType) {
            0 -> {
                val binding = CardCoach1TitleBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTitle(binding)
            }
            1 -> {
                val binding = CardCoach2NewLessonListBinding.inflate(layoutInflater, parent, false)
                return ViewHolderNewLessonList(binding)
            }
            2 -> {
                val binding = CardCoach3LessonTitleBinding.inflate(layoutInflater, parent, false)
                return ViewHolderLessonTitle(binding)
            }
            3 -> {
                val binding = CardLessonBinding.inflate(layoutInflater, parent, false)
                return ViewHolderLesson(binding)
            }
            4 -> {
                val binding = CardCoach5TailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }
        val binding = CardCoach5TailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolderTitle -> {
                holder.setResult(getItem(position) as CoachTitle)
            }
            is ViewHolderNewLessonList -> {
                holder.setResult(getItem(position) as CoachNewLessonList)
            }
            is ViewHolderLessonTitle -> {
                holder.setResult(getItem(position) as CoachLessonTitle)
            }
            is ViewHolderLesson -> {
                holder.setResult(getItem(position) as Course)
            }
            is ViewHolderTail -> {

            }
        }
    }

}
