package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.UserScheduleDaily
import com.gcreate.jiajia.data.UserScheduleDailyItem
import com.gcreate.jiajia.data.UserScheduleDailyTail

import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardUserScheduleDailyBinding
import com.gcreate.jiajia.views.user.schedule.UserScheduleDailyByDateFragment.Companion.itemClickType

class UserScheduleDailyRecyclerViewAdapter (
    private val onClick: (Int) ->Unit
) : ListAdapter<UserScheduleDailyItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<UserScheduleDailyItem>() {

        override fun areItemsTheSame(oldItem: UserScheduleDailyItem, newItem: UserScheduleDailyItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: UserScheduleDailyItem, newItem: UserScheduleDailyItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardUserScheduleDailyBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imgRemove.setOnClickListener {
                itemClickType = "remove"
                onClick(adapterPosition)
            }

            binding.root.setOnClickListener {
                itemClickType = "item"
                onClick(adapterPosition)
            }

        }

        fun setResult(dataItem: UserScheduleDaily) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is UserScheduleDaily -> return 0
            is UserScheduleDailyTail -> return 1
        }
        return 1
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType){
            0 ->{
                val binding = CardUserScheduleDailyBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 ->{
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as UserScheduleDaily)

            }
            is ViewHolderTail -> {

            }
        }
    }
}