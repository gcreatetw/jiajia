package com.gcreate.jiajia.adapters.home.notification

import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.user.response.Notification
import com.gcreate.jiajia.databinding.RvItemNotificationBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder


class NotificationsAdapter : BaseQuickAdapter<Notification, DataBindBaseViewHolder>(R.layout.rv_item_notification) {

    override fun convert(holder: DataBindBaseViewHolder, item: Notification) {
        val binding: RvItemNotificationBinding = holder.getViewBinding() as RvItemNotificationBinding
        binding.recyclerData = item
        binding.executePendingBindings()

        /**
         * 訂閱分類       type category
         * 訂閱教練       type instructor
         * 行事曆         type calendar
         * */
        when (item.type) {
            "category" -> {
                Glide.with(holder.itemView.context)
                    .load(R.drawable.baseline_add_alert_24)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }

            "instructor" -> {
                Glide.with(holder.itemView.context)
                    .load(item.user_image)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }

            "calendar"-> {
                Glide.with(holder.itemView.context)
                    .load(R.drawable.baseline_add_alert_24)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }

            "familygroup"-> {
                Glide.with(holder.itemView.context)
                    .load(R.drawable.baseline_add_alert_24)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }

            "expired"-> {
                Glide.with(holder.itemView.context)
                    .load(R.drawable.baseline_add_alert_24)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }

            "family_group_expired"-> {
                Glide.with(holder.itemView.context)
                    .load(R.drawable.baseline_add_alert_24)
                    .transform(CenterCrop(), RoundedCorners(24))
                    .thumbnail(0.1f).into(holder.getView(R.id.notification_img))
            }
        }

    }
}