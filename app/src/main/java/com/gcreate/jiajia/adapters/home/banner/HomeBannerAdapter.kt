package com.gcreate.jiajia.adapters.home.banner

import android.widget.ImageView
import android.widget.TextView
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.HomeBanner
import com.gcreate.jiajia.util.loadImgByGlide
import com.zhpan.bannerview.BaseBannerAdapter
import com.zhpan.bannerview.BaseViewHolder

class HomeBannerAdapter(val onClickListener: (position: Int) -> Unit) : BaseBannerAdapter<HomeBanner>() {

    override fun bindData(holder: BaseViewHolder<HomeBanner>?, data: HomeBanner, position: Int, pageSize: Int) {

        //載入banner資料
        val imageBanner = holder?.findViewById<ImageView>(R.id.home_banner_imageView)

        imageBanner?.apply {
            this.maxWidth = MainActivity.width
            this.maxHeight = MainActivity.width
            this.minimumWidth = MainActivity.width
            this.minimumHeight = MainActivity.width

            loadImgByGlide(this, data.image)
            setOnClickListener {
                onClickListener(position)
            }
        }
        val title = holder?.findViewById<TextView>(R.id.home_banner_title)
        title?.text = data.heading
        val content = holder?.findViewById<TextView>(R.id.home_banner_content)
        content?.text = data.sub_heading
    }

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.banner_home_recommend
    }
}