package com.gcreate.jiajia.adapters.membercentre

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.user.response.UserCouponRecordData
import com.gcreate.jiajia.databinding.RvItemCouponRecordBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder


class UserCouponRecordAdapter(dataList: MutableList<UserCouponRecordData>) :
    BaseQuickAdapter<UserCouponRecordData, DataBindBaseViewHolder>(R.layout.rv_item_coupon_record, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: UserCouponRecordData) {
        val binding: RvItemCouponRecordBinding = holder.getViewBinding() as RvItemCouponRecordBinding
        binding.item = item
        binding.executePendingBindings()
    }

}
