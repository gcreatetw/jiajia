package com.gcreate.jiajia.adapters.home

import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.databinding.CardClassBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class HomeHotCourseAdapter(data: MutableList<Course>) :
    BaseQuickAdapter<Course, DataBindBaseViewHolder>(R.layout.card_class, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: Course) {
        val binding: CardClassBinding = holder.getViewBinding() as CardClassBinding
        binding.data = item
        binding.executePendingBindings()

        if (item.live_time.equals("")) {
            binding.iconVedio.visibility = View.GONE
        } else {
            binding.iconVedio.visibility = View.VISIBLE
        }
    }

}

