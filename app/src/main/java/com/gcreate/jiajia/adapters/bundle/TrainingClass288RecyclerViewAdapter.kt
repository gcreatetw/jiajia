package com.gcreate.jiajia.adapters.bundle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.Bundle
import com.gcreate.jiajia.data.TrainingClassItem
import com.gcreate.jiajia.data.TrainingClassTail
import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardTrainingClass288Binding

class TrainingClass288RecyclerViewAdapter(
    private val onClickItem: (Int) -> Unit,
) : ListAdapter<TrainingClassItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<TrainingClassItem>() {

        override fun areItemsTheSame(oldItem: TrainingClassItem, newItem: TrainingClassItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: TrainingClassItem, newItem: TrainingClassItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardTrainingClass288Binding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickItem(adapterPosition)
            }
        }

        fun setResult(dataItem: Bundle) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root)


    override fun getItemViewType(position: Int): Int {
        when (getItem(position)) {
            is Bundle -> return 0
            is TrainingClassTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardTrainingClass288Binding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 -> {
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as Bundle)

            }
            is ViewHolderTail -> {

            }
        }
    }


}