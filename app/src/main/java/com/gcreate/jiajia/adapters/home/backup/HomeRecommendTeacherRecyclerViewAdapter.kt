package com.gcreate.jiajia.adapters.home.backup

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.HomeCoach
import com.gcreate.jiajia.databinding.CardHomeRecommendTeacherBinding
import com.gcreate.jiajia.views.home.HomeRecommendFragment.Companion.coachClickType

class HomeRecommendTeacherRecyclerViewAdapter(
    private val onClickBtn: (position: Int) -> Unit,
) : ListAdapter<HomeCoach, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<HomeCoach>() {

        override fun areItemsTheSame(oldItem: HomeCoach, newItem: HomeCoach): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: HomeCoach, newItem: HomeCoach): Boolean {
            return oldItem.name == newItem.name
        }
    }

    inner class ViewHolder(private val binding: CardHomeRecommendTeacherBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.btnSubscription.setOnClickListener {
                coachClickType = "button"
                onClickBtn(adapterPosition)
            }

            binding.cardHomeRecommendTeacherLayout.setOnClickListener {
                coachClickType = "itemView"
                onClickBtn(adapterPosition)
            }
        }

        fun setResult(dataItem: HomeCoach) {

            if (dataItem.isFollowed) {
                binding.btnSubscription.setIconResource(R.drawable.ic_bookmark)
            } else {
                binding.btnSubscription.setIconResource(R.drawable.outline_bookmark_border_24)
            }

            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardHomeRecommendTeacherBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as HomeCoach)
            }
        }
    }

}

