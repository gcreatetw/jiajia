package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.user.follow.*

class UserFollowTabAdapter(private val count:Int, baseFragment: UserFollowFragment) : FragmentStateAdapter(baseFragment) {
    //tab數量
    override fun getItemCount(): Int {

        return count
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UserFollowByTypeFragment()
            1 -> UserFollowByCoachFragment()
            2 -> UserFollowByBundleCategoryFragment()
            else -> UserFollowByAddedValueFragment()
        }
    }
}