package com.gcreate.jiajia.adapters.course

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.databinding.CardClassBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class VideoCoursByTypeAdapter(dataList: MutableList<Course>) : BaseQuickAdapter<Course, DataBindBaseViewHolder>(R.layout.card_class, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: Course) {
        val binding: CardClassBinding = holder.getViewBinding() as CardClassBinding
        binding.data = item
        binding.executePendingBindings()
    }

}
