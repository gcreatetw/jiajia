package com.gcreate.jiajia.adapters.home.coach


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.Lesson
import com.gcreate.jiajia.databinding.CardLessonBigBinding

class CoachNewLessonRecyclerViewAdapter(
    private val onClickNewLesson: (Int) -> Unit,
) : ListAdapter<Lesson, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Lesson>() {

        override fun areItemsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
            return oldItem === newItem
        }


        override fun areContentsTheSame(oldItem: Lesson, newItem: Lesson): Boolean {
            return oldItem.name == newItem.name
        }
    }

    inner class PlayerHolder(private val binding: CardLessonBigBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickNewLesson(adapterPosition)
            }
        }

        fun setResult(dataItem: Lesson) {
            binding.lesson = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardLessonBigBinding.inflate(layoutInflater, parent, false)
        return PlayerHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is PlayerHolder -> {
                holder.setResult(getItem(position) as Lesson)

            }
        }
    }

}