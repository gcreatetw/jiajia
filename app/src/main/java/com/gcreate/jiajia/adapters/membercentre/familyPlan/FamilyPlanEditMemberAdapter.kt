package com.gcreate.jiajia.adapters.membercentre.familyPlan

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.user.response.FamilyListData

import com.gcreate.jiajia.databinding.RvItemEditFamilyMemberBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.util.AES256Util


class FamilyPlanEditMemberAdapter(dataList: MutableList<FamilyListData>) :
    BaseQuickAdapter<FamilyListData, DataBindBaseViewHolder>(R.layout.rv_item_edit_family_member, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: FamilyListData) {
        val binding: RvItemEditFamilyMemberBinding = holder.getViewBinding() as RvItemEditFamilyMemberBinding
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.user_image)
            .error(R.drawable.baseline_account_circle_24)
            .into(binding.imgMemberAvatar)

        binding.tvName.text = AES256Util.AES256Decrypt(context.getString(R.string.AESkey),context.getString(R.string.AESIVkey),item.name)
    }

}
