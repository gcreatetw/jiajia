package com.gcreate.jiajia.adapters.home.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.api.dataobj.Bundle
import com.gcreate.jiajia.databinding.CardTrainingClassBinding

class SearchBundleAdapter(dataList: MutableList<Bundle>) :
    BaseQuickAdapter<Bundle, DataBindBaseViewHolder>(R.layout.card_training_class, dataList), LoadMoreModule {

    override fun convert(holder: DataBindBaseViewHolder, item: Bundle) {
        val binding: CardTrainingClassBinding = holder.getViewBinding() as CardTrainingClassBinding
        binding.data = item
        binding.executePendingBindings()
    }

}