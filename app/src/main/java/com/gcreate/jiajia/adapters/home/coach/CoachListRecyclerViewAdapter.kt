package com.gcreate.jiajia.adapters.home.coach

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.HomeCoach
import com.gcreate.jiajia.databinding.CardCoachListBinding

class CoachListRecyclerViewAdapter(
    private val onItemBtnClicked: (position: Int) -> Unit,
) : ListAdapter<HomeCoach, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<HomeCoach>() {

        override fun areItemsTheSame(oldItem: HomeCoach, newItem: HomeCoach): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: HomeCoach, newItem: HomeCoach): Boolean {
            return oldItem.name == newItem.name
        }
    }

    inner class PlayerHolder(private val binding: CardCoachListBinding) : RecyclerView.ViewHolder(binding.root) {

        val logo = binding.cardLogo
        val copyright = binding.cardCopyright

        init {
            binding.itemView.setOnClickListener {
                onItemBtnClicked(adapterPosition)
            }
        }

        fun setResult(dataItem: HomeCoach) {
            binding.coach = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardCoachListBinding.inflate(layoutInflater, parent, false)
        return PlayerHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is PlayerHolder -> {
                holder.setResult(getItem(position) as HomeCoach)
                if (position < itemCount - 1) {
                    holder.logo.visibility = View.GONE
                    holder.copyright.visibility = View.GONE
                }
            }
        }
    }

}