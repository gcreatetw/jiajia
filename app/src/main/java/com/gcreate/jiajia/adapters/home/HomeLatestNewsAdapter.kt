package com.gcreate.jiajia.adapters.home

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.HomeNews
import com.gcreate.jiajia.databinding.CardNews288Binding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class HomeLatestNewsAdapter(data: MutableList<HomeNews>) : BaseQuickAdapter<HomeNews, DataBindBaseViewHolder>(R.layout.card_news_288, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: HomeNews) {
        val binding: CardNews288Binding = holder.getViewBinding() as CardNews288Binding
        binding.data = item
        binding.executePendingBindings()
    }

}


