package com.gcreate.jiajia.adapters.home

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.databinding.CardLessonBinding

class HomeCourseRecommendAdapter(dataList: MutableList<Course>) :
    BaseQuickAdapter<Course, DataBindBaseViewHolder>(R.layout.card_lesson, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: Course) {
        val binding: CardLessonBinding = holder.getViewBinding() as CardLessonBinding
        binding.lesson = item
        binding.executePendingBindings()
    }
}

