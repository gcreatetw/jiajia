package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.home.HomeCourseRecommendFragment
import com.gcreate.jiajia.views.home.HomeFragment
import com.gcreate.jiajia.views.home.HomeRecommendFragment
import com.gcreate.jiajia.views.home.HomeValueFragment

//tabLayout的adapter
class HomeTabAdapter(homeFragment: HomeFragment, private val isShowBonusChannel: Boolean) : FragmentStateAdapter(homeFragment) {
    //tab數量
    override fun getItemCount(): Int {
        return 2
//        if (isShowBonusChannel) {
//            2
//        } else {
//            1
//        }

    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> HomeRecommendFragment()
            else -> {
                if (isShowBonusChannel) {
                    HomeValueFragment()
                } else {
                    HomeCourseRecommendFragment()
                }
            }
        }
    }
}