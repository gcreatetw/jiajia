package com.gcreate.jiajia.adapters.home.notification

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.api.user.response.FirebaseNotification
import com.gcreate.jiajia.databinding.RvItemFirebaseNotificationBinding


class FirebaseNotificationsAdapter : BaseQuickAdapter<FirebaseNotification, DataBindBaseViewHolder>(R.layout.rv_item_firebase_notification) {

    override fun convert(holder: DataBindBaseViewHolder, item: FirebaseNotification) {
        val binding: RvItemFirebaseNotificationBinding = holder.getViewBinding() as RvItemFirebaseNotificationBinding
        binding.recyclerData = item
        binding.executePendingBindings()
    }
}