package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.articlelist.ArticleListActivityFragment
import com.gcreate.jiajia.views.articlelist.ArticleListKnowledgeFragment

class ArticleListTabAdapter(activity : FragmentActivity) : FragmentStateAdapter(activity) {
    //tab數量
    override fun getItemCount(): Int {
        return 2
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ArticleListActivityFragment()      // 活動檔期
            else -> ArticleListKnowledgeFragment()  // 運動知識
        }
    }
}