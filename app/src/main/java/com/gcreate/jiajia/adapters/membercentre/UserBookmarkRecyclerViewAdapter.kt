package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.databinding.CardLessonBinding
import com.gcreate.jiajia.databinding.CardTailBinding

class UserBookmarkRecyclerViewAdapter(
    private val onClickLesson: (Int) ->Unit
) : ListAdapter<BeingRecyclerViewItemData, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<BeingRecyclerViewItemData>() {

        override fun areItemsTheSame(oldItem: BeingRecyclerViewItemData, newItem: BeingRecyclerViewItemData): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: BeingRecyclerViewItemData, newItem: BeingRecyclerViewItemData): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardLessonBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickLesson(adapterPosition)
            }
        }

        fun setResult(dataItem: Course) {
            binding.lesson = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is Course -> return 0
            is BeingRecyclerViewItemDataTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType){
            0 ->{
                val binding = CardLessonBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 ->{
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as Course)

            }
            is ViewHolderTail -> {

            }
        }
    }


}