package com.gcreate.jiajia.adapters.membercentre

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.Lesson
import com.gcreate.jiajia.databinding.CardLessonBigBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class UserLessonRecyclerViewAdapter(dataList: MutableList<Lesson>) :
    BaseQuickAdapter<Lesson, DataBindBaseViewHolder>(R.layout.card_lesson_big, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: Lesson) {
        val binding: CardLessonBigBinding = holder.getViewBinding() as CardLessonBigBinding
        binding.lesson = item
        binding.executePendingBindings()
    }
}

