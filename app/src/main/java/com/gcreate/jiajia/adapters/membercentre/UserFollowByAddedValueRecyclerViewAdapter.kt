package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.UserFollowByAddedValue
import com.gcreate.jiajia.data.UserFollowByAddedValueItem
import com.gcreate.jiajia.data.UserFollowByAddedValueTail
import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardUserFollowByAddedValueBinding

class UserFollowByAddedValueRecyclerViewAdapter (
    private val onClickFollow: (Int) ->Unit
) : ListAdapter<UserFollowByAddedValueItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<UserFollowByAddedValueItem>() {

        override fun areItemsTheSame(oldItem: UserFollowByAddedValueItem, newItem: UserFollowByAddedValueItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: UserFollowByAddedValueItem, newItem: UserFollowByAddedValueItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardUserFollowByAddedValueBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imgFollow.setOnClickListener {
                onClickFollow(adapterPosition)
            }
        }

        fun setResult(dataItem: UserFollowByAddedValue) {
            binding.item = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is UserFollowByAddedValue -> return 0
            is UserFollowByAddedValueTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType){
            0 ->{
                val binding = CardUserFollowByAddedValueBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 ->{
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as UserFollowByAddedValue)

            }
            is ViewHolderTail -> {

            }
        }
    }


}