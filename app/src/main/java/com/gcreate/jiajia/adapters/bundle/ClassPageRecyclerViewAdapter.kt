package com.gcreate.jiajia.adapters.bundle

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.ClassPageItem
import com.gcreate.jiajia.data.ClassPageLesson
import com.gcreate.jiajia.data.ClassPageTail
import com.gcreate.jiajia.data.ClassPageTitle
import com.gcreate.jiajia.databinding.CardClasspageTailBinding
import com.gcreate.jiajia.databinding.CardClasspageTitleBinding
import com.gcreate.jiajia.databinding.CardLesson2Binding


class ClassPageRecyclerViewAdapter(
    private val onClickLesson: (Int) ->Unit
) : ListAdapter<ClassPageItem, RecyclerView.ViewHolder>(
    DiffCallback
) {

    companion object DiffCallback : DiffUtil.ItemCallback<ClassPageItem>() {

        override fun areItemsTheSame(oldItem: ClassPageItem, newItem: ClassPageItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ClassPageItem, newItem: ClassPageItem): Boolean {
            return false
        }
    }

    inner class ViewHolderTitle(private val binding: CardClasspageTitleBinding) : RecyclerView.ViewHolder(binding.root) {

        fun setResult(dataItem: ClassPageTitle) {
            binding.item = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderLesson(private val binding: CardLesson2Binding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickLesson(adapterPosition)
            }
        }

        fun setResult(dataItem: ClassPageLesson) {
            binding.lesson2 = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardClasspageTailBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun getItemViewType(position: Int): Int {
        when (getItem(position)) {
            is ClassPageTitle -> return 0
            is ClassPageLesson -> return 1
            is ClassPageTail -> return 2
        }
        return 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardClasspageTitleBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTitle(binding)
            }
            1 -> {
                val binding = CardLesson2Binding.inflate(layoutInflater, parent, false)
                return ViewHolderLesson(binding)
            }
            2 -> {
                val binding = CardClasspageTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardClasspageTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolderTitle -> {
                holder.setResult(getItem(position) as ClassPageTitle)
            }
            is ViewHolderLesson -> {
                holder.setResult(getItem(position) as ClassPageLesson)

            }

            is ViewHolderTail -> {

            }
        }
    }
}
