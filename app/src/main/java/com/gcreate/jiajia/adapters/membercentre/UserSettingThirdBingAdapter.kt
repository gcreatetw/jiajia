package com.gcreate.jiajia.adapters.membercentre

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.ThirdBing
import com.gcreate.jiajia.databinding.RvItemThirdBingBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class UserSettingThirdBingAdapter(dataList: MutableList<ThirdBing>) :
    BaseQuickAdapter<ThirdBing, DataBindBaseViewHolder>(R.layout.rv_item_third_bing, dataList) {

    override fun convert(holder: DataBindBaseViewHolder, item: ThirdBing) {
        val binding: RvItemThirdBingBinding = holder.getViewBinding() as RvItemThirdBingBinding
        binding.recyclerData = item
        binding.executePendingBindings()

        Glide.with(holder.itemView.context).load(item.iconImg)
            .into(binding.imgMemberAvatar)
    }

}
