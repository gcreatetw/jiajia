package com.gcreate.jiajia.adapters.bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.ChannelCourse
import com.gcreate.jiajia.databinding.CardChannelListBinding

class ChannelListRecyclerViewAdapter(
    private val onClickItem: (Int) -> Unit,
) : ListAdapter<ChannelCourse, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<ChannelCourse>() {

        override fun areItemsTheSame(oldItem: ChannelCourse, newItem: ChannelCourse): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ChannelCourse, newItem: ChannelCourse): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.id == newItem.id
        }
    }

    inner class ViewHolder(private val binding: CardChannelListBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickItem(adapterPosition)
            }
        }

        fun setResult(dataItem: ChannelCourse) {
            if (adapterPosition < itemCount - 1) {
                binding.cardChannelListLogo.visibility = View.GONE
                binding.cardChannelListCopyright.visibility = View.GONE
            }
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardChannelListBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).setResult(getItem(position) as ChannelCourse)
    }
}