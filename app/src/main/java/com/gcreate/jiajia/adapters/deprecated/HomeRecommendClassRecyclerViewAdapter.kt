package com.gcreate.jiajia.adapters.deprecated

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.HomeClass
import de.hdodenhof.circleimageview.CircleImageView


class HomeRecommendClassRecyclerViewAdapter(private val dataList : MutableList<HomeClass>, val context: Context, val width : Int) :RecyclerView.Adapter<HomeRecommendClassRecyclerViewAdapter.ViewHolder>(){

    inner class ViewHolder(ItemView : View) : RecyclerView.ViewHolder(ItemView){
        val image = ItemView.findViewById<ImageView>(R.id.card_home_recommend_class_image)
        val price = ItemView.findViewById<TextView>(R.id.card_home_recommend_class_price)
        val profile = ItemView.findViewById<CircleImageView>(R.id.card_home_recommend_class_profile)
        val mName = ItemView.findViewById<TextView>(R.id.card_home_recommend_class_name)
        val date = ItemView.findViewById<TextView>(R.id.card_home_recommend_class_date)
        val content = ItemView.findViewById<TextView>(R.id.card_home_recommend_class_content)
        val layout = ItemView.findViewById<ConstraintLayout>(R.id.card_home_recommend_class_layout)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_home_recommend_class,parent,false)
        return ViewHolder(view)    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //依螢幕寬度設定大小
        holder.layout.maxWidth = width*288/320
        holder.layout.minWidth = width*288/320
        holder.layout.maxHeight = width*270/320
        holder.layout.minHeight = width*270/320
        //確認網路連結是否存在
        if (dataList[position].imageUrl.isNotEmpty()){
            // TODO: 2021/9/28 獲取網路圖片
        }else{
            // TODO: 2021/9/28 加載本地圖片
        }
        if (dataList[position].profileUrl.isNotEmpty()){
            // TODO: 2021/9/28 獲取網路圖片
        }else{
            // TODO: 2021/9/28 加載本地圖片
        }
        holder.price.text = dataList[position].price
        holder.mName.text = dataList[position].name
        holder.date.text = dataList[position].date
        holder.content.text = dataList[position].content
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}

