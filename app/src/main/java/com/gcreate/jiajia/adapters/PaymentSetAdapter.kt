package com.gcreate.jiajia.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.payment.response.Plan

import com.gcreate.jiajia.databinding.AdapterPaymentSetBinding
import com.gcreate.jiajia.util.baseRecyclerViewHelper.BaseItemViewHolder

class PaymentSetAdapter(private val context: Context) : RecyclerView.Adapter<BaseItemViewHolder<*>>() {

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int, data: Plan)
    }

    var onItemClickListener: OnItemClickListener? = null
    var dataList = mutableListOf<Plan>()
        set(value) {
            val oldField = mutableListOf<Plan>()
            oldField.addAll(field)
            field.clear()
            field.addAll(value)
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize() = oldField.size
                override fun getNewListSize() = field.size
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = true
                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int,
                ): Boolean = oldField[oldItemPosition].id == field[newItemPosition].id
            }).dispatchUpdatesTo(this)
        }
    var focusItem: Plan? = null
        set(value) {
            val oldField = field
            field = value
            notifyItemChanged(dataList.indexOfFirst { it == oldField })
            notifyItemChanged(dataList.indexOfFirst { it == field })
        }

    inner class GeneralViewHolder internal constructor(binding: AdapterPaymentSetBinding) : BaseItemViewHolder<AdapterPaymentSetBinding>(binding) {
        override fun bindView(position: Int) {
            val data = dataList[position]
            binding.root.setOnClickListener {
                onItemClickListener?.onItemClick(it, position, data)
            }
            data.also {
                binding.tvName.text = it.name
                binding.tvDescription.text = it.description
                binding.tvPrice.text = it.price
                binding.root.background = if (focusItem == it) {
                    ContextCompat.getDrawable(context, R.drawable.layout_checked)
                } else {
                    ContextCompat.getDrawable(context, R.drawable.layout_uncheck)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseItemViewHolder<*> {
        val binding: AdapterPaymentSetBinding =
            AdapterPaymentSetBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GeneralViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseItemViewHolder<*>, position: Int) {
        holder.bindView(position)
    }

    override fun getItemCount(): Int = dataList.size
}