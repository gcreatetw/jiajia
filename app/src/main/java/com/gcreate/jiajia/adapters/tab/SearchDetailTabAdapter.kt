package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.search.SearchDetailBundleFragment
import com.gcreate.jiajia.views.search.SearchDetailCourseFragment
import com.gcreate.jiajia.views.search.SearchDetailFragment
import com.gcreate.jiajia.views.search.SearchDetailLiveStreamFragment

class SearchDetailTabAdapter(private val count: Int, baseFragment: SearchDetailFragment, private val keyword: String) :
    FragmentStateAdapter(baseFragment) {

    //tab數量
    override fun getItemCount(): Int {
        return count
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> SearchDetailCourseFragment(keyword)
            1 -> SearchDetailBundleFragment(keyword)
            else -> SearchDetailLiveStreamFragment(keyword)
        }
    }
}