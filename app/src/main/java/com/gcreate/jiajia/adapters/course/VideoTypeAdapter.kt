package com.gcreate.jiajia.adapters.course

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.VideoType
import com.gcreate.jiajia.data.VideoTypeItem
import com.gcreate.jiajia.databinding.CardVideoTypeBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder


class VideoTypeAdapter() : BaseQuickAdapter<VideoTypeItem, DataBindBaseViewHolder>(R.layout.card_video_type) {

    override fun convert(holder: DataBindBaseViewHolder, item: VideoTypeItem) {
        val binding: CardVideoTypeBinding = holder.getViewBinding() as CardVideoTypeBinding
        binding.item = (item as VideoType)
        binding.executePendingBindings()
    }

}
