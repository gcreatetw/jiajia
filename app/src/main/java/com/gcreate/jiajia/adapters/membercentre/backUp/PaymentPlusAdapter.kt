package com.gcreate.jiajia.adapters.membercentre.backUp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.payment.response.PlusChannel
import com.gcreate.jiajia.databinding.AdapterPaymentPlusBinding
import com.gcreate.jiajia.util.baseRecyclerViewHelper.BaseItemViewHolder

class PaymentPlusAdapter(
    private val context: Context,
) : RecyclerView.Adapter<BaseItemViewHolder<*>>() {
    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int, data: PlusChannel)
    }

    var onItemClickListener: OnItemClickListener? = null
    var dataList = mutableListOf<PlusChannel>()
        set(value) {
            val oldField = mutableListOf<PlusChannel>()
            oldField.addAll(field)
            field.clear()
            field.addAll(value)
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize() = oldField.size
                override fun getNewListSize() = field.size
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = true
                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int,
                ): Boolean = oldField[oldItemPosition].id == field[newItemPosition].id
            }).dispatchUpdatesTo(this)
        }
    var focusItem: PlusChannel? = null
        set(value) {
            val oldField = field
            field = value
            notifyItemChanged(dataList.indexOfFirst { it == oldField })
            notifyItemChanged(dataList.indexOfFirst { it == field })
        }

    inner class GeneralViewHolder internal constructor(binding: AdapterPaymentPlusBinding) :
        BaseItemViewHolder<AdapterPaymentPlusBinding>(binding) {
        override fun bindView(position: Int) {
            val data = dataList[position]
            binding.root.setOnClickListener {
                onItemClickListener?.onItemClick(it, position, data)
            }
            data.also {
                binding.tvName.text = it.name
                binding.tvDetail.text = it.detail
                binding.tvPrice.text = it.price.toString()
                binding.root.background = if (focusItem == it) {
                    ContextCompat.getDrawable(context, R.drawable.layout_checked)
                } else {
                    ContextCompat.getDrawable(context, R.drawable.layout_uncheck)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseItemViewHolder<*> {
        val binding: AdapterPaymentPlusBinding =
            AdapterPaymentPlusBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GeneralViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseItemViewHolder<*>, position: Int) {
        holder.bindView(position)
    }

    override fun getItemCount(): Int = dataList.size
}