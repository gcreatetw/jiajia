package com.gcreate.jiajia.adapters.home.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.search.response.Record
import com.gcreate.jiajia.databinding.RvItemSearchBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder

class SearchAdapter : BaseQuickAdapter<Record, DataBindBaseViewHolder>(R.layout.rv_item_search) {

    override fun convert(holder: DataBindBaseViewHolder, item: Record) {
        val binding: RvItemSearchBinding = holder.getViewBinding() as RvItemSearchBinding
        binding.recyclerData = item
        binding.executePendingBindings()
    }

}