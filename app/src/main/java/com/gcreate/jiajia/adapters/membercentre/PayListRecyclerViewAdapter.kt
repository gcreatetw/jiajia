package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.Pay
import com.gcreate.jiajia.databinding.CardPayBinding

class PayListRecyclerViewAdapter (
    private val onClick: (Int) -> Unit)
    : ListAdapter<Pay, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Pay>() {

        override fun areItemsTheSame(oldItem: Pay, newItem: Pay): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Pay, newItem: Pay): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardPayBinding) : RecyclerView.ViewHolder(binding.root){

        init {
            binding.root.setOnClickListener {
                onClick(adapterPosition)
            }
        }

        fun setResult(dataItem: Pay) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardPayBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).setResult(getItem(position) as Pay)
    }
}