package com.gcreate.jiajia.adapters.membercentre

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.payment.response.PlusChannel
import com.gcreate.jiajia.databinding.AdapterPaymentPlusBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.views.payment.PaymentPlusFragment.Companion.orderPlusIdList

class Payment2PlusAdapter(dataList: MutableList<PlusChannel>) :
    BaseQuickAdapter<PlusChannel, DataBindBaseViewHolder>(R.layout.adapter_payment_plus, dataList) {


    override fun convert(holder: DataBindBaseViewHolder, item: PlusChannel) {
        val binding: AdapterPaymentPlusBinding = holder.getViewBinding() as AdapterPaymentPlusBinding
        binding.executePendingBindings()

        holder.setText(R.id.tv_name, item.name)
        holder.setText(R.id.tv_detail, item.detail)
        holder.setText(R.id.tv_price, item.price.toString())

        if (orderPlusIdList.contains(item.id)) {
            holder.itemView.setBackgroundResource(R.drawable.layout_checked)
        } else {
            holder.itemView.setBackgroundResource(R.drawable.layout_uncheck)
        }

        holder.itemView.setOnClickListener {
            if (orderPlusIdList.contains(item.id)) {
                holder.itemView.setBackgroundResource(R.drawable.layout_uncheck)
                orderPlusIdList.remove(item.id)
            } else {
                holder.itemView.setBackgroundResource(R.drawable.layout_checked)
                orderPlusIdList.add(item.id)
            }
        }
    }
}