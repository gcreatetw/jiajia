package com.gcreate.jiajia.adapters.home.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.data.LiveStream
import com.gcreate.jiajia.databinding.CardSearchLiveStreamBinding

class SearchLiveAdapter(dataList: MutableList<LiveStream>) :
    BaseQuickAdapter<LiveStream, DataBindBaseViewHolder>(R.layout.card_search_live_stream, dataList), LoadMoreModule {

    override fun convert(holder: DataBindBaseViewHolder, item: LiveStream) {
        val binding: CardSearchLiveStreamBinding = holder.getViewBinding() as CardSearchLiveStreamBinding
        binding.liveStream = item
        binding.executePendingBindings()
    }

}