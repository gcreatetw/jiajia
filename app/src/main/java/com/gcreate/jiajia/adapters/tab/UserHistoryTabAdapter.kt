package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.user.UserHistoryClassFragment
import com.gcreate.jiajia.views.user.UserHistoryFragment
import com.gcreate.jiajia.views.user.UserHistoryTrainingFragment

class UserHistoryTabAdapter(baseFragement: UserHistoryFragment) : FragmentStateAdapter(baseFragement) {
    //tab數量
    override fun getItemCount(): Int {
        return 2
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UserHistoryClassFragment()
            else -> UserHistoryTrainingFragment()
        }
    }
}