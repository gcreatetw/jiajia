package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.user.setting.UserCouponRecordFragment
import com.gcreate.jiajia.views.user.setting.UserSettingSubscription
import com.gcreate.jiajia.views.user.setting.UserSettingSubscriptionTypeFragment

//tabLayout的adapter
class SubscriptionRecordTypeAdapter(fragment: UserSettingSubscriptionTypeFragment) : FragmentStateAdapter(fragment) {

    //tab數量
    override fun getItemCount(): Int {
        return 2
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UserSettingSubscription()
            else -> UserCouponRecordFragment()
        }
    }
}