package com.gcreate.jiajia.adapters.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gcreate.jiajia.views.user.schedule.UserScheduleDailyByDateFragment

class UserScheduleDailyTabAdapter(baseFragment: Fragment, val dateList : List<String>) : FragmentStateAdapter(baseFragment) {
    //tab數量
    override fun getItemCount(): Int {
        return dateList.size
    }

    //tab引導頁面
    override fun createFragment(position: Int): Fragment {
        return UserScheduleDailyByDateFragment(dateList[position])
    }
}