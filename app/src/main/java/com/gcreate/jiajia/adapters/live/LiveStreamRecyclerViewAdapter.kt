package com.gcreate.jiajia.adapters.live

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.LiveStream
import com.gcreate.jiajia.data.LiveStreamItem
import com.gcreate.jiajia.data.LiveStreamTail
import com.gcreate.jiajia.databinding.CardLiveStreamBinding
import com.gcreate.jiajia.databinding.CardTailBinding

class LiveStreamRecyclerViewAdapter(
    private val onClickLesson: (Int) -> Unit,
) : ListAdapter<LiveStreamItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<LiveStreamItem>() {

        override fun areItemsTheSame(oldItem: LiveStreamItem, newItem: LiveStreamItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: LiveStreamItem, newItem: LiveStreamItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardLiveStreamBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onClickLesson(adapterPosition)
            }
        }

        fun setResult(dataItem: LiveStream) {
            binding.liveStream = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root)


    override fun getItemViewType(position: Int): Int {
        when (getItem(position)) {
            is LiveStream -> return 0
            is LiveStreamTail -> return 1
        }
        return 1
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardLiveStreamBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 -> {
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as LiveStream)
            }
            is ViewHolderTail -> {
            }
        }
    }
}