package com.gcreate.jiajia.adapters.home.banner

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.SplashBanner
import com.gcreate.jiajia.util.loadImgByGlide
import com.zhpan.bannerview.BaseBannerAdapter
import com.zhpan.bannerview.BaseViewHolder

class SplashBannerAdapter(
    val context : Context
): BaseBannerAdapter<SplashBanner>() {

    override fun bindData(holder: BaseViewHolder<SplashBanner>?, data: SplashBanner, position: Int, pageSize: Int) {

        //載入banner資料
        val imageBanner = holder?.findViewById<ImageView>(R.id.img)
        loadImgByGlide(imageBanner!!, data.url)

        val title = holder?.findViewById<TextView>(R.id.tv_title)
        title?.text = data.title
        val content = holder?.findViewById<TextView>(R.id.tv_content)
        content?.text = data.content
    }

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.banner_splash
    }
}