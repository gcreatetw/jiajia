package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.UserFollowByCoach
import com.gcreate.jiajia.data.UserFollowByCoachItem
import com.gcreate.jiajia.data.UserFollowByCoachTail
import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardUserFollowByCoachBinding

class UserFollowByCoachRecyclerViewAdapter (
    private val onClickFollow: (Int) ->Unit
) : ListAdapter<UserFollowByCoachItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<UserFollowByCoachItem>() {

        override fun areItemsTheSame(oldItem: UserFollowByCoachItem, newItem: UserFollowByCoachItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: UserFollowByCoachItem, newItem: UserFollowByCoachItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardUserFollowByCoachBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imgFollow.setOnClickListener {
                onClickFollow(adapterPosition)
            }
        }

        fun setResult(dataItem: UserFollowByCoach) {
            binding.item = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is UserFollowByCoach -> return 0
            is UserFollowByCoachTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType){
            0 ->{
                val binding = CardUserFollowByCoachBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 ->{
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as UserFollowByCoach)

            }
            is ViewHolderTail -> {

            }
        }
    }


}