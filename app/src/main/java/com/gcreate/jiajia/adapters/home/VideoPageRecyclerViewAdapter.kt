package com.gcreate.jiajia.adapters.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.databinding.*
import com.gcreate.jiajia.adapters.bundle.TrainingClass288RecyclerViewAdapter
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.horizontalSpacingItemDecoration
import kotlin.math.roundToInt


class VideoPageRecyclerViewAdapter (
    val context: Context,
    private val onClickDetail: () ->Unit,
    private val onClickTraining: (Int) -> Unit,
    private val onClickLesson: (Int) ->Unit)
    : ListAdapter<VideoPageItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<VideoPageItem>() {

        override fun areItemsTheSame(oldItem: VideoPageItem, newItem: VideoPageItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: VideoPageItem, newItem: VideoPageItem): Boolean {
            return false
        }
    }

    inner class ViewHolderTitle(private val binding: CardVideoPage1TitleBinding) : RecyclerView.ViewHolder(binding.root){
        init{
            binding.btnDetail.setOnClickListener {
                onClickDetail()
            }
        }
        fun setResult(dataItem: VideoPageTitle) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTraining(private val binding: CardVideoPage2TrainingBinding) : RecyclerView.ViewHolder(binding.root){
        fun setResult(dataItem: VideoPageTraining) {
            binding.data = dataItem

            val layoutManager = LinearLayoutManager(context)
            val rv = binding?.recyclerView
            layoutManager.orientation = LinearLayoutManager.HORIZONTAL
            rv?.layoutManager = layoutManager
            rv?.addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f,context).roundToInt()))
            rv?.adapter = TrainingClass288RecyclerViewAdapter(){
                onClickTraining(it)
            }

            binding.executePendingBindings()
        }
    }

    inner class ViewHolderOtherLessonTitle(private val binding: CardVideoPage3OtherLessonTitleBinding) : RecyclerView.ViewHolder(binding.root){

    }

    inner class ViewHolderLesson(private val binding: CardLessonBinding) : RecyclerView.ViewHolder(binding.root){
        init{
            binding.root.setOnClickListener {
                onClickLesson(adapterPosition)
            }
        }
        fun setResult(dataItem: Course) {
            binding.lesson = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardVideoPage5TailBinding) : RecyclerView.ViewHolder(binding.root){

    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is VideoPageTitle -> return 0
            is VideoPageTraining -> return 1
            is VideoPageOtherLessonTitle -> return 2
            is Course -> return 3
            is VideoPageTail -> return 4
        }
        return 0
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardVideoPage1TitleBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTitle(binding)
            }
            1 -> {
                val binding = CardVideoPage2TrainingBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTraining(binding)
            }
            2 -> {
                val binding = CardVideoPage3OtherLessonTitleBinding.inflate(layoutInflater, parent, false)
                return ViewHolderOtherLessonTitle(binding)
            }
            3 -> {
                val binding = CardLessonBinding.inflate(layoutInflater, parent, false)
                return ViewHolderLesson(binding)
            }
            else -> {
                val binding = CardVideoPage5TailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolderTitle -> {
                holder.setResult(getItem(position) as VideoPageTitle)
            }
            is ViewHolderTraining -> {
                holder.setResult(getItem(position) as VideoPageTraining)
            }
            is ViewHolderOtherLessonTitle -> {

            }
            is ViewHolderLesson -> {
                holder.setResult(getItem(position) as Course)
            }
            is ViewHolderTail -> {

            }
        }
    }
}