package com.gcreate.jiajia.adapters.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.api.dataobj.Channel
import com.gcreate.jiajia.databinding.CardHomeValueBinding


class HomeValueRecyclerViewAdapter(
    private val onClickMore: (Int) -> Unit,
) : ListAdapter<Channel, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Channel>() {

        override fun areItemsTheSame(oldItem: Channel, newItem: Channel): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Channel, newItem: Channel): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.detail == newItem.detail
                    && oldItem.language == newItem.language
        }
    }

    inner class ViewHolder(private val binding: CardHomeValueBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.homeValueChannelMoreButton.setOnClickListener {
                onClickMore(adapterPosition)
            }
        }

        fun setResult(dataItem: Channel) {
            if (adapterPosition < itemCount - 1) {
                binding.homeValueLogo.visibility = View.GONE
                binding.homeValueCopyright.visibility = View.GONE
            }

            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardHomeValueBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).setResult(getItem(position) as Channel)
    }
}