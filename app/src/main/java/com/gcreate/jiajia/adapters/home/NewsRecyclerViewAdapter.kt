package com.gcreate.jiajia.adapters.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.HomeNews
import com.gcreate.jiajia.databinding.CardNewsBinding


class NewsRecyclerViewAdapter(
    private val onItemImgViewClicked: (position: Int) -> Unit
) : ListAdapter<HomeNews, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<HomeNews>() {

        override fun areItemsTheSame(oldItem: HomeNews, newItem: HomeNews): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: HomeNews, newItem: HomeNews): Boolean {
            return oldItem.heading == newItem.heading
        }
    }

    inner class ViewHolder(private val binding: CardNewsBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onItemImgViewClicked(adapterPosition)
            }
        }

        fun setResult(dataItem: HomeNews) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CardNewsBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as HomeNews)
            }
        }
    }

}

