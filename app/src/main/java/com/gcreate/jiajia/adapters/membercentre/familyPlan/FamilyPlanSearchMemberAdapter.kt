package com.gcreate.jiajia.adapters.membercentre.familyPlan

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.user.response.SearchFamilyMemberResultData

import com.gcreate.jiajia.databinding.RvItemSearchFamilyMemberBinding
import com.gcreate.jiajia.adapters.DataBindBaseViewHolder
import com.gcreate.jiajia.util.AES256Util


class FamilyPlanSearchMemberAdapter(data:SearchFamilyMemberResultData) :
    BaseQuickAdapter<SearchFamilyMemberResultData, DataBindBaseViewHolder>(R.layout.rv_item_search_family_member, mutableListOf(data)) {

    override fun convert(holder: DataBindBaseViewHolder, item: SearchFamilyMemberResultData) {
        val binding: RvItemSearchFamilyMemberBinding = holder.getViewBinding() as RvItemSearchFamilyMemberBinding

        Glide.with(holder.itemView.context).load(item.user_image)
            .into(binding.imgMemberAvatar)

        binding.tvFamilyMemberName.text = AES256Util.AES256Decrypt(context.getString(R.string.AESkey),context.getString(R.string.AESIVkey),item.name)
        binding.tvFamilyMemberMobile.text = AES256Util.AES256Decrypt(context.getString(R.string.AESkey),context.getString(R.string.AESIVkey),item.mobile)
    }

}
