package com.gcreate.jiajia.adapters.membercentre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.UserFollowByType
import com.gcreate.jiajia.data.UserFollowByTypeItem
import com.gcreate.jiajia.data.UserFollowByTypeTail
import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardUserFollowByTypeBinding

class UserFollowByTypeRecyclerViewAdapter(
    private val onClickFollow: (Int) -> Unit,
) : ListAdapter<UserFollowByTypeItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<UserFollowByTypeItem>() {

        override fun areItemsTheSame(oldItem: UserFollowByTypeItem, newItem: UserFollowByTypeItem): Boolean {
            return oldItem === newItem
        }


        override fun areContentsTheSame(oldItem: UserFollowByTypeItem, newItem: UserFollowByTypeItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardUserFollowByTypeBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imgFollow.setOnClickListener {
                onClickFollow(adapterPosition)
            }
        }

        fun setResult(dataItem: UserFollowByType) {
            binding.item = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when (getItem(position)) {
            is UserFollowByType -> return 0
            is UserFollowByTypeTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardUserFollowByTypeBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 -> {
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as UserFollowByType)


            }
            is ViewHolderTail -> {

            }
        }
    }


}