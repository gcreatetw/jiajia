package com.gcreate.jiajia.adapters.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.data.VideoType
import com.gcreate.jiajia.data.VideoTypeItem
import com.gcreate.jiajia.data.VideoTypeTail
import com.gcreate.jiajia.databinding.CardTailBinding
import com.gcreate.jiajia.databinding.CardVideoTypeBinding
import com.gcreate.jiajia.adapters.course.ClassRecyclerViewAdapter


class VideoTypeRecyclerViewAdapter(
    val context: Context,
    private val onClickMore: (Int) -> Unit,
    private val onClickClass: (typeIndex: Int, classIndex: Int) -> Unit,
) : ListAdapter<VideoTypeItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<VideoTypeItem>() {

        override fun areItemsTheSame(oldItem: VideoTypeItem, newItem: VideoTypeItem): Boolean {
            return oldItem === newItem
        }


        override fun areContentsTheSame(oldItem: VideoTypeItem, newItem: VideoTypeItem): Boolean {
            return false
        }
    }

    inner class ViewHolder(private val binding: CardVideoTypeBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.tvMore.setOnClickListener {
                onClickMore(adapterPosition)
            }
        }

        fun setResult(dataItem: VideoType) {
            binding.item = dataItem

            if (dataItem.classList.isNotEmpty()) {
                binding.apply {
                    tvNoData.visibility = View.INVISIBLE
                    recyclerView.apply {
                        visibility = View.VISIBLE
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        //addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f,context).roundToInt()))
                        adapter = ClassRecyclerViewAdapter() {
                            onClickClass(adapterPosition, it)
                        }
                    }
                }
            } else {
                binding.apply {
                    recyclerView.visibility = View.INVISIBLE
                    tvNoData.visibility = View.VISIBLE
                }
            }
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTail(private val binding: CardTailBinding) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getItemViewType(position: Int): Int {
        when (getItem(position)) {
            is VideoType -> return 0
            is VideoTypeTail -> return 1
        }
        return 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            0 -> {
                val binding = CardVideoTypeBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
            1 -> {
                val binding = CardTailBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTail(binding)
            }
        }

        val binding = CardTailBinding.inflate(layoutInflater, parent, false)
        return ViewHolderTail(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolder -> {
                holder.setResult(getItem(position) as VideoType)

            }
            is ViewHolderTail -> {

            }
        }
    }


}