package com.gcreate.jiajia.util.baseRecyclerViewHelper

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

abstract class BaseItemViewHolder<T : ViewBinding>(var binding: T) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bindView(position: Int)
}