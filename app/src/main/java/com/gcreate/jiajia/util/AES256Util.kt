package com.gcreate.jiajia.util

import android.os.Build
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.util.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import kotlin.experimental.and

object AES256Util {
    /**
     * AES256加密
     *
     * @param stringToEncode
     * @param keyString
     * @return Bses64格式密文
     */
    @Throws(NullPointerException::class)
    fun AES256Encode(keyString: String, ivKey: String, stringToEncode: String): String {
        if (keyString.isEmpty()) {
            return ""
        }
        if (stringToEncode!!.isEmpty()) {
            return ""
        }
        try {
            val skeySpec = getKey(keyString)
            val data = stringToEncode.toByteArray(charset("UTF8"))
            val iv = ByteArray(16)
            Arrays.fill(iv, 0x00.toByte())
            val ivParameterSpec = getIvKey(ivKey)

            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec)
            return Base64.encodeToString(cipher.doFinal(data),
                Base64.DEFAULT)
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     *
     * 说明 :AES256解密
     *
     * @param text
     * Base64格式密文
     * @param keyString
     * 密钥
     * @return String格式明文
     */
    fun AES256Decrypt(keyString: String, ivKey: String, text: String): String {
        // byte[] rawKey = getRawKey(key);
        if (keyString!!.isEmpty()) {
            return ""
        }
        if (text!!.isEmpty()) {
            return ""
        }
        try {
            val key: SecretKey = getKey(keyString)
            val ivParameterSpec = getIvKey(ivKey)

            val data = Base64.decode(text, Base64.DEFAULT)
            val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
            cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec)
            val decrypedValueBytes = cipher.doFinal(data)
            return String(decrypedValueBytes, Charsets.UTF_8)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     *
     * 说明 :将密钥转行成SecretKeySpec格式
     *
     * @param password
     * 16进制密钥
     * @return SecretKeySpec格式密钥
     */
    @Throws(UnsupportedEncodingException::class)
    private fun getKey(password: String): SecretKeySpec {
        // 如果为128将长度改为128即可
        val keyLength = 256
        val keyBytes = ByteArray(keyLength / 8)
        // explicitly fill with zeros
        Arrays.fill(keyBytes, 0x0.toByte())
        val passwordBytes = toByte(convertStringToHex(password))
        val length = if (passwordBytes.size < keyBytes.size) passwordBytes.size else keyBytes.size
        System.arraycopy(passwordBytes, 0, keyBytes, 0, length)
        return SecretKeySpec(keyBytes, "AES")
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getIvKey(password: String): IvParameterSpec {
        // 如果为128将长度改为128即可
        val keyLength = 128
        val keyBytes = ByteArray(keyLength / 8)
        Arrays.fill(keyBytes, 0x0.toByte())
        val passwordBytes = toByte(convertStringToHex(password))
        val length = if (passwordBytes.size < keyBytes.size) passwordBytes.size else keyBytes.size
        System.arraycopy(passwordBytes, 0, keyBytes, 0, length)
        return IvParameterSpec(keyBytes)
    }

    /**
     *
     * 说明 :随机生成一组AES密钥
     *
     * @return 16进制AES密钥
     */
    fun getRawKey(): String? {
        var kgen: KeyGenerator? = null
        var sr: SecureRandom? = null
        try {
            kgen = KeyGenerator.getInstance("AES/CBC/PKCS7Padding")
            // SHA1PRNG 强随机种子算法, 要区别4.2以上版本的调用方法
            sr = if (Build.VERSION.SDK_INT >= 17) {
                SecureRandom.getInstance("SHA1PRNG", "Crypto")
            } else {
                SecureRandom.getInstance("SHA1PRNG")
            }
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        kgen?.init(256, sr) // 256 bits or 128 bits,192bits
        val skey: SecretKey = kgen!!.generateKey()
        val raw: ByteArray = skey.encoded
        return bytes2Hex(raw)
    }

    /**
     * byte数组转换为16进制字符串
     *
     * @param bts
     * 数据源
     * @return 16进制字符串
     */
    private fun bytes2Hex(bts: ByteArray): String {
        var des: String? = ""
        var tmp: String? = null
        for (i in bts.indices) {
            tmp = Integer.toHexString((bts[i] and 0xFF.toByte()).toInt())
            if (tmp.length == 1) {
                des += "0"
            }
            des += tmp
        }
        return des!!
    }

    /**
     * 将16进制转换为byte数组
     *
     * @param hexString
     * 16进制字符串
     * @return byte数组
     */
    private fun toByte(hexString: String): ByteArray {
        val len = hexString.length / 2
        val result = ByteArray(len)
        for (i in 0 until len) result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
            16).toByte()
        return result
    }

    private fun convertStringToHex(str: String): String {
        val hex = StringBuffer()

        // loop chars one by one
        for (temp in str.toCharArray()) {

            // convert char to int, for char `a` decimal 97
            val decimal = temp.code

            // convert int to hex, for decimal 97 hex 61
            hex.append(Integer.toHexString(decimal))
        }
        return hex.toString()
    }

}