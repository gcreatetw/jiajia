package com.gcreate.jiajia.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.gcreate.jiajia.R

fun loadImgByGlide(imageView: ImageView, url: String?) {
    if(url == null || url.isEmpty()){
        Glide.with(imageView.context)
            .load(R.drawable.jiajia_default_2)
            .centerCrop()
            .into(imageView)
    }
    else {
        Glide.with(imageView.context)
            .load(url)
            .error(R.drawable.jiajia_default_2)
            .centerCrop()
            .into(imageView)
    }

}

fun loadImgByGlide2(imageView: ImageView, subscription: Boolean) {
    if (subscription){
        Glide.with(imageView.context)
            .load(R.drawable.membership_card_register)
            .error(R.drawable.jiajia_default_2)
            .centerCrop()
            .into(imageView)
    }else{
        Glide.with(imageView.context)
            .load(R.drawable.membership_card_plan)
            .error(R.drawable.jiajia_default_2)
            .centerCrop()
            .into(imageView)
    }

}