package com.gcreate.jiajia.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class LimitInputTextWatcher : TextWatcher {

    private var et: EditText? = null

    /**
     * 篩選條件
     */
    private var regex: String

    /**
     * 默認篩選條件(正则:只能輸入中英文數字)
     */
    private val DEFAULT_REGEX = "[^a-zA-Z0-9\u4E00-\u9FA5]"

    constructor(et: EditText?) {
        this.et = et
        regex = DEFAULT_REGEX
    }

    /**
     * @param et    editText
     * @param regex 篩選條件
     */
    constructor(et: EditText?, regex: String) {
        this.et = et
        this.regex = regex
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
    override fun afterTextChanged(editable: Editable) {
        val str = editable.toString()
        val inputStr = clearLimitStr(regex, str)
        et!!.removeTextChangedListener(this)
        // et.setText方法可能会引起键盘变化,所以用editable.replace来显示内容
        editable.replace(0, editable.length, inputStr.trim { it <= ' ' })
        et!!.addTextChangedListener(this)
    }

    /**
     * 清除不符合條件的內容
     * @param regex
     * @return
     */
    private fun clearLimitStr(regex: String, str: String): String {
        return str.replace(regex.toRegex(), "")
    }
}