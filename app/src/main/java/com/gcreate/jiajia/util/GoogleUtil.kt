package com.gcreate.jiajia.util

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.gcreate.jiajia.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

class GoogleUtil(private val mActivity: FragmentActivity) {

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var mGoogleSignInClient: GoogleSignInClient? = null

        fun getGoogleSignInClient(): GoogleSignInClient {
            return mGoogleSignInClient!!
        }

        fun signOut() {
            mGoogleSignInClient!!.signOut().addOnCompleteListener {
            }
        }
    }


    fun init() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(mActivity.getString(R.string.firebase_server_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity as Activity, gso)
    }


}