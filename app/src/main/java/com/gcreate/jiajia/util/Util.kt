package com.gcreate.jiajia.util

import android.content.Context
import android.util.DisplayMetrics

internal class Util {
    //轉px to dp
    fun dpToPixel(dp: Float,context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}