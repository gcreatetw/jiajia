package com.gcreate.jiajia.util

import android.view.LayoutInflater
import android.widget.RadioButton
import android.widget.RadioGroup
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.dataobj.ChannelCategory
import com.gcreate.jiajia.data.ClassCategory
import com.gcreate.jiajia.data.ItemName
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup

fun initChipGroup(layoutInflater: LayoutInflater, chipGroup: ChipGroup, textList: List<String>) {

    chipGroup.removeAllViews()

    var id = 0
    for (text in textList) {
        val chip = layoutInflater.inflate(R.layout.card_chip, chipGroup, false) as Chip
        chip.text = text
        chip.id = id
        chipGroup.addView(chip)

        id++
    }
}

fun initClassCategoryChipGroup(layoutInflater: LayoutInflater, chipGroup: ChipGroup, list: List<ClassCategory>) {
    chipGroup.removeAllViews()

    for (item in list) {
        val chip = layoutInflater.inflate(R.layout.card_chip, chipGroup, false) as Chip
        chip.text = item.title
        chip.id = item.id
        chipGroup.addView(chip)
    }
}

fun initChannelCategoryChipGroup(layoutInflater: LayoutInflater, channelName: String, chipGroup: ChipGroup, list: List<ChannelCategory>) {
    chipGroup.removeAllViews()

    for (item in list) {
        val chip = layoutInflater.inflate(R.layout.card_chip, chipGroup, false) as Chip
        chip.text = "$channelName-${item.name}"
        chip.id = item.id
        chipGroup.addView(chip)
    }
}

fun initItemNameChipGroup(layoutInflater: LayoutInflater, chipGroup: ChipGroup, list: List<ItemName>) {
    chipGroup.removeAllViews()

    for (item in list) {
        val chip = layoutInflater.inflate(R.layout.card_chip, chipGroup, false) as Chip
        chip.text = item.name
        chip.id = item.id
        chipGroup.addView(chip)
    }
}

fun initItemTitleChipGroup(layoutInflater: LayoutInflater, chipGroup: ChipGroup, list: List<ItemName>) {
    chipGroup.removeAllViews()

    for (item in list) {
        val chip = layoutInflater.inflate(R.layout.card_chip, chipGroup, false) as Chip
        chip.text = item.name
        chip.id = item.id
        chipGroup.addView(chip)
    }
}

fun initRadioGroup(layoutInflater: LayoutInflater, radioGroup: RadioGroup, textList: MutableList<String>) {

    initRadioGroup(layoutInflater, radioGroup, R.layout.card_radio_button, textList)
}

fun initRadioGroup(layoutInflater: LayoutInflater, radioGroup: RadioGroup, radioButtonResourceId: Int, textList: MutableList<String>) {

    radioGroup.removeAllViews()

    var id = 0
    for (text in textList) {
        val item = layoutInflater.inflate(radioButtonResourceId, radioGroup, false) as RadioButton
        item.text = text
        item.id = id
        radioGroup.addView(item)

        id++
    }
}
