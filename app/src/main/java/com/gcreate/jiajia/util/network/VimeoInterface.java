package com.gcreate.jiajia.util.network;

import com.gcreate.jiajia.data.VimeoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface VimeoInterface {
    @GET("videos/{videoId}")
    Call<VimeoResponse> getVimeoUrlResponse(@Path("videoId") String id);

}
