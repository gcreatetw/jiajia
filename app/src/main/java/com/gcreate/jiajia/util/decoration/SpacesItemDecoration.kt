package com.gcreate.jiajia.util.decoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class SpacesItemDecoration(
    private val top: Int,
    private val left: Int,
    private val right: Int,
    private val bottom: Int,
    ) : ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.also {
            it.top = top
            it.left = left
            it.right = right
            it.bottom = bottom
        }
    }
}