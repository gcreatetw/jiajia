package com.gcreate.jiajia.util

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.views.signupotp.SignupOtpCodeCheckFragmentDirections
import com.gcreate.jiajia.views.signupotp.SignupOtpSendingFragmentDirections
import com.gcreate.jiajia.views.signupotp.SignupOtpType
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit

class OtpUtil(private val mActivity: FragmentActivity, val model: MainViewModel, val auth: FirebaseAuth, private val mode: SignupOtpType) {

    private val logTag = "OtpUtil"

    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    companion object {
        private var storedVerificationId: String = ""
        var isAlreadyGetOTP = false
        var resendToken000 = PhoneAuthProvider.ForceResendingToken.zza()
    }

    /** 參考 DOC
     * https://firebase.google.com/docs/auth/android/phone-auth?authuser=1#enable-app-verification
     * */
    fun getOTP(fragment: Fragment, mode: SignupOtpType, phoneForVerify: String) {
        val phoneNumber = "+886$phoneForVerify"
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.w(logTag, "onVerificationCompleted:$credential")
//                signInWithPhoneAuthCredential(fragment, auth, credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {

                when (e) {

                    is FirebaseAuthInvalidCredentialsException -> {
                        model.progressInfo.set(ProgreeeInfo("您已暫時被停用，請稍後再試", ProgreeeInfo.Type.ERROR))
                        Handler(Looper.getMainLooper()).postDelayed({
                            findNavController(fragment).navigateUp()
                        }, 1000)
                    }
                    is FirebaseTooManyRequestsException -> {
                        model.progressInfo.set(ProgreeeInfo("簡訊驗證請求次數超過，請稍後再試", ProgreeeInfo.Type.ERROR))
                        Handler(Looper.getMainLooper()).postDelayed({
                            findNavController(fragment).navigateUp()
                        }, 1000)
                    }
                    else -> {
                        model.progressInfo.set(ProgreeeInfo(e.toString(), ProgreeeInfo.Type.ERROR))
                        Handler(Looper.getMainLooper()).postDelayed({
                            findNavController(fragment).navigateUp()
                        }, 1000)
                    }
                }

            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                // Save verification ID and resending token so we can use them later

                storedVerificationId = verificationId
                resendToken = token
                resendToken000 = token
                isAlreadyGetOTP = true

                // 送出簡訊後跳轉畫面
                model.progressInfo.set(ProgreeeInfo("已發送", ProgreeeInfo.Type.OK))

                if (!fragment.toString().startsWith("SignupOtpNoCodeFragment")) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        //Do something after NNN ms
                        var action = SignupOtpSendingFragmentDirections.actionSignupOtpSendingFragmentToSignupOtpCodeInputFragment()
                        action.mode = mode
                        action.phone = phoneForVerify
                        findNavController(fragment).navigate(action)

                    }, 1000)
                }
            }
        }

        if (isAlreadyGetOTP) {
            Log.w(logTag, "resendVerificationCode")
            resendVerificationCode(phoneNumber, resendToken000, auth)
        } else {
            Log.w(logTag, "startPhoneNumberVerification")
            startPhoneNumberVerification(phoneNumber, auth)
        }

    }

    private fun startPhoneNumberVerification(phoneNumber: String, auth: FirebaseAuth) {
        val options = PhoneAuthOptions.newBuilder(auth).setPhoneNumber(phoneNumber) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(mActivity) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    var mobile = ""

    fun verifyPhoneNumberWithCode(fragment: Fragment, OTP: String, phone: String) {
        mobile = phone
        if (storedVerificationId.isNotEmpty()) {
            val credential = PhoneAuthProvider.getCredential(storedVerificationId, OTP)
            signInWithPhoneAuthCredential(fragment, auth, credential)
        }

    }

    private fun resendVerificationCode(phoneNumber: String, token: PhoneAuthProvider.ForceResendingToken?, auth: FirebaseAuth) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth).setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS)      // Timeout and unit
            .setActivity(mActivity)                     // Activity (for callback binding)
            .setCallbacks(callbacks)                            // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token)        // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }


    private fun signInWithPhoneAuthCredential(fragment: Fragment, auth: FirebaseAuth, credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential).addOnCompleteListener(mActivity) { task ->
            if (task.isSuccessful) {
                // Sign in success, update UI with the signed-in user's information
                val user = task.result?.user
                Log.w(logTag, "OTP verify success")
                //  註冊成功
                model.otpCheckFailCount = 0
                model.progressInfo.set(ProgreeeInfo("完成", ProgreeeInfo.Type.OK))
                Handler(Looper.getMainLooper()).postDelayed({
                    if (mode == SignupOtpType.Signup) {
                        val action = SignupOtpCodeCheckFragmentDirections.actionSignupOtpCodeCheckFragmentToMemberSignupSetPasswordFragment()
                        action.mode = mode
                        action.phone = mobile
                        findNavController(fragment).navigate(action)
                    } else if (mode == SignupOtpType.ModifyPassword || mode == SignupOtpType.ForgetPassword) {
                        val action = SignupOtpCodeCheckFragmentDirections.actionSignupOtpCodeCheckFragmentToMemberSignupSetPasswordFragment()
                        action.mode = mode
                        action.phone = mobile
                        findNavController(fragment).navigate(action)
                    }

                }, 1000)

            } else {
                Log.w(logTag, "OTP verify fail")
                // Sign in failed, display a message and update the UI
                model.progressInfo.set(
                    ProgreeeInfo("驗證失敗", ProgreeeInfo.Type.ERROR)
                )

                Handler(Looper.getMainLooper()).postDelayed({
                    findNavController(fragment).popBackStack()
                }, 2000)


                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    // The verification code entered was invalid
                    Log.w(logTag, " The verification code entered was invalid = " + task.exception)
                }

            }
        }
    }
}