package com.gcreate.jiajia.sharedpreferences.model



class ThirdLoginResponse(
    private var singUpMethod : String,
    private var accountId: String,
    private var accountDisplayName: String,
    private var accountPhotoUrl: String) {

    fun getSingUpMethod(): String {
        return singUpMethod
    }

    fun getAccountId(): String {
        return accountId
    }

    fun getAccountDisplayName(): String {
        return accountDisplayName
    }

    fun getAccountPhotoUrl(): String {
        return accountPhotoUrl
    }

}