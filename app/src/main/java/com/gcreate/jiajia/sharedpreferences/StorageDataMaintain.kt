package com.gcreate.jiajia.sharedpreferences

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.gcreate.jiajia.data.NotificationSetting
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object StorageDataMaintain {

    var thirdLoginResponse: ThirdLoginResponse? = null


    //--------------------------------------------------------------------------------------
    fun getSingUpResponse(context: Context): ThirdLoginResponse? {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val thirdLoginResponseString = sp.getString("GoogleLoginResponse", null)

        val gson = Gson()
        thirdLoginResponse = gson.fromJson(thirdLoginResponseString, object : TypeToken<ThirdLoginResponse?>() {}.type)

        return thirdLoginResponse
    }

    //------------------------------------------- Save  -------------------------------------------
    fun saveSingUpResponse(context: Context, thirdLoginResponse: ThirdLoginResponse?) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("GoogleLoginResponse", Gson().toJson(thirdLoginResponse))
        editor.apply()
        editor.commit()
    }

    //------------------------------------------- Add  -------------------------------------------
    //    public static void addFavStockGroupItem(Context context, thirdLoginResponse objectStockGroupItem) {
    //        if (thirdLoginResponse != null) {
    //            thirdLoginResponse.add(objectStockGroupItem);
    //        }
    //        saveFavStockGroupList(context);
    //        getFavStockGroupList(context);
    //    }
    //------------------------------------------- Delete  -------------------------------------------
    fun deleteFavStockGroupItem(context: Context) {
        val settings = context.getSharedPreferences("MainActivity", Context.MODE_PRIVATE)
        settings.edit().remove("GoogleLoginResponse").apply()
    }

    fun getInvoiceType(context: Context): Int {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        return sp.getInt("invoiceType", 0)
    }

    fun saveInvoiceType(context: Context, invoiceType: Int) {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        sp.putInt("invoiceType", invoiceType)
        sp.apply()
    }

    fun getPusherList(context: Context): NotificationSetting {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val pusherList = sp.getString("pusherList", null)

        if (pusherList.isNullOrEmpty()) {
            val dataList = mutableListOf<Boolean>()
            dataList.add(0, true)
            dataList.add(1, true)
            dataList.add(2, true)
            dataList.add(3, true)

            val editor = context.getPreferences(Context.MODE_PRIVATE).edit()
            editor.putString("pusherList", Gson().toJson(dataList))
            editor.apply()

            return NotificationSetting(dataList[0], dataList[1], dataList[2], dataList[3])

        } else {
            val gson = Gson()
            val jasonList = gson.fromJson<MutableList<Boolean>>(pusherList, object : TypeToken<MutableList<Boolean>>() {}.type)
            return NotificationSetting(jasonList[0], jasonList[1], jasonList[2], jasonList[3])
        }

    }

    fun savePusherList(context: Context, dataList: List<Boolean>) {
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("pusherList", Gson().toJson(dataList))
        editor.apply()
    }

    fun getFirebaseToken(context: Context): String {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        return sp.getString("firebaseToken", "")!!
    }

    fun saveFirebaseToken(context: Context, firebaseToken: String) {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        sp.putString("firebaseToken", firebaseToken)
        sp.apply()
    }

}