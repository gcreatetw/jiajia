package com.gcreate.jiajia

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.adapters.bundle.ChannelListRecyclerViewAdapter
import com.gcreate.jiajia.adapters.bundle.ClassPageRecyclerViewAdapter
import com.gcreate.jiajia.adapters.bundle.TrainingClass288RecyclerViewAdapter
import com.gcreate.jiajia.adapters.bundle.TrainingClassRecyclerViewAdapter
import com.gcreate.jiajia.adapters.course.ClassRecyclerViewAdapter
import com.gcreate.jiajia.adapters.home.*
import com.gcreate.jiajia.adapters.home.backup.HomeRecommendTeacherRecyclerViewAdapter
import com.gcreate.jiajia.adapters.home.backup.News288RecyclerViewAdapter
import com.gcreate.jiajia.adapters.home.coach.CoachListRecyclerViewAdapter
import com.gcreate.jiajia.adapters.home.coach.CoachNewLessonRecyclerViewAdapter
import com.gcreate.jiajia.adapters.home.coach.CoachRecyclerViewAdapter
import com.gcreate.jiajia.adapters.live.LiveStreamRecyclerViewAdapter
import com.gcreate.jiajia.adapters.membercentre.*
import com.gcreate.jiajia.adapters.membercentre.subcription.AvailableRecordListRecyclerViewAdapter
import com.gcreate.jiajia.api.dataobj.Channel
import com.gcreate.jiajia.api.dataobj.ChannelCourse
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.user.response.Order
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.util.loadImgByGlide
import com.gcreate.jiajia.util.loadImgByGlide2


@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView, url: String?) {
    loadImgByGlide(imageView, url)
}

@BindingAdapter("imageUrl2")
fun bindImage(imageView: ImageView, resource: Boolean) {
    loadImgByGlide2(imageView, resource)
}

@BindingAdapter("addValueList")
fun bindRecyclerViewWithChannelData(recyclerView: RecyclerView, list: List<Channel>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is HomeValueRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}


@BindingAdapter("channelCourseList")
fun bindRecyclerViewWithChannelCourseData(recyclerView: RecyclerView, list: List<ChannelCourse>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is ChannelListRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("beingClassList")
fun bindRecyclerViewWithBeingClass(recyclerView: RecyclerView, list: List<Course>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is ClassRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("news")
fun bindRecyclerViewWithNews(recyclerView: RecyclerView, list: List<HomeNews>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is NewsRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("news288")
fun bindRecyclerViewWithNews288(recyclerView: RecyclerView, list: List<HomeNews>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is News288RecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("homeCoachList")
fun bindRecyclerViewWithHomeCoachList(recyclerView: RecyclerView, list: List<HomeCoach>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is HomeRecommendTeacherRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("trainingClassList")
fun bindRecyclerViewWithTrainingClassList(recyclerView: RecyclerView, list: List<TrainingClassItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is TrainingClassRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("trainingClass288List")
fun bindRecyclerViewWithTrainingClass288List(recyclerView: RecyclerView, list: List<TrainingClassItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is TrainingClass288RecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("classPage")
fun bindRecyclerViewWithClassPage(recyclerView: RecyclerView, list: List<ClassPageItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is ClassPageRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("coachList")
fun bindRecyclerViewWithCoachList(recyclerView: RecyclerView, coachList: List<HomeCoach>?) {
    coachList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is CoachListRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("coachItemList")
fun bindRecyclerViewWithCoachItem(recyclerView: RecyclerView, coachItemList: List<CoachItem>?) {
    coachItemList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is CoachRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("coachNewLessonList")
fun bindRecyclerViewWithCoachNewLessonList(recyclerView: RecyclerView, coachNewLessonList: List<Lesson>?) {
    coachNewLessonList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is CoachNewLessonRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("hotVideoLessonList")
fun bindRecyclerViewWithLessonList(recyclerView: RecyclerView, hotVideoLessonList: List<BeingRecyclerViewItemData>?) {
    hotVideoLessonList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is HotVideoRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("videoSortList")
fun bindRecyclerViewWithVideoSortList(recyclerView: RecyclerView, videoSortList: List<BeingRecyclerViewItemData>?) {
    videoSortList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is VideoSortRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("liveStreamList")
fun bindRecyclerViewWithLiveStreamList(recyclerView: RecyclerView, liveStreamList: List<LiveStreamItem>?) {
    liveStreamList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is LiveStreamRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("userScheduleDailyList")
fun bindRecyclerViewWithUserScheduleDailyList(recyclerView: RecyclerView, list: List<UserScheduleDailyItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is UserScheduleDailyRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

//@BindingAdapter("userLessonList")
//fun bindRecyclerViewWithUserLessonList(recyclerView: RecyclerView, userLessonList: List<Lesson>?) {
//    userLessonList?.let {
//        recyclerView.adapter?.apply {
//            when (this) {
//                is UserLessonRecyclerViewAdapter -> submitList(it)
//            }
//        }
//    }
//}

@BindingAdapter("userBookmark")
fun bindRecyclerViewWithUserBookmark(recyclerView: RecyclerView, userBookmarkList: List<BeingRecyclerViewItemData>?) {
    userBookmarkList?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is UserBookmarkRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("userFollowByType")
fun bindRecyclerViewWithUserFollowByType(recyclerView: RecyclerView, list: List<UserFollowByTypeItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is UserFollowByTypeRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}


@BindingAdapter("userFollowByCoach")
fun bindRecyclerViewWithUserFollowByCoach(recyclerView: RecyclerView, list: List<UserFollowByCoachItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is UserFollowByCoachRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("userFollowByAddedValue")
fun bindRecyclerViewWithUserFollowByAddedValue(recyclerView: RecyclerView, list: List<UserFollowByAddedValueItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is UserFollowByAddedValueRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("videoTypeList")
fun bindRecyclerViewWithVideoType(recyclerView: RecyclerView, list: List<VideoTypeItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is VideoTypeRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}


@BindingAdapter("videoPage")
fun bindRecyclerViewWithVideoPage(recyclerView: RecyclerView, list: List<VideoPageItem>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is VideoPageRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("payList")
fun bindRecyclerViewWithPayList(recyclerView: RecyclerView, list: List<Pay>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is PayListRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}

@BindingAdapter("availableRecordList")
fun bindRecyclerViewWithAvailableRecordList(recyclerView: RecyclerView, list: List<Order>?) {
    list?.let {
        recyclerView.adapter?.apply {
            when (this) {
                is AvailableRecordListRecyclerViewAdapter -> submitList(it)
            }
        }
    }
}