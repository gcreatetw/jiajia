package com.gcreate.jiajia.widget

interface FractionView {
    var flLeft : Float
    var flTop : Float
    var flWidth : Float
    var flHeight : Float
}