package com.gcreate.jiajia.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.gcreate.jiajia.R

class ViewFL : View, FractionView {

    constructor(context: Context) : super(context) {
        init(null)
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private var _flLeft: Float = 0f
    private var _flTop: Float = 0f
    private var _flWidth: Float = 50f
    private var _flHeight: Float = 50f


    private fun init(attrs: AttributeSet?) {

        initAttrs(attrs)

    }

    private fun initAttrs(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.FractionLayout)

        _flLeft = typedArray.getFloat(R.styleable.FractionLayout_fl_left, _flLeft)
        _flTop = typedArray.getFloat(R.styleable.FractionLayout_fl_top, _flTop)
        _flWidth = typedArray.getFloat(R.styleable.FractionLayout_fl_width, _flWidth)
        _flHeight = typedArray.getFloat(R.styleable.FractionLayout_fl_height, _flHeight)

        typedArray.recycle()
    }

    override var flLeft: Float
        get() = _flLeft
        set(value) { _flLeft = value }
    override var flTop: Float
        get() = _flTop
        set(value) { _flTop = value }
    override var flWidth: Float
        get() = _flWidth
        set(value) { _flWidth = value }
    override var flHeight: Float
        get() = _flHeight
        set(value) { _flHeight = value }
}