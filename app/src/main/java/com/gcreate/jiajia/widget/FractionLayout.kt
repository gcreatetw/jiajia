package com.gcreate.jiajia.widget

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView
import com.gcreate.jiajia.R

//class FractionLayout  : ViewGroup {
class FractionLayout  : CardView {

    constructor(context: Context) : super(context) {
        init(null)
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }
//    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
//        init(attrs)
//    }

    // attrs
    private var aspectRatio = 1f
    private var flWidth = 100f
    private var flHeight = 100f
    private var flCardCornerRadius = 2f

    private fun init(attrs: AttributeSet?) {

        initAttrs(attrs)


    }

    private fun initAttrs(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.FractionLayout)

        flWidth = typedArray.getFloat(R.styleable.FractionLayout_fl_width, flWidth)
        flHeight = typedArray.getFloat(R.styleable.FractionLayout_fl_height, flHeight)
        flCardCornerRadius = typedArray.getFloat(R.styleable.FractionLayout_fl_cardCornerRadius, flCardCornerRadius)
        aspectRatio = flHeight /flWidth

        typedArray.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val specModeW = MeasureSpec.getMode(widthMeasureSpec)
        val specSizeW = MeasureSpec.getSize(widthMeasureSpec)

        setMeasuredDimension(
            getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
            (specSizeW * aspectRatio).toInt()
        )
    }

    override fun onLayout(change: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val thisW = right - left
        val thisH = bottom - top

        radius = thisH * flCardCornerRadius / flHeight

        for(index in 0 until childCount){
            val view = getChildAt(index)
            if(view is FractionView){
                val l = view.flLeft / flWidth * thisW
                val t = view.flTop / flHeight * thisH
                val w = view.flWidth / flWidth * thisW
                val h = view.flHeight / flHeight * thisH
                view.layout(l.toInt(), t.toInt(), (l + w).toInt(), (t + h).toInt())
            }
        }
    }
}