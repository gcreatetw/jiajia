package com.gcreate.jiajia.views.login

enum class SetPersonalizeFragmentType {
    UserSetting,
    Signup
}