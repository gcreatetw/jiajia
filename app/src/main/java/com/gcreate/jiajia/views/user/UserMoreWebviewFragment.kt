package com.gcreate.jiajia.views.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserMoreWebviewBinding

class UserMoreWebviewFragment  : Fragment() {

    val arg : UserMoreWebviewFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels{
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding : FragmentUserMoreWebviewBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentUserMoreWebviewBinding.inflate(inflater, container, false)
        binding?. apply {

            toolbar?.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar?.setNavigationOnClickListener {
                backPressed()
            }

            toolbar?.title = arg.title

            // webView , arg.url
            webView.loadUrl(arg.url)
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}