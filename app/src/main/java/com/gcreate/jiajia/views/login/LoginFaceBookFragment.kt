package com.gcreate.jiajia.views.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestVerifyThirdPartyBody
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginFaceBookBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking

class LoginFaceBookFragment : Fragment() {

    private val logTag = "LoginFaceBookFragment"
    private var mAuth: FirebaseAuth? = null
    private lateinit var callbackManager: CallbackManager
    private var isIntentFB = false

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentLoginFaceBookBinding.inflate(inflater, container, false)

        binding.apply {
            model.progressInfo.set(
                ProgreeeInfo(" ", ProgreeeInfo.Type.PROGRESS)
            )
            viewModel = model
        }

        loginFacebook()

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.UserSelect) {
            findNavController().navigateUp()
        }
    }

    private fun loginFacebook() {
        callbackManager = CallbackManager.Factory.create()

        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, callbackManager, listOf("email", "user_photos", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                handleFacebookAccessToken(result.accessToken)
            }

            override fun onCancel() {
                model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                val dataList = ThirdLoginResponse("", "", "", "")
                StorageDataMaintain.saveSingUpResponse(requireContext(), dataList)
                Firebase.auth.signOut()
                Toast.makeText(requireActivity(), "取消Facebook登入", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: FacebookException) {
                Firebase.auth.signOut()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(logTag, "signInWithCredential:success")
                    val user = mAuth!!.currentUser

                    StorageDataMaintain.saveSingUpResponse(requireContext(),
                        ThirdLoginResponse("facebook", user!!.uid, user.displayName!!, user.photoUrl.toString()))

                    UserApiClient.verifyThirdPartyId(RequestVerifyThirdPartyBody("facebook", user.uid),
                        object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                            override fun onSuccess(response: ResponseSimpleFormat) {
                                // 表示後台有Uer用過餓第三方 ID 註冊 or 綁定過
                                if (!response.error && response.have_user) {
                                    // 判斷資料庫有無該 facebook UID
                                    val paramObject = JsonObject()
                                    paramObject.addProperty("providor", "facebook")
                                    paramObject.addProperty("code", user.uid)
                                    paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

                                    UserApiClient.login(
                                        paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                                            override fun onSuccess(response: ResponseLogin) {
                                                runBlocking {
                                                    model.appState.setAccessToken(response.access_token)
                                                    model.appState.setRefreshToken(response.refresh_token)
                                                }
                                                val action = LoginFaceBookFragmentDirections.actionLoginFaceBookFragmentToMainFragment()
                                                findNavController().navigate(action)
                                            }
                                        }
                                    )
                                } else {
                                    // 無帳號，註冊
                                    findNavController().navigate(LoginFaceBookFragmentDirections.actionLoginFaceBookFragmentToLoginCheckAccountFragment())
                                }
                            }
                        })
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(logTag, "FaceBook sign in failed ", task.exception)
                    model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                    findNavController().navigateUp()
                }
            }
    }

    override fun onResume() {
        super.onResume()
        if (isIntentFB) {
            if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.UserSelect) {
                Firebase.auth.signOut()
                findNavController().navigateUp()
            }
        } else {
            isIntentFB = !isIntentFB
        }
    }
}