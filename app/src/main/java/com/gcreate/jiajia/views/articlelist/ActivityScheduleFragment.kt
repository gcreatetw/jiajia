package com.gcreate.jiajia.views.articlelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentActivityScheduleBinding
import com.gcreate.jiajia.views.video.VideoPageAddVideoFragmentArgs


class ActivityScheduleFragment : Fragment() {

    private val args: ActivityScheduleFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentActivityScheduleBinding.inflate(inflater, container, false)

        binding.toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }

        Glide.with(requireActivity()).load(args.imageUrl).into(binding.imgContent)

        binding.imgContent.setOnClickListener {
            val action = ActivityScheduleFragmentDirections.actionActivityScheduleFragmentToEventFragment(args.imageLink)
            findNavController().navigate(action)
        }

        return binding.root
    }

}