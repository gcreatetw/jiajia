package com.gcreate.jiajia.views.user.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponsePayedRecord
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.Pay
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingSubscriptionBinding
import com.gcreate.jiajia.adapters.membercentre.PayListRecyclerViewAdapter


class UserSettingSubscription : Fragment() {

    private var dataList = mutableListOf<Pay>()

    private var binding: FragmentUserSettingSubscriptionBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadDataFromAPI()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserSettingSubscriptionBinding.inflate(inflater, container, false)



        binding?.apply {
            viewModel = model


            val layoutManager = LinearLayoutManager(requireActivity())
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            rvContent.layoutManager = layoutManager
            // recyclerView?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f,requireContext()).roundToInt()))
            rvContent.adapter = PayListRecyclerViewAdapter {
            }

            tvBind.setOnClickListener {
                // 前往綁定
                val action = UserSettingSubscriptionTypeFragmentDirections.actionUserSettingSubscriptionTypeFragmentToLoginOpenPointFragment()
                action.loginOrBind = "bingOP"
                (activity as MainActivity).rootNavController.navigate(action)
            }

        }

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    private fun loadDataFromAPI() {
        dataList.clear()
        UserApiClient.getPayedRecord(model.appState.accessToken.get()!!, object : ApiController<ResponsePayedRecord>(requireActivity(), false) {
            override fun onSuccess(response: ResponsePayedRecord) {
                val recordList = response.order
                for (i in recordList.indices) {
                    val price = if (recordList[i].point_counted) {
                        "點數已失效"
                    } else {
                        recordList[i].openpoint.toString() + "點"
                    }
                    if (recordList[i].products != null) {
                        dataList.add(Pay(recordList[i].products!!.name, recordList[i].enroll_start, recordList[i].amount.toLong(), price,recordList[i].status))
                    }
                }
                binding!!.list = dataList

                val user = model.me.get()
                user?.point = response.total_amount
                val totalPointText = "可累積 ${user!!.point} 點"
                binding!!.tvPointCount.text = totalPointText

            }
        })
    }

}