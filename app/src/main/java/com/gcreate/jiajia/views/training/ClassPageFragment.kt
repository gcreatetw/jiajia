package com.gcreate.jiajia.views.training

import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.bundle.ClassPageRecyclerViewAdapter
import com.gcreate.jiajia.api.bundle.BundleApiClient
import com.gcreate.jiajia.api.bundle.request.RequestBundleDetail
import com.gcreate.jiajia.api.bundle.response.ResponseBundleDetail
import com.gcreate.jiajia.api.dataobj.CourseDetail
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentClasspageBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import kotlin.math.roundToInt

class ClassPageFragment : Fragment() {

    private var binding: FragmentClasspageBinding? = null
    private var dataList: MutableList<ClassPageItem> = mutableListOf()
    private var courseDetail: CourseDetail? = null

    private val args: ClassPageFragmentArgs by navArgs()

    private lateinit var responseObject: ResponseBundleDetail

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentClasspageBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.classpageToolbar
        toolBar?.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }
        toolBar?.title = args.title


        val layoutManager = LinearLayoutManager(requireActivity())
        val rv = binding?.recyclerView
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = layoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = ClassPageRecyclerViewAdapter {
            val action = ClassPageFragmentDirections.actionClassPageFragmentToVideoPageFragment()
            action.courseId = responseObject.bundle_courses[it - 1].id
            findNavController().navigate(action)
        }

        binding?.btnBooking?.setOnClickListener {
            if (model.appState.accessToken.get().isNullOrEmpty()) {
                val action = ClassPageFragmentDirections.actionClassPageFragmentToLoginAllinOneFragment()
                findNavController().navigate(action)
            } else {
                val action = ClassPageFragmentDirections.actionClassPageFragmentToVideoPageAddVideoFragment()
                action.bundleId = args.bundleId.toString()
                findNavController().navigate(action)
            }

        }

        setHasOptionsMenu(true)
        loadDataFromApi()

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.classpage_toolbar_menu, menu)
    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.classpage_shared -> {
                try {
                    ShareCompat.IntentBuilder(requireActivity())
                        .setType("text/plain")
                        .setText("我在 #BEING運動影音頻道，體驗專業的訓練規畫，快把訓練計劃加入你的行事曆，期待每一次運動，帶給你的價值！\n ${Uri.parse("https://www.being.com.tw/training/${args.bundleId}")}")
                        .startChooser()
                } catch (e: Exception) {
                    //e.toString();
                }
                true
            }

            R.id.classpage_detail_item -> {
                model.courseDetailTmp = courseDetail
                val action = ClassPageFragmentDirections.actionClassPageFragmentToClassPageDetailFragment()
                binding!!.root.findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun loadDataFromApi() {
        val request = RequestBundleDetail(args.bundleId)
        BundleApiClient.bundleDetail(model.appState.accessToken.get(),
            request, object : ApiController<ResponseBundleDetail>(requireContext()) {
                override fun onSuccess(response: ResponseBundleDetail) {
                    responseObject = response

                    val data = mutableListOf<ClassPageItem>()
                    val bundle = response.bundle

                    courseDetail = CourseDetail(bundle.detail,
                        bundle.accessory,
                        bundle.suggest,
                        bundle.fit_situation,
                        bundle.unfit_situation,
                        bundle.before_fitness,
                        bundle.body_response,
                        bundle.note
                    )

                    var equipment = ""
                    for (i in bundle.accessory.indices) {
                        equipment += (bundle.accessory[i].name + " ")
                    }

                    data.add(
                        ClassPageTitle(
                            bundle.preview_image,
                            "強度TEXT",
                            bundle.Calorie + "卡洛里",
                            bundle.detail,
                            equipment)
                    )

                    for (course in response.bundle_courses) {
                        data.add(ClassPageLesson(course.id,
                            course.preview_image,
                            course.title,
                            Integer.parseInt(course.views).toLong(),
                            course.created_at, false))
                    }

                    // add data
                    dataList = data
                    binding?.list = dataList
                }
            })
    }

}