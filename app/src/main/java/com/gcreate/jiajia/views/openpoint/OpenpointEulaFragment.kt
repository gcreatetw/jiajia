package com.gcreate.jiajia.views.openpoint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentOpenpointEulaBinding


class OpenpointEulaFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: FragmentOpenpointEulaBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentOpenpointEulaBinding.inflate(inflater, container, false)
        binding?.apply {

            toolbar?.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar?.setNavigationOnClickListener {
                backPressed()
            }

            val html = "OPEN POINT"
            webView.loadData(html, "text/html", "UTF-8")

            tvNext.setOnClickListener {
                if (checkBox.isChecked) {
                    val action = OpenpointEulaFragmentDirections.actionOpenpointEulaFragmentToOpenpointBindFragment()
                    findNavController().navigate(action)
                }
            }
        }



        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}