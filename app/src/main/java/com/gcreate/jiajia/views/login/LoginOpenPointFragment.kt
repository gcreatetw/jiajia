package com.gcreate.jiajia.views.login

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestVerifyThirdPartyBody
import com.gcreate.jiajia.api.user.response.ResponseBingOP
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseOpenPointJson
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginOpenPointBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody

class LoginOpenPointFragment : Fragment() {

    private lateinit var binding: FragmentLoginOpenPointBinding
    private val args: LoginOpenPointFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLoginOpenPointBinding.inflate(inflater, container, false)

        getOpRequestUrl()
        webViewSetting()

        return binding.root
    }

    private fun getOpRequestUrl() {
        UserApiClient.getOpRequestUrl(object : ApiController<ResponseBody>(requireContext(), false) {
            override fun onSuccess(response: ResponseBody) {
                val url: String = response.string().trim()
                binding.opWebview.loadUrl(url)
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun webViewSetting() {

        binding.opWebview.settings.apply {
            javaScriptEnabled = true
            setSupportZoom(true)
        }

        binding.opWebview.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url!!.startsWith("https://group-test.openpoint.com.tw")) {
                    binding.opWebview.loadUrl(url)
                    return false

                } else {
                    var path = url.split('=')[0]
                    path = path.substring(0, path.length - 2)
                    val query = url.split('=')[1]

                    UserApiClient.getOpenPointResponseJSON(path,
                        query,
                        object : ApiController<ResponseOpenPointJson>(requireActivity(), false) {
                            override fun onSuccess(response: ResponseOpenPointJson) {
                                if (args.loginOrBind == "bingOP") {
                                    openPointBinding(response.gid_bar_code)
                                } else {
                                    openPointLogin(response.gid_bar_code)
                                }
                            }
                        })
                    return true
                }
            }
        }
    }

    private fun openPointLogin(gid: String) {
        StorageDataMaintain.saveSingUpResponse(requireContext(),
            ThirdLoginResponse("openpoint", gid, "", ""))

        UserApiClient.verifyThirdPartyId(RequestVerifyThirdPartyBody("openpoint", gid),
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    // 表示後台有Uer用過餓第三方 ID 註冊 or 綁定過
                    if (!response.error && response.have_user) {
                        // 判斷資料庫有無該 facebook UID
                        val paramObject = JsonObject()
                        paramObject.addProperty("providor", "openpoint")
                        paramObject.addProperty("code", gid)
                        paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

                        UserApiClient.login(
                            paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                                override fun onSuccess(response: ResponseLogin) {
                                    runBlocking {
                                        model.appState.setAccessToken(response.access_token)
                                        model.appState.setRefreshToken(response.refresh_token)

                                        findNavController().navigate(LoginOpenPointFragmentDirections.actionLoginOpenPointFragmentToMainFragment())
                                    }
                                }
                            }
                        )
                    } else {
                        // 無帳號，註冊
                        findNavController().navigate(LoginOpenPointFragmentDirections.actionLoginOpenPointFragmentToLoginCheckAccountFragment())
                    }
                }
            })
    }

    private fun openPointBinding(gid: String) {

        val paramObject = JsonObject()
        paramObject.addProperty("op_id", gid)
        UserApiClient.bindOpenPoint(model.appState.accessToken.get()!!,
            paramObject, object : ApiController<ResponseBingOP>(requireContext(), false) {
                override fun onSuccess(response: ResponseBingOP) {
                    if (!response.error) {
                        model.userInfo!!.user.op_bundled = true
                    } else {
                        Toast.makeText(requireActivity(), response.error_message, Toast.LENGTH_SHORT).show()
                    }
                    findNavController().navigateUp()
                }
            }
        )
    }

    override fun onStop() {
        super.onStop()
        if (args.loginOrBind == "login") {
            model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
        }
    }
}

