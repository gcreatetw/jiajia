package com.gcreate.jiajia.views.payment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.membercentre.PlusChannelOrderAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.PaymentApiClient
import com.gcreate.jiajia.api.payment.request.RequestApplyCoupon
import com.gcreate.jiajia.api.payment.request.RequestProcessToCheckout
import com.gcreate.jiajia.api.payment.response.ResponseProcessToCheckout
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.PaymentInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentOrderBinding

class PaymentOrderFragment : Fragment() {

    private var binding: FragmentPaymentOrderBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPaymentOrderBinding.inflate(inflater, container, false)

        binding?.apply {
            vm = model

            subscription = model.subscriptionsPlan
            totalPrice = model.totalAmount.toLong()
            discountPrice = model.discountPrice?.toLong()
            firstPay = model.firstPay

            rvPlusOrder.apply {
                layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                adapter = PlusChannelOrderAdapter(model.plusChannelOrderList)
            }

            // TODO 要跟訂閱的支付相同，故後面付款方式限定
            when (model.appState.payMethod.get()) {
                "openwallet" -> {
                    layoutOpenpoint.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                    imgOpenpointCheck.visibility = View.VISIBLE
                    model.paymentInfo.type = PaymentInfo.Type.Openpoint
//                    layoutIcash.isEnabled = false
//                    layoutCreditCard.isEnabled = false
                }
                "icashpay" -> {
                    layoutIcash.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                    imgIcashCheck.visibility = View.VISIBLE
                    model.paymentInfo.type = PaymentInfo.Type.iCash
//                    layoutOpenpoint.isEnabled = false
//                    layoutCreditCard.isEnabled = false
                }
                "cathaybk" -> {
                    layoutCreditCard.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                    imgCreditCardCheck.visibility = View.VISIBLE
                    model.paymentInfo.type = PaymentInfo.Type.CreditCard
//                    layoutOpenpoint.isEnabled = false
//                    layoutIcash.isEnabled = false
                }
            }

            layoutOpenpoint.setOnClickListener {
                unCheckLayout()
                layoutOpenpoint.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                imgOpenpointCheck.visibility = View.VISIBLE

                model.paymentInfo.type = PaymentInfo.Type.Openpoint
            }

            layoutIcash.setOnClickListener {
                unCheckLayout()
                layoutIcash.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                imgIcashCheck.visibility = View.VISIBLE

                model.paymentInfo.type = PaymentInfo.Type.iCash
            }

            layoutCreditCard.setOnClickListener {
                unCheckLayout()
                layoutCreditCard.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_checked)
                imgCreditCardCheck.visibility = View.VISIBLE

                model.paymentInfo.type = PaymentInfo.Type.CreditCard
            }

            editInput.setOnEditorActionListener { _, _, _ ->
                hideSoftKeyBroad()
                true
            }

            btnUseCoupon.setOnClickListener {
                hideSoftKeyBroad()

                val requestBody = RequestApplyCoupon(binding!!.editInput.text.toString())
                PaymentApiClient.applyCoupon(model.appState.accessToken.get()!!,
                    requestBody,
                    object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseSimpleFormat2) {
                            when (response.error) {
                                0 -> {
                                    // 使用成功 & Reload
                                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()

                                    PaymentApiClient.processToCheckout(
                                        model.appState.accessToken.get()!!,
                                        RequestProcessToCheckout(PaymentPlusFragment.orderPlusIdList),
                                        object : ApiController<ResponseProcessToCheckout>(requireContext()) {
                                            override fun onSuccess(response: ResponseProcessToCheckout) {
                                                model.subscriptionsPlan = response.cart.SubscriptionPlan
                                                if (response.cart.PlusChannel.isNotEmpty()) {
                                                    model.plusChannel = response.cart.PlusChannel[0]
                                                    model.plusChannelOrderList = response.cart.PlusChannel
                                                }
                                                model.totalAmount = response.total_amount
                                                model.discountPrice = response.cart.Coupon
                                                model.firstPay = response.firstpay

                                                binding.apply {
                                                    totalPrice = model.totalAmount.toLong()
                                                    discountPrice = model.discountPrice?.toLong()
                                                }
                                            }
                                        }
                                    )
                                }
                                1 -> {
                                    //使用失敗
                                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                                }
                            }
                        }

                    })
            }

            tvCancelCoupon.setOnClickListener {
                PaymentApiClient.removeCoupon(model.appState.accessToken.get()!!,
                    object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseSimpleFormat2) {
                            when (response.error) {
                                0 -> {
                                    // 移除成功 & Reload
                                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                                    PaymentApiClient.processToCheckout(
                                        model.appState.accessToken.get()!!,
                                        RequestProcessToCheckout(PaymentPlusFragment.orderPlusIdList),
                                        object : ApiController<ResponseProcessToCheckout>(requireContext()) {
                                            override fun onSuccess(response: ResponseProcessToCheckout) {
                                                model.subscriptionsPlan = response.cart.SubscriptionPlan

                                                if (response.cart.PlusChannel.isNotEmpty()) {
                                                    model.plusChannel = response.cart.PlusChannel[0]
                                                    model.plusChannelOrderList = response.cart.PlusChannel
                                                }
                                                model.totalAmount = response.total_amount
                                                model.discountPrice = response.cart.Coupon
                                                model.firstPay = response.firstpay

                                                binding.apply {
                                                    totalPrice = model.totalAmount.toLong()
                                                    discountPrice = model.discountPrice?.toLong()
                                                }
                                            }
                                        }
                                    )
                                }
                                1 -> {
                                    // 移除失敗
                                    Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    })
            }
        }

        return binding?.root
    }

    private fun unCheckLayout() {
        binding?.apply {
            layoutOpenpoint.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_uncheck)
            layoutIcash.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_uncheck)
            layoutCreditCard.background = ContextCompat.getDrawable(requireActivity(), R.drawable.layout_uncheck)

            imgOpenpointCheck.visibility = View.GONE
            imgIcashCheck.visibility = View.GONE
            imgCreditCardCheck.visibility = View.GONE
        }
    }


    private fun hideSoftKeyBroad() {
        binding!!.editInput.clearFocus()
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding?.root?.windowToken, 0)
    }

}