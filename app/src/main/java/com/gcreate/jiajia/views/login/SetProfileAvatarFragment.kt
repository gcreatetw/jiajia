package com.gcreate.jiajia.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gcreate.jiajia.databinding.FragmentSetProfileAvatarBinding

import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SetProfileAvatarFragment : BottomSheetDialogFragment() {

    var listener: OnDialogButtonFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentSetProfileAvatarBinding.inflate(inflater, container, false)

        binding.tvSelectCamera.setOnClickListener {
            listener?.onSelectDialog("camera", 0)
            dismiss()
        }

        binding.tvSelectDocument.setOnClickListener {
            listener?.onSelectDialog("document", 1)
            dismiss()
        }


        return binding.root
    }

    interface OnDialogButtonFragmentListener {
        fun onSelectDialog(select: String, value: Int)
    }

}