package com.gcreate.jiajia.views.payment

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.PaymentSetAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.PaymentApiClient
import com.gcreate.jiajia.api.payment.request.RequestAddSubscription
import com.gcreate.jiajia.api.payment.response.Plan
import com.gcreate.jiajia.api.payment.response.ResponseAddSubscription
import com.gcreate.jiajia.api.payment.response.ResponseSubscriptions
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentSetBinding
import com.gcreate.jiajia.util.decoration.SpacesItemDecoration
import com.gcreate.jiajia.util.Util

class PaymentSetFragment : Fragment() {

    private lateinit var binding: FragmentPaymentSetBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPaymentSetBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.apply {
            vm = model

            if (model.userInfo?.user?.first_purchase == "0"){
                val spannableString = SpannableString("首次訂閱享${model.userInfo!!.user.first_purchase_days}天免費")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(colorSpan, 5, spannableString.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                binding.tvFistPayTitle.text = spannableString
            }

            val adapter = PaymentSetAdapter(requireActivity())
            adapter.onItemClickListener = object : PaymentSetAdapter.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int, data: Plan) {
                    adapter.focusItem = data
                    // 購買訂閱方案
                    PaymentApiClient.addSubscription(
                        model.appState.accessToken.get() ?: "",
                        RequestAddSubscription(data.id),
                        object : ApiController<ResponseAddSubscription>(requireContext()) {
                            override fun onSuccess(response: ResponseAddSubscription) {

                            }
                        }
                    )
                }
            }
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(
                SpacesItemDecoration(
                    Util().dpToPixel(24f, requireContext()).toInt(),
                    Util().dpToPixel(16f, requireContext()).toInt(),
                    Util().dpToPixel(16f, requireContext()).toInt(),
                    Util().dpToPixel(16f, requireContext()).toInt()
                )
            )

            // 取得訂閱方案
            if (!model.appState.haveSubscription.get()!!) {
                PaymentApiClient.subscriptions(
                    model.appState.accessToken.get()!!,
                    object : ApiController<ResponseSubscriptions>(requireContext()) {
                        override fun onSuccess(response: ResponseSubscriptions) {
                            adapter.dataList = response.plan.toMutableList()
                            if (response.plan.isNotEmpty()) {
                                adapter.focusItem = response.plan[0]
                                // 購買訂閱方案
                                PaymentApiClient.addSubscription(
                                    model.appState.accessToken.get() ?: "",
                                    RequestAddSubscription(response.plan[0].id),
                                    object : ApiController<ResponseAddSubscription>(requireContext()) {
                                        override fun onSuccess(response: ResponseAddSubscription) {
                                        }
                                    }
                                )
                            }
                        }
                    }
                )
            }

        }
    }
}