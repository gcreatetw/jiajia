package com.gcreate.jiajia.views.membersignup

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestLogin
import com.gcreate.jiajia.api.user.request.RequestUserAES
import com.gcreate.jiajia.api.user.request.RequestUserPassword
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseUserAES
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentMemberSignupSetPasswordBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.util.AES256Util
import com.gcreate.jiajia.views.login.SetPersonalizeFragmentType
import com.gcreate.jiajia.views.signupotp.SignupOtpType
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class MemberSignupSetPasswordFragment : Fragment() {
    private val logTag = "MemberSignupSetPasswordFragment"
    private var binding: FragmentMemberSignupSetPasswordBinding? = null
    private val arg: MemberSignupSetPasswordFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMemberSignupSetPasswordBinding.inflate(inflater, container, false)
        binding?.apply {

            when (arg.mode) {
                SignupOtpType.Signup -> {
                    toolbar.title = "設定密碼"
                }
                SignupOtpType.ModifyPassword -> {
                    toolbar.title = "重設密碼"
                }
                SignupOtpType.ForgetPassword -> {
                    toolbar.title = "重設密碼"
                }
            }

            textInputLayoutPasswordConfirm.apply {
                editText?.doOnTextChanged { text, start, before, count ->
                    error = null
                }
            }

            btnOk.setOnClickListener {
                val password = textInputLayoutPassword.editText?.text!!
                val passwordConfirm = textInputLayoutPasswordConfirm.editText?.text!!

                if (password.isEmpty() || passwordConfirm.isEmpty()) {
                    if (password.isEmpty()) {
                        textInputLayoutPassword.error = "密碼輸入空白"
                    } else {
                        textInputLayoutPassword.isErrorEnabled = false
                        textInputLayoutPassword.error = ""
                    }
                    if (passwordConfirm.isEmpty()) {
                        textInputLayoutPasswordConfirm.error = "密碼輸入空白"
                    }
                } else if (!password.contentEquals(passwordConfirm)) {
                    textInputLayoutPasswordConfirm.error = "密碼不符。"
                } else {
                    setUserPassword()
                }
            }
        }

        return binding?.root
    }

    private fun setUserPassword() {
        when (arg.mode) {
            SignupOtpType.Signup -> register()
            SignupOtpType.ModifyPassword -> modifyPassword()
            SignupOtpType.ForgetPassword -> modifyPassword()
        }
    }

    private fun register() {
        val savedData = StorageDataMaintain.getSingUpResponse(requireActivity())
        if (savedData!!.getSingUpMethod() == "registeredLogin") {
            val requestBody = RequestLogin(arg.phone, binding!!.textInputLayoutPasswordConfirm.editText!!.text.toString(), model.firebaseToken)
            UserApiClient.register(requestBody, object : ApiController<ResponseLogin>(requireActivity(), false) {
                override fun onSuccess(response: ResponseLogin) {
                    runBlocking {
                        model.appState.setAccessToken(response.access_token)
                        model.appState.setRefreshToken(response.refresh_token)
                    }
                    val action = MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToSetPersonalizeFragment()
                    action.mode = SetPersonalizeFragmentType.Signup
                    findNavController().navigate(action)
                }
            })
        } else {
            loginAgainForRegistered(savedData)
        }
//        val requestBody = RequestLogin(arg.phone, binding!!.textInputLayoutPasswordConfirm.editText!!.text.toString())
//        UserApiClient.register(requestBody, object : ApiController<ResponseLogin>(requireActivity(), false) {
//            override fun onSuccess(response: ResponseLogin) {
//                runBlocking {
//                    model.appState.setAccessToken(response.access_token)
//                    model.appState.setRefreshToken(response.refresh_token)
//                }
//                val action = MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToSetPersonalizeFragment()
//                action.mode = SetPersonalizeFragmentType.Signup
//                findNavController().navigate(action)
//            }
//        })
    }

    private fun modifyPassword() {
        getToken()
    }

    private fun getToken() {
        val phone = arg.phone
        val now: String = SimpleDateFormat("yyyyMMdd").format(Date())
        val phoneAES = AES256Util.AES256Encode(getString(R.string.AESkey), getString(R.string.AESIVkey), "$phone-$now")
        val requestBody = RequestUserAES(phoneAES)

        UserApiClient.getShortTimeToken(requestBody, object : ApiController<ResponseUserAES>(requireActivity(), false) {
            override fun onSuccess(response: ResponseUserAES) {

                val requestUserPasswordBody = RequestUserPassword(arg.phone, binding!!.textInputLayoutPassword.editText?.text.toString())
                UserApiClient.setUserPassword(response.token,
                    requestUserPasswordBody,
                    object : ApiController<ResponseSimpleFormat>(requireActivity(), true) {
                        override fun onSuccess(response: ResponseSimpleFormat) {
                            val isError = response.error
                            if (!isError) {
                                Toast.makeText(requireActivity(), "密碼變更成功", Toast.LENGTH_SHORT).show()
                                if (arg.mode == SignupOtpType.ModifyPassword) {
                                    findNavController().popBackStack(R.id.userSettingFragment,true)
                                } else if (arg.mode == SignupOtpType.ForgetPassword) {
                                    val action =
                                        MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToLoginAllinOneFragment()
                                    findNavController().navigate(action)
                                }
                            } else {
                                Toast.makeText(requireActivity(), "密碼變更失敗", Toast.LENGTH_SHORT).show()
                                if (arg.mode == SignupOtpType.ModifyPassword) {
                                    val action =
                                        MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToUserSettingFragment()
                                    findNavController().navigate(action)
                                } else if (arg.mode == SignupOtpType.ForgetPassword) {
                                    val action =
                                        MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToLoginAllinOneFragment()
                                    findNavController().navigate(action)
                                }
                            }
                        }
                    })
            }
        })
    }


    private fun loginAgainForRegistered(savedData: ThirdLoginResponse) {
        val passwordText = binding!!.textInputLayoutPasswordConfirm.editText!!.text.toString()
        val paramObject = JsonObject()
        paramObject.addProperty("mobile", arg.phone)
        paramObject.addProperty("password", passwordText)
        paramObject.addProperty("providor", savedData.getSingUpMethod())
        paramObject.addProperty("code", savedData.getAccountId())
        paramObject.addProperty("name", savedData.getAccountDisplayName())
        paramObject.addProperty("firebaseToken", model.firebaseToken)

        UserApiClient.login(
            paramObject, object : ApiController<ResponseLogin>(requireContext(), false) {
                override fun onSuccess(response: ResponseLogin) {
                    runBlocking {
                        model.appState.setAccessToken(response.access_token)
                        model.appState.setRefreshToken(response.refresh_token)
                    }
                    val action = MemberSignupSetPasswordFragmentDirections.actionMemberSignupSetPasswordFragmentToSetPersonalizeFragment()
                    action.mode = SetPersonalizeFragmentType.Signup
                    findNavController().navigate(action)
                }

                override fun onFail(httpResponse: Response<ResponseLogin>): Boolean {
                    Log.d(logTag, "Error")
                    return false
                }
            }
        )
    }

}