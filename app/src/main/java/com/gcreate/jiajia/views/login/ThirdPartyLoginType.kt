package com.gcreate.jiajia.views.login

enum class ThirdPartyLoginType {
    Signup,
    Login
}