package com.gcreate.jiajia.views.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.ApiClient.user
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestVerifyThirdPartyBody
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginLineBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.google.gson.JsonObject
import com.linecorp.linesdk.LineApiResponseCode
import com.linecorp.linesdk.Scope
import com.linecorp.linesdk.auth.LineAuthenticationParams
import com.linecorp.linesdk.auth.LineLoginApi
import kotlinx.coroutines.runBlocking
import retrofit2.Response

class LoginLineFragment : Fragment() {

    private val logTag = "LoginLineFragment"
    private var binding: FragmentLoginLineBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onStart() {
        super.onStart()
        if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.UserSelect) {
            findNavController().navigateUp()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginLineBinding.inflate(inflater, container, false)

        binding?.apply {
            model.progressInfo.set(
                ProgreeeInfo(" ", ProgreeeInfo.Type.PROGRESS)
            )
            viewModel = model
        }

        lineLogin()

        return binding?.root
    }

    private fun lineLogin() {
        val lineChannelId = requireContext().getString(R.string.line_channel_id)
        val loginIntent = LineLoginApi.getLoginIntent(
            requireContext(), lineChannelId,
            LineAuthenticationParams.Builder()
                .scopes(listOf(Scope.PROFILE))
                .botPrompt(LineAuthenticationParams.BotPrompt.normal)
                .build()
        )

        forLineLogin.launch(loginIntent)

    }

    private val forLineLogin = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) {

        if (it.data != null) {
            handleLoginResult(it.data!!)
        }
    }

    private fun handleLoginResult(data: Intent) {
        val loginResult = LineLoginApi.getLoginResultFromIntent(data)

        when (loginResult.responseCode) {
            LineApiResponseCode.SUCCESS -> {

                StorageDataMaintain.saveSingUpResponse(requireContext(),
                    ThirdLoginResponse(
                        "line",
                        loginResult.lineProfile!!.userId,
                        loginResult.lineProfile!!.displayName,
                        loginResult.lineProfile!!.pictureUrl.toString()))

                UserApiClient.verifyThirdPartyId(RequestVerifyThirdPartyBody("line", loginResult.lineProfile!!.userId),
                    object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseSimpleFormat) {
                            // 表示後台有Uer用過餓第三方 ID 註冊 or 綁定過
                            if (!response.error && response.have_user) {
                                // 判斷資料庫有無該 facebook UID
                                val paramObject = JsonObject()
                                paramObject.addProperty("providor", "line")
                                paramObject.addProperty("code", loginResult.lineProfile!!.userId)
                                paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

                                UserApiClient.login(
                                    paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                                        override fun onSuccess(response: ResponseLogin) {
                                            runBlocking {
                                                model.appState.setAccessToken(response.access_token)
                                                model.appState.setRefreshToken(response.refresh_token)
                                            }
                                            val action = LoginLineFragmentDirections.actionLoginLineFragmentToMainFragment()
                                            findNavController().navigate(action)
                                        }
                                    }
                                )
                            } else {
                                // 無帳號，註冊
                                findNavController().navigate(LoginLineFragmentDirections.actionLoginLineFragmentToLoginCheckAccountFragment())
                            }
                        }
                    })
            }
            LineApiResponseCode.CANCEL -> {
                loginError()
            }
            LineApiResponseCode.SERVER_ERROR -> {
                loginError()
            }
            LineApiResponseCode.NETWORK_ERROR -> {
                loginError()
            }
            LineApiResponseCode.INTERNAL_ERROR -> {
                loginError()
            }
            LineApiResponseCode.AUTHENTICATION_AGENT_ERROR -> {
                loginError()
            }

            else -> {
                Log.e(logTag, "Login FAILED!")
                Log.e(logTag, loginResult.errorData.toString())
            }
        }
    }

    private fun loginError(){
        model.progressInfo.set(
            ProgreeeInfo("Line 登入失敗", ProgreeeInfo.Type.ERROR)
        )
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
        findNavController().navigateUp()
    }

}