package com.gcreate.jiajia.views.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentUserHistoryBinding
import com.gcreate.jiajia.adapters.tab.UserHistoryTabAdapter
import com.google.android.material.tabs.TabLayoutMediator

class UserHistoryFragment : Fragment(){

    private var binding: FragmentUserHistoryBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserHistoryBinding.inflate(inflater, container, false)


        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.userHistoryToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }

        //TabLayout
        val adapter = UserHistoryTabAdapter(this)
        val tabLayout = binding?.tabLayout
        val viewPager = binding?.viewpager
        viewPager?.isUserInputEnabled = false
        viewPager?.adapter = adapter
        TabLayoutMediator(tabLayout!!, viewPager!!) { tab, position ->
            when (position) {
                0 -> tab.text = "課程"
                1 -> tab.text = "訓練計畫"
            }
        }.attach()

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}