package com.gcreate.jiajia.views.openpoint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentOpenpointBindBinding

class OpenPointBindFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: FragmentOpenpointBindBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentOpenpointBindBinding.inflate(inflater, container, false)
        binding?.apply {
            viewModel = model

            model.progressInfo.set(
                ProgreeeInfo("正在綁定OPEN錢包", ProgreeeInfo.Type.PROGRESS)
            )

            toolbar?.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar?.setNavigationOnClickListener {
                backPressed()
            }

            btnOk.setOnClickListener {
                model.progressInfo.set(
                    ProgreeeInfo("綁定完成", ProgreeeInfo.Type.OK)
                )
            }

            btnFail.setOnClickListener {
                model.progressInfo.set(
                    ProgreeeInfo("綁定沒有完成，請重新操作", ProgreeeInfo.Type.ERROR)
                )
            }

        }


        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}