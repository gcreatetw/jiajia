package com.gcreate.jiajia.views.video

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.vimeo.VimeoApiClient2
import com.gcreate.jiajia.api.vimeo.response.SingleVideoResponse
import com.gcreate.jiajia.databinding.ActivityVimeoVideoBinding
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.norulab.exofullscreen.preparePlayer
import com.norulab.exofullscreen.setSource
import com.vimeo.networking.Configuration
import com.vimeo.networking.VimeoClient
import retrofit2.Response

class VimeoVideoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVimeoVideoBinding
    private lateinit var player: SimpleExoPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        binding = ActivityVimeoVideoBinding.inflate(layoutInflater)

        binding.videoPlayToolbar.setNavigationOnClickListener() {
            finish()
        }

        setContentView(binding.root)
        configVimeoClient()

        player = SimpleExoPlayer.Builder(this).build()
        player.preparePlayer(binding.videoView, true)
        player.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(state: Int) {
                super.onPlaybackStateChanged(state)
                // 影片加載完就播放
                if (state == Player.STATE_READY) {
                    player.play()
                }
            }
        })
        callVimeoAPIRequest()
    }


    private fun configVimeoClient() {
        val configBuilder = Configuration.Builder(getString(R.string.vimeo_token)).setCacheDirectory(this.cacheDir)
        VimeoClient.initialize(configBuilder.build())
    }

    private fun callVimeoAPIRequest() {
        VimeoApiClient2.getVideoData(
            intent.getStringExtra("accessToken").toString(),
            intent.getStringExtra("videoId").toString(),
            object : ApiController<SingleVideoResponse>(this@VimeoVideoActivity, false) {
                override fun onSuccess(response: SingleVideoResponse) {
                    val videoFiles = response.files
                    if (!videoFiles.isNullOrEmpty()) {
                        for (item in videoFiles) {
                            if (item.rendition == "1080p") {
                                player.setSource(applicationContext, item.link)
                                break
                            } else if (item.rendition == "720p") {
                                player.setSource(applicationContext, item.link)
                                break
                            } else if (item.rendition == "540p") {
                                player.setSource(applicationContext, item.link)
                                break
                            } else if (item.rendition == "360p") {
                                player.setSource(applicationContext, item.link)
                                break
                            }
                        }
                    }
                }

                override fun onFail(httpResponse: Response<SingleVideoResponse>): Boolean {
                    Log.d("ben","onFail")
                    return super.onFail(httpResponse)
                }
            })
    }

    // ---------------------- 橫屏不重新加載activity ----------------------
    override fun onConfigurationChanged(newConfig: android.content.res.Configuration) {
        super.onConfigurationChanged(newConfig)
        val c = resources.configuration
        if (c.orientation == android.content.res.Configuration.ORIENTATION_PORTRAIT) {
            binding.videoPlayToolbar.visibility = View.VISIBLE
        } else if (c.orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
            binding.videoPlayToolbar.visibility = View.GONE
        }
    }

    override fun onPause() {
        super.onPause()
        player.pause()
    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
    }
}