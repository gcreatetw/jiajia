package com.gcreate.jiajia.views.receipt

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseInvoiceResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.Receipt
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentReceiptBinding

class ReceiptFragment : Fragment() {

    private var binding: FragmentReceiptBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentReceiptBinding.inflate(inflater, container, false)

        getInvoice()

        binding?.apply {

            toolbar.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }
        }
        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun getInvoice() {

        UserApiClient.getInvoice(model.appState.accessToken.get()!!, object : ApiController<ResponseInvoiceResult>(requireContext(), false) {
            override fun onSuccess(response: ResponseInvoiceResult) {
                val company = Receipt.Company(
                    response.user_data.invoice_tax_company,
                    response.user_data.invoice_tax_number,
                    response.user_data.invoice_tax_email
                )
                model.invoiceResult = response
                model.receipt.company = company

                binding?.apply {
                    layoutEr.setOnClickListener {
                        // 電子發票
                        val action = ReceiptFragmentDirections.actionReceiptFragmentToReceiptErFragment()
                        findNavController().navigate(action)
                    }
                    layoutCompany.setOnClickListener {
                        // 三聯式
                        val action = ReceiptFragmentDirections.actionReceiptFragmentToReceiptCompanyFragment()
                        findNavController().navigate(action)
                    }
                    layoutDonate.setOnClickListener {
                        // 捐贈
                        val action = ReceiptFragmentDirections.actionReceiptFragmentToReceiptDonateFragment()
                        findNavController().navigate(action)
                    }
                }
            }
        })
    }
}