package com.gcreate.jiajia.views.user

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserLoginBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.views.login.LoginAllinOneFragmentType
import com.gcreate.jiajia.views.signupotp.SignupOtpType
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import retrofit2.Response

class UserLoginFragment : Fragment() {

    private var binding: FragmentUserLoginBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserLoginBinding.inflate(inflater, container, false)

        binding?.apply {
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            tvForgetPassword.apply {
                val spannableString = SpannableString("您是否忘記密碼?")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(colorSpan, 3, 7, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString

                setOnClickListener {
                    val action = UserLoginFragmentDirections.actionUserLoginFragmentToSignupOtpFragment()
                    action.mode = SignupOtpType.ForgetPassword
                    findNavController().navigate(action)
                }
            }

            btnLogin.setOnClickListener {

                StorageDataMaintain.saveSingUpResponse(requireContext(),
                    ThirdLoginResponse("registeredLogin", "", "", ""))

                val paramObject = JsonObject()
                paramObject.addProperty("mobile", textInputLayoutMobile.editText?.text.toString())
                paramObject.addProperty("password", textInputLayoutPassword.editText?.text.toString())
                paramObject.addProperty("name", "")
                paramObject.addProperty("providor", "login")
                paramObject.addProperty("code", "")
                paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

                UserApiClient.login(
                    paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                        override fun onSuccess(response: ResponseLogin) {
                            runBlocking {
                                model.appState.setAccessToken(response.access_token)
                                model.appState.setRefreshToken(response.refresh_token)
                            }

                            val action = UserLoginFragmentDirections.actionUserLoginFragmentToMainFragment()
                            findNavController().navigate(action)
                        }

                        override fun onFail(httpResponse: Response<ResponseLogin>): Boolean {
                            Toast.makeText(requireContext(), "登入失敗！", Toast.LENGTH_LONG).show()
                            return true
                        }
                    }
                )
            }

            tvRegister.setOnClickListener {
                StorageDataMaintain.saveSingUpResponse(requireContext(),
                    ThirdLoginResponse("registeredLogin", "", "", ""))
                val action = UserLoginFragmentDirections.actionUserLoginFragmentToSignupOtpFragment()
                findNavController().navigate(action)
            }
        }

        return binding?.root
    }

    private fun backPressed() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
        findNavController().navigateUp()
    }
}