package com.gcreate.jiajia.views

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.VideoTypeRecyclerViewAdapter
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentVideoBinding
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Response

class VideoFragment : Fragment() {

    private var binding: FragmentVideoBinding? = null
    private var dataList = mutableListOf<VideoTypeItem>()
    private var catCount = 0

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (model.isLogined()) {
            model.loadFollowState(requireContext())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentVideoBinding.inflate(inflater, container, false)

        val mAdapter = VideoTypeRecyclerViewAdapter(requireContext(), this::onClickMore, this::onClickClass)

        setHasOptionsMenu(true)
        loadDataFromApi()

        binding!!.apply {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                adapter = mAdapter
            }

            refreshLayout.apply {
                setOnRefreshListener {
                    refreshLayout.isRefreshing = false
                    if (catCount == 0) {
                        loadDataFromApi()
                    }

                }
            }
        }

        return binding?.root
    }

    private fun loadDataFromApi() {
        model.loadFollowState(requireContext())
        dataList.clear()
        CourseApiClient.category(object : ApiController<ResponseCategory>(requireContext()) {
            override fun onSuccess(response: ResponseCategory) {
                catCount = response.category.size
                for (category in response.category) {
                    dataList.add(VideoType(
                        category.id, category.title, mutableListOf()
                    ))
                }
                dataList.add(VideoTypeTail())

                if (dataList.isNotEmpty()) {
                    for (i in dataList.indices) {
                        try {
                            runBlocking {
                                launch {
                                    loadCourseFromApi(i)
                                }
                            }
                        } catch (e: Exception) {
                            binding!!.refreshLayout.isRefreshing = false
                        }
                    }
                }
            }
        })
    }

    private fun loadCourseFromApi(index: Int) {


        if (dataList[index] is VideoTypeTail) {
            binding?.list = dataList
            return
        }

        val item = dataList[index] as VideoType

        val request = RequestCourses(
            "${item.id}", null, "全部",
            listOf(), listOf(), listOf(), 12, 1, null, null)

        CourseApiClient.courses(model.appState.accessToken.get()!!, request, object : ApiController<ResponseCourses>(requireContext(), false) {
            override fun onSuccess(response: ResponseCourses) {
                item.classList = response.courses
                binding?.recyclerView!!.adapter!!.notifyItemChanged(index)
                if (index == dataList.size - 2) {
                    binding!!.refreshLayout.isEnabled = true
                }
                catCount--

            }

            override fun onFail(httpResponse: Response<ResponseCourses>): Boolean {
                binding!!.refreshLayout.isRefreshing = false
                binding!!.refreshLayout.isEnabled = true
                return super.onFail(httpResponse)
            }
        })
    }

    private fun onClickMore(position: Int) {
        if (model.isLogined()) {
            val action = MainFragmentDirections.actionMainFragmentToVideoSortFragment()
            val categoryId = (dataList[position] as VideoType).id
            action.title = (dataList[position] as VideoType).typeName
            action.categoryId = categoryId
            action.isFollowed = model.isFollowCategory(categoryId)
            (activity as MainActivity).rootNavController.navigate(action)
        } else {
            val action = MainFragmentDirections.actionMainFragmentToLoginAllinOneFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }
    }

    private fun onClickClass(typeIndex: Int, classIndex: Int) {
        if ((dataList[typeIndex] as VideoType).classList[classIndex].videoEnable) {
            val action = MainFragmentDirections.actionMainFragmentToVideoPageFragment()
            action.courseId = (dataList[typeIndex] as VideoType).classList[classIndex].id
            (activity as MainActivity).rootNavController.navigate(action)
        }
    }

    override fun onStart() {
        super.onStart()

        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setTitleTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        (requireActivity() as AppCompatActivity?)!!.supportActionBar!!.apply {
            setDisplayShowTitleEnabled(true)
            title = "課程分類"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.class_toolbar_menu, menu)
        menu.findItem(R.id.class_search_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.class_search_item -> {
                val action = MainFragmentDirections.actionMainFragmentToSearchFragment()
                (activity as MainActivity).rootNavController.navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}