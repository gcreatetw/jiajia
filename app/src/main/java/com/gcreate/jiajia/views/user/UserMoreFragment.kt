package com.gcreate.jiajia.views.user

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.BuildConfig
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserMoreBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.util.GoogleUtil
import com.gcreate.jiajia.views.login.LoginAllinOneFragmentType
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.runBlocking


class UserMoreFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: FragmentUserMoreBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        binding = FragmentUserMoreBinding.inflate(inflater, container, false)
        binding?.apply {

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            tvLabelFaq.setOnClickListener {
                val action = UserMoreFragmentDirections.actionUserMoreFragmentToUserMoreWebviewFragment()
                action.title = tvLabelFaq.text.toString()
                action.url = model.userMore!!.faq
                findNavController().navigate(action)
            }

            tvLabelAbout.setOnClickListener {
                val action = UserMoreFragmentDirections.actionUserMoreFragmentToUserMoreWebviewFragment()
                action.title = tvLabelAbout.text.toString()
                action.url = model.userMore!!.about
                findNavController().navigate(action)
            }

            tvLabelEula.setOnClickListener {
                val action = UserMoreFragmentDirections.actionUserMoreFragmentToUserMoreWebviewFragment()
                action.title = tvLabelEula.text.toString()
                action.url = model.userMore!!.terms_condition
                findNavController().navigate(action)
            }

            tvLabelPrivacy.setOnClickListener {
                val action = UserMoreFragmentDirections.actionUserMoreFragmentToUserMoreWebviewFragment()
                action.title = tvLabelPrivacy.text.toString()
                action.url = model.userMore!!.privacy_policy
                findNavController().navigate(action)
            }

            tvLabelLogout.setOnClickListener {
                // 登出
                val builder = AlertDialog.Builder(requireActivity())
                builder.apply {
                    setTitle("登出")
                    setMessage("請問是否登出BEING?")
                    setPositiveButton("確定") { _: DialogInterface?, _: Int ->
                        UserApiClient.logout(model.appState.accessToken.get()!!,
                            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    if (!response.error) {
                                        runBlocking {
                                            model.appState.setAccessToken("")
                                            model.appState.setRefreshToken("")
                                            model.appState.setHaveSubscription(false)
                                            model.appState.setPayMethod("cathaybk")

                                            model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect

                                            if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "google") {
                                                GoogleUtil.signOut()
                                            } else if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "facebook") {
                                                Firebase.auth.signOut()
                                            }

                                            val dataList = ThirdLoginResponse("", "", "", "")
                                            StorageDataMaintain.saveSingUpResponse(requireContext(), dataList)

                                            val action = UserMoreFragmentDirections.actionUserMoreFragmentToLoginAllinOneFragment()
                                            findNavController().navigate(action)

                                            model.me.set(User(
                                                "",
                                                "",
                                                "",
                                                false,
                                                Subscription.Type.UnRegistered,
                                                "",
                                                "",
                                                "",
                                                "",
                                                0.0,
                                                "",
                                                "",
                                                "",
                                                "",
                                                MyHobby(
                                                    "",
                                                    1,
                                                    "1990-01-01",
                                                    160,
                                                    60,
                                                    1,
                                                    1,
                                                    1,
                                                )
                                            ))
                                        }
                                    }
                                }
                            })
                    }
                    setNegativeButton("取消") { dialog, _ -> dialog.dismiss() }
                    setCancelable(false)
                }

                val alertDialog = builder.create()
                alertDialog.show()

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
            }


            tvVersion.text = "目前版本：${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})"
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}