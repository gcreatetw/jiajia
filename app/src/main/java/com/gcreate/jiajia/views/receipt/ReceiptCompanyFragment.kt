package com.gcreate.jiajia.views.receipt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestInvoice
import com.gcreate.jiajia.api.user.response.ResponseInvoice
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.Receipt
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentReceiptCompanyBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain

class ReceiptCompanyFragment : Fragment() {

    private var binding: FragmentReceiptCompanyBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentReceiptCompanyBinding.inflate(inflater, container, false)

        binding?.apply {

            viewModel = model

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }


            if (StorageDataMaintain.getInvoiceType(requireContext()) == 3) {
                model.receipt.defaultType = Receipt.Default.Company
            }
            swDefault.setOnCheckedChangeListener { _, b ->
                if (b) {
                    val companyTitleTxt = etName.text.toString()
                    val companyUniformNumberTxt = etUniformNumber.text.toString()
                    val companyEmailTxt = etEmail.text.toString()

                    if (companyTitleTxt.isBlank() || companyUniformNumberTxt.isBlank() || companyEmailTxt.isBlank()) {
                        Toast.makeText(requireActivity(), "欄位不得空白", Toast.LENGTH_SHORT).show()
                    } else {
                        model.receipt.defaultType = Receipt.Default.Company
                        setInvoice()
                        StorageDataMaintain.saveInvoiceType(requireContext(), 3)
                    }

                } else {
                    model.receipt.defaultType = Receipt.Default.Nono
                }
            }
        }
        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    override fun onStop() {
        super.onStop()
        binding?.apply {
            val company = Receipt.Company(
                etName.text.toString(),
                etUniformNumber.text.toString(),
                etEmail.text.toString()
            )
            model.receipt.company = company
        }
    }

    private fun setInvoice() {
        val request = RequestInvoice(
            "",
            binding!!.etName.text.toString(),
            binding!!.etUniformNumber.text.toString(),
            binding!!.etEmail.text.toString(),
            "",
            "",
            "",
            "",
            3)
        UserApiClient.setInvoice(model.appState.accessToken.get()!!,
            request,
            object : ApiController<ResponseInvoice>(requireContext(), false) {
                override fun onSuccess(response: ResponseInvoice) {
                }
            })
    }

}