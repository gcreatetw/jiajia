package com.gcreate.jiajia.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginEulaBinding
import kotlinx.coroutines.runBlocking

class LoginEulaFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentLoginEulaBinding.inflate(inflater, container, false)

        binding.apply {
            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            // 服務條款
            webView.loadUrl("https://testapp.being.com.tw/terms_condition")

            checkBox.setOnClickListener {
                runBlocking {
                    model.appState.setEulaAgreed(checkBox.isChecked)
                }
            }

            tvNext.setOnClickListener {
                if (checkBox.isChecked) {
                    backPressed()
                }
            }
        }

        return binding.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}