package com.gcreate.jiajia.views.user.setting

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseAvailableRecord
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingSubscriptionItemBinding
import com.gcreate.jiajia.adapters.membercentre.subcription.AvailableRecordListRecyclerViewAdapter
import retrofit2.Response

class UserSettingSubscriptionItemFragment : Fragment() {

    private lateinit var binding: FragmentUserSettingSubscriptionItemBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserSettingSubscriptionItemBinding.inflate(inflater, container, false)

        loadDataFromAPI()

        binding.apply {

            vm = model

            //設定返回鍵顏色及點擊事件
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireContext(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            recyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = AvailableRecordListRecyclerViewAdapter {

            }

            tvUnsubscribe.setOnClickListener {
                // 取消訂閱
                val action =
                    UserSettingSubscriptionItemFragmentDirections.actionUserSettingSubscriptionItemFragmentToUserSettingUnsubscribeFragment()
                findNavController().navigate(action)
                //userCancelRecord()
            }

            tvSubscribe.setOnClickListener {
                // 重新訂閱
                if (binding.tvStates.text.equals("已過期")) {
                    val action =
                        UserSettingSubscriptionItemFragmentDirections.actionUserSettingSubscriptionItemFragmentToPaymentFrgment()
                    findNavController().navigate(action)
                } else {
                    userReRecord()
                }
            }

//            val payMethod = model.userInfo?.user?.payment_method
//
//            if (payMethod == null || payMethod == "openwallet") {
//                tvCancelSubscribe.visibility = View.INVISIBLE
//            } else {
//                if (payMethod == "icashpay") {
//                    tvCancelSubscribe.text = "解除綁定ICashPay"
//                } else if (payMethod == "cathaybk") {
//                    tvCancelSubscribe.text = "解除綁定cathaybk"
//                }
//            }

            // 取消綁定 ICP
            tvCancelSubscribe.setOnClickListener {
                val token = model.appState.accessToken.get()!!
                val builder = AlertDialog.Builder(requireActivity())
                builder.apply {
                    setMessage("是否確定要解綁此支付方式?")
                    setPositiveButton("確定") { _: DialogInterface?, _: Int ->
                        // cancel ICP
                        UserApiClient.cancelICPBinding(token, object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                            override fun onSuccess(response: ResponseSimpleFormat2) {
                                when (response.error) {
                                    0 -> {
                                        Toast.makeText(requireActivity(), "解除成功", Toast.LENGTH_SHORT).show()
                                        tvCancelSubscribe.visibility = View.GONE
                                    }
                                    1 -> Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFail(httpResponse: Response<ResponseSimpleFormat2>): Boolean {
                                Toast.makeText(requireActivity(), "解除失敗", Toast.LENGTH_SHORT).show()
                                return super.onFail(httpResponse)
                            }
                        })


                    }
                    setNegativeButton("取消") { dialog, _ -> dialog.dismiss() }
                    setCancelable(false)
                }

                val alertDialog = builder.create()
                alertDialog.show()
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
            }

            // 取消綁定 Cathy
            tvCancelCathaySubscribe.setOnClickListener {
                val token = model.appState.accessToken.get()!!
                val builder = AlertDialog.Builder(requireActivity())
                builder.apply {
                    setMessage("是否確定要解綁此支付方式?")
                    setPositiveButton("確定") { _: DialogInterface?, _: Int ->
                        // cancel cathaybk
                        UserApiClient.cancelCathaybkBinding(token, object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                            override fun onSuccess(response: ResponseSimpleFormat2) {
                                when (response.error) {
                                    0 -> {
                                        Toast.makeText(requireActivity(), "解除成功", Toast.LENGTH_SHORT).show()
                                        tvCancelCathaySubscribe.visibility = View.GONE

                                    }
                                    1 -> Toast.makeText(requireActivity(), response.message, Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFail(httpResponse: Response<ResponseSimpleFormat2>): Boolean {
                                Toast.makeText(requireActivity(), "解除失敗", Toast.LENGTH_SHORT).show()
                                return super.onFail(httpResponse)
                            }
                        })
                    }
                    setNegativeButton("取消") { dialog, _ -> dialog.dismiss() }
                    setCancelable(false)
                }

                val alertDialog = builder.create()
                alertDialog.show()
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
            }
        }

        return binding.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    private fun loadDataFromAPI() {
        UserApiClient.getAvailableRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseAvailableRecord>(requireActivity(), false) {
                override fun onSuccess(response: ResponseAvailableRecord) {
                    val recordList = response.order
                    binding.list = recordList

                    when (response.status) {
                        "cancel" -> {
                            binding.tvStates.text = "已啟用"
                            binding.lyUnsubscribe.visibility = View.INVISIBLE
                            binding.tvSubscribe.visibility = View.VISIBLE
                        }
                        "unexpired" -> {
                            binding.tvStates.text = "已啟用"
                            binding.lyUnsubscribe.visibility = View.VISIBLE
                            binding.tvSubscribe.visibility = View.INVISIBLE
                        }
                        "expired" -> {
                            binding.tvStates.text = "已過期"
                            binding.lyUnsubscribe.visibility = View.INVISIBLE
                            binding.tvSubscribe.visibility = View.VISIBLE
                        }
                        "non" -> {
                            binding.tvStates.text = "尚未購買方案"
                            binding.lyUnsubscribe.visibility = View.INVISIBLE
                            binding.tvSubscribe.visibility = View.INVISIBLE
                        }
                    }

                    if (response.icashpay_token) {
                        binding.tvCancelSubscribe.visibility = View.VISIBLE
                    } else {
                        binding.tvCancelSubscribe.visibility = View.GONE
                    }

                    if (response.cathay_card_id) {
                        binding.tvCancelCathaySubscribe.visibility = View.VISIBLE
                    } else {
                        binding.tvCancelCathaySubscribe.visibility = View.GONE
                    }

//                    if (response.icashpay_token || response.cathay_card_id) {
//                        binding.tvCancelSubscribe.visibility = View.VISIBLE
//
//                    }else{
//                        binding.tvCancelSubscribe.visibility = View.INVISIBLE
//                    }
                }
            })
    }


    private fun userReRecord() {
        UserApiClient.setUserReRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    loadDataFromAPI()
                }
            })
    }
}