package com.gcreate.jiajia.views.articlelist

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R

class EventFragment : Fragment() {


    private val args: EventFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_event, container, false)

        val toolBar = view.findViewById<androidx.appcompat.widget.Toolbar>(R.id.article_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        toolBar.setNavigationOnClickListener {
            backPressed()
        }


        var webView = view.findViewById<WebView>(R.id.web_view)

        webView.settings.apply {
          setSupportZoom(true)
            builtInZoomControls = true
            displayZoomControls = true
            blockNetworkImage = false
            javaScriptEnabled = true
            databaseEnabled = true
            domStorageEnabled = true
            cacheMode = WebSettings.LOAD_NO_CACHE
            useWideViewPort = false
            userAgentString = "Android"
        }

        webView.loadUrl(args.url!!)

        return view
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}