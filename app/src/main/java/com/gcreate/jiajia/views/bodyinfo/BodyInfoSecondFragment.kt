package com.gcreate.jiajia.views.bodyinfo

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentBodyInfoSecondBinding
import java.text.DecimalFormat
import kotlin.math.pow


class BodyInfoSecondFragment : Fragment(), NumberPickerFragment.OnDialogButtonFragmentListener {


    private lateinit var binding: FragmentBodyInfoSecondBinding


    override fun onStart() {
        super.onStart()
        //設定返回鍵顏色
        val toolBar = view?.findViewById<Toolbar>(R.id.body_info_second_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "下一頁"
        toolBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        //設定seekbar背景
        val seekBar = binding.bodyInfoSecondSeekBar
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT, intArrayOf(
                ContextCompat.getColor(requireContext(), R.color.gradient1),
                ContextCompat.getColor(requireContext(), R.color.gradient2),
                ContextCompat.getColor(requireContext(), R.color.gradient3),
                ContextCompat.getColor(requireContext(), R.color.gradient4),
            )
        )
        seekBar.progressDrawable = gradientDrawable
        seekBar.isEnabled = true
        //設定文字跟著seekbar走
        val yourBMI = binding.bodyInfoSecondBMI
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
//                yourBMI.x = seekBar.thumb.bounds.left.toFloat()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentBodyInfoSecondBinding.inflate(inflater, container, false)

        calculateBMI(BodyInfoFragment.userHeight, BodyInfoFragment.userWeight)

        binding.bodyInfoSecondHeightButton.apply {
            val textString = "${BodyInfoFragment.userHeight}CM"
            text = textString
            setOnClickListener {
                val bottomSheetFragment = NumberPickerFragment("height")
                bottomSheetFragment.listener = this@BodyInfoSecondFragment
                bottomSheetFragment.show(requireActivity().supportFragmentManager, "settingHeight")
            }
        }

        binding.bodyInfoSecondWeightButton.apply {
            val textString = "${BodyInfoFragment.userWeight}KG"
            text = textString
            setOnClickListener {
                val bottomSheetFragment = NumberPickerFragment("weight")
                bottomSheetFragment.listener = this@BodyInfoSecondFragment
                bottomSheetFragment.show(requireActivity().supportFragmentManager, "settingWeight")
            }
        }

        return binding.root
    }


    override fun onSelectDialog(select: String, value: Int) {
        when (select) {
            "HeightCheck" -> {
                val textString = "${value}CM"
                BodyInfoFragment.userHeight = value
                binding.bodyInfoSecondHeightButton.text = textString
            }
            "WeightCheck" -> {
                val textString = "${value}KG"
                BodyInfoFragment.userWeight = value
                binding.bodyInfoSecondWeightButton.text = textString
            }
        }
        calculateBMI(BodyInfoFragment.userHeight, BodyInfoFragment.userWeight)
    }

    private fun calculateBMI(height: Int, weight: Int) {
        val BMI = (weight / (height.toDouble()/100).pow(2))
        val stringFormat = DecimalFormat(".0")
        val textString = "你的BMI:${stringFormat.format(BMI)}"
        binding.bodyInfoSecondBMI.text = textString
        binding.bodyInfoSecondSeekBar.setProgress(BMI.toInt(), true)
    }

}