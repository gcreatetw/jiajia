package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R


class BodyInfoThirdFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_body_info_third, container, false)
    }

    override fun onStart() {
        super.onStart()
        //設定返回鍵顏色
        val toolBar = view?.findViewById<Toolbar>(R.id.body_info_third_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "下一頁"
        toolBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        //設定按鈕
        val lowButton = view?.findViewById<Button>(R.id.body_info_third_low_button)
        lowButton?.text = Html.fromHtml("<b>低度</b>" + "<br/>" + "無運動經驗")
        val midButton = view?.findViewById<Button>(R.id.body_info_third_mid_button)
        midButton?.text = Html.fromHtml("<b>中度</b>" + "<br/>" + "有時運動")
        val highButton = view?.findViewById<Button>(R.id.body_info_third_high_button)
        highButton?.text = Html.fromHtml("<b>強度</b>" + "<br/>" + "運動狂熱者")
        //選擇運動習慣
        when (BodyInfoFragment.fitness_level) {
            1 -> lowButton!!.isSelected = true
            2 -> midButton!!.isSelected = true
            3 -> highButton!!.isSelected = true
        }

        lowButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_level = 1
                if (!this.isSelected) {
                    this.isSelected = true
                    midButton?.isSelected = false
                    highButton?.isSelected = false
                }
            }
        }
        midButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_level = 2
                if (!this.isSelected) {
                    this.isSelected = true
                    lowButton?.isSelected = false
                    highButton?.isSelected = false
                }
            }
        }
        highButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_level = 3
                if (!this.isSelected) {
                    this.isSelected = true
                    midButton?.isSelected = false
                    lowButton?.isSelected = false
                }
            }
        }
    }

}