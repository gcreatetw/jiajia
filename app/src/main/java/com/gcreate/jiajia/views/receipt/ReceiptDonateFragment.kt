package com.gcreate.jiajia.views.receipt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestInvoice
import com.gcreate.jiajia.api.user.response.ResponseDonateMechanism
import com.gcreate.jiajia.api.user.response.ResponseInvoice
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.Receipt
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentReceiptDonateBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.util.initRadioGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ReceiptDonateFragment : Fragment() {

    private var binding: FragmentReceiptDonateBinding? = null

    companion object {
        private val donateMechanismNameList = mutableListOf<String>()
        private val donateMechanismNumberList = mutableListOf<Int>()
        private var donateMechanismNumber = "0"
    }

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentReceiptDonateBinding.inflate(inflater, container, false)
        binding?.apply {

            viewModel = model

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            setDonateMechanism(inflater)

            radioGroup.setOnCheckedChangeListener { radioGroup, i ->
                if (i >= 0) {
                    etCode.text.clear()
                }
                donateMechanismNumber = donateMechanismNumberList[i].toString()
            }

            etCode.doOnTextChanged { text, start, before, count ->
                if (text?.isNotEmpty() == true) {
                    radioGroup.clearCheck()
                }
                donateMechanismNumber = etCode.text.toString()
            }

            if (model.invoiceResult!!.user_data.invoice_type_default == "4") {
                model.receipt.defaultType = Receipt.Default.Donate
            }

            swDefault.setOnCheckedChangeListener { compoundButton, b ->
                if (b) {
                    if (donateMechanismNumber != "0"){
                        model.receipt.defaultType = Receipt.Default.Donate
                        setInvoice()
//                        StorageDataMaintain.saveInvoiceType(requireContext(), 4)
                    }else{
                        Toast.makeText(requireActivity(),"請選擇捐贈機構",Toast.LENGTH_SHORT).show()
                        swDefault.isChecked = false
                    }
                } else {
                    model.receipt.defaultType = Receipt.Default.Nono
                }
            }
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun setDonateMechanism(inflater: LayoutInflater) {
        val storeDonateMechanismString = model.appState.donateMechanism.get()!!
        val gson = Gson()
        val arraysData =
            gson.fromJson<ResponseDonateMechanism>(storeDonateMechanismString, object : TypeToken<ResponseDonateMechanism>() {}.type).data

        if (donateMechanismNameList.isEmpty()) {
            for (i in arraysData.indices) {
                donateMechanismNameList.add(arraysData[i].name)
                donateMechanismNumberList.add(arraysData[i].code)
            }
        }

        initRadioGroup(inflater, binding!!.radioGroup, R.layout.card_receipt_donate_radio_button, donateMechanismNameList)
        if (model.receipt.donate.index >= 0) {
            binding!!.radioGroup.check(model.receipt.donate.index)
        }
    }

    override fun onStop() {
        super.onStop()
        binding?.apply {

            model.receipt.donate.index = radioGroup.checkedRadioButtonId

            model.receipt.donate.code = etCode.text.toString()

        }
    }

    private fun setInvoice() {

        val request = RequestInvoice(
            "",
            "",
            "",
            "",
            donateMechanismNumber,
            "",
            "",
            "",
            4)
        UserApiClient.setInvoice(model.appState.accessToken.get()!!,
            request,
            object : ApiController<ResponseInvoice>(requireContext(), false) {
                override fun onSuccess(response: ResponseInvoice) {
                }
            })
    }

}