package com.gcreate.jiajia.views.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.HomeCourseRecommendAdapter
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.CourseApiClient.courses
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseUserRecommendCourse
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentHomeCourseRecommendBinding
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentType
import com.gcreate.jiajia.views.payment.PaymentFragment.Companion.navController
import retrofit2.Response


class HomeCourseRecommendFragment : Fragment() {

    private lateinit var binding: FragmentHomeCourseRecommendBinding
    private val mAdapter = HomeCourseRecommendAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeCourseRecommendBinding.inflate(inflater, container, false)

        binding.rvHomeCourseRecommend.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
            if (model.appState.accessToken.get()!!.isEmpty()) {
                mAdapter.apply {
                    setEmptyView(R.layout.layout_no_login)
                    emptyLayout?.findViewById<Button>(R.id.btn_ok)?.apply {
                        text = "立即登入/註冊"
                        setOnClickListener {
                            val action = MainFragmentDirections.actionMainFragmentToLoginAllinOneFragment()
                            (activity as MainActivity).rootNavController.navigate(action)
                        }
                    }
                }
            } else {
                if (model.userInfo!!.user.gender.isNullOrEmpty()) {
                    mAdapter.apply {
                        setEmptyView(R.layout.layout_no_login)
                        emptyLayout?.findViewById<Button>(R.id.btn_ok)?.apply {
                            text = "請填寫個人喜好"
                            setOnClickListener {
                                val action = MainFragmentDirections.actionMainFragmentToBodyInfoFragment()
                                action.mode = BodyInfoFragmentType.Main
                                (activity as MainActivity).rootNavController.navigate(action)
                            }
                        }
                    }
                } else {
                    getRecommendCourse()
                    mAdapter.apply {
                        setEmptyView(R.layout.layout_no_data)
                        setOnItemClickListener { adapter, view, position ->
                            val action = MainFragmentDirections.actionMainFragmentToVideoPageFragment()
                            action.courseId = data[position].id
                            (activity as MainActivity).rootNavController.navigate(action)
                        }
                    }
                }
            }
        }

        binding.refreshLayout.apply {
            setOnRefreshListener { getRecommendCourse() }
        }
        return binding.root
    }

    private fun getRecommendCourse() {
        val token = model.appState.accessToken.get()!!

        UserApiClient.getUserRecommendCourse(token, object : ApiController<ResponseUserRecommendCourse>(binding.root.context, false) {
            override fun onSuccess(response: ResponseUserRecommendCourse) {
                mAdapter.data = response.courses
                mAdapter.notifyDataSetChanged()
                binding.refreshLayout.isRefreshing = false
            }
        })
    }

}