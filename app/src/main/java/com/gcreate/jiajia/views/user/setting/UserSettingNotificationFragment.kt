package com.gcreate.jiajia.views.user.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestUserPusher
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.NotificationSetting
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingNotificationBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain

class UserSettingNotificationFragment : Fragment() {

    private var binding: FragmentUserSettingNotificationBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserSettingNotificationBinding.inflate(inflater, container, false)
        binding?.viewModel = model

        binding?.apply {
            toolbar.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            val setting = model.notificationSetting.get()
            swNews.setOnCheckedChangeListener { compoundButton, boolean ->
                setting?.news = boolean
                model.notificationSetting.set(setting)
            }
            swCoach.setOnCheckedChangeListener { compoundButton, boolean ->
                setting?.coach = boolean
                model.notificationSetting.set(setting)
            }
            swType.setOnCheckedChangeListener { compoundButton, boolean ->
                setting?.type = boolean
                model.notificationSetting.set(setting)
            }
            swSchedule.setOnCheckedChangeListener { compoundButton, boolean ->
                setting?.schedule = boolean
                model.notificationSetting.set(setting)
            }
        }

        return binding?.root
    }

    private fun backPressed() {
        saveNotificationSetting()
        findNavController().navigateUp()
    }

    override fun onStop() {
        super.onStop()
        saveNotificationSetting()
    }

    private fun saveNotificationSetting() {
        val notificationSetting = model.notificationSetting.get()!!
        val dataList = mutableListOf<Boolean>()
        dataList.add(0, notificationSetting.getNotificationSettingNews())
        dataList.add(1, notificationSetting.getNotificationSettingCoach())
        dataList.add(2, notificationSetting.getNotificationSettingType())
        dataList.add(3, notificationSetting.getNotificationSettingSchedule())
        StorageDataMaintain.savePusherList(requireContext(), dataList)
        setUserPusher(notificationSetting)
    }

    private fun setUserPusher(notificationSetting: NotificationSetting) {
        val requestBody = RequestUserPusher(
            news = if (notificationSetting.getNotificationSettingNews()) 1
            else 0,

            instructor = if (notificationSetting.getNotificationSettingCoach()) 1
            else 0,

            category = if (notificationSetting.getNotificationSettingType()) 1
            else 0,

            calendar = if (notificationSetting.getNotificationSettingSchedule()) 1
            else 0
        )
        UserApiClient.setUserPusher(model.appState.accessToken.get()!!, requestBody,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {

                }
            })
    }
}