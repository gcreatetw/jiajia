package com.gcreate.jiajia.views.articlelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.tab.ArticleListTabAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ArticleListFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_article_list, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = view.findViewById<androidx.appcompat.widget.Toolbar>(R.id.article_list_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        toolBar.setNavigationOnClickListener {
            backPressed()
        }

        //TabLayout
        val adapter = ArticleListTabAdapter(activity as AppCompatActivity)
        val tabLayout = view.findViewById<TabLayout>(R.id.article_list_tabLayout)
        val viewPager = view.findViewById<ViewPager2>(R.id.article_list_viewpager)
        viewPager?.adapter = adapter
        TabLayoutMediator(tabLayout,viewPager){tab, position ->
            when (position) {
                0 -> tab.text = "活動檔期"
                1 -> tab.text = "運動知識"
            }}.attach()


        return view
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}