package com.gcreate.jiajia.views.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentSearchDetailBinding
import com.gcreate.jiajia.listener.UpdateSearchKeywordListenerManager
import com.gcreate.jiajia.adapters.tab.SearchDetailTabAdapter
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*


class SearchDetailFragment : Fragment() {

    private lateinit var binding: FragmentSearchDetailBinding
    private val arg: SearchDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchDetailBinding.inflate(inflater, container, false)

        initView()

        //TabLayout
        val adapter = SearchDetailTabAdapter(3, this, arg.keyword)
        val tabLayout = binding.tabLayout
        val viewPager = binding.viewpager
        viewPager.isUserInputEnabled = true
        viewPager.adapter = adapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "推薦課程"
                1 -> tab.text = "訓練計畫"
                2 -> tab.text = "直播課程"
            }
        }.attach()

        return binding.root
    }

    private fun initView() {
        binding.searchDetailToolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(binding.searchDetailToolbar)

        binding.searchDetailToolbar.setNavigationOnClickListener {
            backPressed()
        }


        binding.imgCancel.setOnClickListener {
            binding.etSearchDetail.setText("")
            backPressed()
        }


        binding.etSearchDetail.apply {
            setText(arg.keyword)
        }

        binding.etSearchDetail.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                binding.etSearchDetail.clearFocus()
                UpdateSearchKeywordListenerManager.instance!!.sendBroadCast(binding.etSearchDetail.text.toString())
                true
            } else {
                false
            }

        }

    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun backPressed(){
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

}