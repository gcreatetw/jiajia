package com.gcreate.jiajia.views.user.follow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserFollowByCoachBinding
import com.gcreate.jiajia.adapters.membercentre.UserFollowByCoachRecyclerViewAdapter
import kotlinx.android.synthetic.main.card_user_follow_by_type.view.*

class UserFollowByCoachFragment : Fragment() {

    private var binding: FragmentUserFollowByCoachBinding? = null
    private var dataList = mutableListOf<UserFollowByCoachItem>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserFollowByCoachBinding.inflate(inflater, container, false)

        loadCoachFollowState()

        val rv = binding?.recyclerView
        rv?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
//        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f,requireContext()).roundToInt()))
        rv?.adapter = UserFollowByCoachRecyclerViewAdapter() {

            val isFollow = model.listFollow!!.instructor[it].followed
            model.listFollow!!.instructor[it].followed = !isFollow

            val followImg = binding!!.recyclerView.findViewHolderForLayoutPosition(it)!!.itemView.img_follow
            if (model.listFollow!!.instructor[it].followed) {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.baseline_notifications_24))
            } else {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.outline_add_alert_24))
            }

            switchFollowState(model.listFollow!!.instructor[it].id)
        }

        return binding?.root

    }

    private fun loadCoachFollowState() {
        val instructorFollowList = model.listFollow!!.instructor
        for (i in instructorFollowList.indices) {
            val coachCategoryList = instructorFollowList[i].cate
            val coachCategoryString = StringBuffer()
            for (j in coachCategoryList.indices) {
                coachCategoryString.append(coachCategoryList[j].title + " ")
            }

            dataList.add(i, UserFollowByCoach(
                instructorFollowList[i].id,
                instructorFollowList[i].user_img,
                instructorFollowList[i].name,
                coachCategoryString.toString(),
                instructorFollowList[i].detail,
                instructorFollowList[i].followed))
        }
        dataList.add(UserFollowByCoachTail())

        binding?.list = dataList
    }

    private fun switchFollowState(coachId: Int) {
        val request = RequestFollowCategory(coachId.toString(), "", "","")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(requireContext(), false) {
            override fun onSuccess(response: Unit) {
                model.loadFollowState(requireContext())
            }
        })
    }
}