package com.gcreate.jiajia.views.video

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.ScheduleApiClient
import com.gcreate.jiajia.api.schedule.request.RequestAddScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestListScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseAddScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseScheduleList
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentVideoPageAddVideoBinding
import com.gcreate.jiajia.schedule.data.Day
import com.gcreate.jiajia.util.DateTool
import java.util.*

class VideoPageAddVideoFragment : Fragment() {

    private lateinit var binding: FragmentVideoPageAddVideoBinding
    private val arg: VideoPageAddVideoFragmentArgs by navArgs()


    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentVideoPageAddVideoBinding.inflate(inflater, container, false)

        getScheduleList()

        binding.apply {
            //設定返回鍵顏色及點擊事件
            val toolBar = binding?.toolbar
            toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolBar)

            toolBar?.setNavigationOnClickListener {
                backPressed()
            }

            scheduleView.setOnClickDayListener { year, month, day ->
                scheduleView.pickTime(year, month/* 0:1月 */, day) {

                    if (it == null) {
                        // cancel

                    } else {
                        val startDatetime = "$year-${month + 1}-$day ${it.hour}:${it.minute}:00"
                        // ok
                        val date = Date()
                        val calender = java.util.Calendar.getInstance()
                        calender.time = date

                        if (DateTool.DateToLong(startDatetime) < Date().time) {
                            Toast.makeText(requireActivity(), "不能選過去的時間", Toast.LENGTH_SHORT).show()
                        } else {
                            addSchedule(startDatetime)
                        }
                    }
                }
            }
        }

        return binding.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun getScheduleList() {
        val requestListScheduleBody = RequestListScheduleBody("01")
        ScheduleApiClient.listSchedule(model.appState.accessToken.get()!!,
            requestListScheduleBody,
            object : ApiController<ResponseScheduleList>(requireActivity(), false) {
                override fun onSuccess(response: ResponseScheduleList) {
                    val map = response.calendars
                    // 0是一月
                    for (scheduleValue in map.values) {
                        if (scheduleValue.size > 1) {
                            val scheduleList = mutableListOf<Day>()
                            for (courseList in scheduleValue) {
                                val startDate = courseList.start.split(" ")[0]
                                val year = startDate.split("-")[0].toInt()
                                val month = startDate.split("-")[1].toInt() - 1
                                val day = startDate.split("-")[2].toInt()
                                scheduleList.add(Day(year, month, day))
                            }
                            binding.scheduleView.addBook(scheduleList)

                        } else {
                            for (courseList in scheduleValue) {
                                val startDate = courseList.start.split(" ")[0]
                                val year = startDate.split("-")[0].toInt()
                                val month = startDate.split("-")[1].toInt() - 1
                                val day = startDate.split("-")[2].toInt()
                                binding.scheduleView.addBook(Day(year, month, day))
                            }
                        }
                    }
                    binding.scheduleView.adapter!!.notifyDataSetChanged()
                }
            })
    }

    private fun addSchedule(startDatetime: String) {
        val requestBody = RequestAddScheduleBody(arg.bundleId, arg.courseId, startDatetime)

        ScheduleApiClient.addSchedule(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseAddScheduleBody>(requireActivity(), false) {
                override fun onSuccess(response: ResponseAddScheduleBody) {
                    Toast.makeText(requireActivity(), "加入行事曆成功", Toast.LENGTH_SHORT).show()
                    backPressed()
                }

            })
    }

}


