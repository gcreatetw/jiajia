package com.gcreate.jiajia.views.payment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestInvoice
import com.gcreate.jiajia.api.user.response.ResponseInvoice
import com.gcreate.jiajia.api.user.response.ResponseInvoiceResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.Receipt
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentMailVerifyBinding
import com.gcreate.jiajia.util.AES256Util
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentArgs
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentType
import com.gcreate.jiajia.views.receipt.ReceiptFragmentDirections


class PaymentMailVerifyFragment : Fragment() {

    private lateinit var binding: FragmentPaymentMailVerifyBinding
    private val arg: PaymentMailVerifyFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPaymentMailVerifyBinding.inflate(inflater, container, false)

        binding.apply {

            if (arg.mode == BodyInfoFragmentType.Signup) {
                toolbar.navigationIcon = null
            }
            toolbar.setNavigationOnClickListener {
//                getUserInfo()
                findNavController().popBackStack()
            }

//            tvLabelDescSend.apply {
//                val builder = SpannableStringBuilder(tvLabelDescSend.text.toString())
//                builder.setSpan(ForegroundColorSpan(Color.BLUE), 28, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                builder.setSpan(TextClick(model.appState.accessToken.get()!!, binding),
//                    28,
//                    36,
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                movementMethod = LinkMovementMethod.getInstance()
//                text = builder
//            }

            btnVerify.setOnClickListener {
                val email = binding.etEmailInput.text.toString().trim()
                if (email.isNotEmpty()) {
                    if (isEmailValid(email)) {
                        binding.etEmailInput.clearFocus()
                        sendMailVerify()
                    } else {
                        Toast.makeText(requireActivity(), "信箱格式不符", Toast.LENGTH_SHORT).show()
                    }

                } else {
                    Toast.makeText(requireActivity(), "請輸入信箱", Toast.LENGTH_SHORT).show()
                }
            }

            binding.etEmailInput.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    btnVerify.visibility = View.VISIBLE
                }
            }
        }

        return binding.root
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun sendMailVerify() {
        val request = RequestInvoice(
            binding.etEmailInput.text.toString(),
            "",
            "",
            "",
            "",
            "0",
            "",
            "0",
            1
        )

        UserApiClient.setInvoice(
            model.appState.accessToken.get()!!,
            request,
            object : ApiController<ResponseInvoice>(requireContext(), false) {
                override fun onSuccess(response: ResponseInvoice) {
                    if (arg.mode == BodyInfoFragmentType.Signup) {
                        val action =
                            PaymentMailVerifyFragmentDirections.actionPaymentMailVerifyFragmentToBodyInfoFragment()
                        action.mode = BodyInfoFragmentType.Signup
                        findNavController().navigate(action)
                    } else {

                        if (model.invoiceResult == null) {
                            UserApiClient.getInvoice(
                                model.appState.accessToken.get()!!,
                                object :
                                    ApiController<ResponseInvoiceResult>(requireContext(), false) {
                                    override fun onSuccess(response: ResponseInvoiceResult) {
                                        val company = Receipt.Company(
                                            response.user_data.invoice_tax_company,
                                            response.user_data.invoice_tax_number,
                                            response.user_data.invoice_tax_email
                                        )
                                        model.invoiceResult = response
                                        model.receipt.company = company
                                        model.invoiceResult!!.user_data.email =
                                            binding.etEmailInput.text.toString().trim()
                                    }
                                })
                        } else {
                            model.invoiceResult!!.user_data.email =
                                binding.etEmailInput.text.toString().trim()
                        }
                        Toast.makeText(requireActivity(), "信箱修改完成", Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

//    private fun getUserInfo() {
//        UserApiClient.getInvoice(model.appState.accessToken.get()!!, object : ApiController<ResponseInvoiceResult>(requireContext(), false) {
//            override fun onSuccess(response: ResponseInvoiceResult) {
//                model.userInfo?.user?.email_verified = response.email_verified
//                model.receipt.defaultType = Receipt.Default.Er
//                if (response.email_verified) {
//                    setInvoice()
//                } else {
//                    findNavController().popBackStack()
//                }
//            }
//        })
//    }

//    private fun setInvoice() {
//        val request = RequestInvoice(
//            binding.etEmailInput.text.toString(),
//            "",
//            "",
//            "",
//            "",
//            "",
//            "",
//            "",
//            2)
//        UserApiClient.setInvoice(model.appState.accessToken.get()!!,
//            request,
//            object : ApiController<ResponseInvoice>(requireContext(), false) {
//                override fun onSuccess(response: ResponseInvoice) {
//                    findNavController().popBackStack()
//                }
//            })
//    }
}

//private class TextClick(val token: String, val binding: FragmentPaymentMailVerifyBinding) : ClickableSpan() {
//    override fun onClick(widget: View) {
//        binding.etEmailInput.clearFocus()
//
//        val request = RequestInvoice(
//            binding.etEmailInput.text.toString(),
//            "",
//            "",
//            "",
//            "",
//            2)
//        UserApiClient.setInvoice(token, request, object : ApiController<ResponseInvoice>(widget.context, false) {
//            override fun onSuccess(response: ResponseInvoice) {
//                Toast.makeText(widget.context, "驗證信已重新送出", Toast.LENGTH_SHORT).show()
//            }
//        })
//    }
//}