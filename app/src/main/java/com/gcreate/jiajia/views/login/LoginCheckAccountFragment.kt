package com.gcreate.jiajia.views.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseThirdBindResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginCheckAccountBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.util.GoogleUtil
import com.gcreate.jiajia.views.signupotp.SignupOtpType
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import retrofit2.Response


class LoginCheckAccountFragment : Fragment() {

    private lateinit var binding: FragmentLoginCheckAccountBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLoginCheckAccountBinding.inflate(inflater, container, false)

        binding.apply {

            materialToolbar.setNavigationOnClickListener {

                if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.Google) GoogleUtil.signOut()
                else if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.Facebook) Firebase.auth.signOut()

                StorageDataMaintain.deleteFavStockGroupItem(requireContext())
                model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                findNavController().navigateUp()
            }

            var thirdImg = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_google)
            when (StorageDataMaintain.getSingUpResponse(requireActivity())!!.getSingUpMethod()) {
                "google" -> thirdImg = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_google)
                "facebook" -> thirdImg = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_facebook)
                "line" -> thirdImg = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_line)
                "openpoint" -> thirdImg = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_login_op)
            }

            tvSignInName.apply {
                text = StorageDataMaintain.getSingUpResponse(requireActivity())!!.getAccountDisplayName()
                setCompoundDrawablesWithIntrinsicBounds(thirdImg, null, null, null)
            }

            tvSignInName2.apply {
                text = StorageDataMaintain.getSingUpResponse(requireActivity())!!.getAccountDisplayName()
                setCompoundDrawablesWithIntrinsicBounds(thirdImg, null, null, null)
            }


            textInputLayoutPassword.editText!!.setOnEditorActionListener { _, _, _ ->
                hideSoftKeyBroad()
                btnSend()
                true
            }

            btnSend.setOnClickListener {
                hideSoftKeyBroad()

                if (btnHasAccount.isChecked) {
                    btnSend()
                } else if (btnHasNoAccount.isChecked) {
                    val action = LoginCheckAccountFragmentDirections.actionLoginCheckAccountFragmentToSignupOtpFragment()
                    action.mode = SignupOtpType.Signup
                    findNavController().navigate(action)
                }
            }
        }
        return binding.root
    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.textInputLayoutPhone.clearFocus()
        binding.textInputLayoutPassword.clearFocus()
    }

    private fun btnSend() {
        val phoneText = binding.textInputLayoutPhone.editText!!.text.toString()
        val passwordText = binding.textInputLayoutPassword.editText!!.text.toString()

        if (phoneText.isNotEmpty() && passwordText.isNotEmpty()) login(phoneText, passwordText)
        else Toast.makeText(requireActivity(), "帳密不能空白", Toast.LENGTH_SHORT).show()
    }

    private fun login(phoneText: String, passwordText: String) {
        val paramObject = JsonObject()
        paramObject.addProperty("mobile", phoneText)
        paramObject.addProperty("password", passwordText)
        paramObject.addProperty("name", "")
        paramObject.addProperty("providor", "login")
        paramObject.addProperty("code", "")
        paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

        UserApiClient.login(
            paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                override fun onSuccess(response: ResponseLogin) {
                    runBlocking {
                        model.appState.setAccessToken(response.access_token)
                        model.appState.setRefreshToken(response.refresh_token)
                        thirdBinding()
                    }
                }

                override fun onFail(httpResponse: Response<ResponseLogin>): Boolean {
                    Toast.makeText(requireContext(), "登入失敗！請檢查帳號與密碼", Toast.LENGTH_LONG).show()
                    return true
                }
            }
        )
    }

    // 綁定 API
    private fun thirdBinding() {
        val loginType = StorageDataMaintain.getSingUpResponse(requireActivity())!!.getSingUpMethod()
        val loginUid = StorageDataMaintain.getSingUpResponse(requireActivity())!!.getAccountId()

        val paramObject = JsonObject()

        when (loginType) {
            "google" -> {
                paramObject.addProperty("google_id", loginUid)
            }
            "facebook" -> {
                paramObject.addProperty("facebook_id", loginUid)
            }
            "line" -> {
                paramObject.addProperty("line_id", loginUid)
            }
            "openpoint" -> {
                paramObject.addProperty("op_id", loginUid)
            }
        }

        UserApiClient.bindOtherThirdLogin(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseThirdBindResult>(requireActivity(), false) {
                override fun onSuccess(response: ResponseThirdBindResult) {
                    if (!response.error) {
                        val action = LoginCheckAccountFragmentDirections.actionLoginCheckAccountFragmentToMainFragment()
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(requireActivity(), response.error_message, Toast.LENGTH_LONG).show()
                    }
                }
            })
    }
}