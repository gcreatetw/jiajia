package com.gcreate.jiajia.views.user.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.ScheduleApiClient
import com.gcreate.jiajia.api.schedule.request.RequestListScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseScheduleList
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserScheduleBinding
import com.gcreate.jiajia.schedule.data.Day

class UserScheduleFragment : Fragment() {

    private lateinit var binding: FragmentUserScheduleBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserScheduleBinding.inflate(inflater, container, false)

        getScheduleList()

        binding?.apply {
            //設定返回鍵顏色及點擊事件
            val toolBar = binding.toolbar
            toolBar.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolBar)

            toolBar.setNavigationOnClickListener {
                backPressed()
            }

            scheduleView.setOnClickDayListener { year, month, day ->
                val action = UserScheduleFragmentDirections.actionUserScheduleFragmentToUserScheduleDailyFragment()
                action.year = year
                action.month = month
                action.day = day
                findNavController().navigate(action)
            }
        }

        return binding.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun getScheduleList() {
        val requestListScheduleBody = RequestListScheduleBody("01")
        ScheduleApiClient.listSchedule(model.appState.accessToken.get()!!,
            requestListScheduleBody,
            object : ApiController<ResponseScheduleList>(requireActivity(), false) {
                override fun onSuccess(response: ResponseScheduleList) {
                    val map = response.calendars
                    // 0是一月
                    for (scheduleValue in map.values) {
                        if (scheduleValue.size > 1) {
                            val scheduleList = mutableListOf<Day>()
                            for (courseList in scheduleValue) {
                                val startDate = courseList.start.split(" ")[0]
                                val year = startDate.split("-")[0].toInt()
                                val month = startDate.split("-")[1].toInt() - 1
                                val day = startDate.split("-")[2].toInt()
                                scheduleList.add(Day(year, month, day))
                            }
                            binding.scheduleView.addBook(scheduleList)

                        } else {
                            for (courseList in scheduleValue) {
                                val startDate = courseList.start.split(" ")[0]
                                val year = startDate.split("-")[0].toInt()
                                val month = startDate.split("-")[1].toInt() - 1
                                val day = startDate.split("-")[2].toInt()
                                binding.scheduleView.addBook(Day(year, month, day))
                            }
                        }
                    }
                    binding.scheduleView.adapter!!.notifyDataSetChanged()
                }
            })
    }
}