package com.gcreate.jiajia.views.payment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.PaymentApiClient
import com.gcreate.jiajia.api.payment.request.RequestPurchaseResult
import com.gcreate.jiajia.api.payment.response.ResponseOrderPurchaseResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentProgressBinding
import kotlinx.coroutines.runBlocking

class PaymentProgressFragment : Fragment() {

    private lateinit var binding: FragmentPaymentProgressBinding
    private val args: PaymentProgressFragmentArgs by navArgs()
    private var isLaunchPurchaseView = false

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPaymentProgressBinding.inflate(inflater, container, false)

        binding.apply {
            viewModel = model

            model.progressInfo.set(
                ProgreeeInfo("訂單處理中", ProgreeeInfo.Type.PROGRESS)
            )

        }

        return binding.root
    }



    override fun onStart() {
        super.onStart()
        if (args.objectID != null) {
            // 從 paymentFragment 來的 deeplink 參數
            checkOrderResult(args.objectID!!)
        } else {
            // 如果還沒跳出 APP 就打開其他 APP 支付畫面
            if (isLaunchPurchaseView) {
                // 若已開啟支付畫面，再次回到 progressview 則顯示交易結果
                if (PaymentFragment.payMethod == "Openpoint") {
                    checkOrderResult(PaymentFragment.responsePayStoreByOpenWallet!!.objectID!!)
                } else {
                    model.progressInfo.set(
                        ProgreeeInfo("支付沒有完成，請重新操作", ProgreeeInfo.Type.ERROR)
                    )
                    PaymentFragment.payMethod = ""
                    model.plusChannelOrderList = null
                    PaymentPlusFragment.orderPlusIdList.clear()

                    Handler(Looper.getMainLooper()).postDelayed({
                        //Do something after NNN ms
                        (activity as MainActivity).rootNavController.navigateUp()
                    }, 1000)
                }
            }
            isLaunchPurchaseView = true
        }
    }

    private fun checkOrderResult(objectID: String) {

        val requestBody = RequestPurchaseResult(objectID)
        PaymentApiClient.getOrderPurchaseResult(model.appState.accessToken.get()!!, requestBody,
            object : ApiController<ResponseOrderPurchaseResult>(requireActivity(), false) {
                override fun onSuccess(response: ResponseOrderPurchaseResult) {
                    if (response.error == 0) {
                        // 交易成功
                        model.progressInfo.set(
                            ProgreeeInfo("訂單已支付完成", ProgreeeInfo.Type.OK)
                        )

                        runBlocking {
                            model.appState.setHaveSubscription(true)
                        }

                        Handler(Looper.getMainLooper()).postDelayed({
                            //Do something after NNN ms
                            (activity as MainActivity).rootNavController.navigateUp()
                        }, 1000)

                    } else {
                        model.progressInfo.set(
                            ProgreeeInfo("支付沒有完成，請重新操作", ProgreeeInfo.Type.ERROR)
                        )

                        Handler(Looper.getMainLooper()).postDelayed({
                            //Do something after NNN ms
                            (activity as MainActivity).rootNavController.popBackStack()
                        }, 1000)
                    }
                }
            })

        PaymentFragment.payMethod = ""
    }
}