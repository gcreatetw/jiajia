package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R


class BodyInfoStartFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_body_info_start, container, false)
    }

    override fun onStart() {
        super.onStart()
        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "開始"
        //設定略過按鈕
        val skipButton = view?.findViewById<TextView>(R.id.body_info_start_skip)
        skipButton?.setOnClickListener {
            backPressed()
        }
    }

    private fun backPressed() {
        if (BodyInfoFragment.isComeFromSignUp) {
            // 註冊後進來的
            val action = BodyInfoFragmentDirections.actionBodyInfoFragmentToMainFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        } else {
            val navController = (activity as MainActivity).rootNavController
            navController.navigateUp()
        }
    }

}