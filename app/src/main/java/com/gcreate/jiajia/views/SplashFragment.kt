package com.gcreate.jiajia.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.gcreate.jiajia.BuildConfig
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.banner.SplashBannerAdapter
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.SplashBanner
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.util.Util
import com.google.firebase.messaging.FirebaseMessaging
import com.zhpan.bannerview.BannerViewPager
import com.zhpan.bannerview.constants.IndicatorGravity
import com.zhpan.bannerview.constants.PageStyle
import com.zhpan.bannerview.utils.BannerUtils
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import kotlinx.coroutines.runBlocking
import kotlin.math.roundToInt

class SplashFragment : Fragment() {

    private lateinit var mViewPager: BannerViewPager<SplashBanner>
    private lateinit var btnGo: Button
    private var bannerData = mutableListOf<SplashBanner>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (BuildConfig.DEBUG) {
            FirebaseMessaging.getInstance().subscribeToTopic("UatNews")
        } else {
            FirebaseMessaging.getInstance().subscribeToTopic("news")
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            if (!it.isSuccessful) {
                return@addOnCompleteListener
            }
            // Get new FCM registration token
            val token = it.result
            model.firebaseToken = token
            StorageDataMaintain.saveFirebaseToken(requireContext(), token)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_splash, container, false)

        mViewPager = view.findViewById(R.id.splash_banner_view)
        btnGo = view.findViewById<Button>(R.id.btn_go)
        btnGo.setOnClickListener {
            go()
        }
        btnGo.visibility = View.GONE

        setupViewPager(mViewPager)
        bannerData = model.splashBannerData(requireContext())
        mViewPager.refreshData(bannerData)

        return view
    }

    // 設定輪播圖參數
    private fun setupViewPager(viewPager: BannerViewPager<SplashBanner>) {
        viewPager.apply {
            adapter = SplashBannerAdapter(context.applicationContext)

            setLifecycleRegistry(lifecycle)
            setIndicatorStyle(IndicatorStyle.ROUND_RECT)
            setIndicatorSlideMode(IndicatorSlideMode.NORMAL)
            setIndicatorSliderColor(
                resources.getColor(R.color.outlinebutton_background),
                resources.getColor(R.color.green)
            )
            setIndicatorSliderGap(BannerUtils.dp2px(4F))
            setIndicatorSliderWidth(20, 40)
            setIndicatorHeight(20)
            setPageStyle(PageStyle.NORMAL)
            setIndicatorGravity(IndicatorGravity.END)
            setIndicatorMargin(0, 0,
                Util().dpToPixel(16F, requireContext()).roundToInt(),
                Util().dpToPixel(54F, requireContext()).roundToInt())

            setAutoPlay(false)
            setCanLoop(false)

            registerOnPageChangeCallback(onPageChangeCallback)

        }.create()
    }

    private fun go() {
        runBlocking {
            model.appState.setOnBoard()
        }
        val action = SplashFragmentDirections.actionSplashFragmentToLoginAllinOneFragment()
        findNavController().navigate(action)
    }

    private var onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int,
        ) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            if (position == bannerData.size - 1) {
                btnGo?.visibility = View.VISIBLE
            } else {
                btnGo?.visibility = View.GONE
            }
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).supportActionBar?.hide()

        // 加入是不是第一次開啟此 app 的判斷
        if (model.appState.onBoard.get() == true) {
            go()
        }
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }
}