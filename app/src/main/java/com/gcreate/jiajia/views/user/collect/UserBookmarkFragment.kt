package com.gcreate.jiajia.views.user.collect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestUserCollection
import com.gcreate.jiajia.api.user.response.ResponseUserCollection
import com.gcreate.jiajia.data.BeingRecyclerViewItemData
import com.gcreate.jiajia.data.ClassCategory
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserBookmarkBinding
import com.gcreate.jiajia.adapters.membercentre.UserBookmarkRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.initClassCategoryChipGroup
import kotlin.math.roundToInt

class UserBookmarkFragment : Fragment() {

    private var dataList: MutableList<BeingRecyclerViewItemData> = mutableListOf()

    private var binding: FragmentUserBookmarkBinding? = null


    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserBookmarkBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.userBookmarkToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }
        initViews()
        loadDataFromApi(inflater)

        binding?.bookmarkList = dataList

        val layoutManager = LinearLayoutManager(requireActivity())
        val rv = binding?.recyclerView
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = layoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = UserBookmarkRecyclerViewAdapter() {
            val action = UserBookmarkFragmentDirections.actionUserBookmarkFragmentToVideoPageFragment()
            action.courseId = (binding?.bookmarkList!![it] as Course).id
            findNavController().navigate(action)
        }

        setHasOptionsMenu(true)

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    private fun loadDataFromApi(inflater: LayoutInflater) {
        // 最新上架-分類
        CourseApiClient.category(object : ApiController<ResponseCategory>(requireContext(), true) {
            override fun onSuccess(response: ResponseCategory) {
                val chipGroupType = binding?.chipGroupType
                val categoryList = mutableListOf<ClassCategory>()
                categoryList.add(ClassCategory(0, "全部", false))
                categoryList.addAll(response.category)

                initClassCategoryChipGroup(inflater, chipGroupType!!, categoryList)
                chipGroupType.check(0)
                chipGroupType.setOnCheckedChangeListener { chipGroupType, checkedId ->
                    loadDataNewClass(checkedId)
                }
            }
        })
    }

    private fun loadDataNewClass(id: Int) {
        val request = RequestUserCollection(id)

        UserApiClient.getUserCollection(model.appState.accessToken.get()!!,
            request,
            object : ApiController<ResponseUserCollection>(requireContext(), true) {
                override fun onSuccess(response: ResponseUserCollection) {

                    binding?.bookmarkList = response.courses
                }
            })

    }

    private fun initViews() {
        var chipGroup = binding?.chipGroupType
        chipGroup?.setOnCheckedChangeListener { group, checkedId ->
            loadDataNewClass(checkedId)
        }
    }
}