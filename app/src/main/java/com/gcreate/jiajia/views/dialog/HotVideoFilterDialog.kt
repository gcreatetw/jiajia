package com.gcreate.jiajia.views.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.DialogHotVideoFilterBinding
import com.gcreate.jiajia.util.initItemNameChipGroup

class HotVideoFilterDialog(
    var filter: Filter,
    val onOk: (filter : Filter) -> Unit
): DialogFragment() {

    data class Filter(
        var date : String ,
        var intensityIdList : List<Int>,
        var coachIdList : List<Int>,
        var equipmentIdList : List<Int>
    )

    private val model: MainViewModel by activityViewModels{
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: DialogHotVideoFilterBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogHotVideoFilterBinding.inflate(inflater, container, false)

        binding?.imgUploadDateDropdown?.setOnClickListener{
            val popup = PopupMenu(requireContext(), binding?.tvUploadDate!! )
            popup.getMenuInflater().inflate(R.menu.hot_video_filter_upload_date_menu, popup.getMenu())

            popup.setOnMenuItemClickListener {
                binding?.tvUploadDate?.text = it.title
                filter.date = it.title.toString()
                true
            }

            popup.show()
        }

        var chipGroupIntensity = binding?.chipGroupIntensity
        var textList = model.courseFilterIntensity
        initItemNameChipGroup(inflater, chipGroupIntensity!!, textList)
        chipGroupIntensity.setOnCheckedChangeListener { group, checkedId ->

        }
        // chipGroupIntensity.checkedChipIds

        var chipGroupCoach = binding?.chipGroupCoach
        textList = model.courseFilterCoach
        initItemNameChipGroup(inflater, chipGroupCoach!!, textList)
        chipGroupCoach.setOnCheckedChangeListener { group, checkedId ->

        }

        var chipGroupEquipment = binding?.chipGroupEquipment
        textList = model.courseFilterEquipment
        initItemNameChipGroup(inflater, chipGroupEquipment!!, textList)
        chipGroupEquipment.setOnCheckedChangeListener { group, checkedId ->

        }

        binding?.tvUploadDate?.text = filter.date
        for(id in filter.intensityIdList){
            chipGroupIntensity.check(id)
        }
        for(id in filter.coachIdList){
            chipGroupCoach.check(id)
        }
        for(id in filter.equipmentIdList){
            chipGroupEquipment.check(id)
        }

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //取消事件
        val cancelButton = binding?.tvCancel
        cancelButton?.setOnClickListener {
            dismiss()
        }
        //套用
        val confirmButton = binding?.tvConfirm
        confirmButton?.setOnClickListener {
            onOk(Filter(
                binding?.tvUploadDate?.text.toString(),
                binding?.chipGroupIntensity?.checkedChipIds!!,
                binding?.chipGroupCoach?.checkedChipIds!!,
                binding?.chipGroupEquipment?.checkedChipIds!!
            ))
            dismiss()
        }
    }
}