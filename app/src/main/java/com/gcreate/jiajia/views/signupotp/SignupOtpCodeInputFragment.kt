package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.databinding.FragmentSignupOtpCodeInputBinding


class SignupOtpCodeInputFragment : Fragment() {

    private lateinit var binding: FragmentSignupOtpCodeInputBinding
    private val arg: SignupOtpCodeInputFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSignupOtpCodeInputBinding.inflate(inflater, container, false)

        whenFocusChange()

        binding.apply {
            tvPhone.text = arg.phone
            tvCode1.doOnTextChanged { text, _, _, _ ->
                if (text?.length!! > 0) {
                    tvCode2.requestFocus()
                }
            }
            tvCode2.doOnTextChanged { text, _, _, _ ->
                if (text?.length!! > 0) {
                    tvCode3.requestFocus()
                }
            }
            tvCode3.doOnTextChanged { text, _, _, _ ->
                if (text?.length!! > 0) {
                    tvCode4.requestFocus()
                }
            }
            tvCode4.doOnTextChanged { text, _, _, _ ->
                if (text?.length!! > 0) {
                    tvCode5.requestFocus()
                }
            }
            tvCode5.doOnTextChanged { text, _, _, _ ->
                if (text?.length!! > 0) {
                    tvCode6.requestFocus()
                }
            }


            tvPhone.setOnClickListener {
                val action = SignupOtpCodeInputFragmentDirections.actionSignupOtpCodeInputFragmentToSignupOtpPhoneNumberFragment()
                action.mode = arg.mode
                findNavController().navigate(action)
            }

            tvDidNotReceiveCode.setOnClickListener {
                // TODO 確認 SignupOtpNoCodeFragment
                val action = SignupOtpCodeInputFragmentDirections.actionSignupOtpCodeInputFragmentToSignupOtpNocodeFragment()
                action.mode = arg.mode
                action.phone = arg.phone
                findNavController().navigate(action)
            }

            btnContinue.setOnClickListener {
                if (tvCode1.editableText.isNotEmpty() &&
                    tvCode2.editableText.isNotEmpty() &&
                    tvCode3.editableText.isNotEmpty() &&
                    tvCode4.editableText.isNotEmpty() &&
                    tvCode5.editableText.isNotEmpty() &&
                    tvCode6.editableText.isNotEmpty()
                ) {

                    val otpString =
                        tvCode1.text.toString() + tvCode2.text.toString() + tvCode3.text.toString() + tvCode4.text.toString() + tvCode5.text.toString() + tvCode6.text.toString()

                    val action = SignupOtpCodeInputFragmentDirections.actionSignupOtpCodeInputFragmentToSignupOtpCodeCheckFragment(otpString)
                    action.mode = arg.mode
                    action.phone = arg.phone
                    findNavController().navigate(action)
                }

            }
        }

        return binding.root
    }

    private fun whenFocusChange() {
        binding.apply {
            tvCode1.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode1.editableText.clear()

            }

            tvCode2.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode2.editableText.clear()

            }

            tvCode3.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode3.editableText.clear()

            }

            tvCode4.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode4.editableText.clear()

            }

            tvCode5.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode5.editableText.clear()

            }

            tvCode6.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) tvCode6.editableText.clear()

            }
        }
    }


}