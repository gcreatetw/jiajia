package com.gcreate.jiajia.views.user

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.membercentre.UserLessonRecyclerViewAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.ScheduleApiClient
import com.gcreate.jiajia.api.schedule.request.RequestDayScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseDaySchedule
import com.gcreate.jiajia.data.Lesson
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserLessonByDateBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.horizontalSpacingItemDecoration
import kotlin.math.roundToInt

class UserLessonByDateFragment(val date: String) : Fragment() {

    private lateinit var binding: FragmentUserLessonByDateBinding
    private val mAdapter = UserLessonRecyclerViewAdapter(mutableListOf())

    private var dataList = mutableListOf<Lesson>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserLessonByDateBinding.inflate(inflater, container, false)

        getSingleDaySchedule()

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = mAdapter
            mAdapter.apply {
                setEmptyView(R.layout.layout_prograss_full)
                setOnItemClickListener { adapter, view, position ->
                }
            }
        }

        return binding.root
    }

    private fun getSingleDaySchedule() {
        dataList.clear()
        val requestBody = RequestDayScheduleBody(date)
        ScheduleApiClient.daySchedule(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseDaySchedule>(requireActivity(), false) {
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(response: ResponseDaySchedule) {
                    val courseList = response.Course
                    for (i in courseList.indices) {
                        dataList.add(
                            Lesson(
                                courseList[i].id,
                                courseList[i].preview_image,
                                courseList[i].title,
                                0,
                                courseList[i].start,
                                0,
                                courseList[i].video_enable)
                        )
                    }
                    if (dataList.size == 0) {
                        mAdapter.setEmptyView(R.layout.layout_no_data)
                    }

                    mAdapter.data = dataList
                    mAdapter.notifyDataSetChanged()
                }
            })
    }
}