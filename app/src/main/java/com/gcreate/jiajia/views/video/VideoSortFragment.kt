package com.gcreate.jiajia.views.video

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.VideoSortRecyclerViewAdapter
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.BeingRecyclerViewItemData
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentVideoSortBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import kotlin.math.roundToInt

class VideoSortFragment : Fragment() {

    private var binding: FragmentVideoSortBinding? = null
    private val args: VideoSortFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }


    private var dataList: MutableList<BeingRecyclerViewItemData> = mutableListOf()
    private var isFollowed = false
    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())
    private var menuItemFollow: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFollowed = args.isFollowed
        model.loadCourseFilterSetting(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentVideoSortBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.videoSortToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)
        toolBar?.title = args.title

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }

        binding?.lessonList = dataList

        val layoutManager = LinearLayoutManager(requireActivity())
        val rv = binding?.recyclerView
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = layoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = VideoSortRecyclerViewAdapter() {
            val action = VideoSortFragmentDirections.actionVideoSortFragmentToVideoPageFragment()
            action.courseId = (binding?.lessonList as List<Course>)[it].id
            binding!!.root.findNavController().navigate(action)
        }

        binding?.imgFilter?.setOnClickListener {
            activity?.supportFragmentManager.let {
                val dialog = HotVideoFilterDialog(filter) { _filter ->
                    filter = _filter
                    setImgFilterColor()
                    loadDataFromApi()
                }
                dialog.show(it!!, "")
            }
        }

        setHasOptionsMenu(true)

        setImgFilterColor()
        loadDataFromApi()

        return binding?.root
    }

    private fun setImgFilterColor() {
        val enable = filter.intensityIdList.isNotEmpty()
                || filter.coachIdList.isNotEmpty()
                || filter.equipmentIdList.isNotEmpty()

        val drawable = binding?.imgFilter?.drawable
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    resources.getColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    resources.getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun setMenuItemFollowColor(item: MenuItem) {
        if (isFollowed) {
            item.setIcon(R.drawable.baseline_add_alert_24)
        } else {
            item.setIcon(R.drawable.outline_add_alert_24)
        }
    }

    private fun loadDataFromApi() {
        val request = RequestCourses(
            "${args.categoryId}", null, filter.date,
            filter.intensityIdList, filter.coachIdList, filter.equipmentIdList, 25, 1, null, null)

        CourseApiClient.courses(model.appState.accessToken.get()!!, request, object : ApiController<ResponseCourses>(requireContext(), true) {
            override fun onSuccess(response: ResponseCourses) {
                binding?.lessonList = response.courses.toList()

                binding?.tvClassCount?.text = "共 ${response.total} 筆結果"
            }
        })
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.video_sort_menu, menu)
        //改變menu item顏色
        menu.findItem(R.id.video_sort_search_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))

        if (isFollowed) {
            menu.findItem(R.id.video_sort_add_item).setIcon(R.drawable.baseline_add_alert_24)
        } else {
            menu.findItem(R.id.video_sort_add_item).setIcon(R.drawable.outline_add_alert_24)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.video_sort_search_item -> {
                val action = VideoSortFragmentDirections.actionVideoSortFragmentToSearcFragment()
                findNavController().navigate(action)
                true
            }
            R.id.video_sort_add_item -> {
                switchFollowState(item)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun switchFollowState(item: MenuItem) {
        // set by api
        val request = RequestFollowCategory("", args.categoryId.toString(), "", "")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(requireContext(), false) {
            override fun onSuccess(response: Unit) {
                isFollowed = !isFollowed
                setMenuItemFollowColor(item)
                model.loadFollowState(requireContext())
            }
        })
    }
}