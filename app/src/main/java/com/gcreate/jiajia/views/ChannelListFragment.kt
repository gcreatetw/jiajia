package com.gcreate.jiajia.views

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.dataobj.ChannelCategory
import com.gcreate.jiajia.api.dataobj.ChannelCourse
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.request.RequestChannelCategory
import com.gcreate.jiajia.api.homerecommend.request.RequestChannelCourse
import com.gcreate.jiajia.api.homerecommend.response.ResponseChannelCategory
import com.gcreate.jiajia.api.homerecommend.response.ResponseChannelCourse
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentChannelListBinding
import com.gcreate.jiajia.adapters.bundle.ChannelListRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.initChannelCategoryChipGroup
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import kotlin.math.roundToInt

class ChannelListFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    val args: ChannelListFragmentArgs by navArgs()

    private var binding: FragmentChannelListBinding? = null

    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())
    private var isFollowed = false
    private var menuItemFollow: MenuItem? = null

    private val categoryList = mutableListOf<ChannelCategory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFollowed = args.isFollowed
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentChannelListBinding.inflate(inflater, container, false)
        loadDataFromApi(inflater)

        binding?.apply {
            //設定返回鍵顏色及點擊事件
            toolbar.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener {
                backPressed()
            }
            toolbar.title = args.title

            list = mutableListOf<ChannelCourse>()

            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))

                adapter = ChannelListRecyclerViewAdapter {
                    val action = ChannelListFragmentDirections.actionChannelListFragmentToVideoPageFragment()
                    action.courseId = Integer.parseInt(binding?.list!![it].id)
                    findNavController().navigate(action)
                }

            }

            chipGroup.setOnCheckedChangeListener { group, checkedId ->
                loadChannelCourse(checkedId)
            }

            btnChannelListFilter.setOnClickListener {
                activity?.supportFragmentManager.let {
                    val dialog = HotVideoFilterDialog(filter) { _filter ->
                        filter = _filter
                        loadChannelCourse(chipGroup.checkedChipId)
                    }
                    dialog.show(it!!, "")
                }
            }
        }

        setHasOptionsMenu(true)

        model.loadCourseFilterSetting(requireContext())


        return binding?.root
    }

    private fun loadDataFromApi(inflater: LayoutInflater) {
        val token = model.appState.accessToken.get()
        val channelId = args.channelId
        categoryList.clear()
        HomeRecommendApiClient.channelCategory(token!!,
            RequestChannelCategory(channelId),
            object : ApiController<ResponseChannelCategory>(requireContext(), true) {
                override fun onSuccess(response: ResponseChannelCategory) {
                    val chipGroup = binding?.chipGroup!!
                    categoryList.add(ChannelCategory(0, "全部"))
                    categoryList.addAll(response.channel_category)
                    initChannelCategoryChipGroup(inflater, args.title, binding?.chipGroup!!, categoryList)
                    if (response.channel_category.isEmpty()) {
                        loadChannelCourse(-1)
                    } else {
                        chipGroup.check(0)
                    }
                }

            })
    }

    private fun loadChannelCourse(categoryId: Int) {
        if (categoryId < 0) {
            binding?.list = listOf()
        } else {
            val catIdList = mutableListOf<Int>()
            if (categoryId == 0) {
                for (i in 1 until categoryList.size) {
                    catIdList.add(categoryList[i].id)
                }
            } else {
                catIdList.add(categoryId)
            }

            val token = model.appState.accessToken.get()
            HomeRecommendApiClient.channelCourse(
                token!!,
                RequestChannelCourse(
                    catIdList,
                    filter.date,
                    filter.intensityIdList,
                    filter.coachIdList,
                    filter.equipmentIdList, 5, 0),
                object : ApiController<ResponseChannelCourse>(requireContext(), true) {
                    override fun onSuccess(response: ResponseChannelCourse) {
                        binding?.list = response.courses
                    }
                })
        }
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    private fun setMenuItemFollowColor(item: MenuItem) {
        if (isFollowed) {
            item.setIcon(R.drawable.baseline_add_alert_24)
        } else {
            item.setIcon(R.drawable.outline_add_alert_24)
        }
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.channel_list_menu, menu)
        if (isFollowed) {
            menu.findItem(R.id.channel_list_notification_item).setIcon(R.drawable.baseline_add_alert_24)
        } else {
            menu.findItem(R.id.channel_list_notification_item).setIcon(R.drawable.outline_add_alert_24)
        }
        super.onCreateOptionsMenu(menu, inflater)

    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.channel_list_notification_item -> {
                switchFollowState(item)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun switchFollowState(item: MenuItem) {
        // set by api
        UserApiClient.followChannel(model.appState.accessToken.get()!!,
            args.channelId,
            object : ApiController<ResponseSimpleFormat>(requireContext(), true) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    isFollowed = !isFollowed
                    setMenuItemFollowColor(item)
                }
            })
    }
}