package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.findFragment
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestUserHobby
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.MyHobby
import com.gcreate.jiajia.dataStore

class BodyInfoFragment : Fragment() {

    private var navController: NavController? = null
    private val arg: BodyInfoFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        var userGender = 1
        var userBirthday = "1990-01-01"
        var userHeight = 0
        var userWeight = 0
        var fitness_level = 1
        var fitness_target = 1
        var fitness_howlong = 1

        var isComeFromSignUp = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initParams(model.me.get()!!.myHobby)

        isComeFromSignUp = arg.mode == BodyInfoFragmentType.Signup
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_body_info, container, false)

        //設定下一頁按鈕
        val fragmentView = view.findViewById<FragmentContainerView>(R.id.fragmentContainerView2)
        navController = NavHostFragment.findNavController(fragmentView.findFragment<Fragment>())

        val optionsRightToLeft = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.slide_right_to_left_in)
            .setExitAnim(R.anim.slide_right_to_left_out)
            .setPopEnterAnim(R.anim.slide_left_to_right_in)
            .setPopExitAnim(R.anim.slide_left_to_right_out)
            .build()

        val nextButton = view.findViewById<Button>(R.id.body_info_next_button)
        nextButton.setOnClickListener {
            when (navController?.currentDestination?.id) {
                R.id.bodyInfoStartFragment -> {
                    var action = BodyInfoStartFragmentDirections.actionBodyInfoStartFragmentToBodyInfoFirstFragment()
                    if (isComeFromSignUp) {
                        action.mode = 1
                    } else {
                        action.mode = 0
                    }

                    navController?.navigate(action)
                }
                R.id.bodyInfoFirstFragment -> {
                    navController?.navigate(R.id.action_bodyInfoFirstFragment_to_bodyInfoSecondFragment)
                }
                R.id.bodyInfoSecondFragment -> {
                    navController?.navigate(R.id.action_bodyInfoSecondFragment_to_bodyInfoThirdFragment)
                }
                R.id.bodyInfoThirdFragment -> {
                    navController?.navigate(R.id.action_bodyInfoThirdFragment_to_bodyInfoFourthFragment)
                }
                R.id.bodyInfoFourthFragment -> {
                    navController?.navigate(R.id.action_bodyInfoFourthFragment_to_bodyInfoFinishFragment)
                }
                R.id.bodyInfoFinishFragment -> {
                    updateUserInfo()
                }
            }
        }

        return view
    }


    private fun initParams(userHobby: MyHobby) {
        userGender = userHobby.gender
        userBirthday = userHobby.birthday
        userHeight = userHobby.height
        userWeight = userHobby.weight
        fitness_level = userHobby.fitness_level
        fitness_target = userHobby.fitness_target
        fitness_howlong = userHobby.fitness_howlong
    }

    private fun updateUserInfo() {
        val requestUserHobby = RequestUserHobby(
            userGender,
            userBirthday,
            userHeight,
            userWeight,
            fitness_level,
            fitness_target,
            fitness_howlong)

        UserApiClient.updateUserHobby(model.appState.accessToken.get()!!,
            requestUserHobby,
            object : ApiController<ResponseSimpleFormat>(requireContext(), true) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    when (arg.mode) {
                        BodyInfoFragmentType.Signup -> { // 註冊後進來的
                            val action = BodyInfoFragmentDirections.actionBodyInfoFragmentToMainFragment()
                            (activity as MainActivity).rootNavController.navigate(action)
                        }
                        else -> {
                            model.userInfo!!.user.apply {
                                gender = userGender.toString()
                                fitness_level = BodyInfoFragment.fitness_level.toString()
                                fitness_target = BodyInfoFragment.fitness_target.toString()
                            }
                            (activity as MainActivity).rootNavController.navigateUp()
                        }
                    }
                }
            })
    }

}