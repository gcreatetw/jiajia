package com.gcreate.jiajia.views.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.BR
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.search.response.Record
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSearchBinding
import java.util.*

class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(inflater, container, false)


        initView()
        setupBinding(makeApiCall())


        return binding.root
    }


    private fun initView() {
        binding.searchToolbar.navigationIcon?.setTint(
            ContextCompat.getColor(
                requireActivity(),
                R.color.white
            )
        )
        (activity as AppCompatActivity).setSupportActionBar(binding.searchToolbar)

        binding.searchToolbar.setNavigationOnClickListener {
            val navController = (activity as MainActivity).rootNavController
            navController.navigateUp()
        }

        binding.imgCancel.setOnClickListener {
            binding.etSearch.setText("")
            binding.latestContent.visibility = View.VISIBLE
            binding.hotContent.visibility = View.VISIBLE
        }


        binding.etSearch.apply {
            setOnFocusChangeListener { _, hasFocus ->

                if (hasFocus) {
                    if (binding.etSearch.text.isNullOrBlank()) {
                        binding.latestContent.visibility = View.GONE
                        binding.hotContent.visibility = View.VISIBLE
                    } else {
                        binding.latestContent.visibility = View.VISIBLE
                        binding.hotContent.visibility = View.GONE
                    }
                } else {
                    if (binding.etSearch.text.isNullOrBlank()) {
                        binding.latestContent.visibility = View.VISIBLE
                        binding.hotContent.visibility = View.VISIBLE
                    }
                }
            }

            doOnTextChanged { text, _, _, count ->

                if (count == 0 && text.isNullOrEmpty()) {
                    binding.latestContent.visibility = View.VISIBLE
                    binding.hotContent.visibility = View.VISIBLE
                } else {
                    binding.latestContent.visibility = View.VISIBLE
                    binding.hotContent.visibility = View.GONE
                }
            }

            setOnEditorActionListener { v, actionId, event ->

                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyBroad()
                    binding.etSearch.clearFocus()
                    if (binding.etSearch.text.toString().trim { it <= ' ' }.isNotEmpty()) {
                        goDetailPage(binding.etSearch.text.toString())
                    }
                    true
                } else {
                    false
                }

            }
        }


    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun setupBinding(viewModel: SearchFragmentViewModel) {

        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()

        binding.rvLatestSearch.apply {
            layoutManager =
                LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        }

        viewModel.getSearchAdapter().setOnItemClickListener { adapter, view, position ->
            //binding.etSearch.setText((adapter.data[position] as Record).keyword)
            if ((adapter.data[position] as Record).keyword != null) {
                goDetailPage((adapter.data[position] as Record).keyword!!)
            }

        }

        binding.searchLabels.setOnLabelClickListener { label, data, position ->
            //binding.etSearch.setText(label.text)
            goDetailPage(label.text.toString())
        }
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): SearchFragmentViewModel {

        val viewModel =
            ViewModelProvider(requireActivity(), SearchFragmentModelFactory(model, binding)).get(
                SearchFragmentViewModel::class.java
            )

        viewModel.getApiData()
        viewModel.getResponseDataObserver().observe(requireActivity(), {
            if (it != null) {
                viewModel.setSearchAdapterData(it)
                binding.searchLabels.setLabels(it.hot) { label, position, data ->
                    data!!.keyword
                }
            }
        })

        return viewModel
    }

    private fun goDetailPage(keyword: String) {
        val action = SearchFragmentDirections.actionSearchFragmentToSearchDetailFragment()
        action.keyword = keyword
        findNavController().navigate(action)
    }

}