package com.gcreate.jiajia.views.dialog

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.gcreate.jiajia.databinding.DialogMessageBinding

class MessageDialog(
    val title: String,
    val message: String,
    val onOK: () -> Unit,

    ) : DialogFragment() {

    private var binding: DialogMessageBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = DialogMessageBinding.inflate(inflater, container, false)
        binding?.apply {
            tvTitle.text = title
            tvMessage.text =

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Html.fromHtml(message,Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(message)
            }
        }

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.tvOk?.setOnClickListener {
            dismiss()
            onOK()
        }
    }
}