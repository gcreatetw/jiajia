package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSignupOtpCodeCheckBinding
import com.gcreate.jiajia.util.OtpUtil
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SignupOtpCodeCheckFragment : Fragment() {

    private lateinit var binding: FragmentSignupOtpCodeCheckBinding
    val arg: SignupOtpCodeCheckFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSignupOtpCodeCheckBinding.inflate(inflater, container, false)

        binding.apply {
            model.progressInfo.set(
                ProgreeeInfo("正在驗證確認碼", ProgreeeInfo.Type.PROGRESS)
            )
            viewModel = model

            OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).verifyPhoneNumberWithCode(this@SignupOtpCodeCheckFragment,
                arg.otpString!!,
                arg.phone)

        }

        return binding.root
    }

}