package com.gcreate.jiajia.views.user.setting

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseThirdBindResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ThirdBing
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingThirdBingBinding
import com.gcreate.jiajia.adapters.membercentre.UserSettingThirdBingAdapter
import com.gcreate.jiajia.util.GoogleUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.gson.JsonObject
import com.linecorp.linesdk.LineApiResponseCode
import com.linecorp.linesdk.Scope
import com.linecorp.linesdk.auth.LineAuthenticationParams
import com.linecorp.linesdk.auth.LineLoginApi

class UserSettingThirdBingFragment : Fragment() {

    private lateinit var binding: FragmentUserSettingThirdBingBinding
    private lateinit var startActivityForResult: ActivityResultLauncher<Intent>
    private lateinit var callbackManager: CallbackManager
    private var mAuth: FirebaseAuth? = null

    private val logTag = "UserSettingThirdBingFragment"

    val dataList = mutableListOf<ThirdBing>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        GoogleUtil(requireActivity()).init()
        startActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            onActivityResult(result)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserSettingThirdBingBinding.inflate(inflater, container, false)

        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        binding.rvThirdBing.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            dataList.clear()
            dataList.add(0, ThirdBing(R.drawable.ic_google, "Google", model.userInfo!!.user.google))
            dataList.add(1, ThirdBing(R.drawable.ic_facebook, "FaceBook", model.userInfo!!.user.facebook))
            dataList.add(2, ThirdBing(R.drawable.ic_line, "Line", model.userInfo!!.user.line))
            dataList.add(3, ThirdBing(R.drawable.ic_login_op, "OpenPoint", model.userInfo!!.user.op_bundled))

            val mAdapter = UserSettingThirdBingAdapter(dataList)

            mAdapter.addChildClickViewIds(R.id.tv_third_is_bing)
            mAdapter.setOnItemChildClickListener { _, view, position ->
                if (view.id == R.id.tv_third_is_bing) {
                    when (position) {
                        0 -> {
                            val textString = view.findViewById<TextView>(R.id.tv_third_is_bing).text.toString()
                            if (textString == "未綁定") {
                                val signInIntent = GoogleUtil.getGoogleSignInClient().signInIntent
                                startActivityForResult.launch(signInIntent)
                            } else {
                                cancelThirdPartyBinding("google")
                            }
                        }
                        1 -> {
                            val textString = view.findViewById<TextView>(R.id.tv_third_is_bing).text.toString()
                            if (textString == "未綁定") {
                                loginFacebook()
                            } else {
                                cancelThirdPartyBinding("facebook")
                            }

                        }
                        2 -> {
                            val textString = view.findViewById<TextView>(R.id.tv_third_is_bing).text.toString()
                            if (textString == "未綁定") {
                                lineLogin()
                            } else {
                                cancelThirdPartyBinding("line")
                            }

                        }
                        3 -> {
                            val textString = view.findViewById<TextView>(R.id.tv_third_is_bing).text.toString()
                            if (textString == "未綁定") {
                                val action = UserSettingThirdBingFragmentDirections.actionUserSettingThirdBingFragmentToLoginOpenPointFragment()
                                action.loginOrBind = "bingOP"
                                findNavController().navigate(action)
                            } else {
                                cancelThirdPartyBinding("openpoint")
                            }
                        }
                    }

                }
            }
            adapter = mAdapter

        }
        return binding.root
    }

    // Google 綁定
    private fun onActivityResult(result: ActivityResult) {
        try {
            val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
            // Google Sign In was successful, authenticate with Firebase
            val googleAccountInfo = task.getResult(ApiException::class.java)
            thirdBinding("Google", googleAccountInfo.id.toString())
            //firebaseAuthWithGoogle(googleAccountInfo.idToken!!)

        } catch (e: ApiException) {
            // Google Sign In failed, update UI appropriately
            Log.d(logTag, "Google sign in failed", e)
            GoogleUtil.signOut()
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(requireActivity()
        ) { task ->
            if (task.isSuccessful) {
                // Sign in success, 綁定google
                GoogleUtil.signOut()
            } else {
                Toast.makeText(requireContext(), "登入失敗！", Toast.LENGTH_LONG).show()
            }
        }
    }

    // faceBook 綁定
    private fun loginFacebook() {

        callbackManager = CallbackManager.Factory.create()

        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, callbackManager, listOf("email", "user_photos", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                handleFacebookAccessToken(result.accessToken)
            }

            override fun onCancel() {
            }

            override fun onError(error: FacebookException) {
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = mAuth!!.currentUser

                    thirdBinding("FaceBook", user!!.uid)
                }
            }
    }

    private fun faceBookSingOut() {
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest(
                AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE,
                GraphRequest.Callback {
                    AccessToken.setCurrentAccessToken(null)
                    LoginManager.getInstance().logOut()
                }
            ).executeAsync()
        }
    }

    // 綁定 LINE
    private fun lineLogin() {
        val lineChannelId = requireContext().getString(R.string.line_channel_id)
        val loginIntent = LineLoginApi.getLoginIntent(
            requireContext(), lineChannelId,
            LineAuthenticationParams.Builder()
                .scopes(listOf(Scope.PROFILE))
                .botPrompt(LineAuthenticationParams.BotPrompt.normal)
                .build()
        )

        forLineLogin.launch(loginIntent)

    }

    private val forLineLogin = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) {

        if (it.data != null) {
            handleLoginResult(it.data!!)
        }
    }

    private fun handleLoginResult(data: Intent) {
        val loginResult = LineLoginApi.getLoginResultFromIntent(data)

        when (loginResult.responseCode) {
            LineApiResponseCode.SUCCESS -> {
                Log.d(logTag, "id:" + loginResult.lineProfile!!.userId)
                thirdBinding("Line", loginResult.lineProfile!!.userId)
            }
            LineApiResponseCode.CANCEL -> {
                loginError()
            }
            LineApiResponseCode.SERVER_ERROR -> {
                loginError()
            }
            LineApiResponseCode.NETWORK_ERROR -> {
                loginError()
            }
            LineApiResponseCode.INTERNAL_ERROR -> {
                loginError()
            }
            LineApiResponseCode.AUTHENTICATION_AGENT_ERROR -> {
                loginError()
            }
            else -> {
            }
        }
    }

    private fun loginError() {
        Toast.makeText(requireActivity(), "Line 登入失敗", Toast.LENGTH_SHORT).show()
    }

    // 綁定 API
    private fun thirdBinding(loginType: String, uid: String) {
        val paramObject = JsonObject()
        when (loginType) {
            "Google" -> {
                paramObject.addProperty("google_id", uid)
            }
            "FaceBook" -> {
                paramObject.addProperty("facebook_id", uid)
            }
            "Line" -> {
                paramObject.addProperty("line_id", uid)
            }
        }
        UserApiClient.bindOtherThirdLogin(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseThirdBindResult>(requireActivity(), false) {
                override fun onSuccess(response: ResponseThirdBindResult) {
                    if (!response.error) {
                        // 綁定成功
                        when (loginType) {
                            "Google" -> {
                                dataList[0].thirdBinding = true
                                model.userInfo!!.user.google = true
                                binding.rvThirdBing.adapter!!.notifyItemChanged(0)
                            }
                            "FaceBook" -> {
                                dataList[1].thirdBinding = true
                                model.userInfo!!.user.facebook = true
                                binding.rvThirdBing.adapter!!.notifyItemChanged(1)
                            }
                            "Line" -> {
                                dataList[2].thirdBinding = true
                                model.userInfo!!.user.line = true
                                binding.rvThirdBing.adapter!!.notifyItemChanged(2)
                            }
                        }
                    } else {
                        Toast.makeText(requireActivity(), response.error_message, Toast.LENGTH_LONG).show()
                    }
                }
            })
    }

    // 解除綁定 API
    private fun cancelThirdPartyBinding(thirdPartyType: String) {
        val paramObject = JsonObject()
        paramObject.addProperty("provider", thirdPartyType)

        UserApiClient.cancelThirdPartyBinding(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (!response.error) {
                        when (thirdPartyType) {
                            "google" -> {
                                GoogleUtil.signOut()
                                dataList[0].thirdBinding = false
                                model.userInfo!!.user.google = false
                                binding.rvThirdBing.adapter!!.notifyItemChanged(0)
                            }
                            "facebook" -> {
                                dataList[1].thirdBinding = false
                                model.userInfo!!.user.facebook = false
                                binding.rvThirdBing.adapter!!.notifyItemChanged(1)
                            }
                            "line" -> {
                                dataList[2].thirdBinding = false
                                model.userInfo!!.user.line = false
                                binding.rvThirdBing.adapter!!.notifyItemChanged(2)
                            }
                            "openpoint" -> {
                                dataList[3].thirdBinding = false
                                model.userInfo!!.user.op_bundled = false
                                binding.rvThirdBing.adapter!!.notifyItemChanged(3)
                            }
                        }
                    }
                }
            })
    }
}