package com.gcreate.jiajia.views.articlelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.adapters.home.NewsRecyclerViewAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.request.RequestNews
import com.gcreate.jiajia.api.homerecommend.response.ResponseNews
import com.gcreate.jiajia.data.HomeNews
import com.gcreate.jiajia.databinding.FragmentArticleListKnowledgeBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import kotlin.math.roundToInt


class ArticleListKnowledgeFragment : Fragment() {

    private var binding: FragmentArticleListKnowledgeBinding? = null
    private var list = listOf<HomeNews>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentArticleListKnowledgeBinding.inflate(inflater, container, false)
        binding?.list = list

        val rv = binding?.articleListKnowledgeRecyclerView
        val newsLayoutManager = LinearLayoutManager(requireActivity())
        newsLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = newsLayoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = NewsRecyclerViewAdapter {
            showNews(it)
        }

        HomeRecommendApiClient.newsAll(RequestNews(2, 25, 1), object : ApiController<ResponseNews>(requireContext(), true) {
            override fun onSuccess(response: ResponseNews) {
                list = response.blog
                binding?.list = list
            }
        })

        return binding?.root
    }

    private fun showNews(position: Int) {

        val action = ArticleListFragmentDirections.actionArticleListFragmentToSportKnowledgeFragment(binding!!.list!![position].url)
        binding!!.root.findNavController().navigate(action)
    }
}