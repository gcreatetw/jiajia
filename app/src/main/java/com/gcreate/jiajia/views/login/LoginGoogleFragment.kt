package com.gcreate.jiajia.views.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestVerifyThirdPartyBody
import com.gcreate.jiajia.api.user.response.ResponseLogin
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginGoogleBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.util.GoogleUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking

class LoginGoogleFragment : Fragment() {

    private lateinit var startActivityForResult: ActivityResultLauncher<Intent>

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GoogleUtil(requireActivity()).init()

        startActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            onActivityResult(result)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentLoginGoogleBinding.inflate(inflater, container, false)

        binding.apply {
            model.progressInfo.set(
                ProgreeeInfo(" ", ProgreeeInfo.Type.PROGRESS)
            )
            viewModel = model
        }

        val signInIntent = GoogleUtil.getGoogleSignInClient().signInIntent
        startActivityForResult.launch(signInIntent)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (model.loginAllinOneFragmentType == LoginAllinOneFragmentType.UserSelect) {
            findNavController().navigateUp()
        }
    }

    private fun onActivityResult(result: ActivityResult) {
        try {
            val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
            // Google Sign In was successful, authenticate with Firebase
            val googleAccountInfo = task.getResult(ApiException::class.java)

            StorageDataMaintain.saveSingUpResponse(requireContext(),
                ThirdLoginResponse(
                    "google",
                    googleAccountInfo.id.toString(),
                    googleAccountInfo.displayName.toString(),
                    googleAccountInfo.photoUrl.toString()))

            UserApiClient.verifyThirdPartyId(RequestVerifyThirdPartyBody("google", googleAccountInfo.id.toString()),
                object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                    override fun onSuccess(response: ResponseSimpleFormat) {
                        if (!response.error && response.have_user) {
                            // 表示後台有Uer用過餓第三方 ID 註冊 or 綁定過
                            val paramObject = JsonObject()
                            paramObject.addProperty("providor", "google")
                            paramObject.addProperty("code", googleAccountInfo.id.toString())
                            paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

                            // 判斷資料庫有無該 google UID
                            UserApiClient.login(
                                paramObject, object : ApiController<ResponseLogin>(requireContext(), true) {
                                    override fun onSuccess(response: ResponseLogin) {
                                        // 有帳號，登入
                                        runBlocking {
                                            model.appState.setAccessToken(response.access_token)
                                            model.appState.setRefreshToken(response.refresh_token)
                                        }
                                        val action = LoginGoogleFragmentDirections.actionLoginGoogleFragmentToMainFragment()
                                        findNavController().navigate(action)
                                    }
                                }
                            )
                        } else {
                            // 無帳號，註冊
                            firebaseAuthWithGoogle(googleAccountInfo.idToken!!)
                        }
                    }
                })


        } catch (e: ApiException) {
            // Google Sign In failed, update UI appropriately
            model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
            findNavController().navigateUp()
        }
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(requireActivity()
        ) { task ->
            if (task.isSuccessful) {
                // Sign in success, transfer to OTP page
                findNavController().navigate(LoginGoogleFragmentDirections.actionLoginGoogleFragmentToLoginCheckAccountFragment())
            } else {
                Toast.makeText(requireContext(), "登入失敗！", Toast.LENGTH_LONG).show()
            }
        }
    }
}