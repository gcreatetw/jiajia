package com.gcreate.jiajia.views.login


import android.app.Activity.RESULT_OK
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSetPersonalizeBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.util.LimitInputTextWatcher
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentType
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException


class SetPersonalizeFragment : Fragment(), SetProfileAvatarFragment.OnDialogButtonFragmentListener {

    private var binding: FragmentSetPersonalizeBinding? = null
    private val arg: SetPersonalizeFragmentArgs by navArgs()
    private var pinCode = ""

    private lateinit var savedData: ThirdLoginResponse

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSetPersonalizeBinding.inflate(inflater, container, false)
        savedData = StorageDataMaintain.getSingUpResponse(requireContext())!!

        permissionPhoto()

        when (arg.mode) {
            SetPersonalizeFragmentType.UserSetting -> {
                initForUserSetting()
            }
            else -> {
                initForSignup()
            }
        }

        binding?.apply {
            layoutModify.setOnClickListener {
                val bottomSheetFragment = SetProfileAvatarFragment()
                bottomSheetFragment.listener = this@SetPersonalizeFragment
                bottomSheetFragment.show(requireActivity().supportFragmentManager, "settingHeight")
            }
        }

        getJsonData()

        return binding?.root
    }

    private fun initForUserSetting() {
        binding?.apply {
            viewModel = model

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener {
                backPressed()
            }
            btnNext.visibility = View.GONE
            lyAffiliateMarketing.visibility = View.GONE

            textInputLayoutNickname.editText!!.setText(model.me.get()!!.name)
            (citySpinner.editText as AutoCompleteTextView).setText(model.me.get()!!.county.toString(), false)
            (districtsSpinner.editText as AutoCompleteTextView).setText(model.me.get()!!.district.toString(), false)

            readMode(this)

            imgMenuBtn.setOnClickListener {
                imgMenuBtn.setImageResource(R.drawable.baseline_check_circle_24)

                editMode(this)

                imgMenuBtn.setOnClickListener {
                    val county = (binding!!.citySpinner.editText as? AutoCompleteTextView)!!.text.toString()
                    val district = (binding!!.districtsSpinner.editText as? AutoCompleteTextView)!!.text.toString()
                    val address = binding!!.textInputAddress.text.toString()

                    if (county.isNotEmpty() && district.isNotEmpty() && address.isNotEmpty()) {
                        updateUserProfile()
                        findNavController().navigateUp()
                    } else {
                        Toast.makeText(requireActivity(), "請輸入縣市地址欄位", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            textInputEdittext.addTextChangedListener(LimitInputTextWatcher(textInputEdittext))

        }
    }

    private fun initForSignup() {
        binding?.apply {

            viewModel = model

            toolbar.navigationIcon = null
            imgMenuBtn.visibility = View.GONE

            model.me.get()!!.name = savedData.getAccountDisplayName()
            model.me.get()!!.usrImageUrl = ""

            lyAffiliateMarketing.visibility = View.VISIBLE

            btnNext.setOnClickListener {
                if (pinCode.isNotEmpty() && binding!!.textInputAddress.text.toString().isNotEmpty()) {
                    if(binding!!.textInputAffiliate.text!!.toString().isNotEmpty()){
                        addAffiliateMarketingReferral(binding!!.textInputAffiliate.text!!.toString())
                    }else{
                        // TODO 如果註冊 上傳要更新這片跟暱稱
                        if (arg.mode == SetPersonalizeFragmentType.Signup) {
                            updateUserProfile()
                        }
                        val action = SetPersonalizeFragmentDirections.actionSetPersonalizeFragmentToPaymentMailVerifyFragment()
                        action.mode = BodyInfoFragmentType.Signup
                        findNavController().navigate(action)
                    }

                } else {
                    Toast.makeText(requireActivity(), "請輸入地址", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun addAffiliateMarketingReferral(code: String) {
        val paramObject = JsonObject()
        paramObject.addProperty("code", code)

        UserApiClient.addAffiliateMarketingReferral(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat2) {
                    var text = ""
                    if (response.error == 0) {
                        text = "輸入成功"
                    } else if (response.error == 1) {
                        text = response.message
                    }

                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage(text)
                        setPositiveButton("確認") { dialog: DialogInterface, _: Int ->
                            if (response.error == 0) {
                                model.isUseAffiliateMarketingReferral = true
                                // TODO 如果註冊 上傳要更新這片跟暱稱
                                if (arg.mode == SetPersonalizeFragmentType.Signup) {
                                    updateUserProfile()
                                }
                                val action = SetPersonalizeFragmentDirections.actionSetPersonalizeFragmentToPaymentMailVerifyFragment()
                                action.mode = BodyInfoFragmentType.Signup
                                findNavController().navigate(action)
                            }
                            dialog.dismiss()
                        }
                        setCancelable(false)
                    }
                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                }
            })
    }

    private fun readMode(binding: FragmentSetPersonalizeBinding) {
        binding.apply {
            tvLabel1Setting.visibility = View.GONE
            tvLabel2Setting.visibility = View.GONE
            layoutModify.visibility = View.GONE
            textInputLayoutNickname.editText?.isEnabled = false
            citySpinner.isEnabled = false
            districtsSpinner.isEnabled = false
            textInputAddress.isEnabled = false
        }
    }

    private fun editMode(binding: FragmentSetPersonalizeBinding) {
        binding.apply {
            tvLabel1Setting.visibility = View.VISIBLE
            tvLabel2Setting.visibility = View.VISIBLE
            layoutModify.visibility = View.VISIBLE
            textInputLayoutNickname.editText?.isEnabled = true
            citySpinner.isEnabled = true
            districtsSpinner.isEnabled = true
            textInputAddress.isEnabled = true
        }
    }

    fun backPressed() {
        findNavController().navigateUp()
    }

    private fun permissionPhoto() {
        ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ), 0
        )
    }

    override fun onSelectDialog(select: String, value: Int) {
        when (value) {
            0 -> getCameraImg()
            1 -> getAlbumImg()
        }
    }

    private val requestCameraCode = 1000
    private val requestAlbumsCode = 1001
    private var part: MultipartBody.Part? = null

    //新增一張照片
    private lateinit var cameraPicture2: File
    private var cameraImageUri: Uri? = null
    private var cameraImagePath: String = ""

    private fun getCameraImg() {
        val camera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val cameraPicture = File(context?.getExternalFilesDir(null), "image.jpg")
        cameraPicture2 = cameraPicture
        //建立uri，這邊拿到的格式就會是 content://
        val outputFileUri = FileProvider.getUriForFile(requireActivity(), "com.jiajia.bookreports.provider", cameraPicture)
        camera.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)

        cameraImageUri = outputFileUri
        cameraImagePath = cameraPicture.absolutePath

        //指定為輸出檔案的位置
        camera.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
        startActivityForResult(camera, requestCameraCode)
    }

    private fun getAlbumImg() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, requestAlbumsCode)
    }

    private fun updateUserProfile() {

        val userName = RequestBody.create(MediaType.parse("text/plain"), binding!!.textInputLayoutNickname.editText!!.text.toString())
        val countyText =
            RequestBody.create(MediaType.parse("text/plain"), (binding!!.citySpinner.editText as? AutoCompleteTextView)!!.text.toString())
        val districtText =
            RequestBody.create(MediaType.parse("text/plain"), (binding!!.districtsSpinner.editText as? AutoCompleteTextView)!!.text.toString())
        val addressText = RequestBody.create(MediaType.parse("text/plain"), binding!!.textInputAddress.text.toString())
        val pinCodeText = RequestBody.create(MediaType.parse("text/plain"), pinCode)


        model.me.get()!!.apply {
            name = binding!!.textInputLayoutNickname.editText!!.text.toString()
            county = (binding!!.citySpinner.editText as? AutoCompleteTextView)!!.text.toString()
            district = (binding!!.districtsSpinner.editText as? AutoCompleteTextView)!!.text.toString()
            address = binding!!.textInputAddress.text.toString()
            pin_code = pinCode
        }

        UserApiClient.updateUserProfile(
            model.appState.accessToken.get()!!,
            userName,
            part,
            countyText,
            districtText,
            addressText,
            pinCodeText,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                }
            })
    }

    private fun getPathFromUri(uri: Uri): String {
        val projection = arrayOf(MediaStore.MediaColumns.DATA)
        val cursor = requireActivity().contentResolver.query(uri, projection, null, null, null)
        val columnIndex: Int? = cursor?.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        cursor?.moveToFirst()
        val result: String = columnIndex?.let { cursor.getString(it) } ?: ""
        cursor?.close()
        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            if (requestCode == requestCameraCode) {
                binding!!.img.setImageURI(cameraImageUri)

                val fileRQ = RequestBody.create(MediaType.parse("image/*"), cameraPicture2)
                part = MultipartBody.Part.createFormData("avatar", "image.jpg", fileRQ)
                model.me.get()!!.usrImageUrl = cameraImageUri.toString()

            } else if (requestCode == requestAlbumsCode) {
                if (data != null && data.data != null) {
                    binding!!.img.setImageURI(data.data)

                    val file = File(data.data?.let { getPathFromUri(it) }!!)
                    val fileRQ = RequestBody.create(MediaType.parse("image/*"), file)
                    part = MultipartBody.Part.createFormData("avatar", "image.jpg", fileRQ)
                    model.me.get()!!.usrImageUrl = data.data.toString()
                }
            }
        }
    }

    private fun getJsonData() {
        try {
            // read json file
            val inputStreamReader = InputStreamReader(requireActivity().assets.open("taiwan_districts.json"), "UTF-8")
            val bufferedReader = BufferedReader(inputStreamReader)

            var line: String?
            val stringBuilder = StringBuilder()
            while ((bufferedReader.readLine().also { line = it }) != null) {
                stringBuilder.append(line)
            }
            bufferedReader.close()
            inputStreamReader.close()

            val jsonObject = JSONObject(stringBuilder.toString())

            // convert json string to object
            val gson = Gson()
            val cityDistricts = gson.fromJson<TaiwanDistricts>(jsonObject.toString(), object : TypeToken<TaiwanDistricts>() {}.type).data
            setMenuItems(cityDistricts)

        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }

    private fun setMenuItems(cityDistricts: MutableList<CityData>) {
        val cityLists = mutableListOf<String>()
        val districtsList = mutableListOf<District>()

        for (i in 0 until cityDistricts.size) {
            cityLists.add(cityDistricts[i].name)
            if (model.me.get()!!.county == cityDistricts[i].name) {
                districtsList.addAll(cityDistricts[i].districts)
            }
        }

        val districtsTextList = mutableListOf<String>()
        for (item in districtsList) {
            districtsTextList.add(item.name)
        }

        val countryAdapter = ArrayAdapter(requireContext(), R.layout.list_item, cityLists)
        (binding!!.citySpinner.editText as? AutoCompleteTextView)?.apply {
            setAdapter(countryAdapter)
            setOnItemClickListener { _, _, position, _ ->
                (binding!!.districtsSpinner.editText as? AutoCompleteTextView)!!.setText("")
                val districts = cityDistricts[position].districts
                val districtsLists = mutableListOf<String>()

                for (item in districts) {
                    districtsLists.add(item.name)
                }
                val districtsAdapter = ArrayAdapter(requireContext(), R.layout.list_item, districtsLists)
                (binding!!.districtsSpinner.editText as? AutoCompleteTextView)?.apply {
                    setAdapter(districtsAdapter)
                    setOnItemClickListener { _, _, position, _ ->
                        pinCode = districts[position].zip
                    }
                }
            }
        }

        val districtsAdapter = ArrayAdapter(requireContext(), R.layout.list_item, districtsTextList)
        (binding!!.districtsSpinner.editText as? AutoCompleteTextView)?.apply {
            setAdapter(districtsAdapter)
            setOnItemClickListener { _, _, position, _ ->
                pinCode = districtsList[position].zip
            }
        }

        (binding!!.districtsSpinner.editText as? AutoCompleteTextView)!!.setOnClickListener {
            if ((binding!!.citySpinner.editText as? AutoCompleteTextView)?.text.isNullOrBlank()) {
                Toast.makeText(requireActivity(), "請先選擇縣市", Toast.LENGTH_SHORT).show()
            }
        }
    }

}