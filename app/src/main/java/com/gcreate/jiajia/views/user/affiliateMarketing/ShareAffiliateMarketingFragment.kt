package com.gcreate.jiajia.views.user.affiliateMarketing

import android.content.DialogInterface
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.membercentre.affiliateMarketing.AffiliateMarketingAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseUserReferralList
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentAffiliateMarketingShareBinding
import com.gcreate.jiajia.util.AES256Util
import com.gcreate.jiajia.views.dialog.MessageDialog
import com.google.gson.JsonObject

class ShareAffiliateMarketingFragment : Fragment() {

    private lateinit var binding: FragmentAffiliateMarketingShareBinding
    private val mAdapter = AffiliateMarketingAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentAffiliateMarketingShareBinding.inflate(inflater, container, false)

        setToolBar()
        initView()
        getApiData()

        return binding.root
    }

    private fun setToolBar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
    }

    private fun initView() {

        binding.apply {
            tvAffiliateMarking.apply {
                val spannableString = SpannableString(getString(R.string.affiliateMarketingText) + "\t更多詳情")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(StyleSpan(Typeface.BOLD),0,30, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
                spannableString.setSpan(colorSpan, spannableString.length - 4, spannableString.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString

                setOnClickListener {
                    requireActivity().supportFragmentManager.let {
                        val dialog = MessageDialog("更多詳情", model.affSetting!!.contant) { }
                        dialog.show(it, "")
                    }
                }
            }

            viewContent.setOnClickListener {
//                val clipboard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//                val clip: ClipData = ClipData.newPlainText("text", tvShareCode.text)
//                clipboard.setPrimaryClip(clip)
//                Toast.makeText(requireActivity(), "已複製: 您的推薦代碼", Toast.LENGTH_SHORT).show()
                try {
                    val androidStoreLink = "https://reurl.cc/x7geGL"
                    val appleStoreLink = "https://reurl.cc/jDqV8n"
                    ShareCompat.IntentBuilder(requireActivity())
                        .setType("text/plain")
                        .setText("點擊連結下載APP：\nandroid:${Uri.parse(androidStoreLink)}\nios:${Uri.parse(appleStoreLink)}\n我的推薦碼：${binding.tvShareCode.text} ")
                        .startChooser()
                } catch (e: Exception) {
                    //e.toString();
                }
            }
            tvShareCode.text =
                model.userInfo!!.user.phone?.let { AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), it) }

            rvContent.apply {
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

                mAdapter.addChildClickViewIds(R.id.tv_affiliate_bind)
                mAdapter.setOnItemChildClickListener { _, view, position ->
                    if (view.id == R.id.tv_affiliate_bind) {
                        val textString = view.findViewById<TextView>(R.id.tv_affiliate_bind).text.toString()
                        if (textString == "未領取") {
                            getReferralReceive(mAdapter.data[position].referral_id, position)
                        }
                    }
                }
                adapter = mAdapter
            }
        }
    }

    private fun getApiData() {
        UserApiClient.getAffiliateMarketingList(model.appState.accessToken.get()!!,
            object : ApiController<ResponseUserReferralList>(requireActivity(), false) {
                override fun onSuccess(response: ResponseUserReferralList) {
                    binding.data = response
                    mAdapter.data = response.list
                    mAdapter.notifyDataSetChanged()
                }
            })
    }

    private fun getReferralReceive(referral_id: Int, position: Int) {
        val paramObject = JsonObject()
        paramObject.addProperty("referral_id", referral_id)
        UserApiClient.getReferralReceive(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat2) {
                    var text = ""
                    if (response.error == 0) {
                        text = "領取成功"
                    } else if (response.error == 1) {
                        text = response.message
                    }

                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage(text)
                        setPositiveButton("確認") { dialog: DialogInterface, _: Int ->
                            if (response.error == 0) {
                                mAdapter.data[position].get_point = true
                                mAdapter.notifyItemChanged(position)
                            }

                            dialog.dismiss()
                        }

                        setCancelable(false)
                    }
                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                }
            })
    }

}