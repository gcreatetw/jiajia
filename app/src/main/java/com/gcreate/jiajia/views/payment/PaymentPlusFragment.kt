package com.gcreate.jiajia.views.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.PaymentApiClient
import com.gcreate.jiajia.api.payment.response.PlusChannel
import com.gcreate.jiajia.api.payment.response.ResponsePlusChannels
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentPlusBinding
import com.gcreate.jiajia.adapters.membercentre.Payment2PlusAdapter
import com.gcreate.jiajia.util.decoration.SpacesItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.views.video.VideoPageFragment


class PaymentPlusFragment : Fragment() {

    private var binding: FragmentPaymentPlusBinding? = null
    private lateinit var plusChannelDataList: MutableList<PlusChannel>

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        val orderPlusIdList = mutableListOf<Int>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPaymentPlusBinding.inflate(inflater, container, false)

        return binding?.root
    }

    override fun onStart() {
        super.onStart()

        PaymentApiClient.plusChannels(
            model.appState.accessToken.get() ?: "",
            object : ApiController<ResponsePlusChannels>(requireContext()) {
                override fun onSuccess(response: ResponsePlusChannels) {
                    plusChannelDataList = response.plus_channels

                    for (i in plusChannelDataList.indices) {
                        if (plusChannelDataList[i].id == VideoPageFragment.plusChannelParentId) {
                            orderPlusIdList.add(plusChannelDataList[i].id)
                        }
                    }

                    binding?.recyclerView?.apply {
                        adapter = Payment2PlusAdapter(plusChannelDataList)

                        addItemDecoration(
                            SpacesItemDecoration(
                                Util().dpToPixel(24f, requireContext()).toInt(),
                                Util().dpToPixel(16f, requireContext()).toInt(),
                                Util().dpToPixel(16f, requireContext()).toInt(),
                                Util().dpToPixel(16f, requireContext()).toInt()
                            )
                        )
                    }
                }
            }
        )


    }
}