package com.gcreate.jiajia.views.video

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.ActivityIframeWebBinding
import com.gcreate.jiajia.util.webview.VideoEnabledWebChromeClient

class IframeWebActivity : AppCompatActivity() {

    private lateinit var binding: ActivityIframeWebBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        binding = ActivityIframeWebBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)

        val decodeVideoString = String(Base64.decode(intent.getStringExtra("video_iframe"), Base64.DEFAULT), charset("UTF-8"))

        val videoIframeCode =
            "<body style='margin:0;'><style> body > iframe{ width: 100% !important; height: 100vh;}</style>$decodeVideoString</body>"

        if (intent.getStringExtra("chat_iframe").isNullOrBlank()) {
            binding.coordinatorLayout.visibility = View.GONE

            val params = binding.videoWebView.layoutParams as ConstraintLayout.LayoutParams
            params.leftToRight = ConstraintSet.PARENT_ID
            params.topToTop = ConstraintSet.PARENT_ID
            params.bottomToBottom = ConstraintSet.PARENT_ID
            binding.videoWebView.requestLayout()

        } else {
            val decodeChatString = String(Base64.decode(intent.getStringExtra("chat_iframe"), Base64.DEFAULT), charset("UTF-8"))
            val chatIframeCode = String.format(resources.getString(R.string.chatIframeHtml), decodeChatString)
            binding.chatWebView.apply {
                settings.apply {
                    javaScriptEnabled = true
                    databaseEnabled = true
                    domStorageEnabled = true
                    cacheMode = WebSettings.LOAD_NO_CACHE
                }
                loadDataWithBaseURL(null, chatIframeCode, "text/html", "utf-8", null)
            }
        }

        binding.toolbar.apply {
            title = ""
            navigationIcon?.setTint(ContextCompat.getColor(this@IframeWebActivity, R.color.white))
            setNavigationOnClickListener {
                finish()
            }
        }

        binding.videoWebView.apply {
            settings.apply {
                javaScriptEnabled = true
                javaScriptCanOpenWindowsAutomatically = true
                databaseEnabled = true
                domStorageEnabled = true
                cacheMode = WebSettings.LOAD_DEFAULT
                webViewClient = WebViewClient()
            }
            webChromeClient = VideoEnabledWebChromeClient(this@IframeWebActivity, binding.parent, binding.videoLayout)
            loadDataWithBaseURL("https://www.being.com.tw", videoIframeCode, "text/html", "utf-8", null)
        }
    }

    // ---------------------- 橫屏不重新加載activity ----------------------
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val c = resources.configuration
        if (c.orientation == Configuration.ORIENTATION_PORTRAIT) {
            binding.toolbar.visibility = View.VISIBLE
            binding.chatWebView.visibility = View.VISIBLE
        } else if (c.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            binding.toolbar.visibility = View.GONE
            binding.chatWebView.visibility = View.GONE
        }
    }
}

