package com.gcreate.jiajia.views.articlelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.response.ResponseNewsDetail
import com.gcreate.jiajia.data.ArticleItem
import com.google.gson.JsonObject

class SportKnowledgeFragment : Fragment() {

    val arg: SportKnowledgeFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_article, container, false)

        val toolBar = view.findViewById<androidx.appcompat.widget.Toolbar>(R.id.article_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        toolBar.setNavigationOnClickListener {
            backPressed()
        }

        val webView = view.findViewById<WebView>(R.id.article_webview)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(arg.url)

        return view
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}