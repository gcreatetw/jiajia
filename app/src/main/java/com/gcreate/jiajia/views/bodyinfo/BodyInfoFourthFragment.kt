package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentBodyInfoFourthBinding


class BodyInfoFourthFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding = FragmentBodyInfoFourthBinding.inflate(inflater,container,false)
        //設定返回鍵顏色
        binding.bodyInfoFinishToolbar.navigationIcon?.setTint(resources.getColor(R.color.white))

        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "下一頁"
        binding.bodyInfoFinishToolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        //設定按鈕
        when (BodyInfoFragment.fitness_target) {
            1 -> binding.bodyInfoFourthBtn1.isSelected = true
            2 -> binding.bodyInfoFourthBtn2.isSelected = true
            3 -> binding.bodyInfoFourthBtn3.isSelected = true
            4 -> binding.bodyInfoFourthBtn4.isSelected = true
        }

        binding.bodyInfoFourthBtn1.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_target = 1
                if (!this.isSelected) {
                    this.isSelected = true
                    binding.bodyInfoFourthBtn2.isSelected = false
                    binding.bodyInfoFourthBtn3.isSelected = false
                    binding.bodyInfoFourthBtn4.isSelected = false
                }
            }
        }

        binding.bodyInfoFourthBtn2.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_target = 2
                if (!this.isSelected) {
                    this.isSelected = true
                    binding.bodyInfoFourthBtn1.isSelected = false
                    binding.bodyInfoFourthBtn3.isSelected = false
                    binding.bodyInfoFourthBtn4.isSelected = false
                }
            }
        }

        binding.bodyInfoFourthBtn3.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_target = 3
                if (!this.isSelected) {
                    this.isSelected = true
                    binding.bodyInfoFourthBtn1.isSelected = false
                    binding.bodyInfoFourthBtn2.isSelected = false
                    binding.bodyInfoFourthBtn4.isSelected = false
                }
            }
        }

        binding.bodyInfoFourthBtn4.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_target = 4
                if (!this.isSelected) {
                    this.isSelected = true
                    binding.bodyInfoFourthBtn1.isSelected = false
                    binding.bodyInfoFourthBtn2.isSelected = false
                    binding.bodyInfoFourthBtn3.isSelected = false
                }
            }
        }

        return binding.root
    }
}