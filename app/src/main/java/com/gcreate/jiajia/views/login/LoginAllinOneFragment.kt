package com.gcreate.jiajia.views.login

import android.content.DialogInterface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseAffSetting
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseSocialLoginSetting
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLoginAllinoneBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import retrofit2.Response

class LoginAllinOneFragment : Fragment() {

    private var binding: FragmentLoginAllinoneBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginAllinoneBinding.inflate(inflater, container, false)

        UserApiClient.socialLoginInfo(object : ApiController<ResponseSocialLoginSetting>(requireActivity(),true) {
            override fun onSuccess(response: ResponseSocialLoginSetting) {
                binding!!.socialsetting = response.data
            }
        })

        binding?.apply {

            val isCheckEULA = model.appState.eulaArgeed.get()!!

            tvSkip.setOnClickListener {
                val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToMainFragment()
                findNavController().navigate(action)
            }

            tvGoogle.setOnClickListener {
                if (isCheckEULA) loginGoogle()
                else gotoEULA(LoginAllinOneFragmentType.Google)
            }

            tvFacebook.setOnClickListener {
                if (isCheckEULA) loginFacebook()
                else gotoEULA(LoginAllinOneFragmentType.Facebook)
            }

            tvLine.setOnClickListener {
                if (isCheckEULA) loginLine()
                else gotoEULA(LoginAllinOneFragmentType.Line)
            }

            tvOpenpoint.setOnClickListener {
                if (isCheckEULA) loginOpenPoint()
                else gotoEULA(LoginAllinOneFragmentType.Openpoint)
            }

            tvJiajia.setOnClickListener {
                if (isCheckEULA) loginJiaJia()
                else gotoEULA(LoginAllinOneFragmentType.Jiajia)
            }

            tvRegistered.apply {
                val spannableString = SpannableString("還沒有帳號?  註冊成為會員吧 !")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(colorSpan, 8, 14, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString

                setOnClickListener {
                    if (isCheckEULA) register()
                    else gotoEULA(LoginAllinOneFragmentType.Regist)
                }
            }

            include.cardLogo.visibility = View.INVISIBLE
        }

        return binding?.root
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).supportActionBar?.hide()

        if (model.isLogined()) {
            verifyRepeatLogin()
        }

        if (model.appState.eulaArgeed.get() == true) {
            when (model.loginAllinOneFragmentType) {
                LoginAllinOneFragmentType.UserSelect -> {
                    // do nothing
                }
                LoginAllinOneFragmentType.Google -> {
                    loginGoogle()
                }
                LoginAllinOneFragmentType.Facebook -> {
                    loginFacebook()
                }
                LoginAllinOneFragmentType.Line -> {
                    loginLine()
                }
                LoginAllinOneFragmentType.Openpoint -> {
                    loginOpenPoint()
                }
                LoginAllinOneFragmentType.Jiajia -> {
                    loginJiaJia()
                }
                LoginAllinOneFragmentType.Regist -> {
                    register()
                }
            }
        }
    }

    private fun gotoEULA(type: LoginAllinOneFragmentType) {
        model.loginAllinOneFragmentType = type
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToLoginEulaFragment()
        findNavController().navigate(action)
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    private fun loginGoogle() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.Google
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToLoginGoogleFragment()
        findNavController().navigate(action)
    }

    private fun loginFacebook() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.Facebook
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToLoginFaceBookFragment()
        findNavController().navigate(action)
    }

    private fun loginLine() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.Line
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToLoginLineFragment()
        findNavController().navigate(action)
    }

    private fun loginOpenPoint() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.Openpoint
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToLoginOpenPointFragment()
        findNavController().navigate(action)
    }

    private fun loginJiaJia() {
        model.loginAllinOneFragmentType = LoginAllinOneFragmentType.Jiajia
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToUserLoginFragment()
        findNavController().navigate(action)

    }

    private fun register() {
        StorageDataMaintain.saveSingUpResponse(requireContext(),
            ThirdLoginResponse("registeredLogin", "", "", ""))
        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToSingupOtpFragment()
        findNavController().navigate(action)
    }

    private fun verifyRepeatLogin() {

        val paramObject = JsonObject()
        paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

        UserApiClient.verifyToken(
            model.appState.accessToken.get()!!,
            paramObject, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (response.error) {
                        // 已被其它裝置登入
                        val builder = AlertDialog.Builder(requireActivity())
                        builder.apply {
                            setMessage("此帳號已在其他裝置登入，請重新登入。")
                            setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                                logout(dialog)
                            }
                            setCancelable(false)
                        }

                        val alertDialog = builder.create()
                        alertDialog.show()
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                            .setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))

                    } else {
                        //相同裝置登入
                        val action = LoginAllinOneFragmentDirections.actionLoginAllinOneFragmentToMainFragment()
                        findNavController().navigate(action)
                        return
                    }
                }

                override fun onFail(httpResponse: Response<ResponseSimpleFormat>): Boolean {
                    when (httpResponse.code()) {
                        401 -> {
                            // token 過期，或找不到人。Unauthenticated
                            runBlocking {
                                model.appState.setAccessToken("")
                                model.appState.setRefreshToken("")
                                model.appState.setHaveSubscription(false)
                                model.appState.setPayMethod("cathaybk")
                                model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                                val dataList = ThirdLoginResponse("", "", "", "")
                                StorageDataMaintain.saveSingUpResponse(requireContext(), dataList)
                            }
                        }
                    }
                    return super.onFail(httpResponse)
                }
            })
    }

    private fun logout(dialog: DialogInterface) {
        UserApiClient.logout(model.appState.accessToken.get()!!, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
            override fun onSuccess(response: ResponseSimpleFormat) {
                runBlocking {
                    model.appState.setAccessToken("")
                    model.appState.setRefreshToken("")
                    model.appState.setHaveSubscription(false)
                    model.appState.setPayMethod("cathaybk")
                    model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect

                    val dataList = ThirdLoginResponse("", "", "", "")
                    StorageDataMaintain.saveSingUpResponse(requireContext(), dataList)
                    dialog.dismiss()
                }
            }
        })
    }
}