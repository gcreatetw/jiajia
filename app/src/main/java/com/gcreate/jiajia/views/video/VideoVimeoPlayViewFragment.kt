package com.gcreate.jiajia.views.video

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.vimeo.VimeoApiClient2
import com.gcreate.jiajia.api.vimeo.response.SingleVideoResponse
import com.gcreate.jiajia.databinding.FragmentVideoVimeoPlayViewBinding
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.util.Util
import com.norulab.exofullscreen.MediaPlayer
import com.norulab.exofullscreen.preparePlayer
import com.norulab.exofullscreen.setSource
import com.vimeo.networking.Configuration
import com.vimeo.networking.VimeoClient
import retrofit2.Response


class VideoVimeoPlayViewFragment : Fragment() {

    /**
     * You have to sign up as developer with vimeo developer dashboard [the developer console](https://developer.vimeo.com/)
     * VIMEO_ACCESS_TOKEN you can it from [the developer console](https://developer.vimeo.com/apps/192457#generate_access_token)
     */

    private var player: ExoPlayer? = null

    //Release references
    private var playWhenReady = false //If true the player auto play the media

    private var currentWindow = 0
    private var playbackPosition: Long = 0

    private var binding: FragmentVideoVimeoPlayViewBinding? = null
    private val args: VideoVimeoPlayViewFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        // Inflate the layout for this fragment
        binding = FragmentVideoVimeoPlayViewBinding.inflate(inflater, container, false)

        binding!!.videoPlayToolbar.setNavigationOnClickListener() {
            findNavController().popBackStack()
        }

        //Build vimeo configuration
        configVimeoClient()

        return binding?.root
    }

    private fun configVimeoClient() {
        // need vimeo video set private to public, if video set private , the api response will not have files column.
        val configBuilder = Configuration.Builder(getString(R.string.vimeo_token)) //Pass app access token
            .setCacheDirectory(requireActivity().cacheDir)
        VimeoClient.initialize(configBuilder.build())
    }

    private fun createMediaItem(url: String) {
        val mediaItem = MediaItem.fromUri(url)
        MediaPlayer.exoPlayer!!.setMediaItem(mediaItem)
    }

    private fun initializePlayer() {
        //To play streaming media, you need an ExoPlayer object.
        //SimpleExoPlayer is a convenient, all-purpose implementation of the ExoPlayer interface.
//        player = ExoPlayer.Builder(requireContext()).build()
        player = SimpleExoPlayer.Builder(requireContext()).build()
        binding!!.videoView.player = player
        callVimeoAPIRequest()
    }

    private fun callVimeoAPIRequest() {

        VimeoApiClient2.getVideoData(
            args.accessToken!!,
            args.vimeoId!!,
            object : ApiController<SingleVideoResponse>(requireContext(), false) {
                override fun onSuccess(response: SingleVideoResponse) {
                    val videoFiles = response.files
                    for (item in videoFiles) {
                        if (item.rendition == "1080p") {
                            createMediaItem(item.link)

                            break
                        }
                    }
                }

                override fun onFail(httpResponse: Response<SingleVideoResponse>): Boolean {
                    return super.onFail(httpResponse)
                }
            })
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        binding!!.videoView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }


    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) {
            initializePlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        //Helper method which allows you to have a full-screen experience.
        //hideSystemUi()
        if (Util.SDK_INT < 24 || player == null) {
            //Init exoplayer builder
            initializePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT < 24) {
            //Frees the player's resources and destroys it.
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT >= 24) {
            //Frees the player's resources and destroys it.
            releasePlayer()
        }
    }

    private fun releasePlayer() {
        if (player != null) {
            playWhenReady = player!!.playWhenReady
            playbackPosition = player!!.currentPosition
            currentWindow = player!!.currentWindowIndex
            player!!.release()
            player = null
        }
    }
}