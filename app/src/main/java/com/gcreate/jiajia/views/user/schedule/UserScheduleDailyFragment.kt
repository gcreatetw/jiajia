package com.gcreate.jiajia.views.user.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.tab.UserScheduleDailyTabAdapter
import com.gcreate.jiajia.databinding.FragmentUserScheduleDailyBinding
import com.gcreate.jiajia.schedule.data.Day
import com.google.android.material.tabs.TabLayoutMediator

class UserScheduleDailyFragment : Fragment() {

    private var binding: FragmentUserScheduleDailyBinding? = null
    private val args: UserScheduleDailyFragmentArgs by navArgs()

    private lateinit var dateList: List<String>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserScheduleDailyBinding.inflate(inflater, container, false)
        dateList = genTabTileList(Day(args.year, args.month, args.day))

        //TabLayout
        val adapter = UserScheduleDailyTabAdapter(this, dateList)
        binding?.apply {
            //設定返回鍵顏色及點擊事件
            val toolBar = binding?.toolbar
            toolBar?.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            toolBar?.title = "${args.month + 1}/${args.day}" // mm/dd
            (activity as AppCompatActivity).setSupportActionBar(toolBar)

            toolBar?.setNavigationOnClickListener {
                backPressed()
            }

            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = adapter
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                val tabText = dateList[position].split('-')[1] + "月" + dateList[position].split('-')[2]
                tab.text = tabText
            }.attach()
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun genTabTileList(day: Day): List<String> {
        val list = mutableListOf<String>()
        var d = day
        for (index in 1..5) {
            val title = "${d.year}-${d.month + 1}-${d.day}"
            list.add(title)
            d = d.nextDay()
        }

        return list
    }
}