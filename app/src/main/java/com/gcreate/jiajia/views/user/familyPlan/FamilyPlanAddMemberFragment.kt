package com.gcreate.jiajia.views.user.familyPlan

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.FamilyListData
import com.gcreate.jiajia.api.user.response.ResponseFamilyList
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentFamilyPlanAddMenberBinding
import com.gcreate.jiajia.adapters.membercentre.familyPlan.FamilyPlanAddMemberAdapter

class FamilyPlanAddMemberFragment : Fragment() {

    private lateinit var binding: FragmentFamilyPlanAddMenberBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        var dataList: MutableList<FamilyListData>? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFamilyPlanAddMenberBinding.inflate(inflater, container, false)

        binding.apply {
            vm = model

            itemViewFamilyPlanAdd.viewContent.setBackgroundResource(R.drawable.layout_checked)

            includeBtnAdd.btnFamilyPlanAdd.setOnClickListener {
                val action = FamilyPlanAddMemberFragmentDirections.actionFamilyPlanAddMemberFragmentToFamilyPlanSearchMemberFragment()
                findNavController().navigate(action)
            }
        }

        setToolBar()
        getApiData()

        return binding.root
    }

    private fun setToolBar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
    }

    private fun getApiData() {
        UserApiClient.getFamilyMember(model.appState.accessToken.get()!!, object : ApiController<ResponseFamilyList>(requireActivity(), true) {
            override fun onSuccess(response: ResponseFamilyList) {
                if (!response.error) {
                    dataList = response.data
                    if (dataList!!.size >= 5) {
                        binding.includeBtnAdd.btnFamilyPlanAdd.visibility = View.GONE
                    }
                    binding.rvContent.apply {
                        layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                        adapter = FamilyPlanAddMemberAdapter(dataList!!)
                    }
                } else {
                    dataList = mutableListOf()
                }
            }
        })
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.family_plan_menu, menu)
        menu.findItem(R.id.family_plan_edit).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
        menu.findItem(R.id.family_plan_edit_check).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.family_plan_edit -> {
                val action = FamilyPlanAddMemberFragmentDirections.actionFamilyPlanAddMemberFragmentToFamilyPlanEditMemberFragment()
                findNavController().navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}