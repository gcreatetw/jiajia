package com.gcreate.jiajia.views.user.setting

import android.content.Context
import android.content.DialogInterface
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseSimpleFormat2
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingReferralInputBinding
import com.gcreate.jiajia.views.dialog.MessageDialog
import com.google.gson.JsonObject
import java.util.*

class UserSettingReferralInputFragment : Fragment() {

    private lateinit var binding: FragmentUserSettingReferralInputBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserSettingReferralInputBinding.inflate(inflater, container, false)

        setToolBar()
        initView()

        return binding.root
    }

    private fun setToolBar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
    }

    private fun initView() {

        binding.apply {
            isUseAffiliateMarketingReferral = model.isUseAffiliateMarketingReferral

            tvAffiliateMarking.apply {
                val spannableString = SpannableString(getString(R.string.affiliateMarketingText) + "\t更多詳情")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(StyleSpan(Typeface.BOLD), 0, 30, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
                spannableString.setSpan(colorSpan, spannableString.length - 4, spannableString.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString

                setOnClickListener {
                    requireActivity().supportFragmentManager.let {
                        val dialog = MessageDialog("更多詳情", model.affSetting!!.contant) { }
                        dialog.show(it, "")
                    }
                }
            }

            textInputLayoutPhoneNumber.apply {
                editText?.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                        hideSoftKeyBroad()
                        binding.textInputLayoutPhoneNumber.clearFocus()
                        true
                    } else {
                        false
                    }
                }

                setEndIconOnClickListener {
                    hideSoftKeyBroad()
                    binding.textInputLayoutPhoneNumber.clearFocus()
                }


                tvShareCode.text = model.userInfo!!.user.referral_code

                btnNext.setOnClickListener {
                    if (textInputLayoutPhoneNumber.editText!!.text.length < 6) {
                        error = "您輸入的推薦代碼錯誤或不存在，請查明後重新輸入。"
                    } else {
                        error = ""
                        addAffiliateMarketingReferral(textInputLayoutPhoneNumber.editText!!.text.toString())
                    }
                }
            }
        }
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun addAffiliateMarketingReferral(code: String) {
        val paramObject = JsonObject()
        paramObject.addProperty("code", code)

        UserApiClient.addAffiliateMarketingReferral(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat2) {
                    var text = ""
                    if (response.error == 0) {
                        text = "輸入成功"
                    } else if (response.error == 1) {
                        text = response.message
                    }

                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage(text)
                        setPositiveButton("確認") { dialog: DialogInterface, _: Int ->
                            if (response.error == 0) {
                                model.isUseAffiliateMarketingReferral = true
                                findNavController().popBackStack()
                            }
                            dialog.dismiss()
                        }
                        setCancelable(false)
                    }
                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                }
            })
    }

}