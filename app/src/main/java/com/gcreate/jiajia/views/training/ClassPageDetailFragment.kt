package com.gcreate.jiajia.views.training

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentClasspageDetailBinding

class ClassPageDetailFragment : Fragment(){

    private val model: MainViewModel by activityViewModels{
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: FragmentClasspageDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentClasspageDetailBinding.inflate(inflater, container, false)
        binding?.data = model.courseDetailTmp

        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.classpageDetailToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}