package com.gcreate.jiajia.views.login

enum class LoginAllinOneFragmentType {
    UserSelect,
    Google,
    Line,
    Facebook,
    Openpoint,
    Jiajia,
    Regist
}