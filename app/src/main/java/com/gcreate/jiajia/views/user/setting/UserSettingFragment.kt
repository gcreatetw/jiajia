package com.gcreate.jiajia.views.user.setting

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingBinding
import com.gcreate.jiajia.views.login.SetPersonalizeFragmentType
import com.gcreate.jiajia.views.signupotp.SignupOtpType

class UserSettingFragment : Fragment() {

    private var binding: FragmentUserSettingBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserSettingBinding.inflate(inflater, container, false)

        binding?.apply {
            isUseAffiliateMarketingReferral = model.isUseAffiliateMarketingReferral

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireContext(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            layoutPersonalize.setOnClickListener {
                // 個人化設定
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToSetPersonalizeFragment()
                action.mode = SetPersonalizeFragmentType.UserSetting
                findNavController().navigate(action)
            }

            layoutModifyPassword.setOnClickListener {
                // 修改密碼
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToSignupOtpFragment()
                action.mode = SignupOtpType.ModifyPassword
                findNavController().navigate(action)
            }

            layoutCode.setOnClickListener {
                // 輸入折扣碼
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingCodeFragment()
                findNavController().navigate(action)
            }

            if (model.affSetting != null) {
                val setting = model.affSetting!!
                if (setting.user_referral || setting.user_referral_renew || setting.user_referral_subscript) {
                    layoutReferralCode.visibility = View.VISIBLE
                } else {
                    layoutReferralCode.visibility = View.GONE
                }
            }

            layoutReferralCode.setOnClickListener {
                // 輸入推薦代碼
                if (model.userInfo != null) {
                    if (model.userInfo!!.user.op_bundled) {
                        val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingReferralInputFragment()
                        findNavController().navigate(action)
                    } else {
                        val builder = AlertDialog.Builder(requireActivity())
                        builder.apply {
                            setTitle(getString(R.string.affiliateMarketingTitle))
                            setMessage(getString(R.string.affiliateMarketingSubTitle))
                            setPositiveButton("前往綁定") { _: DialogInterface?, _: Int ->
                                val action = UserSettingFragmentDirections.actionUserSettingFragmentToLoginOpenPointFragment()
                                action.loginOrBind = "bingOP"
                                (activity as MainActivity).rootNavController.navigate(action)
                            }
                            setNegativeButton("忍痛放棄") { _, _ ->
                                val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingReferralInputFragment()
                                findNavController().navigate(action)
                            }
                            setCancelable(false)
                        }
                        val alertDialog = builder.create()
                        alertDialog.show()
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                            .setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                            .setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    }
                }

            }


            layoutNotifycation.setOnClickListener {
                // 推播設定
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingNotificationFragment()
                findNavController().navigate(action)
            }

            layoutSubscription.setOnClickListener {
                // 訂閱紀錄
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingSubscriptionTypeFragment()
                findNavController().navigate(action)
            }

            layoutThirdBing.setOnClickListener {
                // 設巡綁定
                val action = UserSettingFragmentDirections.actionUserSettingFragmentToUserSettingThirdBingFragment()
                findNavController().navigate(action)
            }

        }

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    override fun onResume() {
        super.onResume()
        binding?.isUseAffiliateMarketingReferral = model.isUseAffiliateMarketingReferral
    }
}