package com.gcreate.jiajia.views.payment

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentReferralInputBinding
import com.gcreate.jiajia.views.dialog.MessageDialog
import java.util.*


class PaymentReferralInputFragment : Fragment() {

    private lateinit var binding: FragmentPaymentReferralInputBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPaymentReferralInputBinding.inflate(inflater, container, false)

        binding.apply {
//            isUseAffiliateMarketingReferral = model.isUseAffiliateMarketingReferral

            tvAffiliateMarking.apply {
                val spannableString = SpannableString(getString(R.string.affiliateMarketingText) + "\t更多詳情")
                val colorSpan = ForegroundColorSpan(ContextCompat.getColor(requireActivity(), R.color.green))
                spannableString.setSpan(StyleSpan(Typeface.BOLD),0,30, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
                spannableString.setSpan(colorSpan, spannableString.length - 4, spannableString.length, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
                text = spannableString



                setOnClickListener {
                    requireActivity().supportFragmentManager.let {
                        val dialog = MessageDialog("更多詳情",model.affSetting!!.contant) { }
                        dialog.show(it, "")
                    }
                }
            }

            textInputLayoutPhoneNumber.apply {

                val parent: Fragment? = (parentFragment as NavHostFragment).parentFragment
                val buttonView = parent?.view?.findViewById<Button>(R.id.btn_next)

                editText?.doOnTextChanged { text, _, _, _ ->

                    buttonView?.text = if (text?.isNotEmpty() == true && text.length <= 10) {
                        "下一步"
                    } else {
                        "略過"
                    }
                }

                editText?.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                        hideSoftKeyBroad()
                        binding.textInputLayoutPhoneNumber.clearFocus()
                        true
                    } else {
                        false
                    }
                }

                setEndIconOnClickListener {
                    hideSoftKeyBroad()
                    binding.textInputLayoutPhoneNumber.clearFocus()
                }

//                tvShareCode.text =
//                    model.userInfo!!.user.referral_code?.let {
//                        AES256Util.AES256Decrypt(getString(R.string.AESkey),
//                            getString(R.string.AESIVkey),
//                            it)
//                    }

//                btnNext.setOnClickListener {
//                    if (textInputLayoutPhoneNumber.editText!!.text.length < 10) {
//                        error = "您輸入的推薦代碼錯誤或不存在，請查明後重新輸入。"
//                    } else {
//                        error = ""
//                        addAffiliateMarketingReferral(textInputLayoutPhoneNumber.editText!!.text.toString())
//                    }
//                }
            }
        }



        return binding.root
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }


}