package com.gcreate.jiajia.views.user

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.tab.UserTabAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseAffSetting
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseUserInfo
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserBinding
import com.gcreate.jiajia.schedule.data.Day
import com.gcreate.jiajia.util.AES256Util
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentType
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.runBlocking
import java.util.*

class UserFragment : Fragment() {

    private lateinit var binding: FragmentUserBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    companion object {
        var isNeedLoading = true
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        binding.vm = model

        getUserInfo(isNeedLoading)
        viewClickEvent()

        setTodayCourseTabs()

        binding.refreshLayout.apply {
            setOnRefreshListener {
                getUserInfo(isNeedLoading)
                getAffSetting()
            }
        }

        return binding.root
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity).supportActionBar?.show()
    }


    private fun getUserInfo(isNeedLoading: Boolean) {
        UserApiClient.uerInfo(model.appState.accessToken.get()!!, object : ApiController<ResponseUserInfo>(requireContext(), isNeedLoading) {
            override fun onSuccess(response: ResponseUserInfo) {
                val userInfo = response.user
                binding.refreshLayout.isRefreshing = false

                runBlocking {
                    model.appState.setHaveSubscription(userInfo.subscription)
                }

                responseNullCheck(userInfo)
                model.collectCourses = response.collect
                model.userMore = response.more
                model.userInfo = response

                model.isUseAffiliateMarketingReferral = (!userInfo.referral_code.isNullOrEmpty())


                binding.apply {
                    try{
                        layoutMemberCard.tvUserName.text =
                            AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), model.userInfo!!.user.name)
                    }catch (e :Exception){
                        layoutMemberCard.tvUserName.text = ""
                    }


                    if (model.userInfo != null) {
                        if (model.userInfo!!.family != null) {
                            itemFamilyPlanChild.tvFamilyTxt.text = "你已經被" + AES256Util.AES256Decrypt(getString(R.string.AESkey),
                                getString(R.string.AESIVkey),
                                model.userInfo!!.family!!.name) + "設定為家庭方案成員"
                        }
                    }
                }

                if (userInfo.op_bundled) {
                    binding.layout3.visibility = View.GONE
                } else {
                    binding.layout3.visibility = View.VISIBLE
                }
                // 判斷家庭方案 layout
                /** 是成員，即為子帳：family 有值
                 * 非成員，即為主帳： family NULL
                 * */
                if (response.family == null) {
                    // 顯示綠色半透明票卡
                    binding.itemFamilyPlanMain.root.apply {
                        visibility = View.VISIBLE
                        when (userInfo.subscription_type) {
                            "0" -> {
                                findViewById<TextView>(R.id.tv_title).text = "個人方案"
                                findViewById<TextView>(R.id.tv_subtitle).text = userInfo.subscription_expire
                            }
                            "1" -> {
                                findViewById<TextView>(R.id.tv_title).text = "家庭方案"
                                findViewById<TextView>(R.id.tv_subtitle).text = userInfo.subscription_expire
                                binding.itemEditFamilyPlanChild.root.visibility = View.GONE
                            }
                            null -> {
                                findViewById<TextView>(R.id.tv_title).text = "註冊會員"
                                findViewById<TextView>(R.id.tv_subtitle).text = "家庭方案付費、成員設定"
                            }
                        }
                    }

                    if (!userInfo.subscription && userInfo.have_member) {
                        binding.itemEditFamilyPlanChild.root.apply {
                            visibility = View.VISIBLE
                            findViewById<TextView>(R.id.tv_title).text = "編輯家庭成員"
                            //findViewById<TextView>(R.id.tv_subtitle).text = userInfo.subscription_expire
                        }
                    } else {
                        binding.itemEditFamilyPlanChild.root.apply {
                            visibility = View.GONE
                        }
                    }
                    binding.itemFamilyPlanChild.root.visibility = View.GONE
                } else {
                    // 顯示綠色票卡
                    binding.itemFamilyPlanMain.root.visibility = View.GONE
                    binding.itemFamilyPlanChild.root.visibility = View.VISIBLE
                }

                if (model.affSetting != null){
                    val setting = model.affSetting!!
                    if(setting.user_referral || setting.user_referral_renew || setting.user_referral_subscript){
                        binding.itemAffiliateMarketing.root.visibility = View.VISIBLE
                    }else{
                        binding.itemAffiliateMarketing.root.visibility = View.GONE
                    }
                }
            }
        })
        Companion.isNeedLoading = false
        model.loadFollowState(requireContext())
    }

    private fun getAffSetting() {
        UserApiClient.getAffSetting(object : ApiController<ResponseAffSetting>(requireActivity(), false) {
            override fun onSuccess(response: ResponseAffSetting) {
                model.affSetting = response
            }
        })
    }

    private fun responseNullCheck(userInfo: com.gcreate.jiajia.api.user.response.User) {

        var subscriptionType: Subscription.Type = Subscription.Type.UnRegistered
        when (userInfo.subscription_level) {
            "月繳方案會員" -> subscriptionType = Subscription.Type.Month
            "季繳方案會員" -> subscriptionType = Subscription.Type.Season
            "年繳方案會員" -> subscriptionType = Subscription.Type.Year
            else -> Subscription.Type.UnRegistered
        }

        var genderString = "1"
        if (!userInfo.gender.isNullOrEmpty()) {
            genderString = userInfo.gender
        }

        var birthdayString = "1990/2/11"
        if (!userInfo.birthday.isNullOrEmpty()) {
            birthdayString = userInfo.birthday
        }

        var heightString = "160"
        if (!userInfo.height.isNullOrEmpty()) {
            if (userInfo.height != "o2Hzq2E2wePTIkWi7Bc/4g==") {
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.height)
            }
        }

        var weightString = "60"
        if (!userInfo.weight.isNullOrEmpty()) {
            if (userInfo.weight != "o2Hzq2E2wePTIkWi7Bc/4g==") {
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.weight)
            }
        }

        var fitnessLevelString = "1"
        if (!userInfo.fitness_level.isNullOrEmpty()) {
            fitnessLevelString = userInfo.fitness_level
        }

        var fitnessTargetString = "1"
        if (!userInfo.fitness_target.isNullOrEmpty()) {
            fitnessTargetString = userInfo.fitness_target
        }

        var fitnessHowLongString = "1"
        if (!userInfo.fitness_howlong.isNullOrEmpty()) {
            fitnessHowLongString = userInfo.fitness_howlong
        }


        try {
            if (model.appState.accessToken.get()!!.isNotEmpty()) {
                model.me.set(User(
                    userInfo.user_img,
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.name),
                    userInfo.member_number,
                    userInfo.subscription,
                    subscriptionType,
                    userInfo.subscription_expire,
                    "",
                    "",
                    "",
                    0.0,
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.county!!),
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.district!!),
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.address!!),
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.pin_code!!),
                    MyHobby(
                        "",
                        Integer.parseInt(genderString),
                        birthdayString,
                        Integer.parseInt(heightString),
                        Integer.parseInt(weightString),
                        Integer.parseInt(fitnessLevelString),
                        Integer.parseInt(fitnessTargetString),
                        Integer.parseInt(fitnessHowLongString),
                    )
                ))
            }
        } catch (e: Exception) {
        }

        model.notificationSetting.get()!!.news = userInfo.news != 0
        model.notificationSetting.get()!!.coach = userInfo.instructor != 0
        model.notificationSetting.get()!!.type = userInfo.category != 0
        model.notificationSetting.get()!!.schedule = userInfo.calendar != 0

    }

    private fun viewClickEvent() {
        binding.imgUserInfo.setOnClickListener {
            // 個人資訊
            val action = MainFragmentDirections.actionMainFragmentToBodyInfoFragment()
            action.mode = BodyInfoFragmentType.Main
            (activity as MainActivity).rootNavController.navigate(action)
        }
        binding.imgBookmark.setOnClickListener {
            // 收藏
            val action = MainFragmentDirections.actionMainFragmentToUserBookmarkFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }
        binding.imgCalendar.setOnClickListener {
            // 行事曆
            val action = MainFragmentDirections.actionMainFragmentToUserScheduleFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }
        binding.imgSetting.setOnClickListener {
            // 個人設定
            val action = MainFragmentDirections.actionMainFragmentToUserSettingFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.layoutFollow.setOnClickListener {
            // 追蹤管理
            val action = MainFragmentDirections.actionMainFragmentToUserFollowFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }
        binding.layoutReceipt.setOnClickListener {
            // 發票開立設定
            val action = MainFragmentDirections.actionMainFragmentToReceiptFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.layoutBrowseRecord.setOnClickListener {
            // 瀏覽紀錄
            val action = MainFragmentDirections.actionMainFragmentToUserHistoryFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.layoutOther.setOnClickListener {
            // 更多
            val action = MainFragmentDirections.actionMainFragmentToUserMoreFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.opLogo.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToLoginOpenPointFragment()
            action.loginOrBind = "bingOP"
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.itemFamilyPlanMain.viewContent.setOnClickListener {
            // 確認有購買家庭方案。 有 ->成員設定、無 -> 購買家庭專案
            if (model.userInfo!!.user.subscription) {
                // 有訂閱家庭方案。才讓主帳新增成員
                if (model.userInfo!!.user.subscription_type == "0") {
                    // 個人方案
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage("您目前訂閱的是個人方案僅限於個人使用，如計畫與朋友共享使用需升級訂閱家庭方案。")
                        setPositiveButton("確定") { dialog: DialogInterface, _: Int -> dialog.dismiss() }
                        setCancelable(false)
                    }

                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))

                } else if (model.userInfo!!.user.subscription_type == "1") {
                    //家庭方案
                    val action = MainFragmentDirections.actionMainFragmentToFamilyPlanAddMemberFragment()
                    (activity as MainActivity).rootNavController.navigate(action)
                }

            } else {
                // 沒訂閱。進購買畫面
                if (model.userInfo!!.user.email_verified) {
                    val action = MainFragmentDirections.actionMainFragmentToPaymentFrgment()
                    (activity as MainActivity).rootNavController.navigate(action)
                } else {
                    // 前往信箱驗證
                    val action = MainFragmentDirections.actionMainFragmentToPaymentMailVerifyFragment()
                    (activity as MainActivity).rootNavController.navigate(action)
                }
            }
        }

        binding.itemFamilyPlanChild.viewContent.setOnClickListener {
            // 子帳自行離開家庭方案
            val builder = AlertDialog.Builder(requireActivity())
            builder.apply {
                setMessage("請問是否離開${ AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), model.userInfo!!.family!!.name)}的家庭方案。")
                setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                    UserApiClient.leaveFamilyGroup(model.appState.accessToken.get()!!,
                        object : ApiController<ResponseSimpleFormat>(requireContext(), false) {
                            override fun onSuccess(response: ResponseSimpleFormat) {
                                dialog.dismiss()
                                Toast.makeText(requireActivity(), response.error_message, Toast.LENGTH_SHORT).show()
                                val navController = findNavController()
                                navController.run {
                                    popBackStack()
                                    navigate(R.id.userFragment)
                                }
                            }
                        })
                }
                setNegativeButton("取消") { dialog: DialogInterface, _: Int ->
                    dialog.dismiss()
                }
                setCancelable(false)
            }

            val alertDialog = builder.create()
            alertDialog.show()
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
        }

        binding.itemEditFamilyPlanChild.viewContent.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToFamilyPlanAddMemberFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        binding.itemAffiliateMarketing.viewContent.setOnClickListener {
            // 分享你的推薦代碼
            if (model.userInfo != null) {
                if (model.userInfo!!.user.op_bundled) {
                    val action = MainFragmentDirections.actionMainFragmentToShareAffiliateMarketingFragment()
                    (activity as MainActivity).rootNavController.navigate(action)
                } else {
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setTitle(getString(R.string.affiliateMarketingTitle))
                        setMessage(getString(R.string.affiliateMarketingSubTitle))
                        setPositiveButton("前往綁定") { _: DialogInterface?, _: Int ->
                            val action = MainFragmentDirections.actionMainFragmentToLoginOpenPointFragment()
                            action.loginOrBind = "bingOP"
                            (activity as MainActivity).rootNavController.navigate(action)
                        }
                        setNegativeButton("忍痛放棄") { dialog, _ ->
                            val action = MainFragmentDirections.actionMainFragmentToShareAffiliateMarketingFragment()
                            (activity as MainActivity).rootNavController.navigate(action)
                        }
                        setCancelable(false)
                    }
                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                }
            } else {
                Toast.makeText(requireActivity(), "請先登入", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun setTodayCourseTabs() {
        //今日課程
        val current = Calendar.getInstance()
        var date = Day(current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH))

        val data = mutableListOf<String>()
        val dateListFull = mutableListOf<String>()

        for (index in 1..5) {
            val title = "${date.month + 1}月${date.day}"
            val titleFull = "${date.year}-${date.month + 1}-${date.day}"
            data.add(title)
            dateListFull.add(titleFull)
            date = date.nextDay()
        }

        //TabLayout
        val adapter = UserTabAdapter(this, dateListFull)
        binding.apply {
            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = adapter
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                tab.text = data[position]
            }.attach()
        }

    }
}