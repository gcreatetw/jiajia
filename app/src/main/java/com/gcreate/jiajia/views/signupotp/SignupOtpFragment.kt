package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.databinding.FragmentSignupOtpBinding

class SignupOtpFragment : Fragment() {

    private val arg: SignupOtpFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentSignupOtpBinding.inflate(inflater, container, false)

        binding.apply {

            singUpToolbar.apply {
                if (arg.mode == SignupOtpType.ModifyPassword) {
                    title = "修改密碼"
                    val textSting = "只要透過簡單的SMS簡訊驗證，我們將會為您重設密碼"
                    tvLabel2.text = textSting
                } else if (arg.mode == SignupOtpType.ForgetPassword) {
                    title = "忘記密碼"
                    val textSting = "只要透過簡單的SMS簡訊驗證，我們將會為您重設密碼"
                    tvLabel2.text = textSting
                }

                setNavigationOnClickListener {
                    findNavController().navigateUp()
                }
            }

            btnOtpVerify.setOnClickListener {
                val action = SignupOtpFragmentDirections.actionSignupOtpFragmentToSignupOtpPhoneNumberFragment()
                action.mode = arg.mode
                findNavController().navigate(action)
            }
        }

        return binding.root
    }
}