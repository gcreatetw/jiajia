package com.gcreate.jiajia.views.payment

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.PaymentApiClient
import com.gcreate.jiajia.api.payment.request.RequestProcessToCheckout
import com.gcreate.jiajia.api.payment.response.*
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.PaymentInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentPaymentBinding
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_payment.*


class PaymentFragment : Fragment() {

    private var binding: FragmentPaymentBinding? = null

    companion object {
        @SuppressLint("StaticFieldLeak")
        var navController: NavController? = null
        var payMethod = ""
        var responsePayStoreByOpenWallet: ResponsePayStoreByOpenWallet? = null
    }

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPaymentBinding.inflate(inflater, container, false)

        binding?.apply {

            toolbar.navigationIcon?.setTint(resources.getColor(R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                model.plusChannel = null
                model.plusChannelOrderList = null
                PaymentPlusFragment.orderPlusIdList.clear()
                backPressed()
            }

            navController = root.findViewById<FragmentContainerView>(R.id.fragmentContainerView).findNavController()

            val localNavHost = childFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
            val graph = localNavHost.navController.navInflater.inflate(R.navigation.payment_nav)

            model.paymentInfo.type = PaymentInfo.Type.noSelected

//            if (model.appState.haveSubscription.get()!!) {
//                // 有訂閱 ->
//                toolbar.title = "選擇加值頻道"
//                btnNext.text = "查看訂單內容"
//
//                graph.setStartDestination(R.id.paymentPlusFragment)
//                localNavHost.navController.graph = graph
//            }

            btnNext.setOnClickListener {
                when (navController?.currentDestination?.id) {

                    R.id.paymentSetFragment -> {
                        if (model.showBonusChannel) {
                            toolbar.title = "選擇加值頻道"
                            val action = PaymentSetFragmentDirections.actionPaymentSetFragmentToPaymentPlusFragment()
                            navController?.navigate(action)
                            btnNext.text = "查看訂單內容"
                        } else {
                            PaymentApiClient.processToCheckout(
                                model.appState.accessToken.get() ?: "",
                                RequestProcessToCheckout(arrayListOf()),
                                object : ApiController<ResponseProcessToCheckout>(requireContext()) {
                                    override fun onSuccess(response: ResponseProcessToCheckout) {
                                        model.subscriptionsPlan = response.cart.SubscriptionPlan

                                        if (response.cart.PlusChannel.isNotEmpty()) {
                                            model.plusChannel = response.cart.PlusChannel[0]
                                            model.plusChannelOrderList = response.cart.PlusChannel
                                        }
                                        model.firstPay = response.firstpay
                                        model.totalAmount = response.total_amount
                                        model.discountPrice = response.cart.Coupon

                                        if(model.isUseAffiliateMarketingReferral){
                                            toolbar.title = "訂單內容"
                                            val action = PaymentSetFragmentDirections.actionPaymentSetFragmentToPaymentOrderFragment()
                                            navController?.navigate(action)
                                            btnNext.text = "付款"
                                        }else{
                                            toolbar.title = "輸入推薦代碼"
                                            val action = PaymentSetFragmentDirections.actionPaymentSetFragmentToPaymentReferralInputFragment()
                                            navController?.navigate(action)
                                            btnNext.text = "略過"
                                        }

                                    }
                                }
                            )
                        }
                    }

                    R.id.paymentReferralInputFragment ->{

                        if( btnNext.text == "略過"){
                            toolbar.title = "訂單內容"
                            val action = PaymentReferralInputFragmentDirections.actionPaymentReferralInputFragmentToPaymentOrderFragment()
                            navController?.navigate(action)
                            btnNext.text = "付款"
                        }else if(btnNext.text == "下一步"){
                            val textInputLayout = root.findViewById<TextInputLayout>(R.id.text_input_layout_phone_number)
                            val referralText =  textInputLayout.editText?.text.toString()

                            if(referralText.length != 6){
                                textInputLayout.editText?.error = "您輸入的推薦代碼錯誤或不存在，請查明後重新輸入。"
                            }else{
                                addAffiliateMarketingReferral(referralText)
                            }
                        }
                    }

                    R.id.paymentPlusFragment -> {

                        if (model.appState.haveSubscription.get()!! && PaymentPlusFragment.orderPlusIdList.size == 0) {
                            Toast.makeText(requireActivity(), "請選擇加值服務", Toast.LENGTH_SHORT).show()
                        } else {
                            PaymentApiClient.processToCheckout(
                                model.appState.accessToken.get() ?: "",
                                RequestProcessToCheckout(PaymentPlusFragment.orderPlusIdList),
                                object : ApiController<ResponseProcessToCheckout>(requireContext()) {
                                    override fun onSuccess(response: ResponseProcessToCheckout) {
                                        model.subscriptionsPlan = response.cart.SubscriptionPlan

                                        if (response.cart.PlusChannel.isNotEmpty()) {
                                            model.plusChannel = response.cart.PlusChannel[0]
                                            model.plusChannelOrderList = response.cart.PlusChannel
                                        }

                                        model.totalAmount = response.total_amount
                                        model.firstPay = response.firstpay
                                        model.discountPrice = response.cart.Coupon
                                        toolbar.title = "訂單內容"
                                        val action = PaymentPlusFragmentDirections.actionPaymentPlusFragmentToPaymentOrderFragment()
                                        navController?.navigate(action)
                                        btnNext.text = "付款"

                                    }
                                }
                            )
                        }
                    }

                    R.id.paymentOrderFragment -> {
                        when (model.paymentInfo.type) {
                            PaymentInfo.Type.Openpoint -> {
                                payMethod = "Openpoint"
                                PaymentApiClient.payStoreByOpenWallet(
                                    model.appState.accessToken.get() ?: "",
                                    object : ApiController<ResponsePayStoreByOpenWallet>(requireContext()) {
                                        override fun onSuccess(response: ResponsePayStoreByOpenWallet) {
                                            // open OpenWallet App
                                            responsePayStoreByOpenWallet = response
                                            val packageName = response.data?.paymentUrlApp ?: ""
                                            val url = response.data?.paymentUrlApp
                                            val launchIntent = requireContext().packageManager.getLaunchIntentForPackage("openpointapp")
                                            when {
                                                launchIntent != null -> {
                                                    startActivity(launchIntent) //null pointer check in case package name was not found
                                                }
                                                url != null -> {
                                                    try {
                                                        val intent = Intent(Intent.ACTION_VIEW)
                                                        intent.data = Uri.parse(url)
                                                        startActivity(intent)
                                                    } catch (e: Exception) {
                                                        Toast.makeText(requireActivity(), "請確認是否安裝 OPEN POINT APP", Toast.LENGTH_LONG).show()
                                                        model.plusChannel = null
                                                        model.plusChannelOrderList = null
                                                        PaymentPlusFragment.orderPlusIdList.clear()
                                                        backPressed()
                                                        return
                                                    }
                                                }
                                                else -> {
                                                    return
                                                }
                                            }
                                            navigateToPaymentProgressFragment()
                                        }
                                    }
                                )
                            }
                            PaymentInfo.Type.CreditCard -> { // 國泰信用卡支付
                                payMethod = "cathaybk"
                                PaymentApiClient.payStoreByCathaybk(
                                    model.appState.accessToken.get() ?: "",
                                    object : ApiController<ResponsePayStoreByCathaybk>(requireContext()) {
                                        override fun onSuccess(response: ResponsePayStoreByCathaybk) {
                                            // open Browser
                                            val url = response.data?.acsUrl ?: "https://www.google.com"
                                            val intent = Intent(Intent.ACTION_VIEW)
                                            intent.data = Uri.parse(url)
                                            startActivity(intent)
                                            navigateToPaymentProgressFragment()
                                        }
                                    }
                                )

                            }
                            PaymentInfo.Type.iCash -> {
                                payMethod = "iCash"
                                PaymentApiClient.payStoreByICash(
                                    model.appState.accessToken.get() ?: "",
                                    object : ApiController<ResponsePayStoreByICash>(requireContext()) {
                                        override fun onSuccess(response: ResponsePayStoreByICash) {
                                            val packageName = response.data?.url ?: ""
                                            val url = response.data?.url
                                            val launchIntent = requireContext().packageManager.getLaunchIntentForPackage(packageName)
                                            when {
                                                launchIntent != null -> {
                                                    startActivity(launchIntent) //null pointer check in case package name was not found
                                                }
                                                url != null -> {
                                                    try {
                                                        val intent = Intent(Intent.ACTION_VIEW)
                                                        intent.data = Uri.parse(url)
                                                        startActivity(intent)
                                                    } catch (e: Exception) {
                                                        Toast.makeText(requireActivity(), "請確認是否安裝 ICASH APP", Toast.LENGTH_LONG).show()
                                                        model.plusChannel = null
                                                        model.plusChannelOrderList = null
                                                        PaymentPlusFragment.orderPlusIdList.clear()
                                                        backPressed()
                                                        return
                                                    }
                                                }
                                                else -> {
                                                    return
                                                }
                                            }
                                            navigateToPaymentProgressFragment()
                                        }
                                    }
                                )
                            }
                            PaymentInfo.Type.noSelected -> {
                                Toast.makeText(requireActivity(), "請選擇支付方式", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }

        getDeepLinkParams()

        return binding?.root
    }

    private fun navigateToPaymentProgressFragment() {
        toolbar.title = model.paymentInfo.type.displayName
        toolbar.navigationIcon = null
        val action = PaymentOrderFragmentDirections.actionPaymentOrderFragmentToPaymentProgressFragment(null)
        navController?.navigate(action)
        binding?.btnNext?.visibility = View.GONE
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun getDeepLinkParams() {
        val data = requireActivity().intent.data
        // TODO 如果是 ICP 跟國泰支付。 會使用deeplink 回到 payment fragment
        if (data != null) {
            val typeString = data.getQueryParameter("type")
            val objectID = data.getQueryParameter("objectID")
            val payMethod = data.getQueryParameter("payment_method")

            binding?.btnNext?.visibility = View.GONE
            binding!!.toolbar.title = model.paymentInfo.type.displayName
            binding!!.toolbar.navigationIcon = null

            if (model.appState.haveSubscription.get()!!) {
                val action = PaymentPlusFragmentDirections.actionPaymentPlusFragmentToPaymentProgressFragment(objectID)
                navController?.navigate(action)
            } else {
                val action = PaymentSetFragmentDirections.actionPaymentSetFragmentToPaymentProgressFragment(objectID)
                navController?.navigate(action)
            }

            requireActivity().intent.data = null
        }
    }

    private fun addAffiliateMarketingReferral(code: String) {
        val paramObject = JsonObject()
        paramObject.addProperty("code", code)

        UserApiClient.addAffiliateMarketingReferral(model.appState.accessToken.get()!!,
            paramObject,
            object : ApiController<ResponseSimpleFormat2>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat2) {
                    var text = ""
                    if (response.error == 0) {
                        text = "輸入成功"
                    } else if (response.error == 1) {
                        text = response.message
                    }
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.apply {
                        setMessage(text)
                        setPositiveButton("確認") { dialog: DialogInterface, _: Int ->
                            if (response.error == 0) {
                                model.isUseAffiliateMarketingReferral = true
                                model.userInfo!!.user.referral_code = code

                                toolbar.title = "訂單內容"
                                val action = PaymentReferralInputFragmentDirections.actionPaymentReferralInputFragmentToPaymentOrderFragment()
                                navController?.navigate(action)
                                binding?.btnNext?.text = "付款"
                            }
                            dialog.dismiss()
                        }
                        setCancelable(false)
                    }
                    val alertDialog = builder.create()
                    alertDialog.show()
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))
                }
            })
    }

}