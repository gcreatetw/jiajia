package com.gcreate.jiajia.views.user.follow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserFollowByAddedValueBinding
import com.gcreate.jiajia.adapters.membercentre.UserFollowByAddedValueRecyclerViewAdapter
import kotlinx.android.synthetic.main.card_user_follow_by_type.view.*

class UserFollowByAddedValueFragment : Fragment() {

    private var binding: FragmentUserFollowByAddedValueBinding? = null
    private var dataList = mutableListOf<UserFollowByAddedValueItem>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserFollowByAddedValueBinding.inflate(inflater, container, false)

        loadAdditionalChannelFollowState()

        val rv = binding?.recyclerView
        rv?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
//        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f,requireContext()).roundToInt()))
        rv?.adapter = UserFollowByAddedValueRecyclerViewAdapter() {
            val isFollow = model.listFollow!!.channel[it].followed
            model.listFollow!!.channel[it].followed = !isFollow

            val followImg = binding!!.recyclerView.findViewHolderForLayoutPosition(it)!!.itemView.img_follow
            if (model.listFollow!!.channel[it].followed) {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.baseline_notifications_24))
            } else {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.outline_add_alert_24))
            }

            switchFollowState(model.listFollow!!.channel[it].id)
        }


        return binding?.root

    }

    private fun loadAdditionalChannelFollowState() {
        val additionalChannelFollowList = model.listFollow!!.channel
        for (i in additionalChannelFollowList.indices) {

            dataList.add(i, UserFollowByAddedValue(
                additionalChannelFollowList[i].name,
                "",
                additionalChannelFollowList[i].followed))
        }

        dataList.add(UserFollowByAddedValueTail())

        binding?.list = dataList
    }

    private fun switchFollowState(channelId: Int) {
        val request = RequestFollowCategory("", "", channelId.toString(),"")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(requireContext(), false) {
            override fun onSuccess(response: Unit) {
                model.loadFollowState(requireContext())
            }
        })
    }
}