package com.gcreate.jiajia.views.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseBrowserCourseRecord
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.VideoType
import com.gcreate.jiajia.data.VideoTypeItem
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserHistoryClassBinding
import com.gcreate.jiajia.adapters.home.VideoTypeRecyclerViewAdapter

class UserHistoryClassFragment : Fragment() {

    private var binding: FragmentUserHistoryClassBinding? = null

    private var dataList = mutableListOf<VideoTypeItem>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentUserHistoryClassBinding.inflate(inflater, container, false)

        loadDataFromApi()

        val dateLayoutManager = LinearLayoutManager(requireActivity())
        val dateRv = binding?.recyclerView
        dateLayoutManager.orientation = LinearLayoutManager.VERTICAL
        dateRv?.layoutManager = dateLayoutManager
        dateRv?.adapter = VideoTypeRecyclerViewAdapter(requireContext(), this::onClickMore, this::onClickClass)

        return binding?.root
    }

    private fun onClickMore(position: Int) {
        val action = UserHistoryFragmentDirections.actionUserHistoryFragmentToVideoSortFragment()
        action.title = (dataList[position] as VideoType).typeName
        action.categoryId = (dataList[position] as VideoType).id
        action.isFollowed = model.isFollowCategory((dataList[position] as VideoType).id)
        binding!!.root.findNavController().navigate(action)
    }

    private fun onClickClass(typeIndex: Int, classIndex: Int) {
        val action = UserHistoryFragmentDirections.actionUserHistoryFragmentToVideoPageFragment()
        action.courseId = (dataList[typeIndex] as VideoType).classList[classIndex].id
        binding!!.root.findNavController().navigate(action)
    }

    private fun loadDataFromApi() {
        UserApiClient.getBrowserCourseRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseBrowserCourseRecord>(requireActivity(), true) {
                override fun onSuccess(response: ResponseBrowserCourseRecord) {
                    val recordList = response.record

                    dataList.clear() // 清空陣列，防止重複加入

                    for (i in recordList.indices) {
                        dataList.add(VideoType(recordList[i].id, recordList[i].title, recordList[i].course))
                    }

                    binding?.list = dataList
                }
            })
    }
}