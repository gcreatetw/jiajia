package com.gcreate.jiajia.views.user.setting

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseUserCouponRecord
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserCounponRecoidBinding
import com.gcreate.jiajia.adapters.membercentre.UserCouponRecordAdapter


class UserCouponRecordFragment : Fragment() {


    val mAdapter = UserCouponRecordAdapter(mutableListOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentUserCounponRecoidBinding.inflate(inflater, container, false)


        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
        }

        UserApiClient.getUserCouponRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseUserCouponRecord>(requireActivity(), false) {
                @SuppressLint("NotifyDataSetChanged")
                override fun onSuccess(response: ResponseUserCouponRecord) {
                    mAdapter.data = response.data
                    mAdapter.notifyDataSetChanged()
                }
            })

        return binding.root
    }


}