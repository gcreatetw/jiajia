package com.gcreate.jiajia.views.user.follow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore

import com.gcreate.jiajia.adapters.tab.UserFollowTabAdapter
import com.gcreate.jiajia.databinding.FragmentUserFollowBinding
import com.google.android.material.tabs.TabLayoutMediator

class UserFollowFragment : Fragment() {

    private var binding: FragmentUserFollowBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserFollowBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = binding?.userFollowToolbar
        toolBar?.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(),R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }

        //TabLayout
        val tabCount = if (model.listFollow!!.channel.isEmpty()) {
            3
        } else {
            4
        }
        val adapter = UserFollowTabAdapter(tabCount, this)
        val tabLayout = binding?.tabLayout
        val viewPager = binding?.viewpager
        viewPager?.isUserInputEnabled = true
        viewPager?.adapter = adapter
        TabLayoutMediator(tabLayout!!, viewPager!!) { tab, position ->
            when (position) {
                0 -> tab.text = "分類"
                1 -> tab.text = "教練"
                2 -> tab.text = "訓練計畫"
                3 -> tab.text = "加值頻道"
            }
        }.attach()

        return binding?.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}