package com.gcreate.jiajia.views.user.familyPlan

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestDeleteFamilyMember
import com.gcreate.jiajia.api.user.response.ResponseDeleteFamilyMember
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentFamilyPlanEditMemberBinding
import com.gcreate.jiajia.adapters.membercentre.familyPlan.FamilyPlanEditMemberAdapter

class FamilyPlanEditMemberFragment : Fragment() {

    private lateinit var binding: FragmentFamilyPlanEditMemberBinding
    private val removeMemberList = mutableListOf<Int>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFamilyPlanEditMemberBinding.inflate(inflater, container, false)

        setToolBar()

        binding.rvContent.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

            val memberDataList = FamilyPlanAddMemberFragment.dataList

            val mAdapter = FamilyPlanEditMemberAdapter(memberDataList!!)
            adapter = mAdapter

            mAdapter.addChildClickViewIds(R.id.btn_remove_member)
            mAdapter.setOnItemChildClickListener { _, _, position ->
                removeMemberList.add(mAdapter.data[position].id)
                memberDataList.removeAt(position)
                mAdapter.notifyItemRemoved(position)
            }

        }

        return binding.root
    }

    private fun setToolBar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.family_plan_menu, menu)
        menu.findItem(R.id.family_plan_edit_check).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
        menu.findItem(R.id.family_plan_edit).isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.family_plan_edit_check -> {
                // call api
                deleteFamilyMember()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun deleteFamilyMember() {
        val requestBody = RequestDeleteFamilyMember(removeMemberList)
        UserApiClient.deleteFamilyMember(model.appState.accessToken.get()!!, requestBody,
            object : ApiController<ResponseDeleteFamilyMember>(requireActivity()) {
                override fun onSuccess(response: ResponseDeleteFamilyMember) {
                    if (!response.error) {
                        // 刪除成功
                        requireActivity().onBackPressed()
                    }
                }
            })
    }

}