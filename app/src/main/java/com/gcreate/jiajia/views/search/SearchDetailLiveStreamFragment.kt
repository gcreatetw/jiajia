package com.gcreate.jiajia.views.search

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.search.SearchLiveAdapter
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.search.SearchApiClient
import com.gcreate.jiajia.api.search.request.RequestSearchCourseBody
import com.gcreate.jiajia.data.LiveStream
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSearcgDetailLiveStreamBinding
import com.gcreate.jiajia.listener.UpdateSearchKeywordListener
import com.gcreate.jiajia.listener.UpdateSearchKeywordListenerManager
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import kotlin.math.roundToInt


class SearchDetailLiveStreamFragment(private var keyword: String) : Fragment(), UpdateSearchKeywordListener {

    private lateinit var binding: FragmentSearcgDetailLiveStreamBinding
    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())

    private var page = 1
    private var maxDataCount = 0
    private lateinit var mAdapterSearchLive: SearchLiveAdapter

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.loadCourseFilterSetting(requireContext())
        UpdateSearchKeywordListenerManager.instance!!.registerListener(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearcgDetailLiveStreamBinding.inflate(inflater, container, false)

        mAdapterSearchLive = SearchLiveAdapter(mutableListOf())

        setImgFilterColor()
        loadDataFromApi()

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = mAdapterSearchLive

            mAdapterSearchLive.apply {
                addFooterView(layoutInflater.inflate(R.layout.layout_footer, null))
                setOnItemClickListener { _, _, position ->
                    if (mAdapterSearchLive.data[position].video_enable) {
                        val action = SearchDetailFragmentDirections.actionSearchDetailFragmentToVideoPageFragment()
                        action.courseId = mAdapterSearchLive.data[position].courseId
                        (activity as MainActivity).rootNavController.navigate(action)
                    }
                }
                loadMoreModule.setOnLoadMoreListener {
                    if (maxDataCount == 0 || mAdapterSearchLive.data.size < maxDataCount) {
                        page++
                        loadDataFromApi()
                        mAdapterSearchLive.loadMoreModule.loadMoreFail()
                    } else {
                        mAdapterSearchLive.loadMoreModule.loadMoreEnd()
                        mAdapterSearchLive.loadMoreModule.loadMoreComplete()
                        mAdapterSearchLive.loadMoreModule.loadMoreFail()
                    }
                }
            }
        }

        binding.imgFilter.setOnClickListener {
            activity?.supportFragmentManager.let {
                val dialog = HotVideoFilterDialog(filter) { _filter ->
                    filter = _filter
                    setImgFilterColor()
                    loadDataFromApi()
                }
                dialog.show(it!!, "")
            }
        }

        return binding.root
    }

    private fun setImgFilterColor() {
        val enable = filter.intensityIdList.isNotEmpty()
                || filter.coachIdList.isNotEmpty()
                || filter.equipmentIdList.isNotEmpty()

        val drawable = binding.imgFilter.drawable
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun loadDataFromApi() {

        val request = RequestSearchCourseBody(
            keyword,
            true,
            filter.date,
            filter.intensityIdList,
            filter.coachIdList,
            filter.equipmentIdList,
            25,
            page)

        SearchApiClient.searchCourse(model.appState.accessToken.get()!!,
            request, object : ApiController<ResponseCourses>(requireContext(), true) {
                override fun onSuccess(response: ResponseCourses) {
                    val responseObject = response.courses

                    for (i in responseObject.indices) {
                        mAdapterSearchLive.addData(LiveStream(
                            responseObject[i].id,
                            responseObject[i].live_time!!,
                            "",
                            responseObject[i].preview_image.toString(),
                            responseObject[i].title,
                            null,
                            responseObject[i].user_img,
                            responseObject[i].name,
                            responseObject[i].isCalendared,
                            responseObject[i].videoEnable,
                        ))
                    }

                    maxDataCount = response.total

                    mAdapterSearchLive.loadMoreModule.loadMoreEnd()
                    mAdapterSearchLive.loadMoreModule.loadMoreComplete()


                    val resultCountText = "共 ${response.total} 筆結果"
                    binding.tvClassCount.text = resultCountText
                }
            })
    }

    override fun updateKeyword(keyword: String?) {
        this.keyword = keyword!!
        loadDataFromApi()
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateSearchKeywordListenerManager.instance!!.unRegisterListener(this)
    }
}