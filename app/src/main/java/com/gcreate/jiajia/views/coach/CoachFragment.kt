package com.gcreate.jiajia.views.coach

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.request.RequestCoachCourseList
import com.gcreate.jiajia.api.homerecommend.request.RequestCoachProfile
import com.gcreate.jiajia.api.homerecommend.response.ResponseCoachCourseList
import com.gcreate.jiajia.api.homerecommend.response.ResponseCoachProfile
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentCoachBinding
import com.gcreate.jiajia.adapters.home.coach.CoachRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import kotlin.math.roundToInt

class CoachFragment : Fragment() {

    private var binding: FragmentCoachBinding? = null
    private val args: CoachFragmentArgs by navArgs()
    private var dataList: MutableList<CoachItem> = mutableListOf()
    private var profile: ResponseCoachProfile? = null
    private val data = mutableListOf<CoachItem>()
    private val newLessonList = mutableListOf<Lesson>()
    private lateinit var courseDataList: List<Course>
    private lateinit var menu: Menu

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCoachBinding.inflate(inflater, container, false)

        val toolBar = binding?.coachToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }
        toolBar?.title = args.title

        binding?.coachItemList = dataList

        val layoutManager = LinearLayoutManager(requireActivity())
        val rv = binding?.recyclerView
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = layoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = CoachRecyclerViewAdapter(requireContext(), inflater, this::onClickNewLesson, this::onClickLesson, this::onClickChip)

        setHasOptionsMenu(true)

        loadDataFromApi(-1)

        return binding?.root
    }

    private fun loadDataFromApi(categoryId: Int) {
        newLessonList.clear()
        if (categoryId < 0) {
            HomeRecommendApiClient.coachProfile(RequestCoachProfile(args.id), object : ApiController<ResponseCoachProfile>(requireContext()) {
                override fun onSuccess(response: ResponseCoachProfile) {
                    profile = response
                    binding!!.coachToolbar.title = profile!!.user.name

                    if (profile!!.category.isNotEmpty()) {
                        loadCourseList(profile!!.category[0].id)
                    } else {
                        val user = profile!!.user
                        data.add(CoachTitle(
                            user.user_img,
                            getCateDisplayStr(profile!!.category),
                            profile!!.course_count,
                            user.detail
                        ))
                        binding?.coachItemList = data
                    }
                }
            })
        } else {
            loadCourseList(categoryId)
        }
    }

    private fun loadCourseList(categoryId: Int) {
        HomeRecommendApiClient.coachCourseList(
            RequestCoachCourseList(args.id, categoryId, 5, 1),
            object : ApiController<ResponseCoachCourseList>(requireContext()) {
                override fun onSuccess(response: ResponseCoachCourseList) {
                    reloadRecyclerView(response, categoryId)
                }
            })
    }

    private fun reloadRecyclerView(courseList: ResponseCoachCourseList, categoryId: Int) {
        val data = mutableListOf<CoachItem>()

        for (lastestCourse in profile!!.latest) {
            var price = 0
            lastestCourse.price?.apply {
                price = this.toFloat().toInt()
            }
            newLessonList.add(
                Lesson(
                    lastestCourse.id,
                    lastestCourse.preview_image,
                    lastestCourse.short_detail,
                    0,
                    "",
                    price,
                    true
                )
            )
        }
        val user = profile!!.user
        data.add(CoachTitle(
            user.user_img,
            getCateDisplayStr(profile!!.category),
            profile!!.course_count,
            user.detail
        ))
        data.add(CoachNewLessonList(newLessonList))

        data.add(CoachLessonTitle(
            user.name,
            profile!!.category,
            categoryId
        ))
        courseDataList = courseList.course
        data.addAll(courseList.course)

        data.add(CoachTail())

        dataList = data
        binding?.coachItemList = dataList
    }

    private fun getCateDisplayStr(cate: List<ClassCategory>): String {
        if (cate.isEmpty()) {
            return ""
        }
        var str = StringBuilder(cate[0].title)
        for (index in 1..cate.lastIndex) {
            str.append(" ${cate[index].title}")
        }

        return str.toString()
    }

    private fun onClickNewLesson(position: Int) {
        val action = CoachFragmentDirections.actionCoachFragmentToVideoPageFragment()
        action.courseId = newLessonList[position].id
        findNavController().navigate(action)

    }

    fun onClickLesson(position: Int) {
        val action = CoachFragmentDirections.actionCoachFragmentToVideoPageFragment()
        action.courseId = courseDataList[position - 3].id
        findNavController().navigate(action)

    }

    fun onClickChip(checkId: Int) {
        loadCourseList(checkId)
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.coach_menu, menu)
        this.menu = menu
        if (args.isFollowed) {
            menu.findItem(R.id.coach_notification_item).setIcon(R.drawable.baseline_add_alert_24)
        } else {
            menu.findItem(R.id.coach_notification_item).setIcon(R.drawable.outline_add_alert_24)
        }

    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.coach_notification_item -> {
                if (model.appState.accessToken.get().isNullOrEmpty()) {
                    val action = CoachFragmentDirections.actionCoachFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)
                } else {
                    for (data in model.listFollow!!.instructor) {
                        if (args.id == data.id) {
                            data.followed = !data.followed
                            if (data.followed) {
                                menu.findItem(R.id.coach_notification_item).setIcon(R.drawable.baseline_add_alert_24)
                            } else {
                                menu.findItem(R.id.coach_notification_item).setIcon(R.drawable.outline_add_alert_24)
                            }
                            switchFollowState(args.id)
                            break
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun switchFollowState(coachId: Int) {
        val request = RequestFollowCategory(coachId.toString(), "", "","")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(requireContext(), false) {
            override fun onSuccess(response: Unit) {
                model.loadFollowState(requireContext())
            }
        })
    }
}