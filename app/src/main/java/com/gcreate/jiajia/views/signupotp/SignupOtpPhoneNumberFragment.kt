package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.FragmentSignupOtpPhoneNumberBinding

class SignupOtpPhoneNumberFragment : Fragment() {

    private lateinit var binding: FragmentSignupOtpPhoneNumberBinding
    private val arg: SignupOtpPhoneNumberFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignupOtpPhoneNumberBinding.inflate(inflater, container, false)

        binding.apply {

            val toolBar = singUpPhoneNumberToolbar
            toolBar.navigationIcon?.setTint(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.white
                )
            )
            (activity as AppCompatActivity).setSupportActionBar(toolBar)

            toolBar.setNavigationOnClickListener {
                backPressed()
            }

            textInputLayoutPhoneNumber.apply {
                editText!!.doOnTextChanged { text, _, _, _ ->
                    error = if (text?.isNotEmpty() == true && text.length < 10) {
                        // todo : 還要再加入判斷 電話格式
                        "格式不符。"
                    } else {
                        null
                    }
                    if (text.toString().isEmpty() || text.toString().length < 10) {
                        btnContinue.setBackgroundColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.green_alpha_80
                            )
                        )
                    } else {
                        btnContinue.setBackgroundColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.green
                            )
                        )
                    }

                }
            }

            btnContinue.setOnClickListener {
                // 驗證電話有無註冊
                if (textInputLayoutPhoneNumber.editText!!.text.isNotEmpty()) {
                    if (textInputLayoutPhoneNumber.editText!!.text.toString().length == 10) {
                        val action =
                            SignupOtpPhoneNumberFragmentDirections.actionSignupOtpPhoneNumberFragmentToSignupOtpSendingFragment()
                        action.mode = arg.mode
                        action.phoneForVerify =
                            textInputLayoutPhoneNumber.editText!!.text.toString()
                        findNavController().navigate(action)
                    } else {
                        textInputLayoutPhoneNumber.error = "格式不符。"
                    }
                } else {
                    textInputLayoutPhoneNumber.error = "請輸入手機號碼"
                }
            }
        }

        return binding.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

}