package com.gcreate.jiajia.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.adapters.live.LiveStreamRecyclerViewAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.liveStream.LiveStreamApiClient
import com.gcreate.jiajia.api.liveStream.request.RequestLiveCourses
import com.gcreate.jiajia.api.liveStream.response.ResponseLiveCourses
import com.gcreate.jiajia.data.LiveStream
import com.gcreate.jiajia.data.LiveStreamItem
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentLiveByDateBinding


class LiveByDateFragment(val date: String) : Fragment() {

    private lateinit var binding: FragmentLiveByDateBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLiveByDateBinding.inflate(inflater, container, false)

        getApiData()

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = LiveStreamRecyclerViewAdapter {
                if ((binding.liveStreamList!![it] as LiveStream).video_enable) {
                    val action = MainFragmentDirections.actionMainFragmentToVideoPageFragment()
                    action.courseId = (binding.liveStreamList!![it] as LiveStream).courseId
                    (activity as MainActivity).rootNavController.navigate(action)
                }
            }
        }

        binding.refreshLayout.apply {
            setOnRefreshListener {
                binding.refreshLayout.isRefreshing = false
                getApiData() }
        }

        return binding.root
    }

    private fun getApiData() {

        val request = RequestLiveCourses(date.substring(5), 10, 1)
        LiveStreamApiClient.liveCourses(model.appState.accessToken.get()!!, request, object : ApiController<ResponseLiveCourses>(requireContext()) {
            override fun onSuccess(response: ResponseLiveCourses) {
                val responseObject = response.courses
                val data = mutableListOf<LiveStreamItem>()

                for (i in responseObject.indices)
                    data.add(
                        LiveStream(
                            responseObject[i].id,
                            responseObject[i].live_time!!.split(" ")[1],
                            "",
                            responseObject[i].preview_image.toString(),
                            responseObject[i].title,
                            null,
                            responseObject[i].user_img,
                            responseObject[i].name,
                            responseObject[i].isCalendared,
                            responseObject[i].videoEnable,
                        )
                    )

                if (data.size == 0) {
                    binding.recyclerView.visibility = View.INVISIBLE
                    binding.tvNoData.visibility = View.VISIBLE
                } else {
                    binding.recyclerView.visibility = View.VISIBLE
                    binding.tvNoData.visibility = View.INVISIBLE
                    binding.liveStreamList = data
                }
            }
        })
    }
}