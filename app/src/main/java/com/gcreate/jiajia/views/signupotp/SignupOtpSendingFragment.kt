package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestVerifyAccountBody
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.ProgreeeInfo
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSignupOtpSendingBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.util.GoogleUtil
import com.gcreate.jiajia.util.OtpUtil
import com.gcreate.jiajia.views.login.LoginAllinOneFragmentType
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.json.JSONObject
import retrofit2.Response


class SignupOtpSendingFragment : Fragment() {

    private var binding: FragmentSignupOtpSendingBinding? = null
    private val arg: SignupOtpSendingFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSignupOtpSendingBinding.inflate(inflater, container, false)

        verifyAccount()
//        if (arg.mode == SignupOtpType.Signup) {
//            verifyAccount()
//        } else  {
//            OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpSendingFragment, arg.mode, arg.phoneForVerify)
//        }

        binding?.apply {
            model.progressInfo.set(
                ProgreeeInfo("正在發送確認碼", ProgreeeInfo.Type.PROGRESS)
            )
            viewModel = model

        }

        return binding?.root
    }

    private fun verifyAccount() {
        // 驗證電話有無被使用
        val requestBody = RequestVerifyAccountBody(arg.phoneForVerify)

        UserApiClient.verifyMobile(
            requestBody, object : ApiController<ResponseSimpleFormat>(requireContext(), true) {
                override fun onSuccess(response: ResponseSimpleFormat) {

                    when (arg.mode) {
                        SignupOtpType.Signup -> {
                            // 該電話可以註冊使用
                            OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpSendingFragment,
                                arg.mode,
                                arg.phoneForVerify)
                        }
                        SignupOtpType.ForgetPassword -> {
                            model.progressInfo.set(
                                ProgreeeInfo("該電話尚未註冊，請先註冊", ProgreeeInfo.Type.ERROR)
                            )
                            model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                            Handler(Looper.getMainLooper()).postDelayed({
                                val action = SignupOtpSendingFragmentDirections.actionSignupOtpSendingFragmentToLoginAllinOneFragment()
                                findNavController().navigate(action)
                            }, 1000)
                        }
                        SignupOtpType.ModifyPassword -> {
                            OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpSendingFragment,
                                arg.mode,
                                arg.phoneForVerify)
                        }
                    }

                }

                override fun onFail(httpResponse: Response<ResponseSimpleFormat>): Boolean {
                    try {
                        val jObjError = JSONObject(httpResponse.errorBody()!!.string())
                        when (arg.mode) {
                            SignupOtpType.Signup -> {
                                model.progressInfo.set(
                                    ProgreeeInfo(jObjError.get("Error Message").toString(), ProgreeeInfo.Type.ERROR)
                                )
                                model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
                                if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "google") {
                                    GoogleUtil.signOut()

                                } else if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "facebook") {
                                    Firebase.auth.signOut()
                                }

                                Handler(Looper.getMainLooper()).postDelayed({
                                    val action = SignupOtpSendingFragmentDirections.actionSignupOtpSendingFragmentToLoginAllinOneFragment()
                                    findNavController().navigate(action)
                                }, 2000)

                            }
                            SignupOtpType.ForgetPassword -> {
                                OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpSendingFragment,
                                    arg.mode,
                                    arg.phoneForVerify)
                            }
                            SignupOtpType.ModifyPassword -> {
                                OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpSendingFragment,
                                    arg.mode,
                                    arg.phoneForVerify)
                            }
                        }
                    } catch (e: Exception) {
                    }
                    return true
                }
            }
        )
    }

}