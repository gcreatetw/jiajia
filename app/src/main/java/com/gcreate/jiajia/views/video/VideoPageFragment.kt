package com.gcreate.jiajia.views.video

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.VideoPageRecyclerViewAdapter
import com.gcreate.jiajia.api.ApiUtil
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourseDetail
import com.gcreate.jiajia.api.course.response.ResponseCourseDetail
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.dataobj.CourseDetail
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.ScheduleApiClient
import com.gcreate.jiajia.api.schedule.request.RequestAddScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseAddScheduleBody
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestCourseId
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.api.user.response.ResponseVideoEnable
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentVideoPageBinding
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.gcreate.jiajia.sharedpreferences.model.ThirdLoginResponse
import com.gcreate.jiajia.util.DateTool
import com.gcreate.jiajia.util.GoogleUtil
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.views.login.LoginAllinOneFragmentType
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonObject
import kotlinx.coroutines.runBlocking
import retrofit2.Response
import java.util.*
import kotlin.math.roundToInt

class VideoPageFragment : Fragment() {

    private lateinit var binding: FragmentVideoPageBinding
    private lateinit var course: Course
    private lateinit var accessToken: String

    private val args: VideoPageFragmentArgs by navArgs()
    private var dataList: MutableList<VideoPageItem> = mutableListOf()
    private var courseDetail: CourseDetail? = null

    companion object {
        private var isCollect = false
        var isUserHaveSubscription = false
        var plusChannelParentId: Int? = null
    }

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentVideoPageBinding.inflate(inflater, container, false)

        binding.videoPageToolbar.apply {
            navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(binding.videoPageToolbar)
            title = ""
            setNavigationOnClickListener { backPressed() }
        }

        binding.list = dataList

        val rv = binding.videoRecyclerView
        rv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        rv.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv.adapter = VideoPageRecyclerViewAdapter(requireContext(), this::onClickDetail, this::onClickTraining, this::onClickLesson)

        binding.btnPlay.setOnClickListener {
            if (model.appState.accessToken.get()!!.isNotEmpty()) {
                verifyRepeatLogin()
            } else {
                // 訪客
                if (course.is_free) {
                    goPayOrWatch(true, course.video_type)
                } else {
                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)
                }
            }
        }

        setHasOptionsMenu(true)

        loadDataFromApi(args.courseId)

        return binding.root
    }

    private fun loadDataFromApi(courseId: Int) {

        val request = RequestCourseDetail(courseId)
        CourseApiClient.courseDetail(model.appState.accessToken.get(), request, object : ApiController<ResponseCourseDetail>(requireContext()) {
            override fun onSuccess(response: ResponseCourseDetail) {
                course = response.course
                courseDetail = response.course_detail
                val coach = response.instructor
                val data = mutableListOf<VideoPageItem>()

                if (response.vimeo_token.isNotEmpty()) {
                    accessToken = response.vimeo_token
                }

                isUserHaveSubscription = course.is_subscription
                plusChannelParentId = course.PlusChannelParentId

                data.add(
                    VideoPageTitle(
                        course.courseImage!!,
                        course.title,
                        coach.user_img,
                        coach.name,
                        coach.cateDisplayString(),
                        coach.courseCount,
                        course.created_at,
                        course.level,
                        course.calorie,
                        course.short_detail,
                        ApiUtil.itemNameDisplayString(courseDetail!!.accessories),
                    )
                )
                data.add(
                    VideoPageTraining(response.bundle)
                )
                data.add(VideoPageOtherLessonTitle())
                for (course in response.featured) {
                    data.add(course)
                }
                data.add(VideoPageTail())
                binding.list = data
                initBtnText()
            }
        })
    }

    private fun initBtnText() {
        if (course.is_free) {
            setBtnPlayText(true)
        } else {
            if (course.is_subscription) {
                if (course.isChannel) {
                    if (course.isBuyChannel) setBtnPlayText(true)
                    else setBtnPlayText(false)
                } else {
                    setBtnPlayText(true)
                }
            } else {
                setBtnPlayText(false)
            }
        }
    }

    private fun setBtnPlayText(isBuy: Boolean) {
        if (isBuy) {
            binding.btnPlay.text = "播放"
        } else {
            binding.btnPlay.text = "前往訂閱"
        }
    }

    private fun onClickDetail() {
        model.courseDetailTmp = courseDetail
        val action = VideoPageFragmentDirections.actionVideoPageFragmentToClassPageDetailFragment()
        findNavController().navigate(action)
    }

    private fun onClickTraining(position: Int) {
        val bundleId = (binding.list!![1] as VideoPageTraining).list[position].id

        val action = VideoPageFragmentDirections.actionVideoPageFragmentToClassPageFragment()
        action.bundleId = bundleId
        findNavController().navigate(action)

    }

    fun onClickLesson(position: Int) {
        val courseId = (binding.list!![position] as Course).id
        loadDataFromApi(courseId)
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.video_page_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

        //改變menu item顏色
        menu.findItem(R.id.video_page_bookmark_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        menu.findItem(R.id.video_page_schedule_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))

        if (!model.appState.accessToken.get().isNullOrEmpty()) {
            val request = RequestCourseDetail(args.courseId)
            CourseApiClient.courseDetail(model.appState.accessToken.get(), request, object : ApiController<ResponseCourseDetail>(requireContext()) {
                override fun onSuccess(response: ResponseCourseDetail) {
                    isCollect = response.course.isCollected
                    if (response.course.isCollected) {
                        menu.findItem(R.id.video_page_bookmark_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
                    } else {
                        menu.findItem(R.id.video_page_bookmark_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
                    }

                    if (response.course.isCalendared) {
                        menu.findItem(R.id.video_page_schedule_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
                    } else {
                        menu.findItem(R.id.video_page_schedule_item).icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
                    }
                }
            })
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.video_page_schedule_item -> {
                if (model.appState.accessToken.get().isNullOrEmpty()) {
                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)
                } else {
                    // 直播就直接新增
                    if (course.live_time!!.isNotEmpty()) {
                        if (!course.isCalendared) {
                            item.icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
                            val requestBody = RequestAddScheduleBody(null, args.courseId.toString(), DateTool.getCurrentDate(Date()))

                            ScheduleApiClient.addSchedule(model.appState.accessToken.get()!!,
                                requestBody,
                                object : ApiController<ResponseAddScheduleBody>(requireActivity(), false) {
                                    override fun onSuccess(response: ResponseAddScheduleBody) {
                                        Toast.makeText(requireActivity(), "加入行事曆成功", Toast.LENGTH_SHORT).show()
                                        course.isCalendared = true
                                    }
                                })
                        }

                    } else {
                        val action = VideoPageFragmentDirections.actionVideoPageFragmentToVideoPageAddVideoFragment()
                        action.courseId = args.courseId.toString()
                        findNavController().navigate(action)
                    }
                }
                true
            }
            R.id.video_page_bookmark_item -> {
                if (model.appState.accessToken.get().isNullOrEmpty()) {
                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)
                } else {
                    val request = RequestCourseId(course.id)
                    if (isCollect) {
                        isCollect = false
                        item.icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))

                        UserApiClient.deleteUserCollection(model.appState.accessToken.get()!!,
                            request,
                            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    val showText = response.error_message
                                    Toast.makeText(requireContext(), showText, Toast.LENGTH_SHORT).show()
                                }
                            })
                    } else {
                        isCollect = true
                        item.icon?.setTint(ContextCompat.getColor(requireActivity(), R.color.green))
                        UserApiClient.addUserCollection(model.appState.accessToken.get()!!,
                            request,
                            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    val showText = response.error_message
                                    Toast.makeText(requireContext(), showText, Toast.LENGTH_SHORT).show()
                                }
                            })
                    }
                }
                true
            }

            R.id.video_page_shared -> {
                try {
                    if (course.live_time!!.isNotEmpty()) {
                        // 直播課程
                        ShareCompat.IntentBuilder(requireActivity())
                            .setType("text/plain")
                            .setText("我在 #BEING運動影音頻道，體驗臨場感Live互動，無限觀看專業教練實境教學，期待每一次運動，帶給你的價值！\n ${Uri.parse("https://www.being.com.tw/showcourse/${course.id}")}")
                            .startChooser()
                    } else {
                        // 一般課程
                        ShareCompat.IntentBuilder(requireActivity())
                            .setType("text/plain")
                            .setText("我在 #BEING運動影音頻道，體驗國外教練的專業訓練課程，期待每一次運動，帶給你的價值！\n ${Uri.parse("https://www.being.com.tw/showcourse/${course.id}")}")
                            .startChooser()
                    }

                } catch (e: Exception) {
                    //e.toString();
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun verifyRepeatLogin() {

        val paramObject = JsonObject()
        paramObject.addProperty("firebaseToken", StorageDataMaintain.getFirebaseToken(requireContext()))

        UserApiClient.verifyToken(
            model.appState.accessToken.get()!!,
            paramObject, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    if (response.error) {
                        // 已被其它裝置登入
                        val builder = AlertDialog.Builder(requireActivity())
                        builder.apply {
                            setMessage("此帳號已在其他裝置登入，請重新登入。")
                            setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                                logout(dialog)
                            }

                            setCancelable(false)
                        }

                        val alertDialog = builder.create()
                        alertDialog.show()
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                            .setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))

                    } else {
                        //相同裝置登入
                        if (course.is_free) {
                            goPayOrWatch(true, course.video_type)
                        } else {
                            if (model.appState.accessToken.get().isNullOrEmpty()) {
                                val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                                findNavController().navigate(action)
                            } else {
                                // 只有主帳才能有交易行為
//                                goPayOrWatch(true, course.video_type)
                                if (model.userInfo != null) {
                                    if (model.userInfo!!.family == null) {
                                        // family == null ，表示該 user 是主帳

                                        // if (model.userInfo!!.user.email_verified) {
                                        if (true) {
                                            if (course.is_subscription) {
                                                if (course.isChannel) {
                                                    if (course.isBuyChannel) goPayOrWatch(true, course.video_type)
                                                    else goPayOrWatch(false, course.video_type)
                                                } else {
                                                    goPayOrWatch(true, course.video_type)
                                                }
                                            } else {
                                                goPayOrWatch(false, course.video_type)
                                            }
                                        } else {
                                            // 前往信箱驗證
                                            val action = VideoPageFragmentDirections.actionVideoPageFragmentToPaymentMailVerifyFragment()
                                            findNavController().navigate(action)
                                        }
                                    } else {
                                        // family != null ，表示該 user 是子帳。 子帳不能進行購買交易
                                        if (model.userInfo!!.user.subscription) {
                                            if (course.is_subscription) {
                                                if (course.isChannel) {
                                                    if (course.isBuyChannel) goPayOrWatch(true, course.video_type)
                                                    else goPayOrWatch(false, course.video_type)
                                                } else {
                                                    goPayOrWatch(true, course.video_type)
                                                }
                                            } else {
                                                Toast.makeText(requireActivity(), "若要購買影片，請通知家庭方案所有者", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                    }
                                } else {
                                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                                    findNavController().navigate(action)
                                }
                            }
                        }
                        return
                    }
                }

                override fun onFail(httpResponse: Response<ResponseSimpleFormat>): Boolean {
                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)
                    return super.onFail(httpResponse)
                }
            })
    }

    private fun goPayOrWatch(canPlayVideo: Boolean, videoType: String) {

        CourseApiClient.checkViewEnable(RequestCourseDetail(course.id), object : ApiController<ResponseVideoEnable>(requireActivity(), true) {
            override fun onSuccess(response: ResponseVideoEnable) {
                if (response.enable) {
                    if (canPlayVideo) {
                        CourseApiClient.recordCourseViewCount(model.appState.accessToken.get(),
                            RequestCourseDetail(course.id), object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                                override fun onSuccess(response: ResponseSimpleFormat) {
                                    if (!response.error) {
                                        when (videoType) {
                                            "0" -> {
                                                // video
                                                if (course.video_url != null) {
                                                    val intent = Intent(requireActivity(), VimeoVideoActivity::class.java).apply { }
                                                    intent.putExtra("accessToken", accessToken)
                                                    intent.putExtra("videoId", course.video_url!!.split("com/")[1])
                                                    startActivity(intent)
                                                } else if (course.video_id != null) {
                                                    // 喬山撥放器
                                                    val intent = Intent(requireActivity(), BrightCoveActivity::class.java).apply { }
                                                    intent.putExtra("videoId", course.video_id)
                                                    startActivity(intent)
                                                }
                                            }
                                            "1" -> {
                                                // iframe
                                                val intent = Intent(requireActivity(), IframeWebActivity::class.java).apply { }
                                                intent.putExtra("video_iframe", course.video_iframe)
                                                intent.putExtra("chat_iframe", course.chat_iframe)
                                                startActivity(intent)
                                            }
                                        }
                                    }
                                }
                            })
                    } else {
                        val action = VideoPageFragmentDirections.actionVideoPageFragmentToPaymentFragment()
                        findNavController().navigate(action)
                    }
                } else {
                    AlertDialog.Builder(requireActivity()).apply {
                        setMessage("此影片已關閉無法觀看。")
                        setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                            dialog.dismiss()
                        }
                        setCancelable(false)
                        create()
                        show()
                    }
                }
            }
        })
    }

    private fun logout(dialog: DialogInterface) {
        UserApiClient.logout(model.appState.accessToken.get()!!, object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
            override fun onSuccess(response: ResponseSimpleFormat) {
                runBlocking {
                    model.appState.setAccessToken("")
                    model.appState.setRefreshToken("")
                    model.appState.setHaveSubscription(false)
                    model.appState.setPayMethod("cathaybk")

                    model.loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect

                    if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "google") {
                        GoogleUtil.signOut()
                    } else if (StorageDataMaintain.getSingUpResponse(requireContext())!!.getSingUpMethod() == "facebook") {
                        Firebase.auth.signOut()
                    }

                    val dataList = ThirdLoginResponse("", "", "", "")
                    StorageDataMaintain.saveSingUpResponse(requireContext(), dataList)

                    model.me.set(User(
                        "",
                        "",
                        "",
                        false,
                        Subscription.Type.UnRegistered,
                        "",
                        "",
                        "",
                        "",
                        0.0,
                        "",
                        "",
                        "",
                        "",
                        MyHobby(
                            "",
                            1,
                            "1990-01-01",
                            160,
                            60,
                            1,
                            1,
                            1,
                        )
                    ))

                    dialog.dismiss()

                    val action = VideoPageFragmentDirections.actionVideoPageFragmentToLoginAllinOneFragment()
                    findNavController().navigate(action)

                }
            }
        })
    }
}