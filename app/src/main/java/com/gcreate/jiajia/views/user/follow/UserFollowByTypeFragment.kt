package com.gcreate.jiajia.views.user.follow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestFollowCategory
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserFollowByTypeBinding
import com.gcreate.jiajia.adapters.membercentre.UserFollowByTypeRecyclerViewAdapter
import kotlinx.android.synthetic.main.card_user_follow_by_type.view.*

class UserFollowByTypeFragment : Fragment() {

    private var binding: FragmentUserFollowByTypeBinding? = null
    private var dataList = mutableListOf<UserFollowByTypeItem>()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserFollowByTypeBinding.inflate(inflater, container, false)

        loadCategoryFollowState()

        val rv = binding?.recyclerView
        rv?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
//        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f,requireContext()).roundToInt()))
        rv?.adapter = UserFollowByTypeRecyclerViewAdapter {

            val isFollow = model.listFollow!!.category[it].followed
            model.listFollow!!.category[it].followed = !isFollow

            val followImg = binding!!.recyclerView.findViewHolderForLayoutPosition(it)!!.itemView.img_follow
            if (model.listFollow!!.category[it].followed) {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.baseline_notifications_24))
            } else {
                followImg.setImageDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.outline_add_alert_24))
            }

            switchFollowState(model.listFollow!!.category[it].id)
        }

        return binding?.root

    }

    private fun loadCategoryFollowState() {

        for (i in model.listFollow!!.category.indices) {
            dataList.add(i, UserFollowByType(model.listFollow!!.category[i].title, "", model.listFollow!!.category[i].followed))
        }
        dataList.add(UserFollowByTypeTail())

        binding?.list = dataList
    }

    private fun switchFollowState(categoryId: Int) {
        val request = RequestFollowCategory("", categoryId.toString(), "","")
        UserApiClient.followCategory(model.appState.accessToken.get()!!, request, object : ApiController<Unit>(requireContext(), false) {
            override fun onSuccess(response: Unit) {
                model.loadFollowState(requireContext())
            }
        })
    }
}