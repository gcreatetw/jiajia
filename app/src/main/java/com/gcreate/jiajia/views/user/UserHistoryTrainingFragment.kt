package com.gcreate.jiajia.views.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseBrowserBundleReCord
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.TrainingClassItem
import com.gcreate.jiajia.data.TrainingClassTail
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserHistoryTrainingBinding
import com.gcreate.jiajia.adapters.bundle.TrainingClassRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import kotlin.math.roundToInt

class UserHistoryTrainingFragment : Fragment() {

    private var binding: FragmentUserHistoryTrainingBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        binding = FragmentUserHistoryTrainingBinding.inflate(inflater, container, false)
        loadDataFromApi()

        val rv = binding?.recyclerView
        val layoutManager = LinearLayoutManager(requireActivity())
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv?.layoutManager = layoutManager
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = TrainingClassRecyclerViewAdapter() {
            val action = UserHistoryFragmentDirections.actionUserHistoryFragmentToClassPageFragment()
            action.title = (binding?.list!![it] as com.gcreate.jiajia.api.dataobj.Bundle).title
            action.bundleId = (binding?.list!![it] as com.gcreate.jiajia.api.dataobj.Bundle).id
            binding!!.root.findNavController().navigate(action)
        }

        return binding?.root
    }

    private fun loadDataFromApi() {
        UserApiClient.getBrowserBundleRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseBrowserBundleReCord>(requireActivity(), true) {
                override fun onSuccess(response: ResponseBrowserBundleReCord) {
                    val list = mutableListOf<TrainingClassItem>()
                    list.addAll(response.record)
                    list.add(TrainingClassTail())
                    binding?.list = list
                }
            })
    }
}