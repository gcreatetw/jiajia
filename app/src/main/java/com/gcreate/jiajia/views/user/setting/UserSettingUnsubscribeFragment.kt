package com.gcreate.jiajia.views.user.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingUnsubscribeBinding
import com.gcreate.jiajia.util.initRadioGroup

class UserSettingUnsubscribeFragment : Fragment() {

    private var binding: FragmentUserSettingUnsubscribeBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserSettingUnsubscribeBinding.inflate(inflater, container, false)
        binding?.apply {

            viewModel = model

            //設定返回鍵顏色及點擊事件
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            initRadioGroup(inflater, radioGroup, model.unsubscribeReasonList)

            btnUnsubscribe.setOnClickListener {
                userCancelRecord()
            }
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun userCancelRecord() {
        UserApiClient.setUserCancelRecord(model.appState.accessToken.get()!!,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    backPressed()
                }
            })
    }
}