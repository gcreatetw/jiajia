package com.gcreate.jiajia.views.hotvideo

enum class HotVideoListFragmentMode(val text: String ){
    HotVideo("熱門課程"),
    NewClass("最新上架")
}