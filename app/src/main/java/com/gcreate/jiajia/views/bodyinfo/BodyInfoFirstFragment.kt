package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.util.AES256Util
import com.google.android.material.datepicker.MaterialDatePicker
import java.text.SimpleDateFormat
import java.util.*


class BodyInfoFirstFragment : Fragment() {

    val arg: BodyInfoFirstFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_body_info_first, container, false)
    }

    override fun onStart() {
        super.onStart()
        Log.d("ben","arg.mode = ${arg.mode}")
        //設定返回鍵顏色
        val toolBar = view?.findViewById<Toolbar>(R.id.body_info_first_toolbar)
        toolBar?.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "下一頁"
        toolBar?.setNavigationOnClickListener {
            when (arg.mode) {
                1 -> {
                    // 註冊後進來的
                    findNavController().navigateUp()
//                    val action = BodyInfoFragmentDirections.actionBodyInfoFragmentToMainFragment()
//                    (activity as MainActivity).rootNavController.navigate(action)
                }
                else -> {
                    findNavController().navigateUp()
                }
            }
        }
        //性別
        val buttonMale = view?.findViewById<Button>(R.id.body_info_first_male_button)
        val buttonFemale = view?.findViewById<Button>(R.id.body_info_first_female_button)

        if (BodyInfoFragment.userGender == 1) {
            buttonMale!!.isSelected = true
        } else if (BodyInfoFragment.userGender == 2) {
            buttonFemale!!.isSelected = true
        }

        buttonMale?.apply {
            this.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            setOnClickListener {
                BodyInfoFragment.userGender = 1
                if (!this.isSelected) {
                    this.isSelected = true
                    this.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    buttonFemale?.isSelected = false
                    buttonFemale?.setTextColor(ContextCompat.getColor(requireActivity(), R.color.green))
                }
            }
        }

        buttonFemale?.apply {
            this.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
            setOnClickListener {
                BodyInfoFragment.userGender = 2
                if (!this.isSelected) {
                    this.isSelected = true
                    this.setTextColor(ContextCompat.getColor(requireActivity(), R.color.white))
                    buttonMale?.isSelected = false
                    buttonMale?.setTextColor(ContextCompat.getColor(requireActivity(), R.color.green))
                }
            }
        }

        // 出生日期
        val buttonBirthday = view?.findViewById<Button>(R.id.body_info_first_born_button)
        val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        val dateFormat = SimpleDateFormat("dd MM月 yyyy", Locale.TAIWAN)
        val dateFormat2 = SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN)

        val birthday =
            when (arg.mode) {
                1 -> {
                    // 註冊後進來的
                    dateFormat2.parse(BodyInfoFragment.userBirthday)
                }
                else -> {
                    try {
                        val birthdayAES =
                            AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), BodyInfoFragment.userBirthday)
                        dateFormat2.parse(birthdayAES)
                    } catch (e: Exception) {
                        dateFormat2.parse(BodyInfoFragment.userBirthday)
                    }
                }
            }


        buttonBirthday?.apply {
            text = (dateFormat.format(birthday!!))
            setOnClickListener {
                val datePicker =
                    MaterialDatePicker.Builder.datePicker()
                        .setTitleText("選取日期")
                        .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                        .build()
                datePicker.show(requireActivity().supportFragmentManager, "birthdayDatePicker")
                datePicker.addOnPositiveButtonClickListener {
                    calendar.timeInMillis = it
                    buttonBirthday.text = dateFormat.format(calendar.time)
                    BodyInfoFragment.userBirthday = dateFormat2.format(calendar.time)
                }
            }
        }
    }
}