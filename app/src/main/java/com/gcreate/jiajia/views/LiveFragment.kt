package com.gcreate.jiajia.views

import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.tab.LiveTabAdapter
import com.gcreate.jiajia.databinding.FragmentLiveBinding
import com.gcreate.jiajia.util.DateTool
import com.google.android.material.tabs.TabLayoutMediator
import java.text.SimpleDateFormat
import java.util.*

class LiveFragment : Fragment() {

    private var binding: FragmentLiveBinding? = null

    private var dateList = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentLiveBinding.inflate(inflater, container, false)
        setHasOptionsMenu(true)
        getDateList()

        return binding?.root
    }

    override fun onStart() {
        super.onStart()
        //設定toolbar
        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setTitleTextColor(ContextCompat.getColor(requireActivity(),R.color.white))
        (activity as AppCompatActivity?)!!.supportActionBar!!.apply {
            setDisplayShowTitleEnabled(true)
            title = "直播課程"
        }

    }

    private fun getDateList() {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.TAIWAN)

        val calendar = Calendar.getInstance()
        dateList.add(dateFormat.format(calendar.time))
        for (i in 0..29) {
            calendar.add(Calendar.DATE, 1)
            dateList.add(dateFormat.format(calendar.time))
        }

        //TabLayout
        val adapter = LiveTabAdapter(this, dateList)
        binding?.apply {
            viewPager2.isUserInputEnabled = false
            viewPager2.adapter = adapter
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                val showText = dateList[position].substring(5, 7) + "月" + dateList[position].substring(8, 10) + "日"
                tab.text = showText
            }.attach()
        }

    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.classpage_toolbar_menu, menu)
        menu.findItem(R.id.classpage_detail_item).isVisible = false

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.classpage_shared -> {
                try {
                    ShareCompat.IntentBuilder(requireActivity())
                        .setType("text/plain")
                        .setText("我在 #BEING運動影音頻道，體驗臨場感Live互動，無限觀看專業教練實境教學，期待每一次運動，帶給你的價值！\n ${Uri.parse("https://www.being.com.tw/courses/live/${DateTool.getCurrentDate(Date())}")}")
                        .startChooser()
                } catch (e: Exception) {
                    //e.toString();
                }
                true
            }

            else -> {
                true
            }
        }
    }

}