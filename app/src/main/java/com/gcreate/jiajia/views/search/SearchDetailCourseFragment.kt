package com.gcreate.jiajia.views.search

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.module.LoadMoreModuleConfig.defLoadMoreView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.search.SearchCourseAdapter
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.search.SearchApiClient
import com.gcreate.jiajia.api.search.request.RequestSearchCourseBody
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSearchDetailCourseBinding
import com.gcreate.jiajia.listener.UpdateSearchKeywordListener
import com.gcreate.jiajia.listener.UpdateSearchKeywordListenerManager
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.baseRecyclerViewHelper.CustomLoadMoreView
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import retrofit2.Response
import kotlin.math.roundToInt


class SearchDetailCourseFragment(private var keyword: String) : Fragment(), UpdateSearchKeywordListener {

    private lateinit var binding: FragmentSearchDetailCourseBinding
    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())
    private var page = 1
    private var maxDataCount = 0
    private lateinit var mAdapterSearchCourse: SearchCourseAdapter

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.loadCourseFilterSetting(requireContext())
        UpdateSearchKeywordListenerManager.instance!!.registerListener(this)
        defLoadMoreView = CustomLoadMoreView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchDetailCourseBinding.inflate(inflater, container, false)

        mAdapterSearchCourse = SearchCourseAdapter(mutableListOf())


        binding.recyclerView.apply {

            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = mAdapterSearchCourse

            mAdapterSearchCourse.apply {
                addFooterView(layoutInflater.inflate(R.layout.layout_footer, null))
                setOnItemClickListener { _, _, position ->
                    val action = SearchDetailFragmentDirections.actionSearchDetailFragmentToVideoPageFragment()
                    action.courseId = mAdapterSearchCourse.data[position].id
                    binding.root.findNavController().navigate(action)
                }

                loadMoreModule.setOnLoadMoreListener {
                    if (maxDataCount == 0 || mAdapterSearchCourse.data.size < maxDataCount) {
                        page++
                        loadDataFromApi()
                        mAdapterSearchCourse.loadMoreModule.loadMoreComplete()
                    } else {
                        mAdapterSearchCourse.loadMoreModule.loadMoreComplete()
                        mAdapterSearchCourse.loadMoreModule.loadMoreEnd()
                    }
                }
            }

        }

        binding.imgFilter.setOnClickListener {
            activity?.supportFragmentManager.let {
                val dialog = HotVideoFilterDialog(filter) { _filter ->
                    filter = _filter
                    setImgFilterColor()
                    loadDataFromApi()
                }
                dialog.show(it!!, "")
            }
        }

        setImgFilterColor()
        loadDataFromApi()

        return binding.root
    }

    private fun setImgFilterColor() {
        val enable = filter.intensityIdList.isNotEmpty()
                || filter.coachIdList.isNotEmpty()
                || filter.equipmentIdList.isNotEmpty()

        val drawable = binding.imgFilter.drawable
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun loadDataFromApi() {

        val request = RequestSearchCourseBody(
            keyword,
            false,
            filter.date,
            filter.intensityIdList,
            filter.coachIdList,
            filter.equipmentIdList,
            25,
            page)

        SearchApiClient.searchCourse(model.appState.accessToken.get()!!, request, object : ApiController<ResponseCourses>(requireContext(), true) {
            override fun onSuccess(response: ResponseCourses) {
                mAdapterSearchCourse.addData(response.courses)
                maxDataCount = response.total
                val resultCountText = "共 ${response.total} 筆結果"
                binding.tvClassCount.text = resultCountText
            }

            override fun onFail(httpResponse: Response<ResponseCourses>): Boolean {
                mAdapterSearchCourse.loadMoreModule.loadMoreComplete()
                mAdapterSearchCourse.loadMoreModule.loadMoreEnd()
                return super.onFail(httpResponse)
            }
        })
    }

    override fun updateKeyword(keyword: String?) {
        this.keyword = keyword!!
        loadDataFromApi()
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateSearchKeywordListenerManager.instance!!.unRegisterListener(this)
    }
}