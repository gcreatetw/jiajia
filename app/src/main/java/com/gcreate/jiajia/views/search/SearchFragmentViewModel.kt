package com.gcreate.jiajia.views.search


import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.search.SearchApiClient
import com.gcreate.jiajia.api.search.response.ResponseSearchWidget
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.databinding.FragmentSearchBinding
import com.gcreate.jiajia.adapters.home.search.SearchAdapter

class SearchFragmentViewModel(
    private val model: MainViewModel,
    private val binding: FragmentSearchBinding,
) : ViewModel() {

    var responseData: MutableLiveData<ResponseSearchWidget> = MutableLiveData()

    private var searchAdapter: SearchAdapter = SearchAdapter()

    fun getSearchAdapter(): SearchAdapter {
        return searchAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setSearchAdapterData(data: ResponseSearchWidget) {

        if (!data.records.isNullOrEmpty()) {
            searchAdapter.data = data.records
        } else {
            searchAdapter.data = mutableListOf()
        }
        searchAdapter.notifyDataSetChanged()
    }

    fun getResponseDataObserver(): MutableLiveData<ResponseSearchWidget> {
        return responseData
    }

    fun getApiData() {

        SearchApiClient.searchWidget(model.appState.accessToken.get()!!, object : ApiController<ResponseSearchWidget>(binding.root.context, false) {
            override fun onSuccess(response: ResponseSearchWidget) {
                responseData.postValue(response)
            }

        })
    }

}

class SearchFragmentModelFactory(
    private val model: MainViewModel,
    private val binding: FragmentSearchBinding,
    //private val navController: NavController,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SearchFragmentViewModel(model, binding) as T
    }

}