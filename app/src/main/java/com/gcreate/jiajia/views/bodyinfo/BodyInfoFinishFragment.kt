package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R

class BodyInfoFinishFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_body_info_finish, container, false)
    }

    override fun onStart() {
        super.onStart()
        //設定返回鍵顏色
        val toolBar = view?.findViewById<Toolbar>(R.id.body_info_finish_toolbar)
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        //設定下一頁按鈕文字
        val nextButton = activity?.findViewById<Button>(R.id.body_info_next_button)
        nextButton?.text = "完成"
        toolBar?.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        //設定按鈕
        val fastButton = view?.findViewById<Button>(R.id.body_info_fourth_btn1)
        fastButton?.text = Html.fromHtml("<b>較快</b>" + "<br/>" + "每天訓練30~40分鐘")
        val midButton = view?.findViewById<Button>(R.id.body_info_fourth_btn2)
        midButton?.text = Html.fromHtml("<b>適中</b>" + "<br/>" + "每天訓練20~30分鐘")
        val slowButton = view?.findViewById<Button>(R.id.body_info_fourth_btn3)
        slowButton?.text = Html.fromHtml("<b>較慢</b>" + "<br/>" + "每天訓練10~20分鐘")

        when (BodyInfoFragment.fitness_howlong) {
            1 -> fastButton!!.isSelected = true
            2 -> midButton!!.isSelected = true
            3 -> slowButton!!.isSelected = true
        }

        //選擇運動習慣
        fastButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_howlong = 1
                if (!this.isSelected) {
                    this.isSelected = true
                    midButton?.isSelected = false
                    slowButton?.isSelected = false
                }
            }
        }
        midButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_howlong = 2
                if (!this.isSelected) {
                    this.isSelected = true
                    fastButton?.isSelected = false
                    slowButton?.isSelected = false
                }
            }
        }
        slowButton?.apply {
            setOnClickListener {
                BodyInfoFragment.fitness_howlong = 3
                if (!this.isSelected) {
                    this.isSelected = true
                    midButton?.isSelected = false
                    fastButton?.isSelected = false
                }
            }
        }
    }
}