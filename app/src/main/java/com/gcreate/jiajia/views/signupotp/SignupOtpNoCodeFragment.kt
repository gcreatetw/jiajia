package com.gcreate.jiajia.views.signupotp

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSignupOtpNoCodeBinding
import com.gcreate.jiajia.util.OtpUtil
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class SignupOtpNoCodeFragment : Fragment() {

    private lateinit var binding: FragmentSignupOtpNoCodeBinding
    val arg: SignupOtpNoCodeFragmentArgs by navArgs()

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = FragmentSignupOtpNoCodeBinding.inflate(inflater, container, false)

        binding.apply {
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }

            tvPhone.apply {
                val phoneString = "+886${arg.phone}"
                tvPhone.text = phoneString

                setOnClickListener {
                    val action = SignupOtpNoCodeFragmentDirections.actionSignupOtpNocodeFragmentToSignupOtpPhoneNumberFragment()
                    findNavController().navigate(action)
                }
            }

            object : CountDownTimer(90000, 1000) {

                override fun onTick(millisUntilFinished: Long) {
                    btnResend.background.alpha = 123
                    btnResend.isClickable = false
                    val countDownTimeText = "於 ${millisUntilFinished / 1000} 秒後再次傳送驗證碼"
                    btnResend.text = countDownTimeText
                }

                override fun onFinish() {
                    btnResend.isClickable = true
                    btnResend.background.alpha = 255
                    btnResend.text = "再次發送!"

                    btnResend.setOnClickListener {
                        OtpUtil(requireActivity(), model, Firebase.auth, arg.mode).getOTP(this@SignupOtpNoCodeFragment, arg.mode, arg.phone)
                        findNavController().navigateUp()
                    }
                }
            }.start()
        }

        return binding.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }
}