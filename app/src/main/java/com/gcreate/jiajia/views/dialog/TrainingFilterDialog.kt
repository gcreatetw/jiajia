package com.gcreate.jiajia.views.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.DialogTrainingFilterBinding
import com.gcreate.jiajia.util.initItemNameChipGroup
import com.gcreate.jiajia.util.initItemTitleChipGroup

class TrainingFilterDialog(
    val filter : Filter,
    val onOk: (filter : Filter) -> Unit
) :DialogFragment() {

    data class Filter(
        var accessoryList : List<Int>,
        var bimList : List<Int>
    )

    private val model: MainViewModel by activityViewModels{
        MainViewModelFactory(requireContext().dataStore)
    }

    private var binding: DialogTrainingFilterBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogTrainingFilterBinding.inflate(inflater, container, false)

        var chipGroupBmi = binding?.dialogTrainingFilterChipGroup
        var bmiList = model.bundleFilter!!.bmi
        initItemTitleChipGroup(inflater, chipGroupBmi!!, bmiList)

        var chipGroupAccessory = binding?.dialogTrainingFilterChipGroup2
        val accessoryList = model.bundleFilter!!.accessory
        initItemNameChipGroup(inflater, chipGroupAccessory!!, accessoryList)

        for(id in filter.bimList){
            chipGroupBmi.check(id)
        }

        for(id in filter.accessoryList){
            chipGroupAccessory.check(id)
        }

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //取消事件
        val cancelButton = view.findViewById<TextView>(R.id.dialog_training_filter_cancel)
        cancelButton.setOnClickListener {
            dismiss()
        }
        //套用
        val confirmButton = view.findViewById<TextView>(R.id.dialog_training_filter_confirm)
        confirmButton.setOnClickListener {
            onOk(
                Filter(
                    binding?.dialogTrainingFilterChipGroup2?.checkedChipIds!!,
                    binding?.dialogTrainingFilterChipGroup?.checkedChipIds!!
                )
            )
            dismiss()
        }
    }
}