package com.gcreate.jiajia.views.notifications

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gcreate.jiajia.adapters.home.notification.FirebaseNotificationsAdapter
import com.gcreate.jiajia.adapters.home.notification.NotificationsAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.Notification
import com.gcreate.jiajia.api.user.response.ResponseFirebaseNotification
import com.gcreate.jiajia.api.user.response.ResponseNotifications
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.databinding.FragmentNotificationsBinding

class NotificationsFragmentViewModel(
    private val model: MainViewModel,
    private val binding: FragmentNotificationsBinding,
) : ViewModel() {

    var responseData: MutableLiveData<ResponseNotifications> = MutableLiveData()
    var firebaseNotificationData: MutableLiveData<ResponseFirebaseNotification> = MutableLiveData()


    private var latestNewsAdapter: NotificationsAdapter = NotificationsAdapter()
    private var beforeNewsAdapter: NotificationsAdapter = NotificationsAdapter()
    private var firebaseNotificationAdapter: FirebaseNotificationsAdapter = FirebaseNotificationsAdapter()

    private var latestNewsDataList: MutableList<Notification> = mutableListOf()
    private var beforeNewsDataList: MutableList<Notification> = mutableListOf()

    fun getLatestNewsAdapter(): NotificationsAdapter {
        return latestNewsAdapter
    }

    fun getBeforeNewsAdapter(): NotificationsAdapter {
        return beforeNewsAdapter
    }

    fun getFirebaseNotificationAdapter(): FirebaseNotificationsAdapter {
        return firebaseNotificationAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setNotificationsAdapterData(data: ResponseNotifications) {
        beforeNewsDataList.clear()
        latestNewsDataList.clear()

        if (!data.notifications.isNullOrEmpty()) {
            for (item in data.notifications) {
                if (item.time.contains("天")) {
                    beforeNewsDataList.add(item)
                } else {
                    latestNewsDataList.add(item)
                }
            }
            beforeNewsAdapter.data = beforeNewsDataList
            latestNewsAdapter.data = latestNewsDataList

        } else {
            beforeNewsAdapter.data = mutableListOf()
            latestNewsAdapter.data = mutableListOf()
        }

        beforeNewsAdapter.notifyDataSetChanged()
        latestNewsAdapter.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFirebaseNotificationsAdapterData(data: ResponseFirebaseNotification) {
        firebaseNotificationAdapter.apply {
            this.data = data.notification
            notifyDataSetChanged()
            setOnItemClickListener { _, _, position ->
                val url = data.notification[position].url!!
                if (url.contains("https")) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(data.notification[position].url))
                    startActivity(binding.root.context, browserIntent, null)
                }
            }
        }
    }

    fun getResponseDataObserver(): MutableLiveData<ResponseNotifications> {
        return responseData
    }

    fun getFirebaseNotificationDataObserver(): MutableLiveData<ResponseFirebaseNotification> {
        return firebaseNotificationData
    }

    fun getApiData() {
        UserApiClient.getNotifications(
            model.appState.accessToken.get()!!, object : ApiController<ResponseNotifications>(binding.root.context, false) {
                override fun onSuccess(response: ResponseNotifications) {
                    responseData.postValue(response)
                }
            })
        UserApiClient.getFirebaseNotifications(
            object : ApiController<ResponseFirebaseNotification>(binding.root.context, false) {
                override fun onSuccess(response: ResponseFirebaseNotification) {
                    firebaseNotificationData.postValue(response)
                }
            })
    }

}

class NotificationsFragmentViewModelFactory(
    private val model: MainViewModel,
    private val binding: FragmentNotificationsBinding,
    //private val navController: NavController,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return NotificationsFragmentViewModel(model, binding) as T
    }

}