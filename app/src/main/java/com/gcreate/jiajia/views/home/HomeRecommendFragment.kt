package com.gcreate.jiajia.views.home

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.SimpleItemAnimator
import com.gcreate.jiajia.*
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.viewModel.HomeRecommendModelFactory
import com.gcreate.jiajia.api.viewModel.HomeRecommendViewModel
import com.gcreate.jiajia.data.HomeBanner
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.databinding.FragmentHomeRecommendBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.horizontalSpacingItemDecoration
import com.gcreate.jiajia.util.initClassCategoryChipGroup
import com.gcreate.jiajia.views.hotvideo.HotVideoListFragmentMode
import com.gcreate.jiajia.views.user.UserFragment
import com.zhpan.bannerview.BannerViewPager
import com.zhpan.bannerview.constants.IndicatorGravity.END
import com.zhpan.bannerview.constants.PageStyle
import com.zhpan.bannerview.utils.BannerUtils
import com.zhpan.indicator.enums.IndicatorSlideMode
import com.zhpan.indicator.enums.IndicatorStyle
import kotlin.math.roundToInt

class HomeRecommendFragment : Fragment() {

    companion object {
        var ScrollViewPositionX: Int = 0
        var ScrollViewPositionY: Int = 0
        var coachClickType: String = ""
        var isChangeNotificationsView = true
    }

    private var binding: FragmentHomeRecommendBinding? = null

    //連結banner view
    private lateinit var mViewPager: BannerViewPager<HomeBanner>
    private lateinit var mViewPager2: BannerViewPager<HomeBanner>

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeRecommendBinding.inflate(inflater, container, false)

        getHomeModelInstance()
        setupBinding(makeApiCall(inflater))
        clickMore()

        // 最新上架-分類
        CourseApiClient.category(object : ApiController<ResponseCategory>(requireContext(), false) {
            override fun onSuccess(response: ResponseCategory) {
                initClassCategoryChipGroup(inflater, binding?.homeRecommendChipGroup!!, response.category)
                binding!!.homeRecommendChipGroup.check(response.category[0].id)
            }
        })

        binding!!.refreshLayout.apply {
            setOnRefreshListener { makeApiCall(inflater) }
        }

        return binding?.root
    }

    private fun clickMore() {
        //最新上架 查看更多
        binding?.homeRecommendNewButton?.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToHotVideoListFragment()
            action.mode = HotVideoListFragmentMode.NewClass
            (activity as MainActivity).rootNavController.navigate(action)
        }

        //熱門課程 查看更多
        binding?.homeRecommendHotButton?.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToHotVideoListFragment()
            action.mode = HotVideoListFragmentMode.HotVideo
            (activity as MainActivity).rootNavController.navigate(action)
        }

        //最新消息 查看更多
        binding?.homeRecommendNewsButton?.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToArticleListFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }

        //推薦師資 查看更多
        binding?.homeRecommendTeacherButton?.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToCoachListFragment()
            (activity as MainActivity).rootNavController.navigate(action)
        }
    }

    override fun onStart() {
        super.onStart()

        if (model.receiveNotificationTitle.isNotEmpty()) {
            if (isChangeNotificationsView) {
                val action = MainFragmentDirections.actionMainFragmentToNotificationsFragment()
                (activity as MainActivity).rootNavController.navigate(action)
                isChangeNotificationsView = false
            }
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                binding?.mainScrollView?.scrollTo(ScrollViewPositionX, ScrollViewPositionY)
            }, 100)
        }
    }

    override fun onStop() {
        super.onStop()
        ScrollViewPositionX = binding?.mainScrollView?.scrollX ?: 0
        ScrollViewPositionY = binding?.mainScrollView?.scrollY ?: 0
    }

    private fun setupBinding(viewModel: HomeRecommendViewModel) {
        binding?.setVariable(BR.viewModel, viewModel)
        binding?.executePendingBindings()

        //載入banner1
        mViewPager = binding?.root?.findViewById(R.id.home_recommend_banner_view)!!
        setupViewPager(mViewPager)

        //載入banner2
        mViewPager2 = binding?.root?.findViewById(R.id.home_recommend_banner_view2)!!
        setupViewPager2(mViewPager2)

        //課程分類
        val chipGroup = binding?.homeRecommendChipGroup
        chipGroup?.setOnCheckedChangeListener { _, checkedId ->
            homeViewModel!!.getLatestCourseByCategoryApiData(checkedId)
        }

        //依分類取得課程
        binding!!.homeRecommendNewRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireActivity(), HORIZONTAL, false)
            addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = viewModel.getLatestCourseByCategoryAdapter()
            hasFixedSize()
        }

        binding!!.homeRecommendHotRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireActivity(), HORIZONTAL, false)
            addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = viewModel.getHotCourseAdapter()
            hasFixedSize()
        }

        binding!!.homeRecommendNewsRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireActivity(), HORIZONTAL, false)
            addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = viewModel.getLatestNewsAdapter()
            hasFixedSize()
        }

        binding!!.homeRecommendTeacherRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireActivity(), HORIZONTAL, false)
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            itemAnimator = null
            addItemDecoration(horizontalSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = viewModel.getTeacherAdapter()
            setHasFixedSize(true)
        }
    }

    private var homeViewModel: HomeRecommendViewModel? = null
    private fun getHomeModelInstance(): HomeRecommendViewModel? {
        if (homeViewModel == null) {
            homeViewModel =
                ViewModelProvider(requireActivity(),
                    HomeRecommendModelFactory(binding!!, (activity as MainActivity).rootNavController, model)).get(
                    HomeRecommendViewModel::class.java)
        }
        return homeViewModel
    }

    private fun makeApiCall(inflater: LayoutInflater): HomeRecommendViewModel {

        homeViewModel!!.getBannerApiData()
        homeViewModel!!.getBannerDataObserver().observe(requireActivity()) {
            if (it != null) {
                mViewPager = binding?.root?.findViewById(R.id.home_recommend_banner_view)!!
                mViewPager.refreshData(it.sliders)
            }
        }

        // 課程分類
        /* TODO 會遇到 Crash,SavedState cannot be cast to android.widget.CompoundButton$SavedState
        homeViewModel!!.getLatestCourseCategoryApiData()
        homeViewModel!!.getLatestCourseCategoryDataObserver().observe(requireActivity(), {
            initClassCategoryChipGroup(inflater, binding!!.homeRecommendChipGroup, it.category)
            binding!!.homeRecommendChipGroup.check(it.category[0].id)
        })
        */

        // 最新消息 依分類取得文章
        homeViewModel!!.getLatestCourseByCategoryDataObserver().observe(requireActivity()) {
            if (it != null) {
                homeViewModel!!.setLatestCourseByCategoryAdapterData(it.courses)
            }
        }

        homeViewModel!!.getHotCourseApiData()
        homeViewModel!!.getHotCourseDataObserver().observe(requireActivity()) {
            if (it != null) {
                homeViewModel!!.setHotCourseAdapterData(it.courses)
            }
        }

        homeViewModel!!.getLatestNewsApiData()
        homeViewModel!!.getLatestNewsDataObserver().observe(requireActivity()) {
            if (it != null) {
                homeViewModel!!.setLatestNewsAdapterData(it.blog)
            }
        }

        homeViewModel!!.getBanner2ApiData()
        homeViewModel!!.getBanner2DataObserver().observe(requireActivity()) {
            if (it != null) {
                mViewPager2 = binding?.root?.findViewById(R.id.home_recommend_banner_view2)!!
                mViewPager2.refreshData(it.sliders)
            }
        }

        homeViewModel!!.getTeacherApiData()
        homeViewModel!!.getTeacherDataObserver().observe(requireActivity()) {
            if (it != null) {
                homeViewModel!!.setTeacherAdapterData(it.instructor)
            }
        }
        binding!!.refreshLayout.isRefreshing = false
        return homeViewModel!!
    }

    // 設定輪播圖參數
    private fun setupViewPager(viewPager: BannerViewPager<HomeBanner>) {
        viewPager.apply {
            adapter = homeViewModel!!.getBannerAdapter()
            setLifecycleRegistry(lifecycle)
            setIndicatorStyle(IndicatorStyle.ROUND_RECT)
            setIndicatorSlideMode(IndicatorSlideMode.NORMAL)
            setIndicatorSliderColor(
                ContextCompat.getColor(requireActivity(), R.color.outlinebutton_background),
                ContextCompat.getColor(requireActivity(), R.color.green)
            )
            setIndicatorSliderGap(BannerUtils.dp2px(4F))
            setIndicatorSliderWidth(20, 40)
            setIndicatorHeight(20)
            setPageStyle(PageStyle.NORMAL)
            setIndicatorGravity(END)
            setIndicatorMargin(
                0, 0,
                Util().dpToPixel(16F, requireContext()).roundToInt(),
                Util().dpToPixel(14F, requireContext()).roundToInt(),
            )

        }.create()
    }

    // 設定輪播圖參數2
    private fun setupViewPager2(viewPager: BannerViewPager<HomeBanner>) {
        viewPager.apply {
            adapter = homeViewModel!!.getBanner2Adapter()
            setLifecycleRegistry(lifecycle)
            setIndicatorStyle(IndicatorStyle.ROUND_RECT)
            setIndicatorSlideMode(IndicatorSlideMode.NORMAL)
            setIndicatorSliderColor(
                ContextCompat.getColor(requireActivity(), R.color.outlinebutton_background),
                ContextCompat.getColor(requireActivity(), R.color.green)
            )
            setIndicatorSliderGap(BannerUtils.dp2px(4F))
            setIndicatorSliderWidth(20, 40)
            setIndicatorHeight(20)
            setPageStyle(PageStyle.NORMAL)
            setIndicatorGravity(END)
            setIndicatorMargin(0, 0, Util().dpToPixel(16F, requireContext()).roundToInt(), 0)
        }.create()
    }

}