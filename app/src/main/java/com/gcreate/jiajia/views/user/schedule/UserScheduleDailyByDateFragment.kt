package com.gcreate.jiajia.views.user.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.adapters.membercentre.UserScheduleDailyRecyclerViewAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.schedule.ScheduleApiClient
import com.gcreate.jiajia.api.schedule.request.RequestDayScheduleBody
import com.gcreate.jiajia.api.schedule.request.RequestRemoveScheduleBody
import com.gcreate.jiajia.api.schedule.response.ResponseDaySchedule
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserScheduleDailyByDateBinding

class UserScheduleDailyByDateFragment(val date: String) : Fragment() {

    private lateinit var binding: FragmentUserScheduleDailyByDateBinding
    private var data = mutableListOf<UserScheduleDailyItem>()

    companion object {
        var itemClickType: String = ""
    }

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUserScheduleDailyByDateBinding.inflate(inflater, container, false)

        getSingleDaySchedule()

        binding?.recyclerView?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

        binding?.recyclerView?.adapter = UserScheduleDailyRecyclerViewAdapter {
            when (itemClickType) {
                "remove" -> {
                    val scheduleId = (binding.list!![it] as UserScheduleDaily).scheduleId
                    val courseId = (binding.list!![it] as UserScheduleDaily).courseId
                    deleteSingleDaySchedule(scheduleId, courseId)
                }

                "item" -> {
                    if ((binding.list!![it] as UserScheduleDaily).video_enable) {
                        val action = UserScheduleDailyFragmentDirections.actionUserScheduleDailyFragmentToVideoPageFragment()
                        action.courseId = (binding.list!![it] as UserScheduleDaily).courseId
                        (activity as MainActivity).rootNavController.navigate(action)
                    }
                }
            }
        }

        return binding.root
    }

    private fun getSingleDaySchedule() {
        data.clear()
        val requestBody = RequestDayScheduleBody(date)
        ScheduleApiClient.daySchedule(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseDaySchedule>(requireActivity(), true) {
                override fun onSuccess(response: ResponseDaySchedule) {
                    val courseList = response.Course

                    for (i in courseList.indices) {
                        data.add(
                            UserScheduleDaily(
                                courseList[i].id,
                                courseList[i].schedule_id,
                                courseList[i].start,
                                "",
                                courseList[i].preview_image,
                                courseList[i].title,
                                courseList[i].user_img,
                                courseList[i].name,
                                courseList[i].video_enable)
                        )
                    }
                    data.add(UserScheduleDailyTail())
                    binding.list = data
                }
            })
    }

    private fun deleteSingleDaySchedule(scheduleId: Int, courseId: Int) {

        val requestBody = RequestRemoveScheduleBody(courseId, scheduleId)
        ScheduleApiClient.removeSchedule(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), true) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    val dataCountWithoutFooter = data.size - 1
                    for (i in 0 until dataCountWithoutFooter) {
                        if (scheduleId == (data[i] as UserScheduleDaily).scheduleId) {
                            data.removeAt(i)
                            binding.list = data
                            binding.recyclerView.adapter?.notifyDataSetChanged()
                            break
                        }
                    }
                }
            })
    }
}