package com.gcreate.jiajia.views.notifications

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.BR
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestReadNotificationBody
import com.gcreate.jiajia.api.user.response.Notification
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.ClassCategory
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentNotificationsBinding

class NotificationsFragment : Fragment() {

    private lateinit var binding: FragmentNotificationsBinding
    private lateinit var categoryList: List<ClassCategory>

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)

        initView()
        setupBinding(makeApiCall())

        return binding.root
    }

    private fun initView() {
        binding.notificationToolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(binding.notificationToolbar)

        binding.notificationToolbar.setNavigationOnClickListener {
            val navController = (activity as MainActivity).rootNavController
            navController.navigateUp()
        }

        getCourseCategory()
        model.loadFollowState(requireContext())
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupBinding(viewModel: NotificationsFragmentViewModel) {

        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()

        binding.rvLatestNews.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        }

        binding.rvBeforeNews.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        }
        binding.rvFirebaseNotification.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        }

        viewModel.getBeforeNewsAdapter().setOnItemClickListener { adapter, _, position ->
            val itemValue = (adapter.getItem(position) as Notification)
            if (!itemValue.is_read) {
                itemValue.is_read = !itemValue.is_read
                readNotification(itemValue.id)
                adapter.notifyDataSetChanged()
            }

            clickEvent(itemValue)
        }

        viewModel.getLatestNewsAdapter().setOnItemClickListener { adapter, _, position ->
            val itemValue = (adapter.getItem(position) as Notification)
            if (!itemValue.is_read) {
                itemValue.is_read = !itemValue.is_read
                readNotification(itemValue.id)
                adapter.notifyDataSetChanged()
            }
            clickEvent(itemValue)
        }

    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): NotificationsFragmentViewModel {

        val viewModel = ViewModelProvider(requireActivity(), NotificationsFragmentViewModelFactory(model, binding)).get(
            NotificationsFragmentViewModel::class.java)

        viewModel.getApiData()

        viewModel.apply {
            getResponseDataObserver().observe(requireActivity()) {
                if (it != null) {
                    viewModel.setNotificationsAdapterData(it)
                }
            }
            getFirebaseNotificationDataObserver().observe(requireActivity()) {
                if (it != null) {
                    viewModel.setFirebaseNotificationsAdapterData(it)
                }
            }
        }

        return viewModel
    }

    private fun readNotification(notificationId: Int) {
        val requestBody = RequestReadNotificationBody(notificationId)
        UserApiClient.readNotification(model.appState.accessToken.get()!!, requestBody,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                }
            })
    }

    private fun getCourseCategory() {
        CourseApiClient.category(object : ApiController<ResponseCategory>(requireContext()) {
            override fun onSuccess(response: ResponseCategory) {
                categoryList = response.category
            }
        })
    }

    private fun getCourseCategoryTitle(courseId: Int): String {
        var title = ""

        for (item in categoryList) {
            if (courseId == item.id) {
                title = item.title
                break
            }
        }

        return title
    }

    private fun clickEvent(itemValue: Notification) {
        /**
         * 訂閱分類       type category
         * 訂閱教練       type instructor
         * 行事曆         type calendar
         * */
        when (itemValue.type) {
            "category" -> {
                val action = NotificationsFragmentDirections.actionNotificationsFragmentToVideoSortFragment()
                action.title = getCourseCategoryTitle(itemValue.type_id.toInt())
                action.categoryId = itemValue.type_id.toInt()
                action.isFollowed = model.isFollowCategory(itemValue.type_id.toInt())
                findNavController().navigate(action)
            }

            "instructor" -> {
                val action = NotificationsFragmentDirections.actionNotificationsFragmentToCoachFragment()
                action.id = itemValue.type_id.toInt()
                action.title = ""
                action.isFollowed = itemValue.is_followed
                findNavController().navigate(action)
            }

            "calendar" -> {
                val action = NotificationsFragmentDirections.actionNotificationsFragmentToVideoPageFragment()
                action.courseId = itemValue.type_id.toInt()
                findNavController().navigate(action)
            }

            "familygroup" -> {
                // 家庭方案成員異動的通知
            }

            "expired" -> {
                // 主帳取消訂閱，訂閱到期、續訂扣款失敗的通知
                if (!model.userInfo!!.user.subscription) {
                    val action = NotificationsFragmentDirections.actionNotificationsFragmentToPaymentFrgment()
                    findNavController().navigate(action)
                }
            }

            "family_group_expired" -> {
                // 主帳退費 or 過期，子帳的通知
            }
        }
    }

}