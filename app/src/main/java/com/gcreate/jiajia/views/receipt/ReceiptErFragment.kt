package com.gcreate.jiajia.views.receipt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestInvoice
import com.gcreate.jiajia.api.user.response.ResponseInvoice
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentReceiptErBinding
import com.gcreate.jiajia.util.AES256Util
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException

class ReceiptErFragment : Fragment() {

    private lateinit var binding: FragmentReceiptErBinding
    private var carrierType = 0
    private var pinCode = ""
    private var isFirstTimeInView = false

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentReceiptErBinding.inflate(inflater, container, false)

        binding.apply {
            viewModel = model

            initView()

            if (model.invoiceResult!!.user_data.invoice_type_default == "1") {
                model.receipt.defaultType = Receipt.Default.Er
            }
        }

        return binding.root
    }

    private fun initView() {
        when (model.invoiceResult!!.user_data.invoice_carrier_default) {
            "0" -> {    // 會員載具
                setViewShowHide("會員載具", View.VISIBLE, View.GONE)
                carrierType = 0
            }
            "1" -> {    // 手機載具
                setViewShowHide("手機載具", View.GONE, View.VISIBLE)
                carrierType = 1
                binding.etCarrier.setText(model.invoiceResult!!.user_data.invoice_mobile_carrier.toString())
            }
            "2" -> {    // 自然人載具
                setViewShowHide("自然人載具", View.GONE, View.VISIBLE)
                carrierType = 2
                binding.etCarrier.setText(model.invoiceResult!!.user_data.invoice_nature_carrier.toString())
            }
        }

        binding.apply {
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
            toolbar.setNavigationOnClickListener { backPressed() }

            (citySpinner.editText as AutoCompleteTextView).setText(model.me.get()!!.county.toString(), false)
            (districtsSpinner.editText as AutoCompleteTextView).setText(model.me.get()!!.district.toString(), false)

            if (model.invoiceResult!!.user_data.email == null) {
                tvEmail.text = "請輸入信箱"
            } else {
                tvEmail.text =
                    AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), model.invoiceResult!!.user_data.email!!)
            }

            //修改信箱
            tvVerify.setOnClickListener {
                val action = ReceiptErFragmentDirections.actionReceiptErFragmentToPaymentMailVerifyFragment()
                findNavController().navigate(action)
            }

            btnSave.setOnClickListener {
                updateUserProfile(carrierType)
            }

            swDefault.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked && isFirstTimeInView) {
                    when (carrierType) {
                        0 -> {
                            switchable(model.invoiceResult!!.user_data.email != null, "請先輸入信箱")
                        }
                        1 -> {
                            switchable(binding.etCarrier.text.toString().isNotEmpty(), "請先輸入手機載具號碼")
                        }
                        2 -> {
                            switchable(binding.etCarrier.text.toString().isNotEmpty(), "請先輸入自然人載具號碼")
                        }
                    }
                } else {
                    model.receipt.defaultType = Receipt.Default.Nono
                }
                isFirstTimeInView = true
            }

            swNeedInvoice.apply {
                setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked) model.invoiceResult!!.user_data.invoice_paper = "1"
                    else model.invoiceResult!!.user_data.invoice_paper = "0"
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        setMenuItems()
        getJsonData()
    }

    // 格式驗證
    private fun checkEgui(egui: String): Boolean {
        return egui.matches(Regex("^/[0-9A-Z+-.]{7}$"))
    }

    private fun checkCitizenDigitalCertificate(digital: String): Boolean {
        return digital.matches(Regex("^[A-Z]{2}[0-9]{14}\$"))
    }

    private fun switchable(canSwitch: Boolean, errorText: String) {
        if (canSwitch) {
            when (carrierType) {
                0 -> {
                    updateUserProfile(0)
                }
                1 -> {
                    if (checkEgui(binding.etCarrier.text.toString())) {
                        setInvoice(1)
                    } else {
                        binding.swDefault.isChecked = false
                        Toast.makeText(requireActivity(), "手機載具格式不符", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
                2 -> {
                    if (checkCitizenDigitalCertificate(binding.etCarrier.text.toString())) {
                        setInvoice(2)
                    } else {
                        binding.swDefault.isChecked = false
                        Toast.makeText(requireActivity(), "自然人憑證格式不符", Toast.LENGTH_SHORT).show()
                        return
                    }
                }
            }
            model.receipt.defaultType = Receipt.Default.Er
        } else {
            model.receipt.defaultType = Receipt.Default.Nono
            binding.swDefault.isChecked = false
            Toast.makeText(requireActivity(), errorText, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setMenuItems() {
        val carrierLists = mutableListOf("會員載具", "手機載具", "自然人載具")

        val carrierAdapter = ArrayAdapter(requireContext(), R.layout.list_item, carrierLists)
        (binding.carrierSpinner.editText as? AutoCompleteTextView)?.apply {
            setAdapter(carrierAdapter)
            setOnItemClickListener { _, _, position, _ ->
                carrierType = position
                when (position) {
                    0 -> setViewShowHide(carrierLists[position], View.VISIBLE, View.GONE)
                    1 -> {
                        setViewShowHide(carrierLists[position], View.GONE, View.VISIBLE)
                        if (model.invoiceResult!!.user_data.invoice_mobile_carrier == null) {
                            binding.etCarrier.setText("")
                        } else {
                            binding.etCarrier.setText(model.invoiceResult!!.user_data.invoice_mobile_carrier.toString())
                        }
                    }
                    2 -> {
                        setViewShowHide(carrierLists[position], View.GONE, View.VISIBLE)
                        if (model.invoiceResult!!.user_data.invoice_nature_carrier == null) {
                            binding.etCarrier.setText("")
                        } else {
                            binding.etCarrier.setText(model.invoiceResult!!.user_data.invoice_nature_carrier.toString())
                        }
                    }
                }
            }
        }
    }

    private fun setViewShowHide(title: String, hideOrShowLayout1: Int, hideOrShowLayout2: Int) {
        binding.apply {
            (carrierSpinner.editText as? AutoCompleteTextView)!!.setText(title, false)
            layout1.visibility = hideOrShowLayout1
            layout2.visibility = hideOrShowLayout2
        }
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun getJsonData() {
        try {
            // read json file
            val inputStreamReader = InputStreamReader(requireActivity().assets.open("taiwan_districts.json"), "UTF-8")
            val bufferedReader = BufferedReader(inputStreamReader)

            var line: String?
            val stringBuilder = StringBuilder()
            while ((bufferedReader.readLine().also { line = it }) != null) {
                stringBuilder.append(line)
            }
            bufferedReader.close()
            inputStreamReader.close()

            val jsonObject = JSONObject(stringBuilder.toString())

            // convert json string to object
            val gson = Gson()
            val cityDistricts = gson.fromJson<TaiwanDistricts>(jsonObject.toString(), object : TypeToken<TaiwanDistricts>() {}.type).data
            setMenuItems(cityDistricts)

        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }

    private fun setMenuItems(cityDistricts: MutableList<CityData>) {
        val cityLists = mutableListOf<String>()
        val districtsList = mutableListOf<District>()

        for (i in 0 until cityDistricts.size) {
            cityLists.add(cityDistricts[i].name)
            if (model.me.get()!!.county == cityDistricts[i].name) {
                districtsList.addAll(cityDistricts[i].districts)
            }
        }

        val districtsTextList = mutableListOf<String>()
        for (item in districtsList) {
            districtsTextList.add(item.name)
        }

        val countryAdapter = ArrayAdapter(requireContext(), R.layout.list_item, cityLists)
        (binding.citySpinner.editText as? AutoCompleteTextView)?.apply {
            setAdapter(countryAdapter)
            setOnItemClickListener { _, _, position, _ ->
                (binding.districtsSpinner.editText as? AutoCompleteTextView)!!.setText("")
                val districts = cityDistricts[position].districts
                val districtsLists = mutableListOf<String>()

                for (item in districts) {
                    districtsLists.add(item.name)
                }
                val districtsAdapter = ArrayAdapter(requireContext(), R.layout.list_item, districtsLists)
                (binding.districtsSpinner.editText as? AutoCompleteTextView)?.apply {
                    setAdapter(districtsAdapter)
                    setOnItemClickListener { _, _, position, _ ->
                        pinCode = districts[position].zip
                    }
                }
            }
        }

        val districtsAdapter = ArrayAdapter(requireContext(), R.layout.list_item, districtsTextList)
        (binding.districtsSpinner.editText as? AutoCompleteTextView)?.apply {
            setAdapter(districtsAdapter)
            setOnItemClickListener { _, _, position, _ ->
                pinCode = districtsList[position].zip
            }
        }

        (binding.districtsSpinner.editText as? AutoCompleteTextView)!!.setOnClickListener {
            if ((binding.citySpinner.editText as? AutoCompleteTextView)?.text.isNullOrBlank()) {
                Toast.makeText(requireActivity(), "請先選擇縣市", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun updateUserProfile(carrierType: Int) {
        when (carrierType) {
            0 -> {
                // 儲存地址資訊
                val countyText =
                    RequestBody.create(MediaType.parse("text/plain"), (binding.citySpinner.editText as? AutoCompleteTextView)!!.text.toString())
                val districtText =
                    RequestBody.create(MediaType.parse("text/plain"), (binding.districtsSpinner.editText as? AutoCompleteTextView)!!.text.toString())
                val addressText = RequestBody.create(MediaType.parse("text/plain"), binding.textInputAddress.text.toString())
                val pinCodeText = RequestBody.create(MediaType.parse("text/plain"), pinCode)

                model.me.get()!!.apply {
                    county = (binding.citySpinner.editText as? AutoCompleteTextView)!!.text.toString()
                    district = (binding.districtsSpinner.editText as? AutoCompleteTextView)!!.text.toString()
                    address = binding.textInputAddress.text.toString()
                    pin_code = pinCode
                }

                UserApiClient.updateUserProfile(
                    model.appState.accessToken.get()!!,
                    null,
                    null,
                    countyText,
                    districtText,
                    addressText,
                    pinCodeText,
                    object : ApiController<ResponseSimpleFormat>(requireActivity(), false) {
                        override fun onSuccess(response: ResponseSimpleFormat) {
                            Toast.makeText(requireActivity(), "儲存成功", Toast.LENGTH_SHORT).show()
                        }
                    })

                setInvoice(0)

            }
            1 -> setInvoice(1)                //儲存手機載具條碼
            2 -> setInvoice(2)                //儲存自然人載具條碼
        }
    }

    private fun setInvoice(carrierType: Int) {
        var emailAddress = ""
        var carrierNumber = ""
        when (carrierType) {
            0 -> {
                emailAddress = binding.tvEmail.text.toString()
            }
            1 -> {
                carrierNumber = binding.etCarrier.text.toString()
                model.invoiceResult!!.user_data.invoice_paper = "0"
                if (checkEgui(carrierNumber)) {
                    model.receipt.defaultType = Receipt.Default.Er
                } else {
                    Toast.makeText(requireActivity(), "條碼格式不符", Toast.LENGTH_SHORT).show()
                    return
                }
            }
            2 -> {
                carrierNumber = binding.etCarrier.text.toString()
                model.invoiceResult!!.user_data.invoice_paper = "0"
                if (checkCitizenDigitalCertificate(carrierNumber)) {
                    model.receipt.defaultType = Receipt.Default.Er
                } else {
                    Toast.makeText(requireActivity(), "自然人憑證格式不符", Toast.LENGTH_SHORT).show()
                    return
                }
            }
        }

        val request = RequestInvoice(
            emailAddress,
            "",
            "",
            "",
            "",
            carrierType.toString(),
            carrierNumber,
            model.invoiceResult!!.user_data.invoice_paper,
            1)

        binding.etCarrier.clearFocus()
        UserApiClient.setInvoice(model.appState.accessToken.get()!!,
            request,
            object : ApiController<ResponseInvoice>(requireContext(), false) {
                override fun onSuccess(response: ResponseInvoice) {
                    binding.swDefault.isChecked = true
                    Toast.makeText(requireActivity(), "儲存成功", Toast.LENGTH_SHORT).show()
                }
            })
    }
}