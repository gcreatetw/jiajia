package com.gcreate.jiajia.views.bodyinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gcreate.jiajia.databinding.FragmentNumberPickerBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class NumberPickerFragment(private val heightOrWeight: String) : BottomSheetDialogFragment() {

    var listener: OnDialogButtonFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding = FragmentNumberPickerBinding.inflate(inflater, container, false)

        binding.numberPicker.apply {
            when (heightOrWeight) {
                "height" -> {
                    value = BodyInfoFragment.userHeight
                    minValue = 0
                    maxValue = 300
                }
                "weight" -> {
                    value = BodyInfoFragment.userWeight
                    minValue = 30
                    maxValue = 300
                }
            }
        }

        binding.btnCheck.setOnClickListener {
            when (heightOrWeight) {
                "height" -> {
                    listener?.onSelectDialog("HeightCheck", binding.numberPicker.value)
                }
                "weight" -> {
                    listener?.onSelectDialog("WeightCheck", binding.numberPicker.value)
                }
            }
            dismiss()
        }

        binding.btnCancel.setOnClickListener {
            dismiss()
        }

        return binding.root
    }

    interface OnDialogButtonFragmentListener {
        fun onSelectDialog(select: String, value: Int)
    }

}