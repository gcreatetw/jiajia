package com.gcreate.jiajia.views.home

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.tab.HomeTabAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.ResponseDonateMechanism
import com.gcreate.jiajia.api.user.response.ResponseNotifications
import com.gcreate.jiajia.api.user.response.ResponseUserInfo
import com.gcreate.jiajia.data.*
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentHomeBinding
import com.gcreate.jiajia.util.AES256Util
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import org.json.JSONObject
import retrofit2.Response


class HomeFragment : androidx.fragment.app.Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private var hasUnRead: Boolean = false

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        val token = model.appState.accessToken.get()

        if (!token.isNullOrEmpty()) {
            UserApiClient.getNotifications(
                model.appState.accessToken.get()!!, object : ApiController<ResponseNotifications>(requireActivity(), false) {
                    override fun onSuccess(response: ResponseNotifications) {
                        hasUnRead = response.unread_count != 0
                        setHasOptionsMenu(true)
                    }
                })

            if (model.me.get()!!.name.isEmpty()) {
                getUserInfo()
            } else {
                setHomeTab()
            }

            if (model.listFollow == null) {
                model.loadFollowState(requireContext())
            }

        } else {
            setHasOptionsMenu(true)
            setHomeTab()
        }

        if(model.appState.donateMechanism.get()!!.toString().equals("")){
            getDonateMechanism()
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //TabLayout
        //setHomeTab()
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home_toolbar_menu, menu)
        if (hasUnRead) {
            menu.findItem(R.id.home_notification_item).setIcon(R.drawable.outline_notifications_with_red_point_24)
        } else {
            menu.findItem(R.id.home_notification_item).setIcon(R.drawable.outline_notifications_24)
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.home_search_item -> {
                val action = MainFragmentDirections.actionMainFragmentToSearchFragment()
                (activity as MainActivity).rootNavController.navigate(action)
                true
            }
            R.id.home_notification_item -> {
                val action = MainFragmentDirections.actionMainFragmentToNotificationsFragment()
                (activity as MainActivity).rootNavController.navigate(action)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getUserInfo() {
        UserApiClient.uerInfo(model.appState.accessToken.get()!!, object : ApiController<ResponseUserInfo>(requireContext(), true) {
            override fun onSuccess(response: ResponseUserInfo) {
                val userInfo = response.user
                responseNullCheck(userInfo)
                model.collectCourses = response.collect
                model.userMore = response.more
                model.userInfo = response
                model.showBonusChannel = false

                runBlocking {
                    model.appState.setHaveSubscription(userInfo.subscription)
                    if (userInfo.payment_method != null) {
                        model.appState.setPayMethod(userInfo.payment_method!!)
                    } else {
                        model.appState.setPayMethod("cathaybk")
                    }
                }

                setHomeTab()
            }
        })
    }

    private fun getDonateMechanism() {
        UserApiClient.getDonateMechanism(model.appState.accessToken.get()!!,
            object : ApiController<ResponseDonateMechanism>(requireContext(), false) {
                override fun onSuccess(response: ResponseDonateMechanism) {
                    val storeDonateMechanismString = model.appState.donateMechanism.get()!!
                    val gson = Gson()
                    val arraysData =
                        gson.fromJson<ResponseDonateMechanism>(storeDonateMechanismString, object : TypeToken<ResponseDonateMechanism>() {}.type)

                    if (arraysData == null || arraysData.data.size != response.data.size) {
                        runBlocking {
                            val jsonObject = JSONObject(Gson().toJson(response))
                            model.appState.setDonateMechanism(jsonObject.toString())
                        }
                    }
                }

                override fun onFail(httpResponse: Response<ResponseDonateMechanism>): Boolean {
                    return super.onFail(httpResponse)
                }
            })
    }

    private fun responseNullCheck(userInfo: com.gcreate.jiajia.api.user.response.User) {

        var subscriptionType: Subscription.Type = Subscription.Type.UnRegistered
        when (userInfo.subscription_level) {
            "月繳方案會員" -> subscriptionType = Subscription.Type.Month
            "季繳方案會員" -> subscriptionType = Subscription.Type.Season
            "年繳方案會員" -> subscriptionType = Subscription.Type.Year
            else -> Subscription.Type.UnRegistered
        }

        var genderString = "1"
        if (!userInfo.gender.isNullOrEmpty()) {
            genderString = userInfo.gender
        }

        var birthdayString = "1990-01-01"
        if (!userInfo.birthday.isNullOrEmpty()) {
            birthdayString = userInfo.birthday
        }

        var heightString = "160"
        if (!userInfo.height.isNullOrEmpty()) {
            if (userInfo.height != "o2Hzq2E2wePTIkWi7Bc/4g==") {
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.height)
            }
        }

        var weightString = "60"
        if (!userInfo.weight.isNullOrEmpty()) {
            if (userInfo.weight != "o2Hzq2E2wePTIkWi7Bc/4g==") {
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.weight)
            }
        }

        var fitnessLevelString = "1"
        if (!userInfo.fitness_level.isNullOrEmpty()) {
            fitnessLevelString = userInfo.fitness_level
        }

        var fitnessTargetString = "1"
        if (!userInfo.fitness_target.isNullOrEmpty()) {
            fitnessTargetString = userInfo.fitness_target
        }

        var fitnessHowLongString = "1"
        if (!userInfo.fitness_howlong.isNullOrEmpty()) {
            fitnessHowLongString = userInfo.fitness_howlong
        }

        try{
            model.me.set(User(
                userInfo.user_img,
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.name),
                userInfo.member_number,
                userInfo.subscription,
                subscriptionType,
                userInfo.subscription_expire,
                "",
                "",
                "",
                0.0,
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.county!!),
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.district!!),
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.address!!),
                AES256Util.AES256Decrypt(getString(R.string.AESkey), getString(R.string.AESIVkey), userInfo.pin_code!!),
                MyHobby(
                    "",
                    Integer.parseInt(genderString),
                    birthdayString,
                    Integer.parseInt(heightString),
                    Integer.parseInt(weightString),
                    Integer.parseInt(fitnessLevelString),
                    Integer.parseInt(fitnessTargetString),
                    Integer.parseInt(fitnessHowLongString),
                )
            ))
        }catch (e:Exception){

        }


        model.notificationSetting.get()!!.news = userInfo.news != 0
        model.notificationSetting.get()!!.coach = userInfo.instructor != 0
        model.notificationSetting.get()!!.type = userInfo.category != 0
        model.notificationSetting.get()!!.schedule = userInfo.calendar != 0
    }

    private fun setHomeTab() {
        val adapter = HomeTabAdapter(this@HomeFragment, model.showBonusChannel)

        val viewPager = binding.homeViewpager
        viewPager.isUserInputEnabled = false
        viewPager?.adapter = adapter
        TabLayoutMediator(binding.homeTabLayout, viewPager) { tab, position ->
            if (model.showBonusChannel) {
                when (position) {
                    0 -> tab.text = "課程首頁"
                    1 -> tab.text = "加值頻道"
                }
            } else {
                when (position) {
                    0 -> tab.text = "課程首頁"
                    1 -> tab.text = "推薦課程"
                }
            }

        }.attach()
    }

}