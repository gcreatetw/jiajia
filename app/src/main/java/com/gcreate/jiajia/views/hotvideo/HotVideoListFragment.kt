package com.gcreate.jiajia.views.hotvideo


import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.HotVideoRecyclerViewAdapter
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.request.RequestCourses
import com.gcreate.jiajia.api.course.response.ResponseCourses
import com.gcreate.jiajia.api.dataobj.Course
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.data.BeingRecyclerViewItemData
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentHotVideoListBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import kotlin.math.roundToInt

class HotVideoListFragment : Fragment() {


    private lateinit var binding: FragmentHotVideoListBinding
    private val args: HotVideoListFragmentArgs by navArgs()
    private var menuItemFilter: MenuItem? = null
    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.loadCourseFilterSetting(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHotVideoListBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        binding.apply {
            hotVideoListToolbar.apply {
                navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
                (activity as AppCompatActivity).setSupportActionBar(hotVideoListToolbar)
                setNavigationOnClickListener {
                    backPressed()
                }
                title = args.mode.text
            }

            lessonList = listOf<BeingRecyclerViewItemData>()
        }


        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = HotVideoRecyclerViewAdapter {
                if ((binding.lessonList!![it] as Course).videoEnable) {
                    val action = HotVideoListFragmentDirections.actionHotVideoListFragmentToVideoPageFragment()
                    action.courseId = (binding.lessonList!![it] as Course).id
                    (activity as MainActivity).rootNavController.navigate(action)
                }
            }
        }

        setHasOptionsMenu(true)

        setMenuItemFilterColor()
        loadDataFromApi()

        return binding.root
    }

    private fun loadDataFromApi() {

        var feature = 0
        if (args.mode == HotVideoListFragmentMode.HotVideo) {
            feature = 1
        }
        val request = RequestCourses(
            "", feature, filter.date,
            filter.intensityIdList, filter.coachIdList, filter.equipmentIdList, 25, 1, null, null)

        CourseApiClient.courses(model.appState.accessToken.get()!!, request, object : ApiController<ResponseCourses>(requireContext(), true) {
            override fun onSuccess(response: ResponseCourses) {
                binding.lessonList = response.courses.toList()
            }
        })
    }

    private fun setMenuItemFilterColor() {
        val enable = filter.intensityIdList.isNotEmpty()
                || filter.coachIdList.isNotEmpty()
                || filter.equipmentIdList.isNotEmpty()

        val drawable = menuItemFilter?.icon
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    resources.getColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    resources.getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.hot_video_menu, menu)
        //改變menu item顏色
        for (i in 0 until menu.size()) {
            if (menu.getItem(i).itemId == R.id.hot_video_filter_item) {
                menuItemFilter = menu.getItem(i)
            }
            val drawable = menu.getItem(i).icon
            if (drawable != null) {
                drawable.mutate()
                drawable.setColorFilter(
                    resources.getColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )

            }
        }
        setMenuItemFilterColor()
        super.onCreateOptionsMenu(menu, inflater)
    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.hot_video_filter_item -> {
                //開啟篩選dialog
                activity?.supportFragmentManager.let {
                    val dialog = HotVideoFilterDialog(filter) { _filter ->
                        filter = _filter
                        setMenuItemFilterColor()
                        loadDataFromApi()
                    }
                    dialog.show(it!!, "")
                }

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}