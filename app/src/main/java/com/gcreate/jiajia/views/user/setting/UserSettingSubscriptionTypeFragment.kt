package com.gcreate.jiajia.views.user.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingSubscripTypeBinding
import com.gcreate.jiajia.adapters.tab.SubscriptionRecordTypeAdapter
import com.google.android.material.tabs.TabLayoutMediator


class UserSettingSubscriptionTypeFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = FragmentUserSettingSubscripTypeBinding.inflate(inflater, container, false)

        val adapter = SubscriptionRecordTypeAdapter(this)
        val tabLayout = binding.subscriptionTabLayout
        val viewPager = binding.subscriptionViewpager
        viewPager.isUserInputEnabled = false
        viewPager?.adapter = adapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "訂閱紀錄"
                1 -> tab.text = "優惠券使用"
            }
        }.attach()

        binding.apply {
            //設定返回鍵顏色及點擊事件
            toolbar.title = ""
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }
            imgItemList.setOnClickListener {
                // 管理訂閱
                val action =
                    UserSettingSubscriptionTypeFragmentDirections.actionUserSettingSubscriptionTypeFragmentToUserSettingSubscriptionItemFragment()
                findNavController().navigate(action)
            }
        }
        return binding.root
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }


}


