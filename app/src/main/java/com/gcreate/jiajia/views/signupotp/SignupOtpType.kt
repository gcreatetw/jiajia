package com.gcreate.jiajia.views.signupotp

enum class SignupOtpType {
    ModifyPassword,
    Signup,
    ForgetPassword
}