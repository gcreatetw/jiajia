package com.gcreate.jiajia.views.user.setting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestUserCoupon
import com.gcreate.jiajia.api.user.response.ResponseSimpleFormat
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentUserSettingCodeBinding
import com.gcreate.jiajia.views.dialog.MessageDialog

class UserSettingCodeFragment : Fragment() {

    private var binding: FragmentUserSettingCodeBinding? = null

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserSettingCodeBinding.inflate(inflater, container, false)
        binding?.viewModel = model

        binding?.apply {
            toolbar.navigationIcon?.setTint(ContextCompat.getColor(requireActivity(), R.color.white))
            (activity as AppCompatActivity).setSupportActionBar(toolbar)

            toolbar.setNavigationOnClickListener {
                backPressed()
            }


            textInputLayoutCode.editText?.doOnTextChanged { text, start, before, count ->
            }

            btnSend.setOnClickListener {
                setUserCoupon()
            }
        }

        return binding?.root
    }

    private fun backPressed() {
        findNavController().navigateUp()
    }

    private fun setUserCoupon() {
        val requestBody = RequestUserCoupon(binding!!.textInputLayoutCode.editText!!.text.toString())
        UserApiClient.setUserCoupon(model.appState.accessToken.get()!!, requestBody,
            object : ApiController<ResponseSimpleFormat>(requireActivity(), true) {
                override fun onSuccess(response: ResponseSimpleFormat) {
                    activity?.supportFragmentManager.let {
                        val dialog = MessageDialog("折扣代碼", response.error_message) {
                            backPressed()
                        }
                        dialog.show(it!!, "")
                    }
                    if (!response.error) {
                        model.setDiscountCode(binding!!.textInputLayoutCode.editText?.text.toString())
                    }
                }
            })
    }
}