package com.gcreate.jiajia.views.coach

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.response.ResponseCategory
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.request.RequestCoach
import com.gcreate.jiajia.api.homerecommend.response.ResponseCoachAll
import com.gcreate.jiajia.data.CoachListItem
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentCoachListBinding
import com.gcreate.jiajia.adapters.home.coach.CoachListRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.initClassCategoryChipGroup
import kotlin.math.roundToInt

class CoachListFragment : Fragment() {

    private var _binding: FragmentCoachListBinding? = null

    private val binding get() = _binding!!

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentCoachListBinding.inflate(inflater, container, false)

        //設定返回鍵顏色及點擊事件
        val toolBar = _binding?.coachListToolbar
        toolBar?.navigationIcon?.setTint(resources.getColor(R.color.white))
        (activity as AppCompatActivity).setSupportActionBar(toolBar)

        toolBar?.setNavigationOnClickListener {
            backPressed()
        }

        var chipGroup = _binding?.coachListChipGroup

        chipGroup?.setOnCheckedChangeListener { group, checkedId ->
            loadDataCoachList(checkedId)
        }


        val rv = _binding?.recyclerView
        rv?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        rv?.addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
        rv?.adapter = CoachListRecyclerViewAdapter() {

            for (item in model.listFollow!!.instructor) {
                if (binding.coachList!![it].id == item.id) {
                    val action = CoachListFragmentDirections.actionCoachListFragmentToCoachFragment()
                    val coach = binding!!.coachList!![it]
                    action.title = coach.name
                    action.id = coach.id
                    action.isFollowed = item.followed
                    binding!!.root.findNavController().navigate(action)
                    break
                }
            }

        }

        loadDataFromApi(inflater)

        return _binding?.root
    }

    private fun loadDataFromApi(inflater: LayoutInflater) {
        CourseApiClient.category(object : ApiController<ResponseCategory>(requireContext(), true) {
            override fun onSuccess(response: ResponseCategory) {
                var chipGroup = binding?.coachListChipGroup
                initClassCategoryChipGroup(inflater, chipGroup!!, response.category)
                chipGroup.check(response.category[0].id)

                loadDataCoachList(response.category[0].id)
            }
        })
    }

    private fun loadDataCoachList(id: Int) {
        val request = RequestCoach(
            id, 25, 1
        )

        HomeRecommendApiClient.coachList(request, object : ApiController<ResponseCoachAll>(requireContext(), true) {
            override fun onSuccess(response: ResponseCoachAll) {
                _binding?.coachList = response.instructor
            }
        })
    }

    private fun backPressed() {
        val navController = (activity as MainActivity).rootNavController
        navController.navigateUp()
    }
}