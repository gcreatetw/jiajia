package com.gcreate.jiajia.views.user.familyPlan

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.membercentre.familyPlan.FamilyPlanSearchMemberAdapter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.request.RequestAddFamilyMember
import com.gcreate.jiajia.api.user.request.RequestSearchFamilyMember
import com.gcreate.jiajia.api.user.response.ResponseAddFamilyMember
import com.gcreate.jiajia.api.user.response.ResponseSearchFamilyMemberResult
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentFamilyPlanSearchMemberBinding
import java.util.*


class FamilyPlanSearchMemberFragment : Fragment() {

    private lateinit var binding: FragmentFamilyPlanSearchMemberBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentFamilyPlanSearchMemberBinding.inflate(inflater, container, false)

        setToolBar()

        binding.apply {
            textInputLayoutPhoneNumber.apply {
                editText?.doOnTextChanged { text, _, _, _ ->
                    error = if (text?.isNotEmpty() == true && text.length < 10) {
                        "格式不符。"
                    } else {
                        null
                    }
                }

                editText?.setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                        hideSoftKeyBroad()
                        binding.textInputLayoutPhoneNumber.clearFocus()

                        if (binding.textInputLayoutPhoneNumber.editText!!.text.length == 10) {
                            searchFamilyMember()
                        } else {
                            binding.tvSearchNoResult.visibility = View.GONE
                            binding.lySearchResult.visibility = View.GONE
                        }

                        true
                    } else {
                        false
                    }
                }

                setEndIconOnClickListener {
                    hideSoftKeyBroad()
                    binding.textInputLayoutPhoneNumber.clearFocus()

                    if (binding.textInputLayoutPhoneNumber.editText!!.text.length == 10) {
                        searchFamilyMember()
                    } else {
                        binding.tvSearchNoResult.visibility = View.GONE
                        binding.lySearchResult.visibility = View.GONE
                    }
                }
            }
        }

        return binding.root
    }

    private fun setToolBar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun searchFamilyMember() {
        binding.tvSearchNoResult.visibility = View.GONE
        binding.lySearchResult.visibility = View.GONE

        val requestBody = RequestSearchFamilyMember(binding.textInputLayoutPhoneNumber.editText!!.text.toString())
        UserApiClient.searchFamilyMember(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseSearchFamilyMemberResult>(requireActivity(), false) {
                override fun onSuccess(response: ResponseSearchFamilyMemberResult) {
                    if (!response.error) {
                        // 使用者搜尋 - 成功
                        binding.lySearchResult.visibility = View.VISIBLE
                        binding.rvMemberSearchContent.apply {
                            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

                            val mAdapter = FamilyPlanSearchMemberAdapter(response.data!!)
                            mAdapter.addChildClickViewIds(R.id.tv_add_family_member)
                            adapter = mAdapter
                            mAdapter.setOnItemChildClickListener { _, _, _ ->
                                addFamilyMember(response.data.id)
                            }
                        }
                    } else {
                        binding.tvSearchNoResult.apply {
                            visibility = View.VISIBLE
                            text = response.error_message
                        }
                        binding.lySearchResult.visibility = View.GONE
                    }
                }
            })
    }

    private fun addFamilyMember(addMemberId: Int) {
        val requestBody = RequestAddFamilyMember(addMemberId)
        UserApiClient.addFamilyMember(model.appState.accessToken.get()!!,
            requestBody,
            object : ApiController<ResponseAddFamilyMember>(requireActivity()) {
                override fun onSuccess(response: ResponseAddFamilyMember) {
                    if (!response.error) {
//                        Toast.makeText(requireActivity(), "加入成功", Toast.LENGTH_SHORT).show()
                        val builder = AlertDialog.Builder(requireActivity())
                        builder.apply {
                            setMessage("加入成功。")
                            setPositiveButton("確定") { dialog: DialogInterface, _: Int ->
                                dialog.dismiss()
                            }
                            setCancelable(false)
                        }

                        val alertDialog = builder.create()
                        alertDialog.show()
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                            .setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.white))

                        binding.apply {
                            lySearchResult.visibility = View.GONE
                            tvSearchNoResult.visibility = View.GONE
                            textInputLayoutPhoneNumber.editText!!.text.clear()
                            binding.textInputLayoutPhoneNumber.clearFocus()
                        }
                    }
                }
            })
    }

}