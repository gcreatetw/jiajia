package com.gcreate.jiajia.views.video

import android.os.Bundle
import android.view.WindowManager
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.event.EventEmitter
import com.brightcove.player.model.Video
import com.brightcove.player.view.BrightcovePlayer
import com.gcreate.jiajia.R
import com.gcreate.jiajia.databinding.ActivityBrightCoveBinding

class BrightCoveActivity : BrightcovePlayer() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        val binding = ActivityBrightCoveBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val eventEmitter: EventEmitter = binding.brightcoveVideoView.eventEmitter
        val account = getString(R.string.account)
        val catalog = Catalog.Builder(eventEmitter, account)
            .setBaseURL(Catalog.DEFAULT_EDGE_BASE_URL)
            .setPolicy(getString(R.string.policy))
            .build()

        catalog.findVideoByID(intent.getStringExtra("videoId").toString(), object : VideoListener() {
            override fun onVideo(video: Video) {

                binding.brightcoveVideoView.apply {
                    add(video)
                    start()
                }
            }
        })

        binding.videoPlayToolbar.setNavigationOnClickListener {
            finish()
        }
    }

}