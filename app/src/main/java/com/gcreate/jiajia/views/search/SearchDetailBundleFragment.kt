package com.gcreate.jiajia.views.search

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.home.search.SearchBundleAdapter
import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.search.SearchApiClient
import com.gcreate.jiajia.api.search.request.RequestSearchCourseBody
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentSearchDetailBundleBinding
import com.gcreate.jiajia.listener.UpdateSearchKeywordListener
import com.gcreate.jiajia.listener.UpdateSearchKeywordListenerManager
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.views.dialog.HotVideoFilterDialog
import retrofit2.Response
import kotlin.math.roundToInt


class SearchDetailBundleFragment(private var keyword: String) : Fragment(), UpdateSearchKeywordListener {

    private lateinit var binding: FragmentSearchDetailBundleBinding
    private var filter = HotVideoFilterDialog.Filter("全部", listOf(), listOf(), listOf())

    private var page = 1
    private var maxDataCount = 0
    private lateinit var mAdapterSearchBundle: SearchBundleAdapter

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.loadCourseFilterSetting(requireContext())
        UpdateSearchKeywordListenerManager.instance!!.registerListener(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchDetailBundleBinding.inflate(inflater, container, false)

        mAdapterSearchBundle = SearchBundleAdapter(mutableListOf())

        setImgFilterColor()
        loadDataFromApi()

        binding.trainingRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))

            adapter = mAdapterSearchBundle

            mAdapterSearchBundle.apply {
                addFooterView(layoutInflater.inflate(R.layout.layout_footer, null))
                setOnItemClickListener { _, _, position ->
                    val action = SearchDetailFragmentDirections.actionSearchDetailFragmentToClassPageFragment()
                    action.title = mAdapterSearchBundle.data[position].title
                    action.bundleId = mAdapterSearchBundle.data[position].id
                    (activity as MainActivity).rootNavController.navigate(action)
                }
                loadMoreModule.setOnLoadMoreListener {
                    if (maxDataCount == 0 || mAdapterSearchBundle.data.size < maxDataCount) {
                        page++
                        loadDataFromApi()
                        mAdapterSearchBundle.loadMoreModule.loadMoreComplete()
                    } else {
                        mAdapterSearchBundle.loadMoreModule.loadMoreComplete()
                        mAdapterSearchBundle.loadMoreModule.loadMoreEnd()
                    }
                }
            }
        }


        binding.imgFilter.setOnClickListener {
            activity?.supportFragmentManager.let {
                val dialog = HotVideoFilterDialog(filter) { _filter ->
                    filter = _filter
                    setImgFilterColor()
                    loadDataFromApi()
                }
                dialog.show(it!!, "")
            }
        }

        return binding.root
    }

    private fun setImgFilterColor() {
        val enable = filter.intensityIdList.isNotEmpty()
                || filter.coachIdList.isNotEmpty()
                || filter.equipmentIdList.isNotEmpty()

        val drawable = binding.imgFilter.drawable
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    ContextCompat.getColor(requireActivity(), R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun loadDataFromApi() {

        val request = RequestSearchCourseBody(
            keyword,
            null,
            filter.date,
            filter.intensityIdList,
            filter.coachIdList,
            filter.equipmentIdList,
            25,
            page)

        SearchApiClient.searchBundle(model.appState.accessToken.get()!!, request,
            object : ApiController<ResponseBundleCourses>(requireContext(), true) {
                override fun onSuccess(response: ResponseBundleCourses) {
                    mAdapterSearchBundle.addData(response.bundle)
                    maxDataCount = response.bundle.size
                    val resultCountText = "共 ${response.bundle.size} 筆結果"
                    binding.tvClassCount.text = resultCountText
                }

                override fun onFail(httpResponse: Response<ResponseBundleCourses>): Boolean {
                    mAdapterSearchBundle.loadMoreModule.loadMoreComplete()
                    mAdapterSearchBundle.loadMoreModule.loadMoreEnd()
                    return super.onFail(httpResponse)
                }
            })
    }

    override fun updateKeyword(keyword: String?) {
        this.keyword = keyword!!
        loadDataFromApi()
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateSearchKeywordListenerManager.instance!!.unRegisterListener(this)
    }

}