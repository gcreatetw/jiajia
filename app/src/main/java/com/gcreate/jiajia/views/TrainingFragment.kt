package com.gcreate.jiajia.views

import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.R
import com.gcreate.jiajia.adapters.bundle.TrainingClassRecyclerViewAdapter
import com.gcreate.jiajia.api.bundle.BundleApiClient
import com.gcreate.jiajia.api.bundle.request.RequestBundleCourses
import com.gcreate.jiajia.api.bundle.response.ResponseBundleCourses
import com.gcreate.jiajia.api.bundle.response.ResponseBundleFilter
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.data.TrainingClassItem
import com.gcreate.jiajia.data.TrainingClassTail
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentTrainingBinding
import com.gcreate.jiajia.util.Util
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.initItemNameChipGroup
import com.gcreate.jiajia.views.bodyinfo.BodyInfoFragmentType
import com.gcreate.jiajia.views.dialog.TrainingFilterDialog
import kotlin.math.roundToInt

class TrainingFragment : Fragment() {

    private var binding: FragmentTrainingBinding? = null
    private var show = false
    private var bodyInfoLayout: ConstraintLayout? = null
    private var bottomLayout: ConstraintLayout? = null
    private var filter = TrainingFilterDialog.Filter(listOf(), listOf())

    private lateinit var menuItemPersonal: MenuItem
    private lateinit var menuItemFilter: MenuItem

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTrainingBinding.inflate(inflater, container, false)

        val chipGroupType = binding?.chipGroupType

        chipGroupType?.setOnCheckedChangeListener { _, checkedId ->
            loadBundleDataFromApi("$checkedId")
        }

        binding!!.apply {
            trainingRecyclerView.apply {
                layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
                adapter = TrainingClassRecyclerViewAdapter {
                    val action = MainFragmentDirections.actionMainFragmentToClassPageFragment()
                    action.title = (binding?.list!![it] as com.gcreate.jiajia.api.dataobj.Bundle).title
                    action.bundleId = (binding?.list!![it] as com.gcreate.jiajia.api.dataobj.Bundle).id
                    (activity as MainActivity).rootNavController.navigate(action)
                }
            }

            refreshLayout.apply {
                setOnRefreshListener {
                    binding!!.refreshLayout.isRefreshing  = false
                    loadDataFromApi(inflater) }
            }
        }

        setHasOptionsMenu(true)
        loadDataFromApi(inflater)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //bodyInfoLayout
        bodyInfoLayout = binding?.trainingBodyInfoLayout
        bottomLayout = binding?.trainingBottomLayout


        //連結至body info 頁面
        binding?.trainingPersonalButton?.setOnClickListener {
            show = false
            val action = MainFragmentDirections.actionMainFragmentToBodyInfoFragment()
            action.mode = BodyInfoFragmentType.Main
            (activity as MainActivity).rootNavController.navigate(action)
        }

    }

    private fun loadDataFromApi(inflater: LayoutInflater) {
        BundleApiClient.bundleFilter(object : ApiController<ResponseBundleFilter>(requireContext()) {
            override fun onSuccess(response: ResponseBundleFilter) {
                model.bundleFilter = response
                val chipGroupType = binding?.chipGroupType
                initItemNameChipGroup(inflater, chipGroupType!!, response.category)
                if (response.category.isNotEmpty()) {
                    chipGroupType.check(response.category[0].id)
                }
            }
        })
    }

    private fun loadBundleDataFromApi(bundleCategoryId: String) {
        val request = RequestBundleCourses(
            bundleCategoryId, filter.accessoryList, filter.bimList)
        BundleApiClient.bundleCourses(request, object : ApiController<ResponseBundleCourses>(requireContext()) {
            override fun onSuccess(response: ResponseBundleCourses) {
                val list = mutableListOf<TrainingClassItem>()
                list.addAll(response.bundle)
                list.add(TrainingClassTail())
                binding?.list = list
            }
        })
    }


    override fun onStart() {
        super.onStart()
        //設定toolbar
        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setTitleTextColor(resources.getColor(R.color.white))
        (activity as AppCompatActivity?)!!.supportActionBar!!.apply {
            setDisplayShowTitleEnabled(true)
            title = "訓練計畫"
            //setDisplayShowHomeEnabled(true)
            //setIcon(R.drawable.baseline_home_24)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            showBodyInfoLayout(false)
        }, 100)

    }

    // action載入menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.training_toolbar_menu, menu)
        //改變menu item顏色
        for (i in 0 until menu.size()) {
            val drawable = menu.getItem(i).icon
            if (drawable != null) {
                drawable.mutate()
                drawable.setColorFilter(
                    resources.getColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )

            }
        }
        //取得menu item
        menuItemPersonal = menu.findItem(R.id.training_personal_item)
        menuItemPersonal.isVisible = false
        menuItemFilter = menu.findItem(R.id.training_filter_item)
        setMenuItemFilterColor()
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setMenuItemFilterColor() {
        val enable = filter.bimList.isNotEmpty()
                || filter.accessoryList.isNotEmpty()

        val drawable = menuItemFilter?.icon
        if (drawable != null) {
            drawable.mutate()
            if (enable) {
                drawable.setColorFilter(
                    resources.getColor(R.color.green),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                drawable.setColorFilter(
                    resources.getColor(R.color.white),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }
    }

    private fun showBodyInfoLayout(withAnimation: Boolean) {
        if (withAnimation) {
            if (bodyInfoLayout != null) {
                if (show) {
                    bodyInfoLayout!!.animate()
                        .translationY(bodyInfoLayout!!.height.toFloat()).duration = 300

                    bottomLayout!!.animate().translationY(bodyInfoLayout!!.height.toFloat()).duration = 300
                } else {
                    bodyInfoLayout!!.animate().translationY(0f).duration = 300
                    bottomLayout!!.animate().translationY(0f).duration = 300
                }
            }
        } else {
            if (bodyInfoLayout != null && bottomLayout != null) {
                if (show) {
                    bodyInfoLayout!!.translationY = bodyInfoLayout!!.height.toFloat()
                    bottomLayout!!.translationY = bodyInfoLayout!!.height.toFloat()
                } else {
                    bodyInfoLayout!!.translationY = 0f
                    bottomLayout!!.translationY = 0f
                }
            }
        }
    }

    //action bar menu點擊事件
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.training_personal_item -> {
                show = !show
                showBodyInfoLayout(true)
                true
            }
            R.id.training_filter_item -> {
                activity?.supportFragmentManager.let {
                    val dialog = TrainingFilterDialog(filter) { _filter ->
                        filter = _filter
                        setMenuItemFilterColor()
                        loadBundleDataFromApi("${binding?.chipGroupType?.checkedChipId}")
                    }
                    dialog.show(it!!, "")
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}