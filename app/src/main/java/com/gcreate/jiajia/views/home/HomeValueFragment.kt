package com.gcreate.jiajia.views.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.MainFragmentDirections
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.homerecommend.HomeRecommendApiClient
import com.gcreate.jiajia.api.homerecommend.response.ResponseChannel
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.dataStore
import com.gcreate.jiajia.databinding.FragmentHomeValueBinding
import com.gcreate.jiajia.adapters.home.HomeValueRecyclerViewAdapter
import com.gcreate.jiajia.util.decoration.BottomSpacingItemDecoration
import com.gcreate.jiajia.util.Util
import kotlin.math.roundToInt

class HomeValueFragment : Fragment() {

    private lateinit var binding: FragmentHomeValueBinding

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeValueBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var token: String? = null

        if (model.isLogined()) {
            token = model.appState.accessToken.get()
        }

        binding.homeValueRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            addItemDecoration(BottomSpacingItemDecoration(Util().dpToPixel(8f, requireContext()).roundToInt()))
            adapter = HomeValueRecyclerViewAdapter {
                if (model.isLogined()) {
                    val action = MainFragmentDirections.actionMainFragmentToChannelListFragment()
                    action.title = binding.list!![it].name
                    action.channelId = binding.list!![it].id
                    action.isFollowed = binding.list!![it].isFollowed
                    (activity as MainActivity).rootNavController.navigate(action)
                } else {
                    val action = MainFragmentDirections.actionMainFragmentToLoginAllinOneFragment()
                    (activity as MainActivity).rootNavController.navigate(action)
                }
            }
        }

        HomeRecommendApiClient.channel(token, object :
            ApiController<ResponseChannel>(requireContext(), true) {
            override fun onSuccess(response: ResponseChannel) {
                binding.list = response.channel
            }
        })
    }

}