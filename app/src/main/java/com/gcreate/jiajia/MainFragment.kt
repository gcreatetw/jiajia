package com.gcreate.jiajia

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.findFragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.gcreate.jiajia.data.MainViewModel
import com.gcreate.jiajia.data.MainViewModelFactory
import com.gcreate.jiajia.sharedpreferences.StorageDataMaintain
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainFragment : Fragment() {

    private val model: MainViewModel by activityViewModels {
        MainViewModelFactory(requireContext().dataStore)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.notificationSetting = ObservableField(StorageDataMaintain.getPusherList(requireContext()))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        //建立底部導覽並設定導航頁面
        val bottomNav = view.findViewById<BottomNavigationView>(R.id.bottom_nav_bar)
        val fragmentView = view.findViewById<FragmentContainerView>(R.id.fragmentContainerView)
        val navController = findNavController(fragmentView.findFragment<Fragment>())

        //主題修改為no action bar，因此標題不再隨bottomNavigationBar改變
        val appBarConfiguration =
            AppBarConfiguration(setOf(
                R.id.homeFragment,
                R.id.videoFragment,
                R.id.trainingFragment,
                R.id.liveFragment,
                R.id.userFragment
            ))
        bottomNav.setupWithNavController(navController)

        //bottom nav 有動畫做法
        val optionsRightToLeft = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.slide_right_to_left_in)
            .setExitAnim(R.anim.slide_right_to_left_out)
            .setPopEnterAnim(R.anim.slide_left_to_right_in)
            .setPopExitAnim(R.anim.slide_left_to_right_out)
            .build()

        val optionsLeftToRight = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.slide_left_to_right_in)
            .setExitAnim(R.anim.slide_left_to_right_out)
            .setPopEnterAnim(R.anim.slide_right_to_left_in)
            .setPopExitAnim(R.anim.slide_right_to_left_out)
            .build()

        bottomNav.setOnNavigationItemSelectedListener {
            // TODO : 可能有更好的寫法
            when (it.itemId) {
                R.id.homeFragment -> {
                    when (navController.currentDestination?.id) {
                        R.id.homeFragment -> {}
                        R.id.videoFragment -> navController.navigate(R.id.homeFragment, null, optionsLeftToRight)
                        R.id.trainingFragment -> navController.navigate(R.id.homeFragment, null, optionsLeftToRight)
                        R.id.liveFragment -> navController.navigate(R.id.homeFragment, null, optionsLeftToRight)
                        R.id.userFragment -> navController.navigate(R.id.homeFragment, null, optionsLeftToRight)
                    }
                }
                R.id.videoFragment -> {
                    when (navController.currentDestination?.id) {
                        R.id.homeFragment -> navController.navigate(R.id.videoFragment, null, optionsRightToLeft)
                        R.id.videoFragment -> {}
                        R.id.trainingFragment -> navController.navigate(R.id.videoFragment, null, optionsLeftToRight)
                        R.id.liveFragment -> navController.navigate(R.id.videoFragment, null, optionsLeftToRight)
                        R.id.userFragment -> navController.navigate(R.id.videoFragment, null, optionsLeftToRight)
                    }
                }
                R.id.trainingFragment -> {
                    when (navController.currentDestination?.id) {
                        R.id.homeFragment -> navController.navigate(R.id.trainingFragment, null, optionsRightToLeft)
                        R.id.videoFragment -> navController.navigate(R.id.trainingFragment, null, optionsRightToLeft)
                        R.id.trainingFragment -> {}
                        R.id.liveFragment -> navController.navigate(R.id.trainingFragment, null, optionsLeftToRight)
                        R.id.userFragment -> navController.navigate(R.id.trainingFragment, null, optionsLeftToRight)
                    }
                }
                R.id.liveFragment -> {
                    when (navController.currentDestination?.id) {
                        R.id.homeFragment -> navController.navigate(R.id.liveFragment, null, optionsRightToLeft)
                        R.id.videoFragment -> navController.navigate(R.id.liveFragment, null, optionsRightToLeft)
                        R.id.trainingFragment -> navController.navigate(R.id.liveFragment, null, optionsRightToLeft)
                        R.id.liveFragment -> {}
                        R.id.userFragment -> navController.navigate(R.id.liveFragment, null, optionsLeftToRight)
                    }
                }
                R.id.userFragment -> {
                    if (!model.isLogined()) {
                        val action = MainFragmentDirections.actionMainFragmentToLoginAllinOneFragment()
                        (activity as MainActivity).rootNavController.navigate(action)
                    } else {
                        when (navController.currentDestination?.id) {
                            R.id.homeFragment -> navController.navigate(R.id.userFragment, null, optionsRightToLeft)
                            R.id.videoFragment -> navController.navigate(R.id.userFragment, null, optionsRightToLeft)
                            R.id.trainingFragment -> navController.navigate(R.id.userFragment, null, optionsRightToLeft)
                            R.id.liveFragment -> navController.navigate(R.id.userFragment, null, optionsRightToLeft)
                            R.id.userFragment -> {}
                        }
                    }
                }
            }
            true
        }

        //設定新的toolbar為support action bar
        val toolbar = view.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar?.apply {
        }

        return view
    }

}