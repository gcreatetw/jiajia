package com.gcreate.jiajia.data

data class PaymentInfo (
    var subscriptionType : Subscription.Type,
    var type : Type
){
    enum class Type(
        val displayName : String
    ){
        noSelected(""),
        Openpoint("OPEN錢包支付"),
        iCash("iCASH支付"),
        CreditCard("信用卡支付")
    }
}