package com.gcreate.jiajia.data

data class NotificationSetting(
    var news: Boolean,
    var coach: Boolean,
    var type: Boolean,
    var schedule: Boolean,
) {

    fun getNotificationSettingNews(): Boolean {
        return news
    }

    fun getNotificationSettingCoach(): Boolean {
        return coach
    }

    fun getNotificationSettingType(): Boolean {
        return type
    }

    fun getNotificationSettingSchedule(): Boolean {
        return schedule
    }

}
