package com.gcreate.jiajia.data

data class Receipt (
    var defaultType : Default,
    var er : Er,
    var company : Company,
    var donate : Donate){

    enum class Default{
        Nono,
        Er,
        Company,
        Donate
    }

    data class Er(
        var email : String,
        var verify : Boolean
    )

    data class Company(
        var name : String?,
        var uniformNumber : String?,
        var email : String?
    )

    data class Donate(
        var index : Int,
        var code : String
    )

}