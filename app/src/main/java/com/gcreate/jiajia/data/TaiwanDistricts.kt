package com.gcreate.jiajia.data

data class TaiwanDistricts(
    val data: MutableList<CityData>
)

data class CityData(
    val districts: MutableList<District>,
    val id: Int,
    val name: String
)
data class District(
    val name: String,
    val zip: String
)