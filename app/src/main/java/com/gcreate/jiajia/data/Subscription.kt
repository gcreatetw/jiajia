package com.gcreate.jiajia.data

import com.gcreate.jiajia.R

data class Subscription(
    val type : Type,
    val name : String,
    val description :String,
    val price : Long
){
    enum class Type(
        val countMonth :Int,
        val imageUrl: Int,
        val subscriptionLevel : String
    ){
        UnRegistered(0, R.drawable.membership_card_plan,"註冊會員"),
        Month(1, R.drawable.membership_card_register,"月繳方案會員"),
        Season(3, R.drawable.membership_card_register,"季繳方案會員"),
        Year(12, R.drawable.membership_card_register,"年繳方案會員")
    }
}

data class SubscriptionAddValue(
    val name : String,
    val price : Long
)

