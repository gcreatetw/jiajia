package com.gcreate.jiajia.data

import com.gcreate.jiajia.api.dataobj.Bundle
import com.gcreate.jiajia.api.dataobj.Course

data class ItemName(
    val id: Int,
    val name: String,
)

data class ItemTitle(
    val id: Int,
    val title: String,
)

data class SplashBanner(
    val url: String,
    val title: String,
    val content: String,
)

//首頁 加值頻道的資料類型
data class ChannelData(
    val id: Int,
    val image: String, // imageUrl
    val heading: String, // title
    val sub_heading: String, // content
    val link: String,
    val language: String,
)

//首頁 推薦課程banner的資料類型
data class HomeBanner(
    val id: Int,
    val heading: String,        // title
    val sub_heading: String,    // content
    var image: String,          // url
    val link: String,
)

//首頁 推薦課程 最新上架 分類
data class ClassCategory(
    val id: Int,
    val title: String,
    var followed: Boolean,
)

//首頁 推薦課程，最新上架課程，熱門課程
open class BeingClass(
    val id: Int,
    val title: String,      // content
    var preview_image: String,   // imageUrl
    val name: String,
    var user_img: String,   // profileUrl
    val created_at: String, // date
    val type: String,
    val price: String,
    val discount_price: String,
    val views: String,
    val video_enable: Boolean,
)

data class HomeClass(
    val imageUrl: String,
    val price: String,
    val profileUrl: String,
    val name: String,
    val date: String,
    val content: String,
)

//首頁 推薦課程 最新消息 的資料類型
data class HomeNews(
//    val type: Int, // 0 : event, 1 : article
//    val imageUrl: String,
//    val content: String,
//    val date: String

    val id: Int,
    val user: String,
    val date: String,
    val category: String,
    val url: String,
    var image: String,      // imageUrl
    val heading: String,    // content
    val detail: String,
    val text: String,
    val approved: String,
    val status: String,
    val created_at: String,
    val updated_at: String,
)

//首頁 推薦課程 推薦師資 的資料類型
data class HomeCoach(
    val id: Int,
    val name: String,
    val cate: List<ClassCategory>,
    val courseCount: Int,
    val courseCate: List<ClassCategory>,
    val detail: String,     // introduce
    var user_img: String,    // imageUrl
    var isFollowed: Boolean,

    ) {
    fun cateDisplayString(): String {
        var cat = cate
        if (cat == null) {
            cat = courseCate
        }

        if (cat.isEmpty()) {
            return ""
        }
        var str = StringBuilder(cat[0].title)
        for (index in 1..cat.lastIndex) {
            str.append(" ${cat[index].title}")
        }

        return str.toString()
    }
}

//channel list 的資料類型
data class ChannelListClass(
    val imageUrl: String,
    val title: String,
    val views: String,
    val date: String,
)

//training class 的資料類型
interface TrainingClassItem
data class TrainingClass(
    val imageUrl: String,
    val title: String,
    val content: String,
) : TrainingClassItem

class TrainingClassTail() : TrainingClassItem


open class ArticleItem()

data class ArticleTitle(
    val imageUrl: String,
    val title: String,
) : ArticleItem()

data class ArticleImg(
    val imageUrl: String,
) : ArticleItem()

data class ArticleText(
    val subTitle: String,
    val text: String,
) : ArticleItem()


open class Lesson(
    val id: Int,
    val imageUrl: String,
    val name: String,
    val watchCount: Long,
    val date: String,
    val price: Int,
    val video_enable: Boolean,
)

interface ClassPageItem
data class ClassPageTitle(
    val imageUrl: String,
    val intensity: String,
    val calorie: String,
    val introduction: String,
    val equipment: String,
) : ClassPageItem

class ClassPageLesson : ClassPageItem, Lesson {
    constructor(
        _id: Int,
        _imageUrl: String,
        _name: String,
        _watchCount: Long,
        _date: String,
        _videoEnable: Boolean,
    ) : super(_id, _imageUrl, _name, _watchCount, _date, 0, _videoEnable) {
    }
}

data class ClassPageTraining(
    val list: List<Bundle>,
) : ClassPageItem

class ClassPageTail() : ClassPageItem

//data class ClassPageDetail(
//    val introduction : String,
//    val equipment : String,
//    val consort : String,
//    val notApplicable : String,
//    val prepare : String,
//    val bodyResponse : String,
//    val suggestion : String,
//    val other : String
//)


data class CoachListItem(
    val imageUrl: String,
    val name: String,
    val beGoodAt: String,
    val introduce: String,
)

open interface CoachItem
data class CoachTitle(
    val imageUrl: String,
    val beGoodAt: String,
    val lessonCount: Int,
    val introduce: String,
) : CoachItem

data class CoachNewLessonList(
    val list: MutableList<Lesson>,
) : CoachItem

data class CoachLessonTitle(
    val coachName: String,
    val categoryList: List<ClassCategory>,
    val categoryId: Int,
) : CoachItem

//class CoachLesson: CoachItem, Lesson{
//    constructor(
//        _id : Int,
//        _imageUrl: String,
//        _name: String,
//        _watchCount : Long,
//        _date: String
//    ):super(_id, _imageUrl, _name, _watchCount, _date, 0){}
//}

class CoachTail() : CoachItem

open interface HotVideoItem
class HotVideoLesson : HotVideoItem, Lesson {
    constructor(
        _id: Int,
        _imageUrl: String,
        _name: String,
        _watchCount: Long,
        _date: String,
        _videoEnable: Boolean,
    ) : super(_id, _imageUrl, _name, _watchCount, _date, 0,_videoEnable) {
    }

    constructor(beingClass: BeingClass) : super(
        beingClass.id,
        beingClass.preview_image,
        beingClass.title,
        beingClass.views.toLong(),
        beingClass.created_at,
        0,
        beingClass.video_enable) {
    }
}

class HotVideoTail() : HotVideoItem

open interface LiveStreamItem
class LiveStream(
    val courseId: Int,
    val lessonStartTime: String,
    val lessonEndTime: String,
    val lessonImageUrl: String,
    val lessonName: String,
    val lessonPrice: Int?,
    val coachImageUrl: String,
    val coachName: String,
    var isCalendared: Boolean,
    val video_enable: Boolean,
) : LiveStreamItem

class LiveStreamTail() : LiveStreamItem

//open interface UserBookmarkItem
//class UserBookmarkLesson: UserBookmarkItem, Lesson{
//    constructor(
//        _id : Int,
//        _imageUrl: String,
//        _name: String,
//        _watchCount : Long,
//        _date: String
//    ):super(_id, _imageUrl, _name, _watchCount, _date, 0){}
//}
//class UserBookmarkTail() : UserBookmarkItem

interface UserFollowByTypeItem
class UserFollowByType(
    val name: String,
    val description: String?,
    val follow: Boolean,
) : UserFollowByTypeItem

class UserFollowByTypeTail : UserFollowByTypeItem

interface UserFollowByCoachItem
class UserFollowByCoach(
    val coachId: Int,
    val imageUrl: String,
    val coachName: String,
    val coachBeGoodAt: String,
    val introduce: String,
    val follow: Boolean,
) : UserFollowByCoachItem

class UserFollowByCoachTail : UserFollowByCoachItem

interface UserFollowByAddedValueItem
class UserFollowByAddedValue(
    val name: String,
    val language: String,
    val follow: Boolean,
) : UserFollowByAddedValueItem

class UserFollowByAddedValueTail : UserFollowByAddedValueItem

interface VideoTypeItem
class VideoType(
    val id: Int,
    val typeName: String,
    var classList: List<Course>,
) : VideoTypeItem

class VideoTypeTail : VideoTypeItem

open interface VideoSortItem
class VideoSortLesson : VideoSortItem, Lesson {
    constructor(
        _id: Int,
        _imageUrl: String,
        _name: String,
        _watchCount: Long,
        _date: String,
        _videoEnable:Boolean,
    ) : super(_id, _imageUrl, _name, _watchCount, _date, 0,_videoEnable) {
    }
}

class VideoSortTail() : VideoSortItem

interface VideoPageItem
data class VideoPageTitle(
    val videoImageUrl: String,
    val videoName: String,
    val coachIamgeUrl: String,
    val coachName: String,
    val coachBeGoogAt: String,
    val coachLessonCount: Int,
    val date: String,
    val intensity: String,
    val calorie: String,
    val introduction: String,
    val equipment: String,
) : VideoPageItem

data class VideoPageTraining(
    val list: List<Bundle>,
) : VideoPageItem

class VideoPageOtherLessonTitle : VideoPageItem

//class VideoPageLesson: VideoPageItem, Lesson{
//    constructor(
//        _id : Int,
//        _imageUrl: String,
//        _name: String,
//        _watchCount : Long,
//        _date: String
//    ):super(_id, _imageUrl, _name, _watchCount, _date, 0){}
//}
class VideoPageTail : VideoPageItem

data class Pay(
    val commodity: String,
    val payDate: String,
    val price: Long,
    val point: String,
    val status: String,
)

interface UserScheduleDailyItem {}
class UserScheduleDaily(
    val courseId: Int,
    val scheduleId: Int,
    val lessonStartTime: String,
    val lessonEndTime: String,
    val lessonImageUrl: String?,
    val lessonName: String,
    val coachImageUrl: String,
    val coachName: String,
    val video_enable: Boolean,
) : UserScheduleDailyItem

class UserScheduleDailyTail() : UserScheduleDailyItem

class ThirdBing(
    val iconImg: Int,
    val thirdName: String,
    var thirdBinding: Boolean,
)