package com.gcreate.jiajia.data

import android.content.Context
import androidx.databinding.ObservableField
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gcreate.jiajia.R
import com.gcreate.jiajia.api.bundle.response.ResponseBundleFilter
import com.gcreate.jiajia.api.course.CourseApiClient
import com.gcreate.jiajia.api.course.response.ResponseCourseFilter
import com.gcreate.jiajia.api.dataobj.CourseDetail
import com.gcreate.jiajia.api.handle.ApiController
import com.gcreate.jiajia.api.payment.response.ResponseProcessToCheckout
import com.gcreate.jiajia.api.user.UserApiClient
import com.gcreate.jiajia.api.user.response.*
import com.gcreate.jiajia.views.login.LoginAllinOneFragmentType

class MainViewModelFactory(
    private val dataStore: DataStore<Preferences>,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(dataStore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class MainViewModel(
    private val dataStore: DataStore<Preferences>,
) : ViewModel() {

    val appState = AppState(dataStore)
    var loginAllinOneFragmentType = LoginAllinOneFragmentType.UserSelect
    var firebaseToken: String = ""
    var userInfo: ResponseUserInfo? = null
    var userMore: More? = null
    var showBonusChannel: Boolean = false
    var receiveNotificationTitle: String = ""
    var isUseAffiliateMarketingReferral: Boolean = false
    var affSetting: ResponseAffSetting? = null

    var me = ObservableField<User>(
        User(
            "",
            "",
            "",
            false,
            Subscription.Type.UnRegistered,
            "",
            "",
            "",
            "",
            0.0,
            "",
            "",
            "",
            "",
            MyHobby(
                "",
                1,
                "1990-01-01",
                175,
                75,
                1,
                1,
                1,
            )
        )
    )

    var isUserHaveSubcription = false

    val unsubscribeReasonList = mutableListOf<String>(
        "我找到了更好的應用程式",
        "實用相關問題",
        "技術問題",
        "我不常使用這項服務",
        "其他",
        "拒絕回答"
    )

    var progressInfo = ObservableField<ProgreeeInfo>(
        ProgreeeInfo(
            "ERROR",
            ProgreeeInfo.Type.PROGRESS
        )
    )

    var otpCheckFailCount: Int = 0

    var notificationSetting = ObservableField<NotificationSetting>(
        NotificationSetting(news = false, coach = false, type = false, schedule = false)
    )


    var receipt = Receipt(
        Receipt.Default.Nono,
        Receipt.Er(
            "",
            false
        ),
        Receipt.Company(
            "", "", ""
        ),
        Receipt.Donate(
            -1,
            ""
        )
    )

    var paymentInfo = PaymentInfo(
        Subscription.Type.Month,
        PaymentInfo.Type.Openpoint
    )

    var subscriptionsPlan: ResponseProcessToCheckout.SubscriptionPlan? = null
    var firstPay: ResponseProcessToCheckout.FirstPay ?= null
    var plusChannel: ResponseProcessToCheckout.PlusChannel? = null
    var plusChannelOrderList: MutableList<ResponseProcessToCheckout.PlusChannel>? = null
    var totalAmount: Int = 0
    var discountPrice: String? = ""


    var courseFilterIntensity = listOf<ItemName>()
    var courseFilterCoach = listOf<ItemName>()
    var courseFilterEquipment = listOf<ItemName>()

    var bundleFilter: ResponseBundleFilter? = null

    var listFollow: ResponseListFollow? = null
    var categoryFollowList: MutableList<Int> = mutableListOf()

    var courseDetailTmp: CourseDetail? = CourseDetail("", listOf(), "", "", "", "", "", "")

    var collectCourses: List<ResponseUserInfo.Collect>? = null

    var invoiceResult: ResponseInvoiceResult? = null
    var socialSetting: SocialSetting ? = null

    fun setDiscountCode(code: String) {
        var user = me.get()
        user?.discountCode = code
        me.set(user)
    }


    fun splashBannerData(context: Context): MutableList<SplashBanner> {
        val data = mutableListOf<SplashBanner>()

        val imgAry = context.resources.getStringArray(R.array.splash_img_ary)
        val titleAry = context.resources.getStringArray(R.array.splash_title_ary)
        val contentAry = context.resources.getStringArray(R.array.splash_content_ary)

        for (index in imgAry.indices) {
            data.add(SplashBanner("file:///android_asset/" + imgAry[index], titleAry[index], contentAry[index]))
        }

        return data
    }

    fun loadCourseFilterSetting(context: Context) {
        CourseApiClient.courseFilter(object : ApiController<ResponseCourseFilter>(context, true) {
            override fun onSuccess(response: ResponseCourseFilter) {
                courseFilterIntensity = response.level
                courseFilterCoach = response.instructor
                courseFilterEquipment = response.accessories
            }
        })
    }

    fun loadFollowState(context: Context) {
        UserApiClient.listFollow(appState.accessToken.get()!!, object : ApiController<ResponseListFollow>(context, false) {
            override fun onSuccess(response: ResponseListFollow) {
                listFollow = response
                categoryFollowList = mutableListOf()
                for (cat in listFollow!!.category) {
                    if (cat.followed) {
                        categoryFollowList.add(cat.id)
                    }
                }
            }
        })
    }

    fun isFollowCategory(id: Int): Boolean {
        return categoryFollowList.contains(id)
    }

    fun isLogined(): Boolean {
        return appState.accessToken.get()?.length!! > 0
    }
}