package com.gcreate.jiajia.data

data class User(
    var usrImageUrl: String,
    var name: String,
    val membershipNo: String,
    val subscription: Boolean,
    val subscriptionType: Subscription.Type,
    val subscriptionExpire: String,
    var nickname: String,
    var phone: String,
    var discountCode: String,
    var point: Double,
    var county:String?,
    var district:String?,
    var address: String?,
    var pin_code:String?,
    var myHobby: MyHobby,
)

/**
 * @param gender 1:男、2:女
 * @param fitness_level 健身經驗(int|1:低度、2:中度、3:強度
 * @param fitness_target 訓練目標(int|1:肌力訓練、2:降低體脂、3:修飾身材、4:維持健康)
 * @param fitness_howlong 達成目標(int|1:較快、2:適中、3:較慢)
 * */
data class MyHobby(
    val mobile: String,
    val gender: Int,
    val birthday: String,
    val height: Int,
    val weight: Int,
    var fitness_level: Int,
    var fitness_target: Int,
    var fitness_howlong: Int,
)