package com.gcreate.jiajia.data

import androidx.databinding.ObservableField
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking


class AppState(
    private val dataStore: DataStore<Preferences>,
) {

    val onBoard = ObservableField<Boolean>(false)
    val eulaArgeed = ObservableField<Boolean>(false)
    val accessToken = ObservableField<String>("")
    val refreshToken = ObservableField<String>("")
    val haveSubscription = ObservableField<Boolean>(false)
    var payMethod = ObservableField<String>("cathybk")
    var donateMechanism = ObservableField<String>("")

    private var ON_BOARD = intPreferencesKey("on_board")
    private var EULA_ARGEED = intPreferencesKey("eula_argeed")
    private var ACCESS_TOKEN = stringPreferencesKey("access_token")
    private var REFRESH_TOKEN = stringPreferencesKey("refresh_token")
    private var HaveSubscription = intPreferencesKey("have_Subscription")
    private var PAYMETHOD = stringPreferencesKey("pay_method")
    private var DonateMechanism = stringPreferencesKey("donateMechanism")

    private val onBoardFlow: Flow<Int> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[ON_BOARD] ?: 0
        }

    private val eulaArgeedFlow: Flow<Int> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[EULA_ARGEED] ?: 0
        }

    private val accessTokenFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[ACCESS_TOKEN] ?: ""
        } as Flow<String>

    private val refreshTokenFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[REFRESH_TOKEN] ?: ""
        } as Flow<String>

    private val haveSubscriptionFlow: Flow<Int> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[HaveSubscription] ?: 0
        }

    private val payMethodFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[PAYMETHOD] ?: ""
        } as Flow<String>

    private val donateMechanismFlow: Flow<String> = dataStore.data
        .map { preferences ->
            // No type safety.
            preferences[DonateMechanism] ?: ""
        } as Flow<String>

    init {
        runBlocking {
            var value = onBoardFlow.first()
            if (value > 0) {
                onBoard.set(true)
            } else {
                onBoard.set(false)
            }

            value = eulaArgeedFlow.first()
            if (value > 0) {
                eulaArgeed.set(true)
            } else {
                eulaArgeed.set(false)
            }

            value = haveSubscriptionFlow.first()
            if (value > 0) {
                haveSubscription.set(true)
            } else {
                haveSubscription.set(false)
            }


            accessToken.set(accessTokenFlow.first())
            refreshToken.set(refreshTokenFlow.first())
            payMethod.set(payMethodFlow.first())
            donateMechanism.set(donateMechanismFlow.first())
        }
    }

    suspend fun setOnBoard() {
        onBoard.set(true)
        dataStore?.edit { settings ->
            settings[ON_BOARD] = 1
        }
    }

    suspend fun setEulaAgreed(v: Boolean) {
        eulaArgeed.set(v)
        dataStore?.edit { settings ->
            if (v) {
                settings[EULA_ARGEED] = 1
            } else {
                settings[EULA_ARGEED] = 0
            }
        }
    }

    suspend fun setAccessToken(v: String) {
        accessToken.set(v)
        dataStore?.edit { settings ->
            settings[ACCESS_TOKEN] = v
        }
    }

    suspend fun setRefreshToken(v: String) {
        refreshToken.set(v)
        dataStore?.edit { settings ->
            settings[REFRESH_TOKEN] = v
        }
    }

    suspend fun setHaveSubscription(v: Boolean) {
        haveSubscription.set(v)
        dataStore.edit { settings ->
            if (v) {
                settings[HaveSubscription] = 1
            } else {
                settings[HaveSubscription] = 0
            }
        }
    }

    suspend fun setPayMethod(v: String) {
        payMethod.set(v)
        dataStore?.edit { settings ->
            settings[PAYMETHOD] = v
        }
    }

    suspend fun setDonateMechanism(v: String) {
        donateMechanism.set(v)
        dataStore.edit { settings ->
            settings[DonateMechanism] = v
        }
    }

}