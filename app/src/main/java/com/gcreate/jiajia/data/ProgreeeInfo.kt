package com.gcreate.jiajia.data

import android.os.Build
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.databinding.BindingAdapter
import com.gcreate.jiajia.R

data class ProgreeeInfo(
    var message : String ,
    var type : Type
){
    enum class Type(){
        OK,
        ERROR,
        PROGRESS
    }

    fun progressVisible(): Boolean{
        return type == Type.PROGRESS
    }
}

@RequiresApi(Build.VERSION_CODES.M)
@BindingAdapter("progressImgType")
fun bindProgressImage(imageView: ImageView, type: ProgreeeInfo.Type) {
    if(type == ProgreeeInfo.Type.PROGRESS){
        imageView.visibility = View.GONE
        return
    }

    when(type){
        ProgreeeInfo.Type.OK -> {
            val color = imageView.context.getColor(R.color.progress_ok)
            imageView.setImageResource(R.drawable.outline_check_24)
            imageView.setColorFilter(color); // White Tint
            imageView.visibility = View.VISIBLE
        }
        ProgreeeInfo.Type.ERROR -> {
            val color = imageView.context.getColor(R.color.progress_error)
            imageView.setImageResource(R.drawable.baseline_error_outline_24)
            imageView.setColorFilter(color); // White Tint
            imageView.visibility = View.VISIBLE
        }
        else -> {

        }
    }

}

