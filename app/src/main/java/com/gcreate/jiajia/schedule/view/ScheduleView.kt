package com.gcreate.jiajia.schedule.view

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.R
import com.gcreate.jiajia.schedule.ScheduleItemRecyclerViewAdapter
import com.gcreate.jiajia.schedule.data.*


class ScheduleView : RecyclerView{

    data class DayViewColor(
        var dayTextColor: Int,
        var markOfSingleDayTextColor: Int,
        var markOfMultiDayTextColor: Int,

        var checkedDayColor: Int,
        var markOfSingleDayColor: Int,
        var markOfMultiDayColor: Int,
    )

    constructor(context: Context) : super(context) {
        init(null)
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private var dayViewColor = DayViewColor(
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFF00FF00.toInt(),
        0x8000FF00.toInt()
    )

    private var scheduleList = ScheduleViewData()
    private var isLoading = false

    private var onClickDay : ((year: Int, month: Int, day: Int)-> Unit)? = null

    private var pickingTime = false
    private var canScorll = true

    private fun init(attrs: AttributeSet?){

        initAttr(attrs)

        val layoutMngr = object : LinearLayoutManager(context){
            override fun canScrollVertically(): Boolean {
                return canScorll
            }
        }
        layoutMngr.orientation = LinearLayoutManager.VERTICAL

        layoutManager = layoutMngr
        adapter = ScheduleItemRecyclerViewAdapter(dayViewColor){ year: Int, month: Int, day: Int ->
            onClickDay?.invoke(year, month, day)
        }

        (adapter as ScheduleItemRecyclerViewAdapter).submitList(scheduleList)


        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == scheduleList!!.size - 1) {
                        //bottom of list!
                        isLoading = true
                        loadMoreBottom()
                    }

                    if (linearLayoutManager != null && linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                        //top of list!
                        isLoading = true
                        loadMoreTop()
                    }
                }
            }
        })
    }

    private fun initAttr(attrs: AttributeSet?){
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SchedulView)

        dayViewColor.dayTextColor = typedArray.getColor(R.styleable.SchedulView_dayTextColor, 0xFFFFFFFF.toInt())
        dayViewColor.markOfSingleDayTextColor = typedArray.getColor(R.styleable.SchedulView_markOfSingleDayTextColor, 0xFF00FF00.toInt())
        dayViewColor.markOfMultiDayTextColor = typedArray.getColor(R.styleable.SchedulView_markOfMultiDayTextColor, 0xFFFFFFFF.toInt())

        dayViewColor.checkedDayColor = typedArray.getColor(R.styleable.SchedulView_checkedDayColor, 0xFF00FF00.toInt())
        dayViewColor.markOfSingleDayColor = typedArray.getColor(R.styleable.SchedulView_markOfSingleDayColor, 0xFF00FF00.toInt())
        dayViewColor.markOfMultiDayColor = typedArray.getColor(R.styleable.SchedulView_markOfMultiDayColor, 0x8000FF00.toInt())

        typedArray.recycle()
    }

    fun loadMoreBottom(){ // 增加六個月
        scheduleList.loadMoreBottom()
        adapter?.notifyDataSetChanged()

        isLoading = false
    }

    fun loadMoreTop(){ // 增加六個月

        val linearLayoutManager = layoutManager as LinearLayoutManager?
        var position =  linearLayoutManager?.findLastCompletelyVisibleItemPosition()

        var sizeCount = scheduleList!!.loadMoreTop()

        adapter?.notifyDataSetChanged()

        scrollToPosition(sizeCount + position!!)

        val smoothScroller = ScrollToTopSmoothScroller(context)
        smoothScroller.targetPosition = sizeCount
        layoutManager!!.startSmoothScroll(smoothScroller)

        isLoading = false
    }

    fun setOnClickDayListener(listener : (year :Int, month:Int, day: Int) -> Unit ){
        onClickDay = listener
    }

    fun isChecked(year :Int, month:Int, day: Int) : Boolean{
        return scheduleList.getDay(year, month, day).checked
    }

    fun setCheck(year :Int, month:Int, day: Int){
        scheduleList.getDay(year, month, day).checked = true
        adapter?.notifyDataSetChanged()
    }

    fun setUncheck(year :Int, month:Int, day: Int){
        scheduleList.getDay(year, month, day).checked = false
        adapter?.notifyDataSetChanged()
    }

    fun pickTime(year :Int, month:Int, day: Int, gotTime: (time: Time?) -> Unit){
        if(pickingTime){
            return
        }
        pickingTime = true

        val smoothScroller = ScrollToTopSmoothScroller(context)
        val index = scheduleList.getIndex(Month(year, month), day) - 1

        setCheck(year, month, day)
        smoothScroller.targetPosition = index

        smoothScroller.setOnStopListener {
            canScorll = false
        }
        layoutManager!!.startSmoothScroll(smoothScroller)

        // scroll to month of year
        val timepicker = TimePicker(12, 30)
        scheduleList.add(
            scheduleList.getIndex(Month(year, month), day) + 1,
            timepicker
        )
        (adapter!! as ScheduleItemRecyclerViewAdapter).setTimePickerListener {
            setUncheck(year, month, day)
            scheduleList.remove(timepicker)
            adapter?.notifyDataSetChanged()
            gotTime(it)

            canScorll = true
            pickingTime = false
        }
        adapter?.notifyDataSetChanged()

    }

    fun addBook(day : Day){
        scheduleList.addBook(day)
    }

    fun addBook(list: List<Day>){
        scheduleList.addBook(list)
    }

}

class ScrollToTopSmoothScroller : LinearSmoothScroller {
    constructor(context: Context) : super(context){}

    private var onStopListener :(() -> Unit)? = null

    override fun getHorizontalSnapPreference(): Int {
        return SNAP_TO_START
    }

    override fun getVerticalSnapPreference(): Int {
        return SNAP_TO_START
    }

    fun setOnStopListener(listener : () -> Unit){
        onStopListener = listener
    }
    override fun onStop() {
        super.onStop()
        onStopListener?.invoke()
    }
}