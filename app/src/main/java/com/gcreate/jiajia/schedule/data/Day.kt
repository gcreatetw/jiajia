package com.gcreate.jiajia.schedule.data

data class Day(
    val year: Int,
    val month: Int,
    val day: Int
){
    var checked = false

    fun compare(other:Day): Int{
        if(year > other.year){
            return 1
        }
        else if(year < other.year) {
            return -1
        }
        else if(month > other.month){
            return 1
        }
        else if(month < other.month) {
            return -1
        }
        else if(day > other.day){
            return 1
        }
        else if(day < other.day) {
            return -1
        }
        else{
            return 0
        }
    }

    fun nextDay(): Day{
        val thisMonth = Month(year, month)
        var days = CalendarUtil.getDaysOfMonth(thisMonth)
        if(day < days){
            return Day(year, month, day + 1)
        }

        val m = thisMonth.genNextMonth()
        return Day(m.year, m.month, 1)
    }
}
