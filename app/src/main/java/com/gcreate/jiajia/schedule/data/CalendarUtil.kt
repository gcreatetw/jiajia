package com.gcreate.jiajia.schedule.data

import java.util.*

object CalendarUtil {

    fun getDaysOfMonth(month: Month) : Int{
        val calendar = Calendar.getInstance()
        calendar.set(month.year, month.month, 1)
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    }

    fun getDayOfWeek(month: Month, date: Int): Int{
        val calendar = Calendar.getInstance()
        calendar.set(month.year, month.month, date)
        return calendar.get(Calendar.DAY_OF_WEEK)
    }

    fun getWeeks(month: Month): Int{
        val num = getDayOfWeek(month, 1) - 1 + getDaysOfMonth(month)
        val remainder  = num % 7
        if(remainder > 0){
            return num / 7 + 1
        }
        else{
            return num / 7
        }
    }



}