package com.gcreate.jiajia.schedule.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.gcreate.jiajia.schedule.data.Day
import com.gcreate.jiajia.schedule.data.DayMark


class DayView : View {

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {}

    var bgColor = 0x0L

    private var dayViewColor = ScheduleView.DayViewColor(
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFF00FF00.toInt(),
        0x8000FF00.toInt()
    )

    var text: String = "10"
    var textSizeToHeightRatio: Float = .35f // % of height
    private var _textColor = Color.WHITE

    private var day: Day? = null
    private var mark: DayMark? = null

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        drawMark(canvas)

        if (text.isNotEmpty()) {
            val xc = (width / 2).toFloat()
            val yc = (height / 2).toFloat()
            drawText(canvas, text, xc, yc)
        }

    }

    private fun drawMark(canvas: Canvas?) {
        var paint = Paint()
        if (day != null && day!!.checked) {
            paint.color = dayViewColor.checkedDayColor
            paint.style = Paint.Style.FILL
            canvas?.drawCircle(
                (width / 2).toFloat(),
                (height / 2).toFloat(),
                (height / 2).toFloat(), paint)
            return
        }
        mark?.apply {
            when (markOfMultiDay) {
                DayMark.MarkOfMultiDay.Start -> {
                    paint.color = dayViewColor.markOfMultiDayColor
                    paint.style = Paint.Style.FILL

                    canvas?.drawArc(
                        (width / 2 - height / 2).toFloat(), 0f, (width / 2 + height / 2).toFloat(), height.toFloat(),
                        90f, 180f,
                        true, paint)

                    canvas?.drawRect((width / 2).toFloat(), 0f, width.toFloat(), height.toFloat(), paint)
                    drawDay(true, markOfOneDay, canvas, paint)
                }
                DayMark.MarkOfMultiDay.During -> {
                    paint.color = dayViewColor.markOfMultiDayColor
                    paint.style = Paint.Style.FILL
                    canvas?.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
                    drawDay(true, markOfOneDay, canvas, paint)
                }
                DayMark.MarkOfMultiDay.End -> {
                    paint.color = dayViewColor.markOfMultiDayColor
                    paint.style = Paint.Style.FILL

                    canvas?.drawArc(
                        (width / 2 - height / 2).toFloat(), 0f, (width / 2 + height / 2).toFloat(), height.toFloat(),
                        -90f, 180f,
                        true, paint)

                    canvas?.drawRect(0f, 0f, (width / 2).toFloat(), height.toFloat(), paint)
                    drawDay(true, markOfOneDay, canvas, paint)
                }
                else -> {
                    drawDay(false, markOfOneDay, canvas, paint)
                }
            }
        }
    }

    private fun drawDay(isBundle: Boolean, markOfOneDay: Boolean, canvas: Canvas?, paint: Paint) {
        paint.color = dayViewColor.markOfSingleDayColor
        paint.strokeWidth = 2f
        if (isBundle) {
            if (markOfOneDay) {
                paint.style = Paint.Style.STROKE
                canvas?.drawCircle(
                    (width / 2).toFloat(),
                    (height / 2).toFloat(),
                    (height / 2).toFloat(), paint)
            }
        } else {
            if (markOfOneDay) {
                paint.style = Paint.Style.FILL_AND_STROKE
                canvas?.drawCircle(
                    (width / 2).toFloat(),
                    (height / 2).toFloat(),
                    (height / 2).toFloat(), paint)
            }
        }
    }

    var textColor: Int
        get() {
//            if (day != null) {
//                if (day!!.checked) {
//                    return dayViewColor.dayTextColor
//                }
//                mark?.apply {
//                    if (markOfMultiDay != DayMark.MarkOfMultiDay.None) {
//                        return dayViewColor.markOfMultiDayTextColor
//                    } else if (markOfOneDay) {
//                        return dayViewColor.markOfSingleDayTextColor
//                    } else {
//                    }
//                }
//
//                return dayViewColor.dayTextColor
//            } else {
//                return _textColor
//            }
            return _textColor
        }
        set(value) {
            _textColor = value
        }

    private fun drawText(canvas: Canvas?, text: String, x: Float, y: Float) {
        var paint = Paint()
        paint.color = textColor
        paint.textSize = textSizeToHeightRatio * height
        paint.textAlign = Paint.Align.LEFT

        val textBounds = Rect()
        paint.getTextBounds(text, 0, text.length, textBounds)

        var xc = x - textBounds.centerX()
        var yc = y - textBounds.centerY()
        canvas?.drawText(text, xc, yc, paint)
    }

    fun setMark(data: DayMark?) {
        mark = data
    }

    fun setDay(data: Day?) {
        day = data
    }

    fun setDayViewColor(colors: ScheduleView.DayViewColor) {
        dayViewColor = colors
    }
}