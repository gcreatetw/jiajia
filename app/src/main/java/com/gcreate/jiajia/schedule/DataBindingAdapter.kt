package com.gcreate.jiajia.schedule

import androidx.databinding.BindingAdapter
import com.gcreate.jiajia.schedule.data.Week
import com.gcreate.jiajia.schedule.view.WeekView

@BindingAdapter("week")
fun bindImage(weekView: WeekView, week: Week) {
    weekView.setData(week)
}