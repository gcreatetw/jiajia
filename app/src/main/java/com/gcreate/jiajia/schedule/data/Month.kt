package com.gcreate.jiajia.schedule.data

data class Month (
    var year: Int,
    var month: Int
    ) : ScheduleItem{

    companion object{
        val MinMonth = 0
        val MaxMonth = 11

        fun genKey(year: Int, month: Int): String{
            return "$year-$month"
        }
    }

    val dayList = mutableListOf<Day>()

    fun key(): String{
        return genKey(year, month)
    }

    fun genPreviousMonth(): Month{
        var m = month - 1
        if(m < MinMonth){
            m = MaxMonth
            return Month(year - 1, m)
        }
        else{
            return Month(year, m)
        }
    }

    fun genNextMonth(): Month{
        var m = month + 1
        if(m > MaxMonth){
            m = MinMonth
            return Month(year + 1, m)
        }
        else{
            return Month(year, m)
        }
    }

    fun compare(other:Month): Int{
        if(year > other.year){
            return 1
        }
        else if(year < other.year) {
            return -1
        }
        else if(month > other.month){
            return 1
        }
        else if(month < other.month) {
            return -1
        }
        else{
            return 0
        }
    }
}