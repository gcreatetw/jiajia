package com.gcreate.jiajia.schedule.data

import java.util.*

class ScheduleViewData : LinkedList<ScheduleItem>() {

    val scheduleBook = ScheduleBook()

    private var monthMap = mutableMapOf<String, Month>() // key = "$year-$month"

    init{
        genInitData()
    }

    fun getDay(year: Int, month: Int, day: Int): Day {
        val month = monthMap.get(Month.genKey(year, month))
        return month!!.dayList[day-1]
    }

    fun genInitData(){
        clear()
        monthMap.clear()

        val today = Calendar.getInstance()
        var month = Month(today.get(Calendar.YEAR), today.get(Calendar.MONTH))

        val list = genWeekList(month)
        addAll(list)

        for(index in 0..4){
            month = month.genNextMonth()
            val list = genWeekList(month)
            addAll(list)
        }
    }

    fun loadMoreBottom() { // 增加六個月

        val item = this[size - 1]

        var month: Month? = null
        when (item) {
            is Month -> {
                month = item
            }
            is Week -> {
                month = item.month
            }
        }

        for (index in 1..6) {
            month = month!!.genNextMonth()
            val list = genWeekList(month!!)
            addAll(list)
        }

    }

    fun loadMoreTop():Int { // 增加六個月

        val item = this[0]

        var month: Month? = null
        when (item) {
            is Month -> {
                month = item
            }
            is Week -> {
                month = item.month
            }
        }
        var sizeCount = 0
        for (index in 1..6) {
            month = month!!.genPreviousMonth()
            val list = genWeekList(month!!)
            sizeCount += list.size
            for (index in (list.size - 1) downTo 0) {
                addFirst(list[index])
            }
        }

        return sizeCount
    }

    private fun genWeekList(month: Month):MutableList<ScheduleItem>{
        var list = mutableListOf<ScheduleItem>()

        list.add(month)

        val weeks = CalendarUtil.getWeeks(month)
        var week = Week.genFirstWeek(scheduleBook,month)
        month.dayList.addAll(week.dayMarkList)
        list.add(week)
        for(index in 0..(weeks - 2)){
            week = week.genNextWeek()
            month.dayList.addAll(week.dayMarkList)
            list.add(week)
        }

        monthMap.put(month.key(), month)

        return list
    }

    fun getIndex(month: Month): Int {
        var index = indexOf(month)
        if(index != -1){
            return index
        }
        var firstMonth = this[0] as Month
        while( firstMonth.compare(month) == 1 ){
            loadMoreTop()
            firstMonth = this[0] as Month
        }
        var lastMonth = (this[this.size -1] as Week).month
        while( lastMonth.compare(month) == -1 ){
            loadMoreBottom()
            lastMonth = (this[this.size -1] as Week).month
        }

        index = indexOf(month)
        if(index == 0){
            loadMoreTop()
        }

        return indexOf(month)
    }

    fun getIndex(month: Month, day: Int): Int {
        var index = getIndex(month)
        while(true){
            index += 1
            val item = this[index]
            if(item is Week){
                if( day >= item.firstDayOfMonth && day <= item.lastDayOfMonth){
                    return index
                }
            }
            else{
                break
            }
        }

        return -1
    }

    fun addBook(day : Day){
        scheduleBook.addSingleDay(day)
    }

    fun addBook(list: List<Day>){
        scheduleBook.addMultiDay(list)
    }
}