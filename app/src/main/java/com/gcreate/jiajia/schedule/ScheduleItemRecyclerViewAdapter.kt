package com.gcreate.jiajia.schedule

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.jiajia.databinding.CardLabelMonthBinding
import com.gcreate.jiajia.databinding.CardTimePickerBinding
import com.gcreate.jiajia.databinding.CardWeekBinding
import com.gcreate.jiajia.schedule.data.*
import com.gcreate.jiajia.schedule.view.ScheduleView


class ScheduleItemRecyclerViewAdapter(
    val dayViewColor: ScheduleView.DayViewColor,
    val onClickDay : (year: Int, month: Int, day: Int)-> Unit
): ListAdapter<ScheduleItem, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<ScheduleItem>() {

        override fun areItemsTheSame(oldItem: ScheduleItem, newItem: ScheduleItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ScheduleItem, newItem: ScheduleItem): Boolean {
            return false
        }
    }

    private var timePickerListener : ((time: Time?) -> Unit)? = null

    fun setTimePickerListener(listener : (time: Time?) -> Unit){
        timePickerListener = listener
    }

    inner class ViewHolderLabel(private val binding: CardLabelMonthBinding) : RecyclerView.ViewHolder(binding.root){
        fun setResult(dataItem: Month) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderWeek(private val binding: CardWeekBinding) : RecyclerView.ViewHolder(binding.root){
        init{
            binding.weekView.setDayViewColor(dayViewColor)
            binding.weekView.setOnClickDayViewListener(onClickDay)
        }
        fun setResult(dataItem: Week) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    inner class ViewHolderTimePicker(private val binding: CardTimePickerBinding) : RecyclerView.ViewHolder(binding.root){
        init{
            binding.tvOk.setOnClickListener {
                timePickerListener?.invoke(Time(
                    binding.numberPickerHour.value,
                    binding.numberPickerMinute.value))
            }

            binding.tvCancel.setOnClickListener {
                timePickerListener?.invoke(null)
            }
        }
        fun setResult(dataItem: TimePicker) {
            binding.data = dataItem
            binding.executePendingBindings()
        }
    }

    override fun getItemViewType(position: Int): Int {
        when(getItem(position)){
            is Month -> return 0
            is Week -> return 1
            is TimePicker -> return 2
        }
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType) {
            0 -> {
                val binding = CardLabelMonthBinding.inflate(layoutInflater, parent, false)
                return ViewHolderLabel(binding)
            }
            1 -> {
                val binding = CardWeekBinding.inflate(layoutInflater, parent, false)
                return ViewHolderWeek(binding)
            }
            2 -> {
                val binding = CardTimePickerBinding.inflate(layoutInflater, parent, false)
                return ViewHolderTimePicker(binding)
            }
        }
        val binding = CardLabelMonthBinding.inflate(layoutInflater, parent, false)
        return ViewHolderLabel(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is ViewHolderLabel -> {
                holder.setResult(getItem(position) as Month)
            }
            is ViewHolderWeek -> {
                holder.setResult(getItem(position) as Week)
            }
            is ViewHolderTimePicker -> {
                holder.setResult(getItem(position) as TimePicker)
            }
        }
    }
}