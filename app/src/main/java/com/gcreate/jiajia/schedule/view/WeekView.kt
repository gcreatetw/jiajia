package com.gcreate.jiajia.schedule.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.ViewGroup
import com.gcreate.jiajia.R
import com.gcreate.jiajia.schedule.data.Month
import com.gcreate.jiajia.schedule.data.Week

class WeekView : ViewGroup {

    constructor(context: Context) : super(context) {
        init(null)
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private var dayViewColor = ScheduleView.DayViewColor(
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFFFFFFFF.toInt(),
        0xFF00FF00.toInt(),
        0xFF00FF00.toInt(),
        0x8000FF00.toInt()
    )

    private var dayViewClicklistener : ((year :Int, month:Int, day: Int) -> Unit)? = null

    private var dayViewList = mutableListOf<DayView>()

    private var aspectRatio = 24f/45f
    private var textSizeToHeightRatio : Float = .45f    // % of height
    private var sundayTextColor : Int = Color.WHITE

    private var texts: String? = null

    private var week = Week(
        null,
        Month(2021, 10), 3)


    private fun init(attrs: AttributeSet?) {

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SchedulView)

        texts = typedArray.getString(R.styleable.SchedulView_texts) // "週日,週一,週二,週三,週四,週五,週六"
        aspectRatio = typedArray.getFraction(R.styleable.SchedulView_aspectRatio,1,1, 24f/45f)
        textSizeToHeightRatio = typedArray.getFraction(R.styleable.SchedulView_textSizeToHeightRatio, 1,1, .45f)
        sundayTextColor = typedArray.getColor(R.styleable.SchedulView_sundayTextColor, Color.WHITE)

        val year = typedArray.getInt(R.styleable.SchedulView_year, 2021)
        val month = typedArray.getInt(R.styleable.SchedulView_month, 10)    // 11月
        val firstDayOfMonth = typedArray.getInt(R.styleable.SchedulView_dayOfMonth, 3)
        week = Week(null, Month(year, month), firstDayOfMonth)

        typedArray.recycle()

        genDayView()

        var textList: List<String>


        if(texts != null) {
            textList = texts!!.split(",").map { it.trim() }
            setDayViewData(textList)
        }
        else {
            setDayViewData()
        }
    }

    fun setData(w : Week){
        week = w
        setDayViewData()
    }

    fun genDayView(){
        for(index in 1..7 ){
            val dayView = DayView(context)
            dayView.setDayViewColor(dayViewColor)
            dayView.text = "$index"
            dayView.textSizeToHeightRatio = textSizeToHeightRatio
            if (index == 1){ // sunday
                dayView.textColor = sundayTextColor
            }
            dayViewList.add(dayView)
            this.addView(dayView)
        }
    }

    fun setDayViewData(textList : List<String>){
        for(index in 0..6 ){
            val dayView = dayViewList[index]
            if(textList.size > index) {
                dayView.text = textList[index]
            }
            else{
                dayView.text = ""
            }
            if (index == 0) { // sunday
                dayView.textColor = sundayTextColor
            }
        }
    }

    fun setDayViewData(){
        for(dw in 1..7 ){
            val dayView = dayViewList[dw - 1]
            if(dw >= week.firstDayOfWeek && dw <= week.lastDayOfWeek){
                val dm = week.firstDayOfMonth + (dw - week.firstDayOfWeek)
                dayView.text = "$dm"
                val day = week.getDay(dm)
                dayView.setDay(day)
                dayView.setMark(week.scheduleBook?.getMark(day))
                dayView.tag = dm
                dayView.setOnClickListener {
                    dayViewClicklistener?.invoke(week.month.year, week.month.month , it.tag as Int)
                }
            }
            else{
                dayView.text = ""
                dayView.setOnClickListener(null)
                dayView.setDay(null)
                dayView.setMark(null)
            }
        }
    }

    fun setDayViewColor(colors: ScheduleView.DayViewColor){
        dayViewColor = colors
        for(day in dayViewList){
            day.setDayViewColor(dayViewColor)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val specModeW = MeasureSpec.getMode(widthMeasureSpec)
        val specSizeW = MeasureSpec.getSize(widthMeasureSpec)

        setMeasuredDimension(
            getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
            (specSizeW * aspectRatio).toInt() / 7)
    }

    override fun onLayout(change: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val width = right - left
        val w = width.toFloat() / dayViewList.size
        val h = w.toFloat() * aspectRatio
        var l = 0f

        for(index in 0..(dayViewList.size - 1 )){
            val dayView = dayViewList.get(index)
            dayView.layout(l.toInt() , 0, (l + w).toInt(), h.toInt())

            l += w
        }
    }

    fun setOnClickDayViewListener(listener : (year :Int, month:Int, day: Int) -> Unit ){
        dayViewClicklistener = listener
    }

}