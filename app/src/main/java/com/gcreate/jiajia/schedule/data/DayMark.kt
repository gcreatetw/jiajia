package com.gcreate.jiajia.schedule.data

data class DayMark (
    var markOfOneDay : Boolean,
    var markOfMultiDay : MarkOfMultiDay
){
    enum class MarkOfMultiDay{
        None,
        Start,
        During,
        End
    }
}