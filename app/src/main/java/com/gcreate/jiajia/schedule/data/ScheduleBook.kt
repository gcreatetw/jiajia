package com.gcreate.jiajia.schedule.data

import android.util.Log

class ScheduleBook{

    private var dayMarkMap = mutableMapOf<Day, DayMark>()
    private var bookList = mutableListOf<List<Day>>()

    fun addSingleDay(day : Day){
        val list = listOf<Day>(day)
        bookList.add(list)

        val mark = getOrCreatMark(day)
        mark.markOfOneDay = true
    }

    fun addMultiDay(list: List<Day>){
        bookList.add(list)

        var minDay = list[0]
        var maxDay = list[0]
        for(index in 0..(list.size - 1)){
            val d = list[index]
            if(minDay.compare(d) == 1){
                minDay = d
            }
            if(maxDay.compare(d) == -1){
                maxDay = d
            }

            val mark = getOrCreatMark(d)
            mark.markOfOneDay = true
        }

        getOrCreatMark(minDay).markOfMultiDay = DayMark.MarkOfMultiDay.Start
        getOrCreatMark(maxDay).markOfMultiDay = DayMark.MarkOfMultiDay.End

        var d = minDay.nextDay()
        while(d.compare(maxDay) == -1){
            getOrCreatMark(d).markOfMultiDay = DayMark.MarkOfMultiDay.During
            d = d.nextDay()
        }
    }

    fun getMark(day: Day): DayMark?{
        return dayMarkMap.get(day)
    }

    private fun getOrCreatMark(day : Day): DayMark{
        var mark = dayMarkMap.get(day)
        if(mark == null){
            mark = DayMark(false, DayMark.MarkOfMultiDay.None)
            dayMarkMap.put(day, mark)
        }

        return mark
    }
}
