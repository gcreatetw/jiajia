package com.gcreate.jiajia.schedule.data


/*
    不會有跨月的週
    month : 0 ~ 11
    day of week : 1 ~ 7
    day of month: 1 ~ 28,29,30,31
 */
class Week : ScheduleItem {
    companion object{
        fun genFirstWeek(scheduleBook : ScheduleBook?,month: Month): Week{
            val firstDM = 1
            val firstDW = CalendarUtil.getDayOfWeek(month, firstDM)
            val lastDM = firstDM + (7 - firstDW)
            val lastDW = 7
            return Week(scheduleBook, month, firstDM, firstDW, lastDM, lastDW)
        }
    }

    constructor(scheduleBook : ScheduleBook?,month: Month, firstDM: Int, firstDW: Int, lastDM:Int, lastDW: Int){
        _scheduleBook = scheduleBook
        _month = month
        _firstDayOfMonth = firstDM
        _firstDayOfWeek = firstDW
        _lastDayOfMonth = lastDM
        _lastDayOfWeek = lastDW
        genDays()
    }

    constructor(scheduleBook : ScheduleBook?, month: Month, day: Int){
        _scheduleBook = scheduleBook
        val dw = CalendarUtil.getDayOfWeek(month, day)
        var firstDM = day - (dw - 1)
        if(firstDM < 1){
            firstDM = 1
        }
        val daysOfThisMonth = CalendarUtil.getDaysOfMonth(month)
        var lastDM = day + (7 - dw)
        if(lastDM > daysOfThisMonth){
            lastDM = daysOfThisMonth
        }
        _firstDayOfMonth = firstDM
        _firstDayOfWeek = dw - (day - firstDM)
        _lastDayOfMonth = lastDM
        _lastDayOfWeek = dw + (lastDM - day)
        genDays()
    }

    private var _scheduleBook : ScheduleBook?

    private var _month: Month = Month(2021, 10)

    private var _firstDayOfMonth: Int = 1
    private var _firstDayOfWeek: Int = 1
    private var _lastDayOfMonth: Int = 7
    private var _lastDayOfWeek: Int = 7

    private var _dayList = mutableListOf<Day>()

    val scheduleBook : ScheduleBook?
        get() = _scheduleBook

    val month : Month
        get() = _month
    val firstDayOfMonth: Int
        get() = _firstDayOfMonth
    val firstDayOfWeek: Int
        get() = _firstDayOfWeek
    val lastDayOfMonth: Int
        get() = _lastDayOfMonth
    val lastDayOfWeek: Int
        get() = _lastDayOfWeek

    val dayMarkList : MutableList<Day>
        get() = _dayList

    fun genDays(){
        _dayList.clear()
        for(index in _firstDayOfMonth.._lastDayOfMonth){
            _dayList.add(Day(month.year, month.month, index))
        }
    }

    fun genPreviousWeek(): Week{
        if(_firstDayOfMonth == 1) { // 本週是第一週
            val pMonth = _month.genPreviousMonth()
            val lastDM = CalendarUtil.getDaysOfMonth(pMonth)
            val lastDW = CalendarUtil.getDayOfWeek(pMonth, lastDM)
            val firstDM = lastDM - (lastDW - 1)
            val firstDW = 1
            return Week(_scheduleBook,pMonth, firstDM, firstDW, lastDM, lastDW)
        }
        val lastDM = _firstDayOfMonth - 1
        val lastDW = 7
        if(lastDM > 7) {// 上週不是第一週
            val firstDM = lastDW - 6
            val firstDW = 1
            return Week(_scheduleBook, _month, firstDM, firstDW, lastDM, lastDW)
        }
        // 上週是第一週
        val firstDM = 1
        val firstDW = lastDW - (lastDM - firstDM)
        return Week(_scheduleBook, _month, firstDM, firstDW, lastDM, lastDW)
    }

    fun genNextWeek(): Week{
        val daysOfThisMonth = CalendarUtil.getDaysOfMonth(_month)

        if(_lastDayOfMonth == daysOfThisMonth){ // 本週是最後一週
            val firstDM = 1
            val firstDW = CalendarUtil.getDayOfWeek(_month.genNextMonth(), firstDM)
            val lastDM = firstDM + (7 - firstDW)
            val lastDW = 7
            return Week(_scheduleBook, _month, firstDM, firstDW, lastDM, lastDW)
        }
        val firstDM = _lastDayOfMonth + 1
        val firstDW = 1
        if(daysOfThisMonth - firstDM >= 7){ // 下週不是最後一週
            val lastDM = firstDM + 6
            val lastDW = 7
            return Week(_scheduleBook, _month, firstDM, firstDW, lastDM, lastDW)
        }
        // 下週是最後一週
        val lastDM = daysOfThisMonth
        val lastDW = daysOfThisMonth - firstDM + firstDW
        return Week(_scheduleBook, _month, firstDM, firstDW, lastDM, lastDW)
    }

    fun getDay(day: Int): Day{
        return _dayList[day - firstDayOfMonth]
    }

    fun isChecked(day: Int): Boolean{
        return _dayList[day - firstDayOfMonth].checked
    }

    fun setCheck(day: Int){
        _dayList[day - firstDayOfMonth].checked = true
    }
    fun setUncheck(day: Int){
        _dayList[day - firstDayOfMonth].checked = false
    }
}