package com.gcreate.jiajia.listener

import android.view.View

import java.util.Calendar

abstract class NoDoubleClickListener : View.OnClickListener {
    private var MIN_CLICK_DELAY_TIME = 350

    private val isAvailableToClick: Boolean
        get() {
            val currentTime = Calendar.getInstance().timeInMillis
            synchronized(this) {
                if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
                    println(currentTime.toString() + " , " + lastClickTime)
                    lastClickTime = currentTime
                    return true
                }
                return false
            }
        }

    constructor() {

    }

    constructor(limit: Int) {
        MIN_CLICK_DELAY_TIME = limit
    }

    override fun onClick(v: View) {
        if (isAvailableToClick) {
            onNoDoubleClick(v)
        }
    }

    protected abstract fun onNoDoubleClick(v: View)

    companion object {
        private var lastClickTime = Calendar.getInstance().timeInMillis
    }
}
