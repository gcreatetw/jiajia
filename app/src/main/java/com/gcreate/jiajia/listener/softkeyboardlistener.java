package com.gcreate.jiajia.listener;


import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

public class softkeyboardlistener {
    private View rootView;  //activity的根檢視
    int rootViewVisibleHeight;  //紀錄根檢視的顯示高度
    private OnSoftKeyBoardChangeListener onSoftKeyBoardChangeListener;

    public softkeyboardlistener(Activity activity) {
        //獲取activity的根檢視
        rootView = activity.getWindow().getDecorView();
        //監聽檢視樹中全域性佈局發生改變或者檢視樹中的某個檢視的可視狀態發生改變
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //獲取當前根檢視在螢幕上顯示的大小
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int visibleHeight = r.height();
                System.out.println("" + visibleHeight);
                if (rootViewVisibleHeight == 0) {
                    rootViewVisibleHeight = visibleHeight;
                    return;
                }
                //根檢視顯示高度沒有變化，可以看作軟鍵盤顯示／隱藏狀態沒有改變
                if (rootViewVisibleHeight == visibleHeight) {
                    return;
                }
                //根檢視顯示高度變小超過200，可以看作軟鍵盤顯示了
                if (rootViewVisibleHeight - visibleHeight > 200) {
                    if (onSoftKeyBoardChangeListener != null) {
                        onSoftKeyBoardChangeListener.keyBoardShow(rootViewVisibleHeight - visibleHeight);
                    }
                    rootViewVisibleHeight = visibleHeight;
                    return;
                }
                //根檢視顯示高度變大超過200，可以看作軟鍵盤隱藏了
                if (visibleHeight - rootViewVisibleHeight > 200) {
                    if (onSoftKeyBoardChangeListener != null) {
                        onSoftKeyBoardChangeListener.keyBoardHide(visibleHeight - rootViewVisibleHeight);
                    }
                    rootViewVisibleHeight = visibleHeight;
                    return;
                }
            }
        });
    }

    private void setOnSoftKeyBoardChangeListener(OnSoftKeyBoardChangeListener onSoftKeyBoardChangeListener) {
        this.onSoftKeyBoardChangeListener = onSoftKeyBoardChangeListener;
    }

    public interface OnSoftKeyBoardChangeListener {
        void keyBoardShow(int height);

        void keyBoardHide(int height);
    }

    public static void setListener(Activity activity, OnSoftKeyBoardChangeListener onSoftKeyBoardChangeListener) {
        softkeyboardlistener softKeyBoardListener = new softkeyboardlistener(activity);
        softKeyBoardListener.setOnSoftKeyBoardChangeListener(onSoftKeyBoardChangeListener);
    }
}
