package com.gcreate.jiajia.listener

interface UpdateSearchKeywordListener {
    fun updateKeyword(keyword: String?)
}