package com.gcreate.jiajia.listener

import java.util.concurrent.CopyOnWriteArrayList

/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */
class UpdateSearchKeywordListenerManager {
    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private val iListenerList: MutableList<UpdateSearchKeywordListener> = CopyOnWriteArrayList()

    /**
     * 注册監聽
     */
    fun registerListener(iListener: UpdateSearchKeywordListener) {
        iListenerList.add(iListener)
    }

    /**
     * 註銷監聽
     */
    fun unRegisterListener(iListener: UpdateSearchKeywordListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener)
        }
    }

    /**
     * 發送廣播
     */
    fun sendBroadCast(keyword: String?) {
        for (updateKeywordListener in iListenerList) {
            updateKeywordListener.updateKeyword(keyword)
        }
    }

    companion object {
        /**
         * 單例模式
         */
        var listenerManager: UpdateSearchKeywordListenerManager? = null

        /**
         * 獲得單例對象對象
         */
        val instance: UpdateSearchKeywordListenerManager?
            get() {
                if (listenerManager == null) {
                    listenerManager = UpdateSearchKeywordListenerManager()
                }
                return listenerManager
            }
    }
}