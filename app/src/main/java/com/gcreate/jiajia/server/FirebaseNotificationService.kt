package com.gcreate.jiajia.server

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.gcreate.jiajia.MainActivity
import com.gcreate.jiajia.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseNotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification != null) {
            sendNotification(
                this,
                remoteMessage.notification!!.title,
                remoteMessage.notification!!.body
            )
        } else {
            val data = remoteMessage.data
            val title = data["title"]
            val body = data["body"]
            sendNotification(this, title, body)
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.i("MyFirebaseService", "token $s")
    }

    private fun sendNotification(context: Context, messageTitle: String?, messageBody: String?) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "default_notification_channel_id"
        val channelDescription = "Others"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var notificationChannel = notificationManager.getNotificationChannel(channelId)
            if (notificationChannel == null) {
                val importance = NotificationManager.IMPORTANCE_HIGH //Set the importance level
                notificationChannel = NotificationChannel(channelId, channelDescription, importance)
                notificationChannel.lightColor = Color.GREEN //Set if it is necesssary
                notificationChannel.enableVibration(true) //Set if it is necesssary
                notificationManager.createNotificationChannel(notificationChannel)
            }
        }

        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.putExtra("messageTitle", messageTitle)


        val contentIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_MUTABLE)
        } else {
            PendingIntent.getActivity(
                context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
            )
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            // in app catch notify
            NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_login_being)
                .setColor(ContextCompat.getColor(context, R.color.green))
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_login_being
                    )
                )
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setChannelId(channelId)
                .setContentIntent(contentIntent)

        if (messageBody!!.split("，").toTypedArray().isNotEmpty()) {
            notificationBuilder.setContentTitle(messageTitle)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}